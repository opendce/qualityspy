/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.wroc.pwr.qualityspy.metrics;

import java.util.ArrayList;
import pl.wroc.pwr.qualityspy.model.ClassDescription;

/**
 *
 */
public interface IMetricsConnector {

    public ArrayList<ClassDescription> getMetric();
}
