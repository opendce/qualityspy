package pl.wroc.pwr.qualityspy.metrics;

import gr.spinellis.ckjm.CkjmBean;
import gr.spinellis.ckjm.CkjmOutputHandler;
import gr.spinellis.ckjm.MetricsFilter;
import java.io.File;

/**
 * Modyfikacja klasy ckjmBean, która poprawia błąd powodujący, że program nie
 * działał pod Windowsem prawidłowo
 */
public class MyCkjmBean extends CkjmBean
{

    @Override
    public synchronized void countMetrics(String classFile, CkjmOutputHandler outputHandler) {
        String[] names = classFile.split(File.pathSeparator);
        MetricsFilter.runMetrics(names, outputHandler, false);
    }
}
