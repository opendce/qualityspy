package pl.wroc.pwr.qualityspy.metrics;

import org.apache.log4j.Logger;
import pl.wroc.pwr.qualityspy.common.CkjmProperties;
import pl.wroc.pwr.qualityspy.common.SourceTypeEnum;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.logging.Level;
import org.apache.maven.shared.invoker.DefaultInvocationRequest;
import org.apache.maven.shared.invoker.DefaultInvoker;
import org.apache.maven.shared.invoker.InvocationRequest;
import org.apache.maven.shared.invoker.Invoker;
import org.apache.maven.shared.invoker.MavenInvocationException;

public class MetricConnectorStarter {

    private static final Logger LOG = Logger.getLogger(MetricConnectorStarter.class);

    private boolean JAR = false;
    private boolean CLASS = false;
    private boolean fileExists = false;
    private boolean classExists = false;
    private boolean goodPath = false;
    private boolean useMaven = false;
    private String mavenPrjDirectory = null;
    private ArrayList<String> project_paths = new ArrayList<String>();
    private ArrayList<String> project_libs = new ArrayList<String>();
    private ArrayList<String> project_files = new ArrayList<String>();
    private MetricsConnector mc = null;

    public boolean isJAR() {
        return this.JAR;
    }

    public boolean isCLASS() {
        return this.CLASS;
    }

    public boolean isGoodPath() {
        return !this.project_paths.isEmpty();
    }

    public boolean isClassExists() {
        return this.classExists;
    }

    public boolean isFileExists() {
        return this.fileExists;
    }
    
    public boolean isMavenInUse() {
        return this.useMaven;
    }
    ////////////////////////////////////////////////////////////////////////////

    public MetricConnectorStarter(CkjmProperties config) {
        if (readProperties(config)) {
            if (!project_paths.isEmpty()) {

                if (analizePaths()) {
                    if (addLibraries()) {
                        mc = new MetricsConnector(getFileList());
                    }
                } else {
                    LOG.warn("Nie udało się znaleźć żadnych plików z klasami");
                    throw new RuntimeException("CKJM - no class files found, looking in: " + project_paths);
                }
            } else {
                LOG.warn("Brak poprawnych ścieżek w pliku z właściwościami");
                throw new RuntimeException("CKJM: no files on the configured paths (ckjmJar or ckjmClass parameter).");
            }
        }
    }

    private boolean readProperties(CkjmProperties props) {
        String mavenDirectory = props.getCkjmMavenProjectDirectory();
        if(mavenDirectory != null && !mavenDirectory.isEmpty()) {
            useMaven = true;
            mavenPrjDirectory = mavenDirectory;
            //LOG.debug("Maven directory: " + mavenDirectory);
            System.out.println("Maven directory: " + mavenDirectory);
        }
        if (props.getCkjmSourceType() == SourceTypeEnum.JAR) {
            JAR = true;
            CLASS = false;
            project_paths.add(props.getCkjmJar());
        } else if (props.getCkjmSourceType() == SourceTypeEnum.CLASS) {
            JAR = false;
            CLASS = true;
            project_paths.add(props.getCkjmClass());
        } else {
            throw new RuntimeException("CKJM: unknown source format.");
        }

        String separator = props.getCkjmLibrariesSeparator();
        if (separator == null || "".equals(separator)) {
            separator = File.pathSeparator;
        }
        if (props.getCkjmLibraries() != null) {
            for (String lib : props.getCkjmLibraries().split(separator)) {
                project_libs.add(lib);
            }
        }
        
        this.fileExists = true;
        return true;

    }

    private boolean makeFileList(String path) {
        FileFinder.clear();
        if (JAR == true) {
            FileFinder.find(path, "jar");
        }
        if (CLASS == true) {
            FileFinder.find(path, "class");
        }
        if (FileFinder.fileList.isEmpty()) {
            LOG.warn("Nie odnaleziono żadnych plików, z klasami w ścieżce " + path);
            return false;
        } else {
            project_files.addAll(FileFinder.fileList);
            return true;
        }
    }

    private boolean analizePaths() {
        for (String s : project_paths) {
            makeFileList(s);
        }
        for (String s : project_files) {
            if (FileFinder.check(new File(s), "jar")) {
                project_libs.add(s);
            }
        }
        if (project_files.isEmpty()) {
            this.classExists = false;
            return false;
        } else {
            this.classExists = true;
            return true;
        }
    }

    private boolean addLibraries() {
        if(useMaven)
            resolveDependenciesWithMaven();
        for (String path_jar : project_libs) {
            try {
                ClassPathHacker.addFile(path_jar);
            } catch (IOException ex) {
                LOG.warn("Wystąpił błąd podczas dodawania bibliotek");
                return false;
            }
        }
        return true;
    }
    
    private boolean resolveDependenciesWithMaven() {
        InvocationRequest request = new DefaultInvocationRequest();
        request.setPomFile( new File( mavenPrjDirectory + "pom.xml" ) );
        request.setGoals( Collections.singletonList( "dependency:resolve" ) );

        Invoker invoker = new DefaultInvoker();
        try {
            invoker.execute( request );
        } catch (MavenInvocationException ex) {
            ex.printStackTrace();
            throw new RuntimeException("CKJM - Maven error: ", ex);
        }
        return true;
    }

    private String getFileList() {
        String fl = "";
        for (String file : project_files) {
            fl += file + File.pathSeparator;
        }
        if (fl.length() > 1) {
            return fl.substring(0, fl.length() - 1);
        }
        return fl;
    }

    public IMetricsConnector getMetricConnector() {
        return mc;
    }

    public String toString() {
        String result = "";
        result += JAR + "\n";
        result += CLASS + "\n";
        result += "PLIKI Z KLASAMI" + "\n";
        for (String s : project_files) {
            result += s + "\n";
        }
        result += "BIBLIOTEKI" + "\n";
        for (String s : project_libs) {
            result += s + "\n";
        }
        return result;
    }
}
