package pl.wroc.pwr.qualityspy.metrics;

/**
 * Klasa odwzorowuje elementy listy z wartościami pozczególnych metryk dla
 * klasy, lub metody name zawiera informacje o tym czego dowyczy wartośc w polu
 * value
 *
 * @author Paweł
 */
public class Element {

    /**
     * nazwa (klucz elementu)
     */
    private String name;
    /**
     * wartosc elementu
     */
    private double value;

    public Element() {

    }

    /**
     * Konstruktor z nadaniem wartości
     */
    public Element(String s, double d) {
        name = s;
        value = d;
    }

    /**
     * pobiera nazwę (klucz elementu)
     */
    public String getName() {
        return name;
    }

    /**
     * pobiera wartość elementu
     */
    public double getValue() {
        return value;
    }

    /**
     * zmienia nazwe elementu na zadana
     */
    public void setName(String new_name) {
        name = new_name;
    }

    /**
     * zmienia wartość elementu na zadaną
     */
    public void setValue(double new_value) {
        value = new_value;
    }

    /**
     * standardowa funkcja zmieniajaca obiekt klasy na String
     */
    public String toString() {
        return name + " : " + value;
    }
}
