package pl.wroc.pwr.qualityspy.metrics;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Klasa wspierająca wyszukiwanie plików w zadanym katalogu o zadanym
 * rozszerzeniu rozszerzeniach
 */
public class FileFinder {

    /**
     * tu zapisuje wyniki wyszkiwania, jako bezwzgledne sciezki do pliku
     */
    static public ArrayList<String> fileList = new ArrayList<String>();

    /**
     * wyszukuje w zadanej sciezce wszystkie pliki o rozszerzeniach zadanych
     * jako drugi argument
     */
    static public void find(String path, String extension) {
        File file = new File(path);
        if (file.isDirectory()) {
            for (File f : file.listFiles()) {
                try {
                    FileFinder.find(f.getCanonicalPath(), extension);
                } catch (IOException ex) {
                }
            }
        } else {
            if (FileFinder.check(file, extension)) {
                try {
                    fileList.add(file.getCanonicalPath());
                } catch (IOException ex) {
                }
            }
        }
    }

    /**
     * zwraca liste bezwzglednych sciezek znalezionych plikow oddzielonych
     * srednikami
     */
    static public String getList() {
        String result = "";
        if (!fileList.isEmpty()) {
            for (String s : fileList) {
                result += s + ";";
            }
        }
        return result;
    }

    /**
     * czysci liste odnalezionych plikow
     */
    static public void clear() {
        fileList.clear();
    }

    /**
     * funkcja sprawdz czy plik ma odpowiednie rozszerzenie
     */
    static public boolean check(File file, String extension) {
        if (file.isFile()) {
            String[] tab = file.getName().split("\\.");
            if (tab[tab.length - 1].equals(extension)) {
                return true;
            }
        }
        return false;
    }
}
