package pl.wroc.pwr.qualityspy.metrics;

import gr.spinellis.ckjm.CkjmOutputHandler;
import gr.spinellis.ckjm.PrintPlainResults;
import org.apache.log4j.Logger;

import java.io.PrintStream;

/**
 * Klasa uruchamiająca CKJM'a
 */
public class Starter {

    private static final Logger LOG = Logger.getLogger(Starter.class);

    /**
     * pole zawierajace sciezek bezwzglednych do plikow z klasami
     */
    private String files_list;
    /**
     * zawiera wynik dzialania CKJM'a
     */
    private String result = "";
    /**
     * zawiera liste bledów ktore wystapiły podczas pracy CKJM'a
     */
    private String errors = "";

    /**
     * konstruktor przyjmujacy jako argument String z lista plikow
     */
    public Starter(String fl) {
        files_list = fl;
    }

    /**
     * funkcja odpowiedzialna za uruchomienie dla zadanje listy plikow program
     * CKJM i przechwycenie jej wyjscia oraz bledow
     */
    public boolean startCKJM() {
        PrintStream se = System.err;
        StringOutputStream esos = new StringOutputStream();
        PrintStream ne = new PrintStream(esos);
        System.setErr(ne);
        StringOutputStream sos = new StringOutputStream();
        PrintStream ps = new PrintStream(sos);
        MyCkjmBean cb = new MyCkjmBean();
        CkjmOutputHandler handler = new PrintPlainResults(ps);
        cb.countMetrics(files_list, handler);
        errors = esos.getString();
        ne.close();
        System.setErr(se);
        if (!errors.isEmpty()) {
            LOG.warn("Wystąpiły błędy podczas działania CKJM'a.");
            return false;
        }
        result = sos.getString();
        return true;
    }

    /**
     * zwraca wynik dzialania CKJM'a
     */
    public String getResult() {
        return result;
    }

    /**
     * zwraca wynik dzialania CKJM'a
     */
    public String getErrors() {
        return errors;
    }
}
