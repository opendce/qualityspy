/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wroc.pwr.qualityspy.metrics;

import org.apache.log4j.Logger;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertTrue;

//import java.util.logging.Logger;

/**
 *
 * @author mateusz
 */
public class ClassPathHackerTest {

    /**
     * logger *
     */
    private static final Logger logger = Logger.getLogger(ClassPathHackerTest.class.getName());
//    private static final Logger logger = Logger.getLogger(ClassPathHackerTest.class.getName());

    /**
     * Test of addFile method, of class ClassPathHacker.
     */
    @Test
    public void testAddFile_String() throws Exception {
        logger.info("addFile");
        String s = "test_item";
        ClassPathHacker.addFile(s);

        String classPath = System.getProperty("java.class.path");
        assertTrue(classPath.contains(s));
    }

    /**
     **/
    @Test
    public void testAddFile() throws Exception {
        File f = new File(".");
        String s = f.getAbsolutePath();
      
        ClassPathHacker.addFile(f);
        
        String classPath = System.getProperty("java.class.path");
        assertTrue(classPath.contains(s));
    }
}