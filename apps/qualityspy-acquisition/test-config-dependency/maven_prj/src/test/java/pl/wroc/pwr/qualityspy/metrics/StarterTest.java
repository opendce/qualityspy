/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wroc.pwr.qualityspy.metrics;

import org.apache.log4j.Logger;
import org.junit.*;
import static org.junit.Assert.*;

/**
 *
 * @author mateusz
 */
public class StarterTest {

    /**
     * logger *
     */
    private static final Logger logger = Logger.getLogger(StarterTest.class.getName());

    public StarterTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getResult method, of class Starter.
     */
    @Test
    public void testGetResult() {
        logger.info("getResult");
        String fl = "/home/mateusz/vpworkspace/PrjOpr2/;";
        Starter instance = new Starter(fl);
        String expResult = "";
        String result = instance.getResult();
        assertEquals(expResult, result);
    }

    /**
     * Test of getErrors method, of class Starter.
     */
    @Test
    public void testGetErrors() {
        logger.info("getErrors");
        String fl = "/home/mateusz/vpworkspace/PrjOpr2/;";
        Starter instance = new Starter(fl);
        String expResult = "";
        String result = instance.getErrors();
        assertEquals(expResult, result);
    }
}