package pl.wroc.pwr.qualityspy.webapp.dto;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.HashSet;
import java.util.Set;

@XmlRootElement
public class ClassesDto implements IDto{

    private Long id;
    private String name;
    private double amc;
    private int ca;
    private double cam;
    private int cbm;
    private int cbo;
    private int ce;
    private double dam;
    private int dit;
    private int ic;
    private int lcom;
    private double lcom3;
    private int loc;
    private double mfa;
    private int moa;
    private int noc;
    private int npm;
    private int rfc;
    private int wmc;
    private double averageCC;
    private int maxCC;
    private Set<Long> methods = new HashSet<Long>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAmc() {
        return amc;
    }

    public void setAmc(double amc) {
        this.amc = amc;
    }

    public int getCa() {
        return ca;
    }

    public void setCa(int ca) {
        this.ca = ca;
    }

    public double getCam() {
        return cam;
    }

    public void setCam(double cam) {
        this.cam = cam;
    }

    public int getCbm() {
        return cbm;
    }

    public void setCbm(int cbm) {
        this.cbm = cbm;
    }

    public int getCbo() {
        return cbo;
    }

    public void setCbo(int cbo) {
        this.cbo = cbo;
    }

    public int getCe() {
        return ce;
    }

    public void setCe(int ce) {
        this.ce = ce;
    }

    public double getDam() {
        return dam;
    }

    public void setDam(double dam) {
        this.dam = dam;
    }

    public int getDit() {
        return dit;
    }

    public void setDit(int dit) {
        this.dit = dit;
    }

    public int getIc() {
        return ic;
    }

    public void setIc(int ic) {
        this.ic = ic;
    }

    public int getLcom() {
        return lcom;
    }

    public void setLcom(int lcom) {
        this.lcom = lcom;
    }

    public double getLcom3() {
        return lcom3;
    }

    public void setLcom3(double lcom3) {
        this.lcom3 = lcom3;
    }

    public int getLoc() {
        return loc;
    }

    public void setLoc(int loc) {
        this.loc = loc;
    }

    public double getMfa() {
        return mfa;
    }

    public void setMfa(double mfa) {
        this.mfa = mfa;
    }

    public int getMoa() {
        return moa;
    }

    public void setMoa(int moa) {
        this.moa = moa;
    }

    public int getNoc() {
        return noc;
    }

    public void setNoc(int noc) {
        this.noc = noc;
    }

    public int getNpm() {
        return npm;
    }

    public void setNpm(int npm) {
        this.npm = npm;
    }

    public int getRfc() {
        return rfc;
    }

    public void setRfc(int rfc) {
        this.rfc = rfc;
    }

    public int getWmc() {
        return wmc;
    }

    public void setWmc(int wmc) {
        this.wmc = wmc;
    }

    public double getAverageCC() {
        return averageCC;
    }

    public void setAverageCC(double averageCC) {
        this.averageCC = averageCC;
    }

    public int getMaxCC() {
        return maxCC;
    }

    public void setMaxCC(int maxCC) {
        this.maxCC = maxCC;
    }

    public Set<Long> getMethods() {
        return methods;
    }

    public void setMethods(Set<Long> methods) {
        this.methods = methods;
    }
}
