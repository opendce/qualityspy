package pl.wroc.pwr.qualityspy.webapp.rest;

import au.com.bytecode.opencsv.CSVWriter;
import io.swagger.annotations.Api;
import org.apache.log4j.Logger;
import org.dozer.MappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.wroc.pwr.qualityspy.webapp.delegate.FilesDelegate;
import pl.wroc.pwr.qualityspy.webapp.dto.FilesDto;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

@Path("/files")
@Api(value="/files", description = "FileRestService")
@Component
public class FileRestService {

    private static final Logger LOGGER = Logger.getLogger(ProjectsRestService.class);
    private static final char CSV_SEPARATOR = ';';

    @Autowired
    private transient FilesDelegate filesDelegate;

    /**
     *
     * @return List of all files as XML.
     */
    @GET
    @Path("/{revisionId}")
    @Produces(MediaType.TEXT_XML)
    @Transactional
    public List<FilesDto> getFilesXML(@PathParam("revisionId") long id) {
        LOGGER.info("Getting Files as XML");
        return filesDelegate.getFiles(id);
    }

    /**
     *
     * @return List of all Files as CSV.
     */
    @GET
    @Path("/{revisionId}")
    @Produces("text/csv")
    @Transactional
    public String getFilesCSV(@PathParam("revisionId") long id) throws MappingException {
        LOGGER.info("Getting Files as CSV");
        List<FilesDto> filesDtos = filesDelegate.getFiles(id);
        StringWriter out = new StringWriter();
        CSVWriter writer = new CSVWriter(out, CSV_SEPARATOR);
        List<String[]> outputString = new ArrayList<String[]>();
        String[] columnNames = {"id", "oldContent", "newContent", "name", "oldPath", "newPath", "repoPath", "changeType", "numberofAddedLines", "numberOfRemovedLines"};
        outputString.add(columnNames);

        for (FilesDto filesDto : filesDtos) {
            String[] rawData = new String[columnNames.length];
            rawData[0] = filesDto.getId() != null ? filesDto.getId().toString() : "";
            rawData[1] = filesDto.getOldContent();
            rawData[2] = filesDto.getNewContent();
            rawData[2] = filesDto.getName();
            rawData[3] = filesDto.getOldPath();
            rawData[4] = filesDto.getNewPath();
            rawData[5] = filesDto.getRepoPath();
            rawData[6] = filesDto.getChangeType().toString();
            rawData[7] = Long.toString(filesDto.getNumberOfAddedLines());
            rawData[8] = Long.toString(filesDto.getNumberOfRemovedLines());
            outputString.add(rawData);
        }
        writer.writeAll(outputString);
        return out.toString();
    }

}
