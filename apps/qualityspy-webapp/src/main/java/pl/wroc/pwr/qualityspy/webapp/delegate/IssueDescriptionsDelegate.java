package pl.wroc.pwr.qualityspy.webapp.delegate;

import org.dozer.DozerBeanMapper;
import org.dozer.MappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.wroc.pwr.qualityspy.model.ProjectHistory;
import pl.wroc.pwr.qualityspy.repositories.ProjectHistoryRepository;
import pl.wroc.pwr.qualityspy.webapp.dto.IssueDescriptionsDto;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class IssueDescriptionsDelegate {
    @Autowired
    private transient ProjectHistoryRepository projectHistoryRepository;

    @Autowired
    private transient DozerBeanMapper mapper;


    public List<IssueDescriptionsDto> getProjectHistoryIssueDescriptions(long projectHistoryID)
            throws MappingException {
        final ProjectHistory projectHistory = projectHistoryRepository.findOne(projectHistoryID);
        if (projectHistory == null) {
            return Collections.emptyList();
        }
        return projectHistory.getIssues().stream()
                .map(issueDescription -> mapper.map(issueDescription, IssueDescriptionsDto.class))
                .collect(Collectors.toList());
    }
}
