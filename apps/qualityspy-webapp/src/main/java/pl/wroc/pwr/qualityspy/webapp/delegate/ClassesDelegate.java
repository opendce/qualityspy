package pl.wroc.pwr.qualityspy.webapp.delegate;

import com.google.common.collect.Lists;
import org.dozer.DozerBeanMapper;
import org.dozer.MappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.wroc.pwr.qualityspy.model.ClassDescription;
import pl.wroc.pwr.qualityspy.webapp.dto.ClassesDto;
import pl.wroc.pwr.qualityspy.repositories.ClassDescriptionRepository;

import java.util.List;

@Service
public class ClassesDelegate {
    @Autowired
    private transient ClassDescriptionRepository classDescriptionRepository;

    @Autowired
    private transient DozerBeanMapper mapper;

    public List<ClassesDto> getClasses(long id) throws MappingException {
        List<ClassDescription> classes = classDescriptionRepository.findAllById(id);
        List<ClassesDto> results = Lists.newArrayList();
        for (ClassDescription classDescription : classes) {
            ClassesDto result = mapper.map(classDescription, ClassesDto.class);
            results.add(result);
        }
        return results;
    }
}
