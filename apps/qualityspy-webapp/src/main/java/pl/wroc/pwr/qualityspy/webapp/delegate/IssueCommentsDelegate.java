package pl.wroc.pwr.qualityspy.webapp.delegate;

import com.google.common.collect.Lists;
import org.dozer.DozerBeanMapper;
import org.dozer.MappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.wroc.pwr.qualityspy.model.IssueComment;
import pl.wroc.pwr.qualityspy.webapp.dto.IssueCommentsDto;
import pl.wroc.pwr.qualityspy.repositories.IssueCommentRepository;

import java.util.List;

@Service
public class IssueCommentsDelegate {
    @Autowired
    private transient IssueCommentRepository issueCommentRepository;

    @Autowired
    private transient DozerBeanMapper mapper;

    public List<IssueCommentsDto> getIssueComments(long id) throws MappingException {
        List<IssueComment> issueCommentSet = issueCommentRepository.findAllById(id);
        List<IssueCommentsDto> results = Lists.newArrayList();
        for (IssueComment issueComment : issueCommentSet) {
            IssueCommentsDto result = mapper.map(issueComment, IssueCommentsDto.class);
            results.add(result);
        }
        return results;
    }
}
