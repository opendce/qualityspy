package pl.wroc.pwr.qualityspy.webapp.rest.jiraexport;

import au.com.bytecode.opencsv.CSVWriter;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.wroc.pwr.qualityspy.webapp.jiraexport.CsvPreprocessor;
import pl.wroc.pwr.qualityspy.model.ProjectHistory;
import pl.wroc.pwr.qualityspy.repositories.ProjectHistoryRepository;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;


@Service
@Api
@Path("jiraexport")
public class JiraExportRestService {

    private static final char SEPARATOR = ',';
    private final ProjectHistoryRepository repo;

    @Autowired
    public JiraExportRestService(ProjectHistoryRepository repo) {
        this.repo = repo;
    }

    @GET
    @Path("/projecthistory/{projectHistoryId}")
    @Produces("text/csv")
    @Transactional
    public String jiraExportCsv(@PathParam("projectHistoryId") long projectHistoryId){
        final ProjectHistory history = repo.findOne(projectHistoryId);
        if(history!=null) {
            final CsvPreprocessor preprocessor = new CsvPreprocessor(history);
            final StringWriter out = new StringWriter();
            final CSVWriter writer = new CSVWriter(out, SEPARATOR);
            final List<String[]> outputString = new ArrayList<>();

            outputString.add(preprocessor.renderHeaders());
            outputString.addAll(preprocessor.renderData());

            writer.writeAll(outputString);
            return out.toString();
        }
        return "";
    }


}
