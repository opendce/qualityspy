package pl.wroc.pwr.qualityspy.webapp.converters;

import org.dozer.DozerConverter;
import pl.wroc.pwr.qualityspy.model.IEntityWithId;

/**
 */
public class IdsConverter extends DozerConverter<IEntityWithId, Long> {

    public IdsConverter() {
        super(IEntityWithId.class, Long.class);
    }

    @Override
    public Long convertTo(IEntityWithId source, Long destination) {
        if(source == null){
            return destination;
        }
        return source.getId();
    }

    @Override
    public IEntityWithId convertFrom(Long source, IEntityWithId destination) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
