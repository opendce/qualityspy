package pl.wroc.pwr.qualityspy.webapp.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class MethodsDto implements IDto{

    private Long id;
    private String name;
    private long cyclomaticComplexity;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getCyclomaticComplexity() {
        return cyclomaticComplexity;
    }

    public void setCyclomaticComplexity(long cyclomaticComplexity) {
        this.cyclomaticComplexity = cyclomaticComplexity;
    }
}
