package pl.wroc.pwr.qualityspy.webapp.delegate;

import com.google.common.collect.Lists;
import org.dozer.DozerBeanMapper;
import org.dozer.MappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.wroc.pwr.qualityspy.model.ProjectHistory;
import pl.wroc.pwr.qualityspy.webapp.dto.ProjectHistoriesDto;
import pl.wroc.pwr.qualityspy.repositories.ProjectHistoryRepository;

import java.util.List;


@Service
public class ProjectHistoriesDelegate {
    @Autowired
    private transient ProjectHistoryRepository projectHistoryRepository ;

    @Autowired
    private transient DozerBeanMapper mapper;

    public List<ProjectHistoriesDto> getProjectHistories(long id) throws MappingException {
        List<ProjectHistory> historySet = projectHistoryRepository.findAllById(id);
        List<ProjectHistoriesDto> results = Lists.newArrayList();
        for (ProjectHistory projectHistory : historySet) {
            ProjectHistoriesDto result = mapper.map(projectHistory, ProjectHistoriesDto.class);
            results.add(result);
        }
        return results;
    }
}
