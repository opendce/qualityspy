package pl.wroc.pwr.qualityspy.webapp.rest;

import au.com.bytecode.opencsv.CSVWriter;
import io.swagger.annotations.Api;
import org.apache.log4j.Logger;
import org.dozer.MappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.wroc.pwr.qualityspy.webapp.delegate.RevisionsDelegate;
import pl.wroc.pwr.qualityspy.webapp.dto.RevisionsDto;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

@Path("/revisions")
@Api
@Component
public class RevisionsRestService {

    private static final Logger LOGGER = Logger.getLogger(ProjectsRestService.class);
    private static final char SEPARATOR = ',';
    private static final char CSV_SEPARATOR = ';';

    @Autowired
    private transient RevisionsDelegate revisionsDelegate;

    /**
     *
     * @return List of all revisions as XML.
     */
    @GET
    @Path("/{projectHistoryId}")
    @Produces(MediaType.TEXT_XML)
    @Transactional
    public List<RevisionsDto> getRevisionsXML(@PathParam("projectHistoryId") long id) {
        LOGGER.info("Getting projects as XML");
        return revisionsDelegate.getRevisions(id);
    }

    /**
     *
     * @return List of all revisions as CSV.
     */
    @GET
    @Path("/{projectHistoryId}")
    @Produces("text/csv")
    @Transactional
    public String getRevisionsCSV(@PathParam("projectHistoryId") long id) throws MappingException {
        LOGGER.info("Getting revisions as CSV");
        List<RevisionsDto> revisions = revisionsDelegate.getRevisions(id);
        StringWriter out = new StringWriter();
        CSVWriter writer = new CSVWriter(out, CSV_SEPARATOR);
        List<String[]> outputString = new ArrayList<String[]>();
        String[] columnNames = {"ID", "Number", "Date", "Tags", "Branch", "Author", "Comment", "Files"};
        outputString.add(columnNames);

        for (RevisionsDto revisionsDto : revisions) {
            String[] rawData = new String[columnNames.length];
            rawData[0] = revisionsDto.getId() != null ? revisionsDto.getId().toString() : "";
            rawData[1] = revisionsDto.getNumber();
            rawData[2] = revisionsDto.getDate().toString();
            rawData[3] = revisionsDto.getTags();
            rawData[4] = revisionsDto.getBranch();
            rawData[5] = revisionsDto.getAuthor();
            rawData[6] = revisionsDto.getComment();

            StringBuilder sb = new StringBuilder();
            for(Long files : revisionsDto.getFiles()){
                if(files !=null){
                    sb.append(files.toString());
                    sb.append(SEPARATOR);
                }
            }
            if(sb.charAt(sb.length()-1) == SEPARATOR){
                sb.deleteCharAt(sb.length() - 1);
            }
            rawData[7] = sb.toString();

            outputString.add(rawData);
        }
        writer.writeAll(outputString);
        return out.toString();
    }

}
