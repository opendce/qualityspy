package pl.wroc.pwr.qualityspy.webapp.rest;

import au.com.bytecode.opencsv.CSVWriter;
import io.swagger.annotations.Api;
import org.apache.log4j.Logger;
import org.dozer.MappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.wroc.pwr.qualityspy.webapp.delegate.BuildsDelegate;
import pl.wroc.pwr.qualityspy.webapp.dto.BuildsDto;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

@Path("/builds")
@Api(value="/builds", description = "BuildsRestService")
@Component
public class BuildsRestService {

    private static final Logger LOGGER = Logger.getLogger(ProjectsRestService.class);
    private static final char CSV_SEPARATOR = ';';

    @Autowired
    private transient BuildsDelegate buildsDelegate;

    /**
     *
     * @return List of all builds as XML.
     */
    @GET
    @Path("/{jobId}")
    @Produces(MediaType.TEXT_XML)
    @Transactional
    public List<BuildsDto> getBuildsXML(@PathParam("jobId") long id) {
        LOGGER.info("Getting Builds as XML");
        return buildsDelegate.getBuilds(id);
    }

    /**
     *
     * @return List of all Builds as CSV.
     */
    @GET
    @Path("/{jobId}")
    @Produces("text/csv")
    @Transactional
    public String getBuildsCSV(@PathParam("jobId") long id) throws MappingException {
        LOGGER.info("Getting Builds as CSV");
        List<BuildsDto> builds = buildsDelegate.getBuilds(id);
        StringWriter out = new StringWriter();
        CSVWriter writer = new CSVWriter(out, CSV_SEPARATOR);
        List<String[]> outputString = new ArrayList<String[]>();
        String[] columnNames = {"id", "number", "date", "result", "totalCount", "failCount", "totalDiff", "failDiff"};
        outputString.add(columnNames);

        for (BuildsDto buildsDto : builds) {
            String[] rawData = new String[columnNames.length];
            rawData[0] = buildsDto.getId() != null ? buildsDto.getId().toString() : "";
            rawData[1] = Integer.toString(buildsDto.getNumber());
            rawData[2] = buildsDto.getDate().toString();
            rawData[3] = buildsDto.getResult();
            rawData[4] = Integer.toString(buildsDto.getTotalCount());
            rawData[5] = Integer.toString(buildsDto.getFailCount());
            rawData[6] = Integer.toString(buildsDto.getTotalDiff());
            rawData[7] = Integer.toString(buildsDto.getFailDiff());
            outputString.add(rawData);
        }
        writer.writeAll(outputString);
        return out.toString();
    }
}
