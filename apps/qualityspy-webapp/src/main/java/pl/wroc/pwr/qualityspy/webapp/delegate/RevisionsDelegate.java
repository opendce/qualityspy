package pl.wroc.pwr.qualityspy.webapp.delegate;

import com.google.common.collect.Lists;
import org.dozer.DozerBeanMapper;
import org.dozer.MappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.wroc.pwr.qualityspy.model.Revision;
import pl.wroc.pwr.qualityspy.webapp.dto.RevisionsDto;
import pl.wroc.pwr.qualityspy.repositories.RevisionRepository;

import java.util.List;

@Service
public class RevisionsDelegate {
    @Autowired
    private transient RevisionRepository revisionRepository;

    @Autowired
    private transient DozerBeanMapper mapper;

    public List<RevisionsDto> getRevisions(long id) throws MappingException {
        List<Revision> revisionSet = revisionRepository.findAllById(id);
        List<RevisionsDto> results = Lists.newArrayList();
        for (Revision revision : revisionSet) {
            RevisionsDto result = mapper.map(revision, RevisionsDto.class);
            results.add(result);
        }
        return results;
    }
}
