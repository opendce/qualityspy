package pl.wroc.pwr.qualityspy.webapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackages = {"pl.wroc.pwr.qualityspy"})
@EnableJpaRepositories(basePackages={"pl.wroc.pwr.qualityspy.repositories"})
@EntityScan(basePackages = {"pl.wroc.pwr.qualityspy.model"})
public class QualityspyWebappApplication {
	public static void main(String[] args) {
		SpringApplication.run(QualityspyWebappApplication.class, args);
	}
}
