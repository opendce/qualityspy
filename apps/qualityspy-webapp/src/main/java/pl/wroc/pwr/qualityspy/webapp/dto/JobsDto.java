package pl.wroc.pwr.qualityspy.webapp.dto;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.LinkedList;
import java.util.List;

@XmlRootElement
public class JobsDto {
    private Long id;
    private String name;
    private String url;
    private String color;
    private List<Long> builds = new LinkedList<Long>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public List<Long> getBuilds() {
        return builds;
    }

    public void setBuilds(List<Long> builds) {
        this.builds = builds;
    }
}
