package pl.wroc.pwr.qualityspy.webapp.rest;

import au.com.bytecode.opencsv.CSVWriter;
import io.swagger.annotations.Api;
import org.apache.log4j.Logger;
import org.dozer.MappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.wroc.pwr.qualityspy.webapp.delegate.MilestonesDelegate;
import pl.wroc.pwr.qualityspy.webapp.dto.MilestonesDto;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

@Path("/milestones")
@Api
@Component
public class MilestonesRestService {
    private static final Logger LOGGER = Logger.getLogger(MilestonesRestService.class);
    private static final char CSV_SEPARATOR = ';';

    @Autowired
    private transient MilestonesDelegate milestonesDelegate;

    /**
     *
     * @return List of all milestones as XML.
     */
    @GET
    @Path("/{projectHistoryId}")
    @Produces(MediaType.TEXT_XML)
    @Transactional
    public List<MilestonesDto> getProjectHistoriesXML(@PathParam("projectHistoryId") long id) {
        LOGGER.info("Getting project histories as XML");
        return milestonesDelegate.getMilestones(id);
    }

    /**
     *
     * @return List of all milestones as CSV.
     */
    @GET
    @Path("/{projectHistoryId}")
    @Produces("text/csv")
    @Transactional
    public String getProjectsCSV(@PathParam("projectHistoryId") long id) throws MappingException {
        LOGGER.info("Getting project histories as CSV");
        List<MilestonesDto> milestones = milestonesDelegate.getMilestones(id);
        StringWriter out = new StringWriter();
        CSVWriter writer = new CSVWriter(out, CSV_SEPARATOR);
        List<String[]> outputString = new ArrayList<String[]>();
        String[] columnNames = {"ID", "Title", "Creator", "Description", "State", "Created", "Deadline"};
        outputString.add(columnNames);

        for (MilestonesDto milestonesDto : milestones) {
            String[] rawData = new String[columnNames.length];
            rawData[0] = milestonesDto.getId() != null ? milestonesDto.getId().toString() : "";
            rawData[1] = milestonesDto.getTitle();
            rawData[2] = milestonesDto.getCreator();
            rawData[3] = milestonesDto.getDescription();
            rawData[4] = milestonesDto.getState();
            rawData[5] = milestonesDto.getCreated().toString();
            rawData[6] = milestonesDto.getDeadline().toString();
            outputString.add(rawData);
        }
        writer.writeAll(outputString);
        return out.toString();
    }
}
