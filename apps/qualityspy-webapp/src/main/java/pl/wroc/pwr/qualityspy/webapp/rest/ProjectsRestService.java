package pl.wroc.pwr.qualityspy.webapp.rest;

import au.com.bytecode.opencsv.CSVWriter;
import io.swagger.annotations.Api;
import org.apache.log4j.Logger;
import org.dozer.MappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.wroc.pwr.qualityspy.webapp.delegate.ProjectsDelegate;
import pl.wroc.pwr.qualityspy.webapp.dto.ProjectDto;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

/**REST service for accessing projects. */
@Path("/projects")
@Api
@Component
public class ProjectsRestService {

    private static final Logger LOGGER = Logger.getLogger(ProjectsRestService.class);
    private static final char PROJECT_HISTORIES_SEPARATOR = ',';
    private static final char CSV_SEPARATOR = ';';

    @Autowired
    private transient ProjectsDelegate projectsDelegate;

    /**
     *
     * @return List of all projects as XML.
     */
    @GET
    @Produces(MediaType.TEXT_XML)
    @Transactional
    public List<ProjectDto> getProjectsXML(){
        LOGGER.info("Getting projects as XML");
        return projectsDelegate.getAllProjects();
    }

    /**
     *
     * @return List of all projects as CSV.
     */
    @GET
    @Produces("text/csv")
    @Transactional
    public String getProjectsCSV() throws MappingException {
        LOGGER.info("Getting projects as CSV");
        List<ProjectDto> projects = projectsDelegate.getAllProjects();
        StringWriter out = new StringWriter();
        CSVWriter writer = new CSVWriter(out, CSV_SEPARATOR);
        List<String[]> outputString = new ArrayList<String[]>();
        String[] columnNames = {"ID", "ProjectName", "ProjectHistories"};
            outputString.add(columnNames);

            for (ProjectDto project : projects) {
            String[] rawData = new String[3];
            rawData[0] = project.getId() != null ? project.getId().toString() : "";
            rawData[1] = project.getProjectName();
            StringBuilder sb = new StringBuilder();
            for(Long history : project.getProjectHistories()){
                if(history!=null){
                    sb.append(history.toString());
                    sb.append(PROJECT_HISTORIES_SEPARATOR);
                }
            }
            if(sb.charAt(sb.length()-1) == PROJECT_HISTORIES_SEPARATOR){
                sb.deleteCharAt(sb.length() - 1);
            }
            rawData[2] = sb.toString();
            outputString.add(rawData);
        }
        writer.writeAll(outputString);
        return out.toString();
    }

}
