package pl.wroc.pwr.qualityspy.webapp.dto;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@XmlRootElement
public class BuildsDto {
    private Long id;
    private Integer number;
    private Date date;
    private String result;
    private Integer totalCount;
    private Integer failCount;
    private Integer totalDiff;
    private Integer failDiff;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public Integer getFailCount() {
        return failCount;
    }

    public void setFailCount(Integer failCount) {
        this.failCount = failCount;
    }

    public Integer getTotalDiff() {
        return totalDiff;
    }

    public void setTotalDiff(Integer totalDiff) {
        this.totalDiff = totalDiff;
    }

    public Integer getFailDiff() {
        return failDiff;
    }

    public void setFailDiff(Integer failDiff) {
        this.failDiff = failDiff;
    }
}
