package pl.wroc.pwr.qualityspy.webapp.dto;

import com.google.common.collect.Sets;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;
import java.util.Set;

@XmlRootElement
public class ProjectHistoriesDto implements IDto{

    private Long id;
    private String version;
    private String startRevision;
    private Date startRevisionDate;
    private String endRevision;
    private Date endRevisionDate;
    private Set<Long> revisions = Sets.newHashSet();
    private Set<Long> classes = Sets.newHashSet();
    private Set<Long> issues = Sets.newHashSet();
    private Set<Long> milestones = Sets.newHashSet();
    private Set<Long> jobs = Sets.newHashSet();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getStartRevision() {
        return startRevision;
    }

    public void setStartRevision(String startRevision) {
        this.startRevision = startRevision;
    }

    public Date getStartRevisionDate() {
        return startRevisionDate;
    }

    public void setStartRevisionDate(Date startRevisionDate) {
        this.startRevisionDate = startRevisionDate;
    }

    public String getEndRevision() {
        return endRevision;
    }

    public void setEndRevision(String endRevision) {
        this.endRevision = endRevision;
    }

    public Date getEndRevisionDate() {
        return endRevisionDate;
    }

    public void setEndRevisionDate(Date endRevisionDate) {
        this.endRevisionDate = endRevisionDate;
    }

    public Set<Long> getRevisions() {
        return revisions;
    }

    public void setRevisions(Set<Long> revisions) {
        this.revisions = revisions;
    }

    public Set<Long> getClasses() {
        return classes;
    }

    public void setClasses(Set<Long> classes) {
        this.classes = classes;
    }

    public Set<Long> getIssues() {
        return issues;
    }

    public void setIssues(Set<Long> issues) {
        this.issues = issues;
    }

    public Set<Long> getMilestones() {
        return milestones;
    }

    public void setMilestones(Set<Long> milestones) {
        this.milestones = milestones;
    }

    public Set<Long> getJobs() {
        return jobs;
    }

    public void setJobs(Set<Long> jobs) {
        this.jobs = jobs;
    }
}
