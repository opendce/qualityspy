package pl.wroc.pwr.qualityspy.webapp.delegate;

import com.google.common.collect.Lists;
import org.dozer.DozerBeanMapper;
import org.dozer.MappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.wroc.pwr.qualityspy.model.File;
import pl.wroc.pwr.qualityspy.webapp.dto.FilesDto;
import pl.wroc.pwr.qualityspy.repositories.FileRepository;

import java.util.List;

@Service
public class FilesDelegate {
    @Autowired
    private transient FileRepository fileRepository;

    @Autowired
    private transient DozerBeanMapper mapper;

    public List<FilesDto> getFiles(long id) throws MappingException {
        List<File> fileSet = fileRepository.findAllById(id);
        List<FilesDto> results = Lists.newArrayList();
        for (File file : fileSet) {
            FilesDto result = mapper.map(file, FilesDto.class);
            results.add(result);
        }
        return results;
    }
}
