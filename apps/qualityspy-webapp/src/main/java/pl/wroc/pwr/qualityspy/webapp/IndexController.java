package pl.wroc.pwr.qualityspy.webapp;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

/**
 * Created by Olaf on 2017-04-29.
 */

@Controller
public class IndexController {

    @GetMapping("/")
    String index() {
        return "index";
    }
}
