package pl.wroc.pwr.qualityspy.webapp.rest;

import au.com.bytecode.opencsv.CSVWriter;
import io.swagger.annotations.Api;
import org.apache.log4j.Logger;
import org.dozer.MappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.wroc.pwr.qualityspy.webapp.delegate.IssueCommentsDelegate;
import pl.wroc.pwr.qualityspy.webapp.dto.IssueCommentsDto;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

@Path("/issuecomments")
@Api(value="/issuecomments", description = "IssueCommentsRestService")
@Component
public class IssueCommentsRestService {

    private static final Logger LOGGER = Logger.getLogger(ProjectsRestService.class);
    private static final char CSV_SEPARATOR = ';';

    @Autowired
    private transient IssueCommentsDelegate issueCommentsDelegate;

    /**
     *
     * @return List of all issue comments as XML.
     */
    @GET
    @Path("/{issueDescriptionId}")
    @Produces(MediaType.TEXT_XML)
    @Transactional
    public List<IssueCommentsDto> getIssueCommentsXML(@PathParam("issueDescriptionId") long id) {
        LOGGER.info("Getting IssueComments as XML");
        return issueCommentsDelegate.getIssueComments(id);
    }

    /**
     *
     * @return List of all IssueComments as CSV.
     */
    @GET
    @Path("/{issueDescriptionId}")
    @Produces("text/csv")
    @Transactional
    public String getIssueCommentsCSV(@PathParam("issueDescriptionId") long id) throws MappingException {
        LOGGER.info("Getting IssueComments as CSV");
        List<IssueCommentsDto> issueCommentsDtos = issueCommentsDelegate.getIssueComments(id);
        StringWriter out = new StringWriter();
        CSVWriter writer = new CSVWriter(out, CSV_SEPARATOR);
        List<String[]> outputString = new ArrayList<String[]>();
        String[] columnNames = {"id", "author", "commentDate", "commentText"};
        outputString.add(columnNames);

        for (IssueCommentsDto issueCommentsDto : issueCommentsDtos) {
            String[] rawData = new String[columnNames.length];
            rawData[0] = issueCommentsDto.getId() != null ? issueCommentsDto.getId().toString() : "";
            rawData[1] = issueCommentsDto.getAuthor();
            rawData[2] = issueCommentsDto.getDate().toString();
            rawData[3] = issueCommentsDto.getCommentText();
            outputString.add(rawData);
        }
        writer.writeAll(outputString);
        return out.toString();
    }

}
