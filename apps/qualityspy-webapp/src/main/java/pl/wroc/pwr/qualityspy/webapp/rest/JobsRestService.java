package pl.wroc.pwr.qualityspy.webapp.rest;

import au.com.bytecode.opencsv.CSVWriter;
import io.swagger.annotations.Api;
import org.apache.log4j.Logger;
import org.dozer.MappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.wroc.pwr.qualityspy.webapp.delegate.JobsDelegate;
import pl.wroc.pwr.qualityspy.webapp.dto.JobsDto;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

@Path("/jobs")
@Api
@Component
public class JobsRestService {

    private static final Logger LOGGER = Logger.getLogger(ProjectsRestService.class);
    private static final char SEPARATOR = ',';
    private static final char CSV_SEPARATOR = ';';

    @Autowired
    private transient JobsDelegate jobsDelegate;

    /**
     *
     * @return List of all jobs as XML.
     */
    @GET
    @Path("/{projectId}")
    @Produces(MediaType.TEXT_XML)
    @Transactional
    public List<JobsDto> getJobsXML(@PathParam("projectId") long id) {
        LOGGER.info("Getting classes as XML");
        return jobsDelegate.getJobs(id);
    }

    /**
     *
     * @return List of all jobs as CSV.
     */
    @GET
    @Path("/{projectId}")
    @Produces("text/csv")
    @Transactional
    public String getJobsCSV(@PathParam("projectId") long id) throws MappingException {
        LOGGER.info("Getting jobs as CSV");
        List<JobsDto> jobsDtos = jobsDelegate.getJobs(id);
        StringWriter out = new StringWriter();
        CSVWriter writer = new CSVWriter(out, CSV_SEPARATOR);
        List<String[]> outputString = new ArrayList<String[]>();
        String[] columnNames = {"id", "name", "url", "color", "builds"};
        outputString.add(columnNames);

        for (JobsDto jobsDto : jobsDtos) {
            String[] rawData = new String[columnNames.length];
            rawData[0] = jobsDto.getId() != null ? jobsDto.getId().toString() : "";
            rawData[1] = jobsDto.getName();
            rawData[2] = jobsDto.getUrl();
            rawData[3] = jobsDto.getColor();

            StringBuilder sb = new StringBuilder();
            for(Long builds : jobsDto.getBuilds()){
                if(builds !=null){
                    sb.append(builds.toString());
                    sb.append(SEPARATOR);
                }
            }
            if(sb.charAt(sb.length()-1) == SEPARATOR){
                sb.deleteCharAt(sb.length() - 1);
            }
            rawData[4] = sb.toString();

            outputString.add(rawData);
        }
        writer.writeAll(outputString);
        return out.toString();
    }

}
