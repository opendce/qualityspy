package pl.wroc.pwr.qualityspy.webapp.delegate;

import com.google.common.collect.Lists;
import org.dozer.DozerBeanMapper;
import org.dozer.MappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.wroc.pwr.qualityspy.model.IssueLog;
import pl.wroc.pwr.qualityspy.webapp.dto.IssueLogsDto;
import pl.wroc.pwr.qualityspy.repositories.IssueLogRepository;

import java.util.List;

@Service
public class IssueLogsDelegate {
    @Autowired
    private transient IssueLogRepository issueLogRepository;

    @Autowired
    private transient DozerBeanMapper mapper;

    public List<IssueLogsDto> getIssueLogs(long id) throws MappingException {
        List<IssueLog> issueLogSet = issueLogRepository.findAllById(id);
        List<IssueLogsDto> results = Lists.newArrayList();
        for (IssueLog issueLog : issueLogSet) {
            IssueLogsDto result = mapper.map(issueLog, IssueLogsDto.class);
            results.add(result);
        }
        return results;
    }
}
