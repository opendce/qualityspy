package pl.wroc.pwr.qualityspy.webapp;

import io.swagger.jaxrs.config.BeanConfig;
import io.swagger.jaxrs.listing.ApiListingResource;
import io.swagger.jaxrs.listing.SwaggerSerializers;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import pl.wroc.pwr.qualityspy.webapp.rest.*;
import pl.wroc.pwr.qualityspy.webapp.rest.jiraexport.JiraExportRestService;

/**
 * Created by Olaf on 2017-04-29.
 */

@Configuration
public class JerseyConfig extends ResourceConfig{

    @Value("${spring.jersey.application-path:/}")
    private String apiPath;


    public JerseyConfig(){
        registerEndpoints();
        configureSwagger();
    }

    private void registerEndpoints() {
        register(IndexController.class);
        register(ProjectsRestService.class);
        register(RevisionsRestService.class);
        register(ProjectHistoriesRestService.class);
        register(MilestonesRestService.class);
        register(MethodsRestService.class);
        register(JobsRestService.class);
        register(IssueLogsRestService.class);
        register(IssueDescriptionsRestService.class);
        register(IssueCommentsRestService.class);
        register(IssueChangesRestService.class);
        register(FileRestService.class);
        register(ClassesRestService.class);
        register(BuildsRestService.class);
        register(JiraExportRestService.class);
    }

    private void configureSwagger() {
        register(ApiListingResource.class);
        register(SwaggerSerializers.class);

        BeanConfig beanConfig = new BeanConfig();
        beanConfig.setVersion("0.5");
        beanConfig.setSchemes(new String[]{"http"});
        beanConfig.setHost("localhost:10080");
        beanConfig.setBasePath("/api");
        beanConfig.setResourcePackage("pl.wroc.pwr.qualityspy.webapp.rest");
        beanConfig.setPrettyPrint(true);
        beanConfig.setScan(true);
    }

}
