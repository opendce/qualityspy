package pl.wroc.pwr.qualityspy.webapp.delegate;

import com.google.common.collect.Lists;
import org.dozer.DozerBeanMapper;
import org.dozer.MappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.wroc.pwr.qualityspy.model.MilestoneDescription;
import pl.wroc.pwr.qualityspy.webapp.dto.MilestonesDto;
import pl.wroc.pwr.qualityspy.repositories.MilestoneDescriptionRepository;

import java.util.List;

@Service
public class MilestonesDelegate {

    @Autowired
    private transient MilestoneDescriptionRepository milestoneDescriptionRepository;

    @Autowired
    private transient DozerBeanMapper mapper;

    public List<MilestonesDto> getMilestones(long id) throws MappingException {
        List<MilestoneDescription> milestones = milestoneDescriptionRepository.findAllById(id);
        List<MilestonesDto> results = Lists.newArrayList();
        for (MilestoneDescription milestoneDescription : milestones) {
            MilestonesDto result = mapper.map(milestoneDescription, MilestonesDto.class);
            results.add(result);
        }
        return results;
    }

}
