package pl.wroc.pwr.qualityspy.webapp.jiraexport;

import org.apache.commons.lang3.StringUtils;
import pl.wroc.pwr.qualityspy.model.IssueComment;
import pl.wroc.pwr.qualityspy.model.IssueDescription;
import pl.wroc.pwr.qualityspy.model.ProjectHistory;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CsvPreprocessor {

    private final static String ATTACHMENT_SEPARATOR = "\n";
    public final static DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm", Locale.US);

    private final ProjectHistory history;
    private final long noAttachments;
    private final long noAffectedVersions;
    private final int noComments;
    private final long noComponents;
    private final long noFixedVersions;
    private final long noLabels;

    private final static String[] staticFields = {"Summary", "Description", "Reporter", "Assignee", "Issue Key", "Issue Type", "Priority",
            "Status", "Environment", "Project", "Created", "Original Estimate", "Remaining Estimate", "Time Spent", "Issue ID", "Parent ID", "Resolved"};
    private final static String attachment = "Attachment";
    private final static String affectedVersion = "AffectsVersion";
    private final static String comment = "Comment";
    private final static String component = "Component";
    private final static String fixedVersion = "FixVersion";
    private final static String label = "Labels";

    public CsvPreprocessor(ProjectHistory history) {
        this.history = history;
        this.noAttachments = countMaxAttachments();
        this.noAffectedVersions = countMaxOfCommaSeparatedAttributes(IssueDescription::getAffectsVersion);
        this.noComponents = countMaxOfCommaSeparatedAttributes(IssueDescription::getComponent);
        this.noFixedVersions = countMaxOfCommaSeparatedAttributes(IssueDescription::getFixVersion);
        this.noLabels = countMaxOfCommaSeparatedAttributes(IssueDescription::getLabels);
        this.noComments = countMaxComments();
    }

    public String[] renderHeaders() {
        final String[] result = new String[noColumns()];

        int i = 0;
        for (; i < staticFields.length; i++) {
            result[i] = staticFields[i];
        }
        i = addDynamicHeaders(result, i, noAttachments, attachment);
        i = addDynamicHeaders(result, i, noAffectedVersions, affectedVersion);
        i = addDynamicHeaders(result, i, noComments, comment);
        i = addDynamicHeaders(result, i, noComponents, component);
        i = addDynamicHeaders(result, i, noFixedVersions, fixedVersion);
        addDynamicHeaders(result, i, noLabels, label);
        return result;
    }

    public Collection<String[]> renderData() {
        return history.getIssues().stream()
                .map(issue -> renderRow(issue))
                .collect(Collectors.toList());
    }

    private int noColumns() {
        long result = staticFields.length + noAttachments + noAffectedVersions + noComments + noComponents + noFixedVersions + noLabels;
        return (int) result;
    }

    private String[] renderRow(IssueDescription issue) {
        final String[] result = new String[noColumns()];
        result[0] = nullSafe(issue.getSummary());
        result[1] = nullSafe(issue.getDescription());
        result[2] = nullSafe(issue.getReporter());
        result[3] = nullSafe(issue.getAssignTo());
        result[4] = nullSafe(issue.getIssueKey());
        result[5] = nullSafe(issue.getType());
        result[6] = nullSafe(issue.getPriority());
        result[7] = nullSafe(issue.getStatus());
        result[8] = nullSafe(issue.getEnvironment());
        result[9] = nullSafe(issue.getProject());
        result[10] = nullSafeDateFormat(issue.getCreated());
        result[11] = String.valueOf(issue.getOriginalEstimate());
        result[12] = String.valueOf(issue.getRemainingEstimate());
        result[13] = String.valueOf(issue.getTimeSpend());
        result[14] = String.valueOf(issue.getIssueId());
        result[15] = findIdOf(issue.getParentKey());
        result[16] = nullSafeDateFormat(issue.getResolved());

        int i = 17;
        i = renderDynamicAttributes(result, i, attachmentsStream(issue.getAttachment()).collect(Collectors.toList()), (int)noAttachments);
        i = renderDynamicAttributes(result, i, comaSeparatedAttributesStream(issue.getAffectsVersion()).collect(Collectors.toList()), (int)noAffectedVersions);
        i = renderDynamicAttributes(result, i, serializedCommentsList(issue.getIssueComments()), noComments);
        i = renderDynamicAttributes(result, i, comaSeparatedAttributesStream(issue.getComponent()).collect(Collectors.toList()), (int)noComponents);
        i = renderDynamicAttributes(result, i, comaSeparatedAttributesStream(issue.getFixVersion()).collect(Collectors.toList()), (int)noFixedVersions);
        renderDynamicAttributes(result, i, comaSeparatedAttributesStream(issue.getLabels()).collect(Collectors.toList()), (int)noLabels);
        return result;
    }

    private String nullSafeDateFormat(Calendar date) {
        return date != null ? DATE_FORMAT.toFormat().format(date.getTime()) : "";
    }

    private String nullSafeDateFormat(LocalDateTime date) {
        return date != null ? date.format(DATE_FORMAT) : "";
    }

    private String nullSafe(String input) {
        return input == null ? "" : input.trim();
    }

    private String findIdOf(String parentKey) {
        if(StringUtils.isBlank(parentKey)) {
            return "";
        }
        final Optional<Long> optionalId = history.getIssues().stream()
                .filter(issue -> parentKey.equalsIgnoreCase(issue.getIssueKey()))
                .map(issue -> issue.getIssueId())
                .findFirst();
        final Long id = optionalId.orElseThrow(() -> new IllegalArgumentException(String.format("The parent issue is not present, i.e. %s", parentKey)));
        return String.valueOf(id);
    }

    private int renderDynamicAttributes(final String[] result, final int i, final List<String> attributesToRender, final int noOfAttributes) {
        int j = i;
        for (; j < i + noOfAttributes; j++) {
            if(attributesToRender.size() > j-i) {
                result[j] = attributesToRender.get(j - i);
            } else {
                result[j] = "";
            }
        }
        return j;
    }



    private long countMaxAttachments() {
        final Optional<Long> result = history.getIssues().stream()
                .map(issue -> issue.getAttachment())
                .map(attachments -> attachmentsStream(attachments).count())
                .max(Comparator.naturalOrder());
        return result.orElse(0L);
    }

    private Stream<String> attachmentsStream(String attachments) {
        if(StringUtils.isBlank(attachments)) {
            return Stream.empty();
        }
        return Arrays.stream(attachments.split(ATTACHMENT_SEPARATOR))
                .filter(attachment -> StringUtils.isNotBlank(attachment))
                .map(attachment -> attachment.trim());
    }

    private long countMaxOfCommaSeparatedAttributes(Function<IssueDescription, String> extractAttributea) {
        final Optional<Long> result = history.getIssues().stream()
                .map(extractAttributea)
                .map(attribute -> comaSeparatedAttributesStream(attribute)
                        .count())
                .max(Comparator.naturalOrder());
        return result.orElse(0L);
    }

    private Stream<String> comaSeparatedAttributesStream(String attributes) {
        if(StringUtils.isBlank(attributes)){
            return Stream.empty();
        }
        return Arrays.stream(attributes.split(","))
                .filter(attribute -> StringUtils.isNotBlank(attribute))
                .map(attribute -> attribute.trim());
    }

    private int countMaxComments() {
        final Optional<Integer> result = history.getIssues().stream()
                .map(issue -> issue.getCommentsSize())
                .max(Comparator.naturalOrder());
        return result.orElse(0);
    }

    private List<String> serializedCommentsList(Set<IssueComment> issueComments) {
        return issueComments.stream()
                .map(comment -> String.format("%s;%s;%s", nullSafeDateFormat(comment.getDate()), nullSafe(comment.getCommentText())))
                .collect(Collectors.toList());
    }

    private int addDynamicHeaders(String[] result, int i, long noHeaders, String header) {
        int j = i;
        for (; j < i + noHeaders; j++) {
            result[j] = header;
        }
        return j;
    }
}
