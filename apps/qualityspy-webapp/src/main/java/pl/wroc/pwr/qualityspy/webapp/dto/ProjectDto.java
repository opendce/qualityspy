package pl.wroc.pwr.qualityspy.webapp.dto;

import com.google.common.collect.Sets;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Set;

@XmlRootElement
public class ProjectDto implements IDto {
    private Long id;
    private String projectName;
    private Set<Long> projectHistories = Sets.newHashSet();

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the projectName
     */
    public String getProjectName() {
        return projectName;
    }

    /**
     * @param projectName the projectName to set
     */
    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    /**
     * @return the projectHistories
     */
    public Set<Long> getProjectHistories() {
        return projectHistories;
    }

    /**
     * @param projectHistories the projectHistories to set
     */
    public void setProjectHistories(Set<Long> projectHistories) {
        this.projectHistories = projectHistories;
    }
}
