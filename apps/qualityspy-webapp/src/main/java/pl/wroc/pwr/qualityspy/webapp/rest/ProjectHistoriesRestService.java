package pl.wroc.pwr.qualityspy.webapp.rest;

import au.com.bytecode.opencsv.CSVWriter;
import io.swagger.annotations.Api;
import org.apache.log4j.Logger;
import org.dozer.MappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.wroc.pwr.qualityspy.webapp.delegate.ProjectHistoriesDelegate;
import pl.wroc.pwr.qualityspy.webapp.dto.ProjectHistoriesDto;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Path("/histories")
@Api
@Component
public class ProjectHistoriesRestService {

    private static final Logger LOGGER = Logger.getLogger(ProjectsRestService.class);
    private static final char SEPARATOR = ',';
    private static final char CSV_SEPARATOR = ';';


    private ProjectHistoriesDelegate projectHistoriesDelegate;

    public ProjectHistoriesRestService(@Autowired ProjectHistoriesDelegate projectHistoriesDelegate) {
        this.projectHistoriesDelegate = projectHistoriesDelegate;
    }

    /**
     *
     * @return List of all project histories as XML.
     */
    @GET
    @Path("/{projectId}")
    @Produces(MediaType.TEXT_XML)
    @Transactional
    public List<ProjectHistoriesDto> getProjectHistoriesXML(@PathParam("projectId") long id) {
        LOGGER.info("Getting project histories as XML");
        return projectHistoriesDelegate.getProjectHistories(id);
    }

    /**
     *
     * @return List of all project histories as CSV.
     */
    @GET
    @Path("/{projectId}")
    @Produces("text/csv")
    @Transactional
    public String getProjectsCSV(@PathParam("projectId") long id) throws MappingException {
        LOGGER.info("Getting project histories as CSV");
        final List<ProjectHistoriesDto> projectHistories = projectHistoriesDelegate.getProjectHistories(id);
        final StringWriter stringWriter = new StringWriter();
        final CSVWriter csvWriter = new CSVWriter(stringWriter, CSV_SEPARATOR);
        final List<String[]> lines = new ArrayList<>();
        final String[] columnNames = {"ID", "Version", "StartRevision", "StartRevisionDate", "EndRevision",
                "EndRevisionDate", "Revisions", "Classes", "Issues", "Milestones", "Jobs"};
        lines.add(columnNames);

        for (final ProjectHistoriesDto projectHistoriesDto : projectHistories) {
            final String[] fields = new String[columnNames.length];

            fields[0] = nullableToString(projectHistoriesDto.getId());
            fields[1] = nullableToString(projectHistoriesDto.getVersion());
            fields[2] = nullableToString(projectHistoriesDto.getStartRevision());
            fields[3] = nullableToString(projectHistoriesDto.getStartRevisionDate());
            fields[4] = nullableToString(projectHistoriesDto.getEndRevision());
            fields[5] = nullableToString(projectHistoriesDto.getEndRevisionDate());
            fields[6] = stringListFromIterable(projectHistoriesDto.getRevisions());
            fields[7] = stringListFromIterable(projectHistoriesDto.getClasses());
            fields[8] = stringListFromIterable(projectHistoriesDto.getIssues());
            fields[9] = stringListFromIterable(projectHistoriesDto.getMilestones());
            fields[10] = stringListFromIterable(projectHistoriesDto.getJobs());

            lines.add(fields);
        }
        csvWriter.writeAll(lines);
        return stringWriter.toString();
    }

    private <T> String nullableToString(final T object) {
        return Optional.ofNullable(object)
                .map(T::toString)
                .orElse("");
    }

    private <T> String stringListFromIterable(final Iterable<T> iterable) {
        return Optional.ofNullable(iterable)
                .map(itr -> StreamSupport.stream(iterable.spliterator(), false)
                        .map(T::toString)
                        .collect(Collectors.joining(String.valueOf(SEPARATOR))))
                .orElse("");
    }
}
