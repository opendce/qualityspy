package pl.wroc.pwr.qualityspy.webapp.rest;

import au.com.bytecode.opencsv.CSVWriter;
import io.swagger.annotations.Api;
import org.apache.log4j.Logger;
import org.dozer.MappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.wroc.pwr.qualityspy.webapp.delegate.IssueDescriptionsDelegate;
import pl.wroc.pwr.qualityspy.webapp.dto.IssueDescriptionsDto;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Path("/issuedescriptions")
@Api
@Component
public class IssueDescriptionsRestService {

    private static final Logger LOGGER = Logger.getLogger(ProjectsRestService.class);
    private static final char SEPARATOR = ',';
    private static final char CSV_SEPARATOR = ';';

    @Autowired
    private transient IssueDescriptionsDelegate issueDescriptionsDelegate;

    /**
     *
     * @return List of all issues as XML.
     */
    @GET
    @Path("/{projectHistoryId}")
    @Produces(MediaType.TEXT_XML)
    @Transactional
    public List<IssueDescriptionsDto> getIssueXML(@PathParam("projectHistoryId") long id) {
        LOGGER.info("Getting issues as XML");
        return issueDescriptionsDelegate.getProjectHistoryIssueDescriptions(id);
    }

    /**
     *
     * @return List of all issues as CSV.
     */
    @GET
    @Path("/{projectHistoryId}")
    @Produces("text/csv")
    @Transactional
    public String getIssuesCSV(@PathParam("projectHistoryId") long id) throws MappingException {
        LOGGER.info("Getting issues as CSV");
        List<IssueDescriptionsDto> issueDescriptionDtos =
                issueDescriptionsDelegate.getProjectHistoryIssueDescriptions(id);
        StringWriter out = new StringWriter();
        CSVWriter writer = new CSVWriter(out, CSV_SEPARATOR);
        List<String[]> outputString = new ArrayList<String[]>();
        String[] columnNames = {"id", "defect", "severity", "summary",
        "description", "reporter", "assignTo", "issueKey", "issueId",
        "type", "priority", "status", "resolution", "created",
        "updated", "component", "affectsVersion", "fixVersion", "environment",
        "attachment", "labels", "originalEstimate", "remainingEstimate",
        "timeSpend", "resolved", "project", "value", "complexity", "isParent",
        "parentKey", "watchers", "issueChanges", "issueComments", "issueLogs"};
        outputString.add(columnNames);
        for (IssueDescriptionsDto issueDescriptionsDto : issueDescriptionDtos) {
            String[] rawData = new String[columnNames.length];
            rawData[0] = issueDescriptionsDto.getId() != null ? issueDescriptionsDto.getId().toString() : "";
            rawData[1] = Boolean.toString(issueDescriptionsDto.isDefect());
            rawData[2] = issueDescriptionsDto.getSeverity();
            rawData[3] = issueDescriptionsDto.getSummary();
            rawData[4] = issueDescriptionsDto.getDescription();
            rawData[5] = issueDescriptionsDto.getReporter();
            rawData[6] = issueDescriptionsDto.getAssignTo();
            rawData[7] = issueDescriptionsDto.getIssueKey();
            rawData[8] = Long.toString(issueDescriptionsDto.getIssueId());
            rawData[9] = issueDescriptionsDto.getPriority();
            rawData[10] = issueDescriptionsDto.getStatus();
            rawData[11] = issueDescriptionsDto.getResolution();
            rawData[12] = issueDescriptionsDto.getCreated().toString();
            rawData[13] = issueDescriptionsDto.getUpdated().toString();
            rawData[14] = issueDescriptionsDto.getComponent();
            rawData[15] = issueDescriptionsDto.getAffectsVersion();
            rawData[16] = issueDescriptionsDto.getFixVersion();
            rawData[17] = issueDescriptionsDto.getEnvironment();
            rawData[18] = issueDescriptionsDto.getAttachment();
            rawData[19] = issueDescriptionsDto.getLabels();
            rawData[20] = Long.toString(issueDescriptionsDto.getOriginalEstimate());
            rawData[21] = Long.toString(issueDescriptionsDto.getRemainingEstimate());
            rawData[22] = Long.toString(issueDescriptionsDto.getTimeSpend());
            rawData[23] = issueDescriptionsDto.getResolved().toString();
            rawData[24] = issueDescriptionsDto.getProject();
            rawData[25] = issueDescriptionsDto.getValue();
            rawData[26] = issueDescriptionsDto.getComplexity();
            rawData[27] = Boolean.toString(issueDescriptionsDto.isParent());
            rawData[28] = issueDescriptionsDto.getParentKey();
            rawData[29] = issueDescriptionsDto.getWatchers();

            rawData[30] = stringListFromIterable(issueDescriptionsDto.getIssueHistory());
            rawData[31] = stringListFromIterable(issueDescriptionsDto.getIssueComments());
            rawData[32] = stringListFromIterable(issueDescriptionsDto.getIssueLogs());

            outputString.add(rawData);
        }
        writer.writeAll(outputString);
        return out.toString();
    }

    private <T> String stringListFromIterable(final Iterable<T> iterable) {
        return Optional.ofNullable(iterable)
                .map(itr -> StreamSupport.stream(iterable.spliterator(), false)
                        .map(T::toString)
                        .collect(Collectors.joining(String.valueOf(SEPARATOR))))
                .orElse("");
    }
}
