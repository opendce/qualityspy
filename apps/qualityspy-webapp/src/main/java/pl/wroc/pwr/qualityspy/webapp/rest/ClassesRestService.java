package pl.wroc.pwr.qualityspy.webapp.rest;

import au.com.bytecode.opencsv.CSVWriter;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.log4j.Logger;
import org.dozer.MappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.wroc.pwr.qualityspy.webapp.delegate.ClassesDelegate;
import pl.wroc.pwr.qualityspy.webapp.dto.ClassesDto;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

@Path("/classes")
@Api(value="/classes", description = "ClassesRestService")
@Component
public class ClassesRestService {

    private static final Logger LOGGER = Logger.getLogger(ProjectsRestService.class);
    private static final char SEPARATOR = ',';
    private static final char CSV_SEPARATOR = ';';

    @Autowired
    private transient ClassesDelegate classesDellegate;

    /**
     *
     * @return List of all clsses as XML.
     */
    @ApiOperation(value = "Gets a hello resource. Version 1 - (version in URL)")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "hello resource found"),
            @ApiResponse(code = 404, message = "Given admin user not found")
    })
    @GET
    @Path("/{projectHistoryId}")
    @Produces(MediaType.TEXT_XML)
    @Transactional
    public List<ClassesDto> getClassesXML(@PathParam("projectHistoryId") long id) {
        LOGGER.info("Getting classes as XML");
        return classesDellegate.getClasses(id);
    }

    /**
     *
     * @return List of all classes as CSV.
     */
    @GET
    @Path("/{projectHistoryId}")
    @Produces("text/csv")
    @Transactional
    public String getClassesCSV(@PathParam("projectHistoryId") long id) throws MappingException {
        LOGGER.info("Getting classes as CSV");
        List<ClassesDto> classes = classesDellegate.getClasses(id);
        StringWriter out = new StringWriter();
        CSVWriter writer = new CSVWriter(out, CSV_SEPARATOR);
        List<String[]> outputString = new ArrayList<String[]>();
        String[] columnNames = {"ID", "Name", "amc", "ca", "cam", "cbm", "cbo", "ce", "dam", "dit", "ic", "lcom", "lcom3",
                "loc", "mfa", "moa", "noc", "npm", "rfc", "wmc", "averageCC", "maxCC", "methods"};
        outputString.add(columnNames);

        for (ClassesDto classesDto : classes) {
            String[] rawData = new String[columnNames.length];
            rawData[0] = classesDto.getId() != null ? classesDto.getId().toString() : "";
            rawData[1] = classesDto.getName();
            rawData[2] = Double.toString(classesDto.getAmc());
            rawData[3] = Double.toString(classesDto.getCa());
            rawData[4] = Double.toString(classesDto.getCam());
            rawData[5] = Double.toString(classesDto.getCbm());
            rawData[6] = Double.toString(classesDto.getCbo());
            rawData[7] = Double.toString(classesDto.getCe());
            rawData[8] = Double.toString(classesDto.getDam());
            rawData[9] = Double.toString(classesDto.getDit());
            rawData[10] = Double.toString(classesDto.getIc());
            rawData[11] = Double.toString(classesDto.getLcom());
            rawData[12] = Double.toString(classesDto.getLcom3());
            rawData[13] = Double.toString(classesDto.getLoc());
            rawData[14] = Double.toString(classesDto.getMfa());
            rawData[15] = Double.toString(classesDto.getMoa());
            rawData[16] = Double.toString(classesDto.getNoc());
            rawData[17] = Double.toString(classesDto.getNpm());
            rawData[18] = Double.toString(classesDto.getRfc());
            rawData[19] = Double.toString(classesDto.getWmc());
            rawData[20] = Double.toString(classesDto.getAverageCC());
            rawData[21] = Double.toString(classesDto.getMaxCC());

            StringBuilder sb = new StringBuilder();
            for(Long methods : classesDto.getMethods()){
                if(methods !=null){
                    sb.append(methods.toString());
                    sb.append(SEPARATOR);
                }
            }
            if(sb.charAt(sb.length()-1) == SEPARATOR){
                sb.deleteCharAt(sb.length() - 1);
            }
            rawData[22] = sb.toString();

            outputString.add(rawData);
        }
        writer.writeAll(outputString);
        return out.toString();
    }

}
