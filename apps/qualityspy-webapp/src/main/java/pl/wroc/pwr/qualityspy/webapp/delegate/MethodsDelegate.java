package pl.wroc.pwr.qualityspy.webapp.delegate;

import com.google.common.collect.Lists;
import org.dozer.DozerBeanMapper;
import org.dozer.MappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.wroc.pwr.qualityspy.model.MethodDescription;
import pl.wroc.pwr.qualityspy.webapp.dto.MethodsDto;
import pl.wroc.pwr.qualityspy.repositories.MethodDescriptionRepository;

import java.util.List;

@Service
public class MethodsDelegate {
    @Autowired
    private transient MethodDescriptionRepository methodDescriptionRepository;

    @Autowired
    private transient DozerBeanMapper mapper;

    public List<MethodsDto> getMethods(long id) throws MappingException {
        List<MethodDescription> methods = methodDescriptionRepository.findAllById(id);
        List<MethodsDto> results = Lists.newArrayList();
        for (MethodDescription methodDescription : methods) {
            MethodsDto result = mapper.map(methodDescription, MethodsDto.class);
            results.add(result);
        }
        return results;
    }
}
