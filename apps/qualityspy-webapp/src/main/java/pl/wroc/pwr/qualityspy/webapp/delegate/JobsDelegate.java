package pl.wroc.pwr.qualityspy.webapp.delegate;

import com.google.common.collect.Lists;
import org.dozer.DozerBeanMapper;
import org.dozer.MappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.wroc.pwr.qualityspy.model.Job;
import pl.wroc.pwr.qualityspy.webapp.dto.JobsDto;
import pl.wroc.pwr.qualityspy.repositories.JobRepository;

import java.util.List;

@Service
public class JobsDelegate {
    @Autowired
    private transient JobRepository jobRepository;

    @Autowired
    private transient DozerBeanMapper mapper;

    public List<JobsDto> getJobs(long id) throws MappingException {
        List<Job> jobSet = jobRepository.findAllById(id);
        List<JobsDto> results = Lists.newArrayList();
        for (Job job : jobSet) {
            JobsDto result = mapper.map(job, JobsDto.class);
            results.add(result);
        }
        return results;
    }
}
