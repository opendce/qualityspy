package pl.wroc.pwr.qualityspy.webapp.rest;

import au.com.bytecode.opencsv.CSVWriter;
import io.swagger.annotations.Api;
import org.apache.log4j.Logger;
import org.dozer.MappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.wroc.pwr.qualityspy.webapp.delegate.IssueChangesDelegate;
import pl.wroc.pwr.qualityspy.webapp.dto.IssueChangesDto;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

@Path("/issuechanges")
@Api(value="/issuechanges", description = "IssueRestService")
@Component
public class IssueChangesRestService {

    private static final Logger LOGGER = Logger.getLogger(ProjectsRestService.class);
    private static final char CSV_SEPARATOR = ';';

    @Autowired
    private transient IssueChangesDelegate issueChangesDelegate;

    /**
     *
     * @return List of all issue changes as XML.
     */
    @GET
    @Path("/{issueDescriptionId}")
    @Produces(MediaType.TEXT_XML)
    @Transactional
    public List<IssueChangesDto> getIssueChangesXML(@PathParam("issueDescriptionId") long id) {
        LOGGER.info("Getting IssueChanges as XML");
        return issueChangesDelegate.getIssueChanges(id);
    }

    /**
     *
     * @return List of all IssueChanges as CSV.
     */
    @GET
    @Path("/{issueDescriptionId}")
    @Produces("text/csv")
    @Transactional
    public String getIssueChangesCSV(@PathParam("issueDescriptionId") long id) throws MappingException {
        LOGGER.info("Getting IssueChanges as CSV");
        List<IssueChangesDto> issueChangesDtos = issueChangesDelegate.getIssueChanges(id);
        StringWriter out = new StringWriter();
        CSVWriter writer = new CSVWriter(out, CSV_SEPARATOR);
        List<String[]> outputString = new ArrayList<String[]>();
        String[] columnNames = {"id", "whoChanged", "dateOfChange", "whatChanged", "oldValue", "newValue", "type"};
        outputString.add(columnNames);

        for (IssueChangesDto issueChangesDto : issueChangesDtos) {
            String[] rawData = new String[columnNames.length];
            rawData[0] = issueChangesDto.getId() != null ? issueChangesDto.getId().toString() : "";
            rawData[1] = issueChangesDto.getWhoChanged();
            rawData[2] = issueChangesDto.getDateOfChange().toString();
            rawData[3] = issueChangesDto.getWhatChanged();
            rawData[4] = issueChangesDto.getOldValue();
            rawData[5] = issueChangesDto.getNewValue();
            rawData[6] = issueChangesDto.getType();
            outputString.add(rawData);
        }
        writer.writeAll(outputString);
        return out.toString();
    }

}
