package pl.wroc.pwr.qualityspy.webapp.delegate;

import com.google.common.collect.Lists;
import org.dozer.DozerBeanMapper;
import org.dozer.MappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.wroc.pwr.qualityspy.model.Build;
import pl.wroc.pwr.qualityspy.webapp.dto.BuildsDto;
import pl.wroc.pwr.qualityspy.repositories.BuildRepository;

import java.util.List;

@Service
public class BuildsDelegate {

    @Autowired
    private transient BuildRepository buildRepository;

    @Autowired
    private transient DozerBeanMapper mapper;

    public List<BuildsDto> getBuilds(long id) throws MappingException {
        List<Build> buildsSet = buildRepository.findAllById(id);
        List<BuildsDto> results = Lists.newArrayList();
        for (Build build : buildsSet) {
            BuildsDto result = mapper.map(build, BuildsDto.class);
            results.add(result);
        }
        return results;
    }
}
