package pl.wroc.pwr.qualityspy.webapp.rest;

import au.com.bytecode.opencsv.CSVWriter;
import io.swagger.annotations.Api;
import org.apache.log4j.Logger;
import org.dozer.MappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.wroc.pwr.qualityspy.webapp.delegate.IssueLogsDelegate;
import pl.wroc.pwr.qualityspy.webapp.dto.IssueLogsDto;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

@Path("/issuelogs")
@Api
@Component
public class IssueLogsRestService {

    private static final Logger LOGGER = Logger.getLogger(ProjectsRestService.class);
    private static final char CSV_SEPARATOR = ';';

    @Autowired
    private transient IssueLogsDelegate issueLogsDelegate;

    /**
     *
     * @return List of all issue logs as XML.
     */
    @GET
    @Path("/{issueDescriptionId}")
    @Produces(MediaType.TEXT_XML)
    @Transactional
    public List<IssueLogsDto> getIssueLogsXML(@PathParam("issueDescriptionId") long id) {
        LOGGER.info("Getting IssueLogs as XML");
        return issueLogsDelegate.getIssueLogs(id);
    }

    /**
     *
     * @return List of all IssueLogs as CSV.
     */
    @GET
    @Path("/{issueDescriptionId}")
    @Produces("text/csv")
    @Transactional
    public String getIssueLogsCSV(@PathParam("issueDescriptionId") long id) throws MappingException {
        LOGGER.info("Getting IssueLogs as CSV");
        List<IssueLogsDto> issueLogsDtos = issueLogsDelegate.getIssueLogs(id);
        StringWriter out = new StringWriter();
        CSVWriter writer = new CSVWriter(out, CSV_SEPARATOR);
        List<String[]> outputString = new ArrayList<String[]>();
        String[] columnNames = {"id", "who", "logDate", "status", "timeSpent", "comment"};
        outputString.add(columnNames);

        for (IssueLogsDto issueLogsDto : issueLogsDtos) {
            String[] rawData = new String[columnNames.length];
            rawData[0] = issueLogsDto.getId() != null ? issueLogsDto.getId().toString() : "";
            rawData[1] = issueLogsDto.getWho();
            rawData[2] = issueLogsDto.getDate().withNano(0).toString();
            rawData[3] = issueLogsDto.getStatus();
            rawData[4] = Long.toString(issueLogsDto.getTimeSpent());
            rawData[5] = issueLogsDto.getComment();
            outputString.add(rawData);
        }
        writer.writeAll(outputString);
        return out.toString();
    }

}
