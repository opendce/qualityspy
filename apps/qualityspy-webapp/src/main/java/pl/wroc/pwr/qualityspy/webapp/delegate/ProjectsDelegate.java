package pl.wroc.pwr.qualityspy.webapp.delegate;

/**
 * Created by Olaf on 2017-04-30.
 */
import com.google.common.collect.Lists;
import org.dozer.DozerBeanMapper;
import org.dozer.MappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.wroc.pwr.qualityspy.model.Project;
import pl.wroc.pwr.qualityspy.webapp.dto.ProjectDto;
import pl.wroc.pwr.qualityspy.repositories.ProjectRepository;

import java.util.List;

@Service
public class ProjectsDelegate {

    @Autowired
    private transient ProjectRepository projectRepository;

    @Autowired
    private transient DozerBeanMapper mapper;

    public List<ProjectDto> getAllProjects() throws MappingException {
        List<Project> projects = (List<Project>) projectRepository.findAll();
        List<ProjectDto> results = Lists.newArrayList();
        for (Project p : projects) {
            ProjectDto result = mapper.map(p, ProjectDto.class);
            results.add(result);
        }
        return results;
    }
}
