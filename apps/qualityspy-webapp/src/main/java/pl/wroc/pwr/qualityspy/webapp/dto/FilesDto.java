package pl.wroc.pwr.qualityspy.webapp.dto;
import pl.wroc.pwr.qualityspy.model.enums.FileType;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class FilesDto {
    private Long id;
    private String oldContent;
    private String newContent;
    private String name;
    private String oldPath;
    private String newPath;
    private String repoPath;
    @Enumerated(EnumType.STRING)
    private ChangeType changeType;
    private long numberOfAddedLines;
    private long numberOfRemovedLines;
    @Enumerated(EnumType.STRING)
    private FileType fileType;

    public FileType getFileType() {
        return fileType;
    }

    public void setFileType(FileType fileType) {
        this.fileType = fileType;
    }

    public long getNumberOfRemovedLines() {
        return numberOfRemovedLines;
    }

    public void setNumberOfRemovedLines(long numberOfRemovedLines) {
        this.numberOfRemovedLines = numberOfRemovedLines;
    }

    public long getNumberOfAddedLines() {
        return numberOfAddedLines;
    }

    public void setNumberOfAddedLines(long numberOfAddedLines) {
        this.numberOfAddedLines = numberOfAddedLines;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOldContent() {
        return oldContent;
    }

    public void setOldContent(String oldContent) {
        this.oldContent = oldContent;
    }

    public String getNewContent() {
        return newContent;
    }

    public void setNewContent(String newContent) {
        this.newContent = newContent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOldPath() {
        return oldPath;
    }

    public void setOldPath(String oldPath) {
        this.oldPath = oldPath;
    }

    public String getNewPath() {
        return newPath;
    }

    public void setNewPath(String newPath) {
        this.newPath = newPath;
    }

    public String getRepoPath() {
        return repoPath;
    }

    public void setRepoPath(String repoPath) {
        this.repoPath = repoPath;
    }

    public ChangeType getChangeType() {
        return changeType;
    }

    public void setChangeType(ChangeType changeType) {
        this.changeType = changeType;
    }

    public enum ChangeType {
        DELETION,
        CREATION,
        CHANGED_PATH,
        CHANGED_CONTENT;
    };
}
