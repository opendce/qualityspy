package pl.wroc.pwr.qualityspy.webapp.rest;

import au.com.bytecode.opencsv.CSVWriter;
import org.apache.log4j.Logger;
import org.dozer.MappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.wroc.pwr.qualityspy.webapp.delegate.MethodsDelegate;
import pl.wroc.pwr.qualityspy.webapp.dto.MethodsDto;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

@Path("/methods")
@Component
public class MethodsRestService {

    private static final Logger LOGGER = Logger.getLogger(ProjectsRestService.class);
    private static final char CSV_SEPARATOR = ';';

    @Autowired
    private transient MethodsDelegate methodsDelegate;

    /**
     *
     * @return List of all methods as XML.
     */
    @GET
    @Path("/{classId}")
    @Produces(MediaType.TEXT_XML)
    @Transactional
    public List<MethodsDto> getMehodsXML(@PathParam("classId") long id) {
        LOGGER.info("Getting methods as XML");
        return methodsDelegate.getMethods(id);
    }

    /**
     *
     * @return List of all methods as CSV.
     */
    @GET
    @Path("/{classId}")
    @Produces("text/csv")
    @Transactional
    public String getMethodsCSV(@PathParam("classId") long id) throws MappingException {
        LOGGER.info("Getting methods as CSV");
        List<MethodsDto> methods = methodsDelegate.getMethods(id);
        StringWriter out = new StringWriter();
        CSVWriter writer = new CSVWriter(out, CSV_SEPARATOR);
        List<String[]> outputString = new ArrayList<String[]>();
        String[] columnNames = {"ID", "Name", "CyclomaticComplexity"};
        outputString.add(columnNames);

        for (MethodsDto methodsDto : methods) {
            String[] rawData = new String[columnNames.length];
            rawData[0] = methodsDto.getId() != null ? methodsDto.getId().toString() : "";
            rawData[1] = methodsDto.getName();
            rawData[2] = Long.toString(methodsDto.getCyclomaticComplexity());
            outputString.add(rawData);
        }
        writer.writeAll(outputString);
        return out.toString();
    }
}
