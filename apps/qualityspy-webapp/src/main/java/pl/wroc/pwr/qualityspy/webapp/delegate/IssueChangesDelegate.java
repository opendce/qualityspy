package pl.wroc.pwr.qualityspy.webapp.delegate;


import com.google.common.collect.Lists;
import org.dozer.DozerBeanMapper;
import org.dozer.MappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.wroc.pwr.qualityspy.model.IssueChange;
import pl.wroc.pwr.qualityspy.webapp.dto.IssueChangesDto;
import pl.wroc.pwr.qualityspy.repositories.IssueChangeRepository;

import java.util.List;

@Service
public class IssueChangesDelegate {
    @Autowired
    private transient IssueChangeRepository issueChangeRepository;

    @Autowired
    private transient DozerBeanMapper mapper;

    public List<IssueChangesDto> getIssueChanges(long id) throws MappingException {
        List<IssueChange> issueChangeSet = issueChangeRepository.findAllById(id);
        List<IssueChangesDto> results = Lists.newArrayList();
        for (IssueChange issueChange : issueChangeSet) {
            IssueChangesDto result = mapper.map(issueChange, IssueChangesDto.class);
            results.add(result);
        }
        return results;
    }
}
