package pl.wroc.pwr.qualityspy.webapp.rest;


import io.github.benas.randombeans.EnhancedRandomBuilder;
import io.github.benas.randombeans.api.EnhancedRandom;
import org.easymock.*;
import org.junit.Test;
import pl.wroc.pwr.qualityspy.webapp.delegate.ProjectHistoriesDelegate;
import pl.wroc.pwr.qualityspy.webapp.dto.ProjectHistoriesDto;
import java.util.Collections;

import static org.junit.Assert.assertNotEquals;

public class ProjectHistoriesRestServiceTest {

    @Test
    public void getProjectsCSV_should_not_throw_NullPointerException_when_given_project_has_null_field() {
        // given
        final ProjectHistoriesDelegate projectHistoriesDelegateMock =
                EasyMock.createNiceMock(ProjectHistoriesDelegate.class);

        final EnhancedRandom enhancedRandom = EnhancedRandomBuilder.aNewEnhancedRandomBuilder()
                .overrideDefaultInitialization(true)
                .collectionSizeRange(1, 1)
                .build();
        final ProjectHistoriesDto randomProjectHistoriesDto = enhancedRandom.nextObject(ProjectHistoriesDto.class);
        randomProjectHistoriesDto.setStartRevisionDate(null);

        EasyMock.expect(projectHistoriesDelegateMock.getProjectHistories(EasyMock.anyInt()))
                .andReturn(Collections.singletonList(randomProjectHistoriesDto))
                .anyTimes();

        EasyMock.replay(projectHistoriesDelegateMock);

        final ProjectHistoriesRestService projectHistoriesRestService =
                new ProjectHistoriesRestService(projectHistoriesDelegateMock);

        // when
        final String responseBody = projectHistoriesRestService.getProjectsCSV(1);

        //then
        assertNotEquals(null, responseBody);
    }
}
