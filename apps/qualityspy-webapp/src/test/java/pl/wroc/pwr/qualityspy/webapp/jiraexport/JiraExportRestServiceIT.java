package pl.wroc.pwr.qualityspy.webapp.jiraexport;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.jayway.restassured.response.Response;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import pl.wroc.pwr.qualityspy.model.IssueComment;
import pl.wroc.pwr.qualityspy.model.IssueDescription;
import pl.wroc.pwr.qualityspy.model.Project;
import pl.wroc.pwr.qualityspy.model.ProjectHistory;
import pl.wroc.pwr.qualityspy.repositories.ProjectRepository;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;

import static com.jayway.restassured.RestAssured.expect;
import static org.junit.Assert.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@RunWith(SpringRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class, TransactionalTestExecutionListener.class})
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.HSQL)
@DatabaseSetup("classpath:dbunit.xml")
public class JiraExportRestServiceIT {

    @Autowired
    private ProjectRepository projectRepo;

    private final String summaryThin = "summary";
    private final String descriptionThin = "description";
    private final String reporterThin = "reporter";
    private final String assignToThin = "assignee";
    private final String issueKeyThin = "KEY-1";
    private final String typeThin = "Defect";
    private final String priorityThin = "Major";
    private final String statusThin = "Open";
    private final String componentThin = null;
    private final String affectsVersionThin = "1.0";
    private final String fixVersionThin = "1.1";
    private final String environmentThin = "Firefox";
    private final String attachmentThin = null;
    private final String labelsThin = "label";
    private final long issueIdThin = 17L;

    private final String summaryFat = "summary fat";
    private final String descriptionFat = "description fat";
    private final String reporterFat = "reporter fat";
    private final String assignToFat = "assignee fat";
    private final String issueKeyFat = "KEY-2";
    private final String typeFat = "Sub-Task";
    private final String priorityFat = "Blocker";
    private final String statusFat = "In Progress";
    private final String componentFat = "camel-core,documentation";
    private final String affectsVersionFat = "3.0.0, 3.0.1";
    private final String fixVersionFat = "3.0.2, 2.10.7";
    private final String environmentFat = "Firefox";
    private final String attachmentFat = "https://java.net/jira/secure/attachment/54705/01_PU_Jako_Akwizytor_konfiguruje_komponenty.pdf\n" +
            "https://java.net/jira/secure/attachment/54706/02_PU_Jako_Akwizytor_definiuje_rozne_zestawy_konfiguracji_komponentow.pdf\n";
    private final String labelsFat = "review-cli, perforce";
    private final String projectFat = "Test project";
    private final Calendar createdFat = toCalendar(LocalDateTime.of(116, 0, 31, 23, 59, 59));
    private final IssueComment comment1Fat = new IssueComment("assignee", LocalDateTime.of(116, 0, 1, 23, 59, 59), "comment 1");
    private final IssueComment comment2Fat = new IssueComment("zassignee fat", LocalDateTime.of(116, 0, 21, 1, 1, 59), "comment 2");
    private final long originalEstimateFat = 10L;
    private final long remainingEstimateFat = 5L;
    private final String fixedFat = "Fixed";
    private final long timeSpendFat = 3600L;
    private final Calendar resolvedFat = toCalendar(LocalDateTime.of(116, 1, 2, 1, 1, 59));
    private final String valueFat = "value";
    private final long issueIdFat = 18L;

    private final String rowTemplate = "\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\"," +
            "\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\"," +
            "\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\"";

    @Test
    public void jiraExportCsv() throws Exception {
        Project project = createTestData();

        Response response = expect().statusCode(200)
                .given().header("Accept", "text/csv")
                .when().get("api/jiraexport/projecthistory/" + getIdOfFirstHistory(project));

        assertNotNull(response);
        String[] responseBody = response.getBody().asString().split("\n");
        assertEquals(csvHeader(), responseBody[0]);
        assertEquals(thinRow(), responseBody[1]);

        final String[] expectedFatRow = fatRow().split(",");
        final String[] actualFatRow = responseBody[2].split(",");
        assertEquals(expectedFatRow.length, actualFatRow.length);
        for (int i = 0; i < expectedFatRow.length; i++) {
            if (i != 21 && i != 22) {
                assertEquals("Column " + i, expectedFatRow[i], actualFatRow[i]);
            }
        }
        assertTrue(expectedFatRow[21].equals(actualFatRow[21]) || expectedFatRow[21].equals(actualFatRow[22]));
        assertTrue(expectedFatRow[22].equals(actualFatRow[21]) || expectedFatRow[22].equals(actualFatRow[22]));

    }

    private Project createTestData() {
        Project project = new Project("name");

        final ProjectHistory history = new ProjectHistory();
        project.getProjectHistories().add(history);

        createThinIssue(history);
        createFatIssue(history);

        project = projectRepo.save(project);
        return project;
    }

    private void createThinIssue(ProjectHistory history) {
        final IssueDescription thinIssue = new IssueDescription(summaryThin, descriptionThin, reporterThin, assignToThin, issueKeyThin,
                typeThin, priorityThin, statusThin, componentThin, affectsVersionThin, fixVersionThin, environmentThin,
                attachmentThin, labelsThin);
        thinIssue.setIssueId(issueIdThin);
        history.getIssues().add(thinIssue);
    }

    private void createFatIssue(ProjectHistory history) {
        final IssueDescription fatIssue = new IssueDescription(summaryFat, descriptionFat, reporterFat, assignToFat, issueKeyFat,
                typeFat, priorityFat, statusFat, componentFat, affectsVersionFat, fixVersionFat, environmentFat,
                attachmentFat, labelsFat);
        fatIssue.setProject(projectFat);
        fatIssue.setCreated(createdFat);
        fatIssue.setIssueComments(new HashSet<>(Arrays.asList(comment1Fat, comment2Fat)));
        fatIssue.setOriginalEstimate(originalEstimateFat);
        fatIssue.setRemainingEstimate(remainingEstimateFat);
        fatIssue.setResolution(fixedFat);
        fatIssue.setTimeSpend(timeSpendFat);
        fatIssue.setResolved(resolvedFat);
        fatIssue.setValue(valueFat);
        fatIssue.setParentKey(issueKeyThin);
        fatIssue.setIssueId(issueIdFat);

        history.getIssues().add(fatIssue);
    }

    private Long getIdOfFirstHistory(Project project) {
        return project.getProjectHistories().iterator().next().getId();
    }

    private String csvHeader() {
        return "\"Summary\",\"Description\",\"Reporter\",\"Assignee\",\"Issue Key\",\"Issue Type\",\"Priority\"," +
                "\"Status\",\"Environment\",\"Project\",\"Created\",\"Original Estimate\"," +
                "\"Remaining Estimate\",\"Time Spent\",\"Issue ID\",\"Parent ID\",\"Resolved\",\"Attachment\",\"Attachment\",\"AffectsVersion\",\"AffectsVersion\"," +
                "\"Comment\",\"Comment\",\"Component\",\"Component\",\"FixVersion\",\"FixVersion\",\"Labels\",\"Labels\"";
    }

    private String thinRow() {
        return String.format(rowTemplate, summaryThin, descriptionThin, reporterThin, assignToThin, issueKeyThin, typeThin, priorityThin,
                statusThin, environmentThin, "", "", "0", "0", "0", String.valueOf(issueIdThin), "", "", "", "", affectsVersionThin,
                "", "", "", "", "", fixVersionThin, "", labelsThin, "");
    }

    private String fatRow() {
        return String.format(rowTemplate, summaryFat, descriptionFat, reporterFat, assignToFat, issueKeyFat, typeFat, priorityFat,
                statusFat, environmentFat, projectFat, CsvPreprocessor.DATE_FORMAT.format(createdFat.getTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime()),
                String.valueOf(originalEstimateFat), String.valueOf(remainingEstimateFat), String.valueOf(timeSpendFat),
                String.valueOf(issueIdFat), String.valueOf(issueIdThin), CsvPreprocessor.DATE_FORMAT.format(resolvedFat.getTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime()),
                "https://java.net/jira/secure/attachment/54705/01_PU_Jako_Akwizytor_konfiguruje_komponenty.pdf",
                "https://java.net/jira/secure/attachment/54706/02_PU_Jako_Akwizytor_definiuje_rozne_zestawy_konfiguracji_komponentow.pdf",
                "3.0.0", "3.0.1", "01/01/2016 23:59;" + comment1Fat.getAuthor() + ";" + comment1Fat.getCommentText(),
                "21/01/2016 01:01;" + comment2Fat.getAuthor() + ";" + comment2Fat.getCommentText(), "camel-core", "documentation", "3.0.2", "2.10.7", "review-cli", "perforce");
    }

    private static Calendar toCalendar(LocalDateTime date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(Date.from(date.atZone(ZoneId.systemDefault()).toInstant()));
        return cal;
    }
}