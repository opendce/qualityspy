package pl.wroc.pwr.qualityspy.webapp.converters;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import pl.wroc.pwr.qualityspy.model.IEntityWithId;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class IdsConverterTest {

    @Mock
    IEntityWithId entity;

    @Test
    public void convertTo() throws Exception {
        //given
        long id = 432L;
        when(entity.getId()).thenReturn(id);

        //when
        long actual = new IdsConverter().convertTo(entity, 0L);

        //then
        assertEquals(id, actual);
    }

}