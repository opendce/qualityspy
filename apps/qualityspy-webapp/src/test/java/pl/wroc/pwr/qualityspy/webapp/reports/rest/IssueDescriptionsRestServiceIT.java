package pl.wroc.pwr.qualityspy.webapp.reports.rest;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.jayway.restassured.response.Response;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import static com.jayway.restassured.RestAssured.expect;
import static org.junit.Assert.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@RunWith(SpringRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class, TransactionalTestExecutionListener.class})
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.HSQL)
@DatabaseSetup("classpath:dbunit.xml")
public class IssueDescriptionsRestServiceIT {

    private static final String MISSPELLED_URL = "api/issuedescription";
    private static final long SAMPLE_ID_CORRECT = 1;
    private static final String PROPER_URL = "api/issuedescriptions";
    private static final long SAMPLE_ID_INCORRECT = -1;

    @Test
    public void givenWrongPath_WhenCallingIssuesXML_ShouldReturn404Error()
    {
        expect().statusCode(406).given().header("Accept", "text/xml").when().get(MISSPELLED_URL + "/" + SAMPLE_ID_INCORRECT);
    }

    @Test
    public void givenCorrectPath_WhenCallingIssuesXML_ShouldReturn200Status() {
        expect().statusCode(200)
                .body(Matchers.hasXPath("/issueDescriptionsDtoes/issueDescriptionsDto/affectsVersion", Matchers.equalTo("VERSION")))
                .body(Matchers.hasXPath("/issueDescriptionsDtoes/issueDescriptionsDto/assignTo", Matchers.equalTo("TO")))
                .body(Matchers.hasXPath("/issueDescriptionsDtoes/issueDescriptionsDto/attachment", Matchers.equalTo("ATA")))
                .body(Matchers.hasXPath("/issueDescriptionsDtoes/issueDescriptionsDto/complexity", Matchers.equalTo("COMPLEX")))
                .body(Matchers.hasXPath("/issueDescriptionsDtoes/issueDescriptionsDto/component", Matchers.equalTo("COMPONENT")))
                .body(Matchers.hasXPath("/issueDescriptionsDtoes/issueDescriptionsDto/created", Matchers.startsWith("2013-01-01T00:00:00"))) //TODO timezone
                .body(Matchers.hasXPath("/issueDescriptionsDtoes/issueDescriptionsDto/defect", Matchers.equalTo("true")))
                .body(Matchers.hasXPath("/issueDescriptionsDtoes/issueDescriptionsDto/description", Matchers.equalTo("DESC")))
                .body(Matchers.hasXPath("/issueDescriptionsDtoes/issueDescriptionsDto/environment", Matchers.equalTo("ENV")))
                .body(Matchers.hasXPath("/issueDescriptionsDtoes/issueDescriptionsDto/fixVersion", Matchers.equalTo("VERSION")))
                .body(Matchers.hasXPath("/issueDescriptionsDtoes/issueDescriptionsDto/id", Matchers.equalTo("1")))
                .body(Matchers.hasXPath("/issueDescriptionsDtoes/issueDescriptionsDto/issueComments", Matchers.equalTo("1")))
                .body(Matchers.hasXPath("/issueDescriptionsDtoes/issueDescriptionsDto/issueHistory", Matchers.equalTo("1")))
                .body(Matchers.hasXPath("/issueDescriptionsDtoes/issueDescriptionsDto/issueId", Matchers.equalTo("1")))
                .body(Matchers.hasXPath("/issueDescriptionsDtoes/issueDescriptionsDto/issueKey", Matchers.equalTo("KEY")))
                .body(Matchers.hasXPath("/issueDescriptionsDtoes/issueDescriptionsDto/issueLogs", Matchers.equalTo("1")))
                .body(Matchers.hasXPath("/issueDescriptionsDtoes/issueDescriptionsDto/labels", Matchers.equalTo("LABELS")))
                .body(Matchers.hasXPath("/issueDescriptionsDtoes/issueDescriptionsDto/originalEstimate", Matchers.equalTo("1")))
                .body(Matchers.hasXPath("/issueDescriptionsDtoes/issueDescriptionsDto/parent", Matchers.equalTo("false")))
                .body(Matchers.hasXPath("/issueDescriptionsDtoes/issueDescriptionsDto/parentKey", Matchers.equalTo("KEY")))
                .body(Matchers.hasXPath("/issueDescriptionsDtoes/issueDescriptionsDto/priority", Matchers.equalTo("PRO")))
                .body(Matchers.hasXPath("/issueDescriptionsDtoes/issueDescriptionsDto/project", Matchers.equalTo("PROJECT")))
                .body(Matchers.hasXPath("/issueDescriptionsDtoes/issueDescriptionsDto/remainingEstimate", Matchers.equalTo("1")))
                .body(Matchers.hasXPath("/issueDescriptionsDtoes/issueDescriptionsDto/reporter", Matchers.equalTo("REPORTER")))
                .body(Matchers.hasXPath("/issueDescriptionsDtoes/issueDescriptionsDto/resolution", Matchers.equalTo("RES")))
                .body(Matchers.hasXPath("/issueDescriptionsDtoes/issueDescriptionsDto/resolved", Matchers.startsWith("2013-01-01T00:00:00"))) //TODO timezone
                .body(Matchers.hasXPath("/issueDescriptionsDtoes/issueDescriptionsDto/severity", Matchers.equalTo("SEVERITY")))
                .body(Matchers.hasXPath("/issueDescriptionsDtoes/issueDescriptionsDto/status", Matchers.equalTo("STATUS")))
                .body(Matchers.hasXPath("/issueDescriptionsDtoes/issueDescriptionsDto/summary", Matchers.equalTo("SUMMARY")))
                .body(Matchers.hasXPath("/issueDescriptionsDtoes/issueDescriptionsDto/timeSpend", Matchers.equalTo("1")))
                .body(Matchers.hasXPath("/issueDescriptionsDtoes/issueDescriptionsDto/type", Matchers.equalTo("TYPE")))
                .body(Matchers.hasXPath("/issueDescriptionsDtoes/issueDescriptionsDto/updated", Matchers.startsWith("2013-01-01T00:00:00"))) //TODO timezone
                .body(Matchers.hasXPath("/issueDescriptionsDtoes/issueDescriptionsDto/value", Matchers.equalTo("VALUE")))
                .body(Matchers.hasXPath("/issueDescriptionsDtoes/issueDescriptionsDto/watchers", Matchers.equalTo("WATCHERS")))
                .given().header("Accept", "text/xml").when().get(PROPER_URL + "/" + SAMPLE_ID_CORRECT);
    }

    @Test
    public void givenWrongPath_WhenCallingIssuesCSV_ShouldReturn404Error()
    {
        expect().statusCode(406).given().header("Accept", "text/csv").when().get(MISSPELLED_URL + "/" + SAMPLE_ID_INCORRECT);
    }

    @Test
    public void givenCorrectPath_WhenCallingIssuesCSV_ShouldReturn200Status() {
        Response resp = expect().statusCode(200).given().header("Accept", "text/csv").when().get(PROPER_URL + "/" + SAMPLE_ID_CORRECT);
        String body = resp.getBody().asString().trim();
        body = body.replaceAll("CET ", ""); //TODO timezone
        body = body.replaceAll("UTC ", "");
        assertEquals("\"id\";\"defect\";\"severity\";\"summary\";\"description\";\"reporter\";\"assignTo\";\"issueKey\";\"issueId\";\"type\";\"priority\";\"status\";\"resolution\";\"created\";\"updated\";\"component\";\"affectsVersion\";\"fixVersion\";\"environment\";\"attachment\";\"labels\";\"originalEstimate\";\"remainingEstimate\";\"timeSpend\";\"resolved\";\"project\";\"value\";\"complexity\";\"isParent\";\"parentKey\";\"watchers\";\"issueChanges\";\"issueComments\";\"issueLogs\"\n" +
                "\"1\";\"true\";\"SEVERITY\";\"SUMMARY\";\"DESC\";\"REPORTER\";\"TO\";\"KEY\";\"1\";\"PRO\";\"STATUS\";\"RES\";\"Tue Jan 01 00:00:00 2013\";\"Tue Jan 01 00:00:00 2013\";\"COMPONENT\";\"VERSION\";\"VERSION\";\"ENV\";\"ATA\";\"LABELS\";\"1\";\"1\";\"1\";\"Tue Jan 01 00:00:00 2013\";\"PROJECT\";\"VALUE\";\"COMPLEX\";\"false\";\"KEY\";\"WATCHERS\";\"1\";\"1\";\"1\";", body);
    }
}