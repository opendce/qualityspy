package pl.wroc.pwr.qualityspy.webapp.reports.rest;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.jayway.restassured.response.Response;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import static com.jayway.restassured.RestAssured.expect;
import static org.junit.Assert.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@RunWith(SpringRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class, TransactionalTestExecutionListener.class})
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.HSQL)
@DatabaseSetup("classpath:dbunit.xml")
public class RevisionsRestServiceIT {

    private static final String MISSPELLED_URL = "api/revision";
    private static final long SAMPLE_ID_CORRECT = 1;
    private static final String PROPER_URL = "api/revisions";
    private static final long SAMPLE_ID_INCORRECT = -1;


    @Test
    public void givenWrongPath_WhenCallingRevisionsXML_ShouldReturn404Error()
    {
        expect().statusCode(406).given().header("Accept", "text/xml").when().get(MISSPELLED_URL + "/" + SAMPLE_ID_INCORRECT);
    }

    @Test
    public void givenCorrectPath_WhenCallingRevisionsXML_ShouldReturn200Status() {
        expect().statusCode(200)
                .body(Matchers.hasXPath("/revisionsDtoes/revisionsDto/author", Matchers.equalTo("AUTHOR")))
                .body(Matchers.hasXPath("/revisionsDtoes/revisionsDto/branch", Matchers.equalTo("BRANCH")))
                .body(Matchers.hasXPath("/revisionsDtoes/revisionsDto/comment", Matchers.equalTo("COMMENT")))
                .body(Matchers.hasXPath("/revisionsDtoes/revisionsDto/date", Matchers.startsWith("2013-01-01T00:00:00"))) //TODO timezone
                .body(Matchers.hasXPath("/revisionsDtoes/revisionsDto/files", Matchers.equalTo("1")))
                .body(Matchers.hasXPath("/revisionsDtoes/revisionsDto/id", Matchers.equalTo("1")))
                .body(Matchers.hasXPath("/revisionsDtoes/revisionsDto/number", Matchers.equalTo("1")))
                .body(Matchers.hasXPath("/revisionsDtoes/revisionsDto/tags", Matchers.equalTo("TAGS")))
                .given().header("Accept", "text/xml").when().get(PROPER_URL + "/" + SAMPLE_ID_CORRECT);
    }

    @Test
    public void givenWrongPath_WhenCallingRevisionsCSV_ShouldReturn404Error()
    {
        expect().statusCode(406).given().header("Accept", "text/csv").when().get(MISSPELLED_URL + "/" + SAMPLE_ID_INCORRECT);
    }

    @Test
    public void givenCorrectPath_WhenCallingRevisionsCSV_ShouldReturn200Status() {
        Response resp = expect().statusCode(200)
                .given().header("Accept", "text/csv")
                .when().get(PROPER_URL + "/" + SAMPLE_ID_CORRECT);
        String body = resp.getBody().asString().trim();
        body = body.replaceAll("CET ", ""); //TODO timezone
        body = body.replaceAll("UTC ", "");
        assertEquals("\"ID\";\"Number\";\"Date\";\"Tags\";\"Branch\";\"Author\";\"Comment\";\"Files\"\n" +
                "\"1\";\"1\";\"Tue Jan 01 00:00:00 2013\";\"TAGS\";\"BRANCH\";\"AUTHOR\";\"COMMENT\";\"1\"", body);
    }
}