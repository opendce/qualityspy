package pl.wroc.pwr.qualityspy.webapp.reports.rest;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.jayway.restassured.response.Response;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import static com.jayway.restassured.RestAssured.expect;
import static org.junit.Assert.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@RunWith(SpringRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class, TransactionalTestExecutionListener.class})
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.HSQL)
@DatabaseSetup("classpath:dbunit.xml")
public class ProjectsRestServiceIT {

    private static final String MISSPELLED_URL = "api//project";
    private static final String PROPER_URL = "api/projects";



    @Test
    public void givenWrongPath_WhenCallingProjectsXML_ShouldReturn404Error()
    {
        expect().statusCode(406).given().header("Accept", "text/xml").when().get(MISSPELLED_URL);
    }

    @Test
    public void testGetProjectsXml() {
        expect().statusCode(200).and()
                .body(Matchers.hasXPath("/projectDtoes/projectDto[1]/projectName", Matchers.equalTo("Test Project")))
                .body(Matchers.hasXPath("/projectDtoes/projectDto[1]/id", Matchers.equalTo("1")))
                .body(Matchers.hasXPath("/projectDtoes/projectDto[1]/projectHistories[1]", Matchers.equalTo("1")))
                .body(Matchers.hasXPath("/projectDtoes/projectDto[1]/projectHistories[2]", Matchers.equalTo("2")))
                .given().header("Accept", "text/xml").when().get(PROPER_URL);
    }

    @Test
    public void givenWrongPath_WhenCallingProjectsCSV_ShouldReturn404Error()
    {
        expect().statusCode(406).given().header("Accept", "text/csv").when().get(MISSPELLED_URL);
    }

    @Test
    public void testGetProjectsCsv() {
        Response resp = expect().statusCode(200).given().header("Accept", "text/csv").when().get(PROPER_URL);
        String body = resp.getBody().asString().trim();
        assertEquals("\"ID\";\"ProjectName\";\"ProjectHistories\"\n\"1\";\"Test Project\";\"1,2\"", body);
    }

}
