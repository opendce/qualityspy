package pl.wroc.pwr.qualityspy.webapp.reports.rest;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.jayway.restassured.response.Response;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import static com.jayway.restassured.RestAssured.expect;
import static org.junit.Assert.assertTrue;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@RunWith(SpringRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class, TransactionalTestExecutionListener.class})
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.HSQL)
@DatabaseSetup("classpath:dbunit.xml")
public class IssueChangesRestServiceIT {

    private static final String MISSPELLED_URL = "api/issuechange";
    private static final long SAMPLE_ID_CORRECT = 1;
    private static final String PROPER_URL = "api/issuechanges";
    private static final long SAMPLE_ID_INCORRECT = -1;

    @Test
    public void givenWrongPath_WhenCallingIssuesXML_ShouldReturn404Error()
    {
        expect().statusCode(406).given().header("Accept", "text/xml").when().get(MISSPELLED_URL + "/" + SAMPLE_ID_INCORRECT);
    }

    @Test
    public void givenCorrectPath_WhenCallingIssuesXML_ShouldReturn200Status() {
        expect().statusCode(200) //TODO timezone
                .body(Matchers.hasXPath("/issueChangesDtoes/issueChangesDto/dateOfChange", Matchers.containsString("2013-01-01T00:00:00")))
                .body(Matchers.hasXPath("/issueChangesDtoes/issueChangesDto/id", Matchers.equalTo("1")))
                .body(Matchers.hasXPath("/issueChangesDtoes/issueChangesDto/newValue", Matchers.equalTo("NEW")))
                .body(Matchers.hasXPath("/issueChangesDtoes/issueChangesDto/oldValue", Matchers.equalTo("OLD")))
                .body(Matchers.hasXPath("/issueChangesDtoes/issueChangesDto/type", Matchers.equalTo("TYPE")))
                .body(Matchers.hasXPath("/issueChangesDtoes/issueChangesDto/whatChanged", Matchers.equalTo("WHAT")))
                .body(Matchers.hasXPath("/issueChangesDtoes/issueChangesDto/whoChanged", Matchers.equalTo("WHO")))
                .given().header("Accept", "text/xml").when().get(PROPER_URL + "/" + SAMPLE_ID_CORRECT);
    }

    @Test
    public void givenWrongPath_WhenCallingIssuesCSV_ShouldReturn404Error()
    {
        expect().statusCode(406).given().header("Accept", "text/csv").when().get(MISSPELLED_URL + "/" + SAMPLE_ID_INCORRECT);
    }

    @Test
    public void givenCorrectPath_WhenCallingIssuesCSV_ShouldReturn200Status() {
        Response resp = expect().statusCode(200).given().header("Accept", "text/csv").when().get(PROPER_URL + "/" + SAMPLE_ID_CORRECT);
        String body = resp.getBody().asString().trim(); //TODO timezone
        assertTrue(body.startsWith("\"id\";\"whoChanged\";\"dateOfChange\";\"whatChanged\";\"oldValue\";\"newValue\";\"type\"\n" +
                "\"1\";\"WHO\";\"Tue Jan 01 00:00:00"));
        assertTrue(body.endsWith("2013\";\"WHAT\";\"OLD\";\"NEW\";\"TYPE\""));
    }
}