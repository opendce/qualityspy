package pl.wroc.pwr.qualityspy.webapp.reports.rest;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import static com.jayway.restassured.RestAssured.expect;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@RunWith(SpringRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class, TransactionalTestExecutionListener.class})
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.HSQL)
@DatabaseSetup("classpath:dbunit.xml")
public class BuildsRestServiceIT {

    private static final String MISSPELLED_URL = "api/build";
    private static final long SAMPLE_ID_CORRECT = 1;
    private static final String PROPER_URL = "api/builds";
    private static final long SAMPLE_ID_INCORRECT = -1;

    @Test
    public void givenWrongPath_WhenCallingBuildsXML_ShouldReturn404Error()
    {
        expect().statusCode(406).given().header("Accept", "text/xml").when().get(MISSPELLED_URL + "/" + SAMPLE_ID_INCORRECT);
    }

    @Test
    public void givenCorrectPath_WhenCallingBuildsXML_ShouldReturn200Status() {
        expect().statusCode(200)
                .body(Matchers.hasXPath("/buildsDtoes/buildsDto/id", Matchers.equalTo("1")))
                .body(Matchers.hasXPath("/buildsDtoes/buildsDto/number", Matchers.equalTo("1"))) //TODO timezone
                .body(Matchers.hasXPath("/buildsDtoes/buildsDto/date", Matchers.containsString("2013-01-01T00:00:00")))
                .body(Matchers.hasXPath("/buildsDtoes/buildsDto/result", Matchers.equalTo("RESULT")))
                .body(Matchers.hasXPath("/buildsDtoes/buildsDto/totalCount", Matchers.equalTo("1")))
                .body(Matchers.hasXPath("/buildsDtoes/buildsDto/failCount", Matchers.equalTo("1")))
                .body(Matchers.hasXPath("/buildsDtoes/buildsDto/totalDiff", Matchers.equalTo("1")))
                .body(Matchers.hasXPath("/buildsDtoes/buildsDto/failDiff", Matchers.equalTo("1")))
                .given().header("Accept", "text/xml").when().get(PROPER_URL + "/" + SAMPLE_ID_CORRECT);
    }

    @Test
    public void givenWrongPath_WhenCallingBuildsCSV_ShouldReturn404Error()
    {
        expect().statusCode(406).given().header("Accept", "text/csv").when().get(MISSPELLED_URL + "/" + SAMPLE_ID_INCORRECT);
    }

    @Test
    public void givenCorrectPath_WhenCallingBuildsCSV_ShouldReturn200Status() {
        expect().statusCode(200).given().header("Accept", "text/csv").when().get(PROPER_URL + "/" + SAMPLE_ID_CORRECT);
    }
}