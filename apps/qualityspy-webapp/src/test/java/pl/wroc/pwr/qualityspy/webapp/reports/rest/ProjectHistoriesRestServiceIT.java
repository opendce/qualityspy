package pl.wroc.pwr.qualityspy.webapp.reports.rest;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.jayway.restassured.response.Response;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import static com.jayway.restassured.RestAssured.expect;
import static org.junit.Assert.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@RunWith(SpringRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class, TransactionalTestExecutionListener.class})
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.HSQL)
@DatabaseSetup("classpath:dbunit.xml")
public class ProjectHistoriesRestServiceIT{

    private static final String MISSPELLED_URL = "api/historie";
    private static final long SAMPLE_ID_CORRECT = 1;
    private static final String PROPER_URL = "api/histories";
    private static final long SAMPLE_ID_INCORRECT = -1;

    @Test
    public void givenWrongPath_WhenCallingProjectHistoriesXML_ShouldReturn404Error()
    {
        expect().statusCode(406).given().header("Accept", "text/xml").when().get(MISSPELLED_URL + "/" + SAMPLE_ID_INCORRECT);
    }

    @Test
    public void givenCorrectPath_WhenCallingProjectHistoriesXML_ShouldReturn200Status() {
        expect().statusCode(200)
                .body(Matchers.hasXPath("/projectHistoriesDtoes/projectHistoriesDto/id", Matchers.equalTo("1")))
                .body(Matchers.hasXPath("/projectHistoriesDtoes/projectHistoriesDto/version", Matchers.equalTo("1.0")))
                .body(Matchers.hasXPath("/projectHistoriesDtoes/projectHistoriesDto/startRevision", Matchers.equalTo("987")))
                .body(Matchers.hasXPath("/projectHistoriesDtoes/projectHistoriesDto/startRevisionDate", Matchers.startsWith("2013-01-01T00:00:00"))) //TODO timezone
                .body(Matchers.hasXPath("/projectHistoriesDtoes/projectHistoriesDto/endRevision", Matchers.equalTo("1234 12:00:00.123456789")))
                .body(Matchers.hasXPath("/projectHistoriesDtoes/projectHistoriesDto/endRevisionDate", Matchers.startsWith("2013-01-21T12:00:00")))
                .body(Matchers.hasXPath("/projectHistoriesDtoes/projectHistoriesDto/classes", Matchers.equalTo("1")))
                .body(Matchers.hasXPath("/projectHistoriesDtoes/projectHistoriesDto/issues", Matchers.equalTo("1")))
                .body(Matchers.hasXPath("/projectHistoriesDtoes/projectHistoriesDto/jobs", Matchers.equalTo("1")))
                .body(Matchers.hasXPath("/projectHistoriesDtoes/projectHistoriesDto/milestones", Matchers.equalTo("1")))
                .body(Matchers.hasXPath("/projectHistoriesDtoes/projectHistoriesDto/revisions", Matchers.equalTo("1")))


                .given().header("Accept", "text/xml").when().get(PROPER_URL + "/" + SAMPLE_ID_CORRECT);
    }

    @Test
    public void givenWrongPath_WhenCallingProjectHistoriesCSV_ShouldReturn404Error()
    {
        expect().statusCode(406).given().header("Accept", "text/csv").when().get(MISSPELLED_URL + "/" + SAMPLE_ID_INCORRECT);
    }

    @Test
    public void givenCorrectPath_WhenCallingProjectHistoriesCSV_ShouldReturn200Status() {
        Response resp = expect().statusCode(200).given().header("Accept", "text/csv").when().get(PROPER_URL + "/" + SAMPLE_ID_CORRECT);
        String body = resp.getBody().asString().trim();
        body = body.replaceAll("CET ", ""); //TODO timezone
        body = body.replaceAll("UTC ", "");
        assertEquals("\"ID\";\"Version\";\"StartRevision\";\"StartRevisionDate\";\"EndRevision\";\"EndRevisionDate\";\"Revisions\";\"Classes\";\"Issues\";\"Milestones\";\"Jobs\"\n\"1\";\"1.0\";\"987\";\"Tue Jan 01 00:00:00 2013\";\"1234 12:00:00.123456789\";\"Mon Jan 21 12:00:00 2013\";\"1\";\"1\";\"1\";\"1\";\"1\"", body);
    }
}