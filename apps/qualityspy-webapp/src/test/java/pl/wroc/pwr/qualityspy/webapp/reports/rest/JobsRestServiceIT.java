package pl.wroc.pwr.qualityspy.webapp.reports.rest;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.jayway.restassured.response.Response;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import static com.jayway.restassured.RestAssured.expect;
import static org.junit.Assert.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@RunWith(SpringRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class, TransactionalTestExecutionListener.class})
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.HSQL)
@DatabaseSetup("classpath:dbunit.xml")
public class JobsRestServiceIT {

    private static final String MISSPELLED_URL = "api/rest/job";
    private static final long SAMPLE_ID_CORRECT = 1;
    private static final String PROPER_URL = "api/jobs";
    private static final long SAMPLE_ID_INCORRECT = -1;

    @Test
    public void givenWrongPath_WhenCallingJobsXML_ShouldReturn404Error()
    {
        expect().statusCode(406).given().header("Accept", "text/xml").when().get(MISSPELLED_URL + "/" + SAMPLE_ID_INCORRECT);
    }

    @Test
    public void givenCorrectPath_WhenCallingJobsXML_ShouldReturn200Status() {
        expect().statusCode(200)
                .body(Matchers.hasXPath("/jobsDtoes/jobsDto/builds", Matchers.equalTo("1")))
                .body(Matchers.hasXPath("/jobsDtoes/jobsDto/color", Matchers.equalTo("COLOR")))
                .body(Matchers.hasXPath("/jobsDtoes/jobsDto/id", Matchers.equalTo("1")))
                .body(Matchers.hasXPath("/jobsDtoes/jobsDto/name", Matchers.equalTo("NAME")))
                .body(Matchers.hasXPath("/jobsDtoes/jobsDto/url", Matchers.equalTo("URL")))
                .given().header("Accept", "text/xml").when().get(PROPER_URL + "/" + SAMPLE_ID_CORRECT);
    }

    @Test
    public void givenWrongPath_WhenCallingJobsCSV_ShouldReturn404Error()
    {
        expect().statusCode(406).given().header("Accept", "text/csv").when().get(MISSPELLED_URL + "/" + SAMPLE_ID_INCORRECT);
    }

    @Test
    public void givenCorrectPath_WhenCallingJobsCSV_ShouldReturn200Status() {
        Response resp = expect().statusCode(200).given().header("Accept", "text/csv").when().get(PROPER_URL + "/" + SAMPLE_ID_CORRECT);
        String body = resp.getBody().asString().trim();
        assertEquals("\"id\";\"name\";\"url\";\"color\";\"builds\"\n" +
                "\"1\";\"NAME\";\"URL\";\"COLOR\";\"1\"", body);
    }
}