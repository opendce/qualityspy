package pl.wroc.pwr.qualityspy.webapp.reports.rest;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.jayway.restassured.response.Response;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import static com.jayway.restassured.RestAssured.expect;
import static org.junit.Assert.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@RunWith(SpringRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class, TransactionalTestExecutionListener.class})
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.HSQL)
@DatabaseSetup("classpath:dbunit.xml")
public class MilestonesRestServiceIT {

    private static final String MISSPELLED_URL = "api/milestone";
    private static final long SAMPLE_ID_CORRECT = 1;
    private static final String PROPER_URL = "api/milestones";
    private static final long SAMPLE_ID_INCORRECT = -1;

    @Test
    public void givenWrongPath_WhenCallingMilestonesXML_ShouldReturn404Error()
    {
        expect().statusCode(406).given().header("Accept", "text/xml").when().get(MISSPELLED_URL + "/" + SAMPLE_ID_INCORRECT);
    }

    @Test
    public void givenCorrectPath_WhenCallingMilestonesXML_ShouldReturn200Status() {
        expect().statusCode(200)
                .body(Matchers.hasXPath("/milestonesDtoes/milestonesDto/created", Matchers.startsWith("2013-01-01T00:00:00"))) //TODO timezone
                .body(Matchers.hasXPath("/milestonesDtoes/milestonesDto/id", Matchers.equalTo("1")))
                .body(Matchers.hasXPath("/milestonesDtoes/milestonesDto/creator", Matchers.equalTo("CREATOR")))
                .body(Matchers.hasXPath("/milestonesDtoes/milestonesDto/deadline", Matchers.startsWith("2013-01-01T00:00:00")))
                .body(Matchers.hasXPath("/milestonesDtoes/milestonesDto/description", Matchers.equalTo("DESCRIPTION")))
                .body(Matchers.hasXPath("/milestonesDtoes/milestonesDto/state", Matchers.equalTo("STATE")))
                .body(Matchers.hasXPath("/milestonesDtoes/milestonesDto/title", Matchers.equalTo("TITLE")))
                .given().header("Accept", "text/xml").when().get(PROPER_URL + "/" + SAMPLE_ID_CORRECT);
    }

    @Test
    public void givenWrongPath_WhenCallingMilestonesCSV_ShouldReturn404Error()
    {
        expect().statusCode(406).given().header("Accept", "text/csv").when().get(MISSPELLED_URL + "/" + SAMPLE_ID_INCORRECT);
    }

    @Test
    public void givenCorrectPath_WhenCallingMilestonesCSV_ShouldReturn200Status() {
        Response resp = expect().statusCode(200).given().header("Accept", "text/csv").when().get(PROPER_URL + "/" + SAMPLE_ID_CORRECT);
        String body = resp.getBody().asString().trim();
        body = body.replaceAll("CET ", ""); //TODO timezone
        body = body.replaceAll("UTC ", "");
        assertEquals("\"ID\";\"Title\";\"Creator\";\"Description\";\"State\";\"Created\";\"Deadline\"\n" +
                "\"1\";\"TITLE\";\"CREATOR\";\"DESCRIPTION\";\"STATE\";\"Tue Jan 01 00:00:00 2013\";\"Tue Jan 01 00:00:00 2013\"", body);
    }
}
