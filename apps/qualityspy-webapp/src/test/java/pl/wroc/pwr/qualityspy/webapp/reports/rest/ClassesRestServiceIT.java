package pl.wroc.pwr.qualityspy.webapp.reports.rest;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.jayway.restassured.response.Response;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import static com.jayway.restassured.RestAssured.expect;
import static org.junit.Assert.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@RunWith(SpringRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class, TransactionalTestExecutionListener.class})
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.HSQL)
@DatabaseSetup("classpath:dbunit.xml")
public class ClassesRestServiceIT {

    private static final String MISSPELLED_URL = "api/class";
    private static final long SAMPLE_ID_CORRECT = 1;
    private static final String PROPER_URL = "api/classes";
    private static final long SAMPLE_ID_INCORRECT = -1;

    @Test
    public void givenWrongPath_WhenCallingClassesXML_ShouldReturn404Error()
    {
        expect().statusCode(406).given().header("Accept", "text/xml").when().get(MISSPELLED_URL + "/" + SAMPLE_ID_INCORRECT);
    }

    @Test
    public void givenCorrectPath_WhenCallingClassesXML_ShouldReturn200Status() {
        expect().statusCode(200)
                .body(Matchers.hasXPath("/classesDtoes/classesDto/amc", Matchers.equalTo("1.0")))
                .body(Matchers.hasXPath("/classesDtoes/classesDto/averageCC", Matchers.equalTo("1.0")))
                .body(Matchers.hasXPath("/classesDtoes/classesDto/ca", Matchers.equalTo("1")))
                .body(Matchers.hasXPath("/classesDtoes/classesDto/cam", Matchers.equalTo("1.0")))
                .body(Matchers.hasXPath("/classesDtoes/classesDto/cbm", Matchers.equalTo("1")))
                .body(Matchers.hasXPath("/classesDtoes/classesDto/cbo", Matchers.equalTo("1")))
                .body(Matchers.hasXPath("/classesDtoes/classesDto/ce", Matchers.equalTo("1")))
                .body(Matchers.hasXPath("/classesDtoes/classesDto/dam", Matchers.equalTo("1.0")))
                .body(Matchers.hasXPath("/classesDtoes/classesDto/dit", Matchers.equalTo("1")))
                .body(Matchers.hasXPath("/classesDtoes/classesDto/ic", Matchers.equalTo("1")))
                .body(Matchers.hasXPath("/classesDtoes/classesDto/id", Matchers.equalTo("1")))
                .body(Matchers.hasXPath("/classesDtoes/classesDto/lcom", Matchers.equalTo("1")))
                .body(Matchers.hasXPath("/classesDtoes/classesDto/lcom3", Matchers.equalTo("1.0")))
                .body(Matchers.hasXPath("/classesDtoes/classesDto/loc", Matchers.equalTo("1")))
                .body(Matchers.hasXPath("/classesDtoes/classesDto/methods", Matchers.equalTo("1")))
                .body(Matchers.hasXPath("/classesDtoes/classesDto/maxCC", Matchers.equalTo("1")))
                .body(Matchers.hasXPath("/classesDtoes/classesDto/mfa", Matchers.equalTo("1.0")))
                .body(Matchers.hasXPath("/classesDtoes/classesDto/moa", Matchers.equalTo("1")))
                .body(Matchers.hasXPath("/classesDtoes/classesDto/name", Matchers.equalTo("NAME")))
                .body(Matchers.hasXPath("/classesDtoes/classesDto/noc", Matchers.equalTo("1")))
                .body(Matchers.hasXPath("/classesDtoes/classesDto/npm", Matchers.equalTo("1")))
                .body(Matchers.hasXPath("/classesDtoes/classesDto/rfc", Matchers.equalTo("1")))
                .body(Matchers.hasXPath("/classesDtoes/classesDto/wmc", Matchers.equalTo("1")))
                .given().header("Accept", "text/xml").when().get(PROPER_URL + "/" + SAMPLE_ID_CORRECT);
    }

    @Test
    public void givenWrongPath_WhenCallingClassesCSV_ShouldReturn404Error()
    {
        expect().statusCode(406).given().header("Accept", "text/csv").when().get(MISSPELLED_URL + "/" + SAMPLE_ID_INCORRECT);
    }

    @Test
    public void givenCorrectPath_WhenCallingClassesCSV_ShouldReturn200Status() {
        Response resp = expect().statusCode(200).given().header("Accept", "text/csv").when().get(PROPER_URL + "/" + SAMPLE_ID_CORRECT);
        String body = resp.getBody().asString().trim();
        assertEquals("\"ID\";\"Name\";\"amc\";\"ca\";\"cam\";\"cbm\";\"cbo\";\"ce\";\"dam\";\"dit\";\"ic\";\"lcom\";\"lcom3\";\"loc\";\"mfa\";\"moa\";\"noc\";\"npm\";\"rfc\";\"wmc\";\"averageCC\";\"maxCC\";\"methods\"\n" +
                "\"1\";\"NAME\";\"1.0\";\"1.0\";\"1.0\";\"1.0\";\"1.0\";\"1.0\";\"1.0\";\"1.0\";\"1.0\";\"1.0\";\"1.0\";\"1.0\";\"1.0\";\"1.0\";\"1.0\";\"1.0\";\"1.0\";\"1.0\";\"1.0\";\"1.0\";\"1\"", body);
    }
}