package pl.wroc.pwr.qualityspy.services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import pl.wroc.pwr.qualityspy.common.ConfigurationManager;
import pl.wroc.pwr.qualityspy.model.Project;
import pl.wroc.pwr.qualityspy.model.ProjectHistory;
import pl.wroc.pwr.qualityspy.repositories.ProjectRepository;

import java.io.IOException;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:persistance-test-context.xml"})
public class ProjectServiceIT {

    @Autowired
    ProjectService projectService;

    @Autowired
    ProjectRepository projectRepository;

    ConfigurationManager configurationManager = new ConfigurationManager("src/test/resources/");

    private static final String projectName = "TestProject";
    private static final String projectVersion = "testVersion";
    private static final String badVersion = "badVersion";

    @Test
    public void deleteVersion_Positive() throws IOException{
        configurationManager.addVersion(projectName, projectVersion);

        assertEquals(1, configurationManager.getListOfVersions(projectName).size());

        configurationManager.setSelectedProjAndVer(projectName,projectVersion);

        Project project = projectService.getOrCreateProject(configurationManager.getConfiguration());
        ProjectHistory  projectHistory = projectService.getOrCreateProjectHistory(configurationManager.getConfiguration(), project);

        assertEquals(projectVersion, projectRepository.findOneByProjectName(projectName).getCurrentVersion());
        assertNotNull(projectRepository.findByProjectNameAndProjectHistories_Version(projectName,projectVersion));

        projectService.deleteVersion(projectName, projectVersion, configurationManager);

        assertEquals(0, configurationManager.getListOfVersions(projectName).size());
        assertEquals("", projectRepository.findOneByProjectName(projectName).getAllVersions());
        assertNull(projectRepository.findByProjectNameAndProjectHistories_Version(projectName,projectVersion));
    }

    @Test
    public void deleteVersion_Negative() throws IOException {
        configurationManager.addVersion(projectName, projectVersion);

        assertEquals(1, configurationManager.getListOfVersions(projectName).size());

        configurationManager.setSelectedProjAndVer(projectName,projectVersion);

        Project project = projectService.getOrCreateProject(configurationManager.getConfiguration());
        ProjectHistory  projectHistory = projectService.getOrCreateProjectHistory(configurationManager.getConfiguration(), project);

        projectService.deleteVersion(projectName, badVersion, configurationManager);

        assertEquals(1, configurationManager.getListOfVersions(projectName).size());

        assertEquals(projectVersion, projectRepository.findOneByProjectName(projectName).getCurrentVersion());
        assertNotNull(projectRepository.findByProjectNameAndProjectHistories_Version(projectName,projectVersion));
    }
}