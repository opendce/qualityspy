package pl.wroc.pwr.qualityspy.persistence.utils;

import static org.junit.Assert.*;
import org.junit.Test;

public class UnitTestVectorComparer {
	
	@Test
	public void testCompare() {
		assertEquals(VersionComparer.compare("1.0", "1.1"), -1);
		assertEquals(VersionComparer.compare("1.0", "1.0"), 0);
		assertEquals(VersionComparer.compare("1.1", "1.0"), 1);
		assertEquals(VersionComparer.compare("1.0", "1.0.11"), -1);
		assertEquals(VersionComparer.compare("11.35.70.43", "11.35.70.43"), 0);
		assertEquals(VersionComparer.compare("1.0.24", "1.0"), 1);
		assertEquals(VersionComparer.compare("2.0", "1.0.35.43"), 1);
		assertEquals(VersionComparer.compare("1.0.7.5.3", "1.0.7.4.3"), 1);
		assertEquals(VersionComparer.compare("1.2-SNAPSHOT", "1.2"), 1);
		assertEquals(VersionComparer.compare("1.2", "1.2-SNAPSHOT"), -1);
		assertEquals(VersionComparer.compare("1.XXX.2", "1.XXX.2"), 0);
		assertEquals(VersionComparer.compare("1.XXX.2", "1.XXX.3"), -1);
		assertEquals(VersionComparer.compare("1.XXX.3", "1.XXX.2"), 1);
	}
}
