package pl.wroc.pwr.qualityspy.persistence;

import com.google.common.collect.Iterators;
import io.github.benas.randombeans.EnhancedRandomBuilder;
import io.github.benas.randombeans.FieldDefinitionBuilder;
import io.github.benas.randombeans.api.EnhancedRandom;
import io.github.benas.randombeans.api.Randomizer;
import io.github.benas.randombeans.randomizers.collection.SetRandomizer;
import org.easymock.EasyMock;
import org.hibernate.Hibernate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import pl.wroc.pwr.qualityspy.common.Properties;
import pl.wroc.pwr.qualityspy.model.*;
import pl.wroc.pwr.qualityspy.repositories.*;
import pl.wroc.pwr.qualityspy.services.ProjectService;
import java.util.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:persistance-test-context.xml"})
public class AcceptanceTestPersistence {

    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private ProjectService projectService;
    @Autowired
    private ProjectHistoryRepository projectHistoryRepository;
    @Autowired
    private IssueDescriptionRepository issueDescriptionRepository;

    private Project randomProject;

    @Before
    public void prepareObjects() {

        final EnhancedRandom enhancedRandom = EnhancedRandomBuilder.aNewEnhancedRandomBuilder()
                .overrideDefaultInitialization(true)
                .collectionSizeRange(1, 1)
                .build();
        randomProject = enhancedRandom.nextObject(Project.class);
    }

    @Test
    @Transactional
    public void testSaveGetProject() {

        //Zapisujemy przykladowy projekt w bazie

        projectRepository.save(randomProject);

        //Proba odczytu tego co wyslalismy
        Project p = projectRepository.findOneByProjectName(randomProject.getProjectName());

        //Sprawdzamy, czy pobrany projekt jest taki sam jak wyslany
        assertEquals(randomProject.getProjectName(), p.getProjectName());
        assertEquals(randomProject.getCurrentVersion(), p.getCurrentVersion());
        assertEquals(randomProject.getAllVersions(), p.getAllVersions());
        assertEquals(randomProject.toCSV(), p.toCSV());
        //assertEquals(project.getId(), p.getId());
        //assertEquals(p, project);
    }

    @Test
    @Transactional
    public void testDeleteProject() {
        //Sprawdzamy, ile obiektow mamy w bazie przed zapisaniem dodatkowego projektu
        Iterator<Project> iterator = projectRepository.findAll().iterator();
        int numberOfObjects0 = Iterators.size(iterator);

        //Zapisujemy przykladowy projekt w bazie
        projectRepository.save(randomProject);
        Project p = projectRepository.findOneByProjectName(randomProject.getProjectName());

        //Sprawdzamy liczbe obiektow po wyslaniu
        Iterator<Project> iterator1 = projectRepository.findAll().iterator();
        int numberOfObjects2 = Iterators.size(iterator1);

        //Proba usuniecia tego co wyslalismy
        projectRepository.delete(p.getId());
        // W razie niepowodzenia, powinien byc wyrzucony wyjatek

        //Sprawdzamy ile obiektow po usunieciu
        Iterator<Project> iterator2 = projectRepository.findAll().iterator();
        int numberOfObjects1 = Iterators.size(iterator2);

        //Sprawdzamy czy liczba obiektow sie zgadza
        assertTrue(numberOfObjects0 == numberOfObjects1 && numberOfObjects2 > numberOfObjects0);
    }

    @Test
    @Transactional
    public void testGetProjects() {
        boolean done = false;

        //Zapisujemy przykladowy projekt w bazie
        projectRepository.save(randomProject);
        Project project = projectRepository.findOneByProjectName(randomProject.getProjectName());

        //Proba odczytu tego co wyslalismy
        Iterator<Project> iterator = projectRepository.findAll().iterator();
        //list = persistence.getProjects();

        //Sprawdzamy, czy ktorykolwiek projekt z pobranej listy
        //Jest taki sam co wyslalismy
        while(iterator.hasNext()){
            Project p = iterator.next();
            if(project.equals(p)){
                done = true;
                break;
            }
        }

        //Sprawdzamy poprawnosc odebranych danych
        assertTrue(done);
    }

    @Test
    @Transactional
    public void shouldRemoveExistingProjectTest() {
        // given
        projectRepository.save(randomProject);
        final Project project = projectRepository.findOneByProjectName(randomProject.getProjectName());
        final ProjectHistory projectHistory = project.getProjectHistories().iterator().next();
        final IssueDescription issueDescription = projectHistory.getIssues().iterator().next();

        // when
        projectService.deleteProject(project.getProjectName());

        // then
        assertNull(projectRepository.findOne(project.getId()));
        assertNull(projectHistoryRepository.findOne(projectHistory.getId()));
        assertNull(issueDescriptionRepository.findOne(issueDescription.getId()));
    }

    @Test
    @Transactional
    public void shouldNotRemoveNotExistingProjectTest() {
        // given
        final String projectName = randomProject.getProjectName();
        projectRepository.save(randomProject);
        final int projectsCount = (int)projectRepository.count();

        // when
        projectService.deleteProject(projectName + "???");

        // then
        assertEquals(projectsCount, projectRepository.count());
    }

    @Test
    @Transactional
    public void should_create_project_history_with_not_null_id() {
        // given
        final EnhancedRandom enhancedRandom = EnhancedRandomBuilder.aNewEnhancedRandomBuilder()
                .build();
        final Project randomProject = enhancedRandom.nextObject(Project.class);
        projectRepository.save(randomProject);

        final Properties config = EasyMock.createNiceMock(Properties.class);
        final String versionName = "some_version";
        EasyMock.expect(config.getVersion())
                .andReturn(versionName)
                .anyTimes();
        EasyMock.replay(config);

        // when
        final ProjectHistory projectHistory = projectService.getOrCreateProjectHistory(config, randomProject);

        // then
        assertNotNull(projectHistory.getId());
        assertThat(randomProject.getProjectHistories())
                .anyMatch(ph -> Objects.equals(ph.getId(), projectHistory.getId()));
    }
}


