package pl.wroc.pwr.qualityspy.persistence.utils;

import io.github.benas.randombeans.EnhancedRandomBuilder;
import io.github.benas.randombeans.api.EnhancedRandom;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import pl.wroc.pwr.qualityspy.model.Project;
import pl.wroc.pwr.qualityspy.model.ProjectHistory;
import pl.wroc.pwr.qualityspy.repositories.ProjectRepository;

import java.util.Optional;

import static org.junit.Assert.assertEquals;

/**
 * Created by Verloren on 01.05.2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:persistance-test-context.xml"})
public class ProjectExtensionsTests {

    @Autowired
    private ProjectRepository projectRepository;

    private Project randomProject;

    @Before
    public void prepareObjects() {
        final EnhancedRandom enhancedRandom = EnhancedRandomBuilder.aNewEnhancedRandomBuilder()
                .overrideDefaultInitialization(true)
                .collectionSizeRange(1, 1)
                .build();
        randomProject = enhancedRandom.nextObject(Project.class);
    }

    @Test
    public void findProjectHistoryInProject_returns_OptionalEmpty_When_ProjectIsNull() {

        Project project = null;
        ProjectExtensions proExt = new ProjectExtensions();
        String version = "2.0";

        Optional<ProjectHistory> result = proExt.findProjectHistoryInProject(project, version);

        assertEquals(result, Optional.empty());
    }

    @Test
    @Transactional
    public void findProjectHistoryInProject_returns_OptionalOf_When_ProjectIsCorrect() {

        ProjectExtensions projExt = new ProjectExtensions();
        projectRepository.save(randomProject);
        final Project project = projectRepository.findOneByProjectName(randomProject.getProjectName());
        final ProjectHistory tempProjHist = project.getProjectHistories().iterator().next();

        Optional<ProjectHistory> result = projExt.findProjectHistoryInProject(project, tempProjHist.getVersion());

        assertEquals(Optional.of(tempProjHist), result);
    }
}