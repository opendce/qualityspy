package pl.wroc.pwr.qualityspy.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.wroc.pwr.qualityspy.model.metric_definition.MetricDefinition;

@Repository
public interface MetricDefinitionRepository extends CrudRepository<MetricDefinition, Long> {

    MetricDefinition findOneByMetricName(String metricName);
    MetricDefinition findOneBySymbol(String symbol);
}
