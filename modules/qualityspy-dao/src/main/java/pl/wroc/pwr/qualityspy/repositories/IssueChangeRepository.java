package pl.wroc.pwr.qualityspy.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.wroc.pwr.qualityspy.model.IssueChange;

import java.util.List;

@Repository
public interface IssueChangeRepository extends CrudRepository<IssueChange, Long> {

    List<IssueChange> findAllById(long id);
}
