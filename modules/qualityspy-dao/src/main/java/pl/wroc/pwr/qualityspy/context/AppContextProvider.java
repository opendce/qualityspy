package pl.wroc.pwr.qualityspy.context;

import javax.swing.JOptionPane;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Klasa zwracająca kontekst Spring aplikacji (singleton) oraz obiekty serwisów (z beanów)
 * @author Tobiasz
 */
public class AppContextProvider {

    private static AppContextProvider instance;
    private ClassPathXmlApplicationContext context;
    
    
    // Prywatny konstruktor
    private AppContextProvider() {
        try {
            context = new ClassPathXmlApplicationContext("classpath:persistance-context.xml");
        } catch (BeanCreationException ex) {
            JOptionPane.showMessageDialog(null, "Check database connection settings!", "Error!", JOptionPane.ERROR_MESSAGE);
        } catch (BeanInitializationException ex) {
            JOptionPane.showMessageDialog(null, "Can't find specific properties file.", "Error!", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public static AppContextProvider getContext() {
      if(instance == null) {
         instance = new AppContextProvider();
      }
      
      return instance;
    }
    
    public <T> T getBean(Class<T> requiredType) {
        return context.getBean(requiredType);
    }
}
