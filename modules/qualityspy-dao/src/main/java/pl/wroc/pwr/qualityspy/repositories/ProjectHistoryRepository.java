package pl.wroc.pwr.qualityspy.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.wroc.pwr.qualityspy.model.ProjectHistory;

import java.util.List;

@Repository
public interface ProjectHistoryRepository extends CrudRepository<ProjectHistory, Long> {

    List<ProjectHistory> findAllById(long projectId);
}
