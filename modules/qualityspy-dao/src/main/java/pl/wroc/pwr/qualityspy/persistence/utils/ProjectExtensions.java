package pl.wroc.pwr.qualityspy.persistence.utils;

import org.springframework.stereotype.Component;
import pl.wroc.pwr.qualityspy.model.Project;
import pl.wroc.pwr.qualityspy.model.ProjectHistory;

import java.util.Optional;

/**
 * Created by Verloren on 01.05.2017.
 */

@Component
public class ProjectExtensions {
    public Optional<ProjectHistory> findProjectHistoryInProject(Project project, String version) {
        if (project == null) {
            return Optional.empty();
        } else {
            for (ProjectHistory projectHistory : project.getProjectHistories()) {
                if (projectHistory.getVersion().equals(version)) {
                    return Optional.of(projectHistory);
                }
            }

            return Optional.empty();
        }
    }
}
