package pl.wroc.pwr.qualityspy.persistence.utils;

public class VersionComparer {
	public static int compare(String version1, String version2)
	{
		String[] v1Numbers = version1.split("\\.");
		String[] v2Numbers = version2.split("\\.");
		
		if ( v1Numbers.length > v2Numbers.length )
		{
			for ( int i = 0; i < v2Numbers.length; i++ )
			{
				int i1 = 0;
				int i2 = 0;
				
				try {
					i1 = new Integer (v1Numbers[i]);
					i2 = new Integer (v2Numbers[i]);
				} catch (NumberFormatException e) {
					int res = charArrayComparer(v1Numbers[i], v2Numbers[i]);
					if ( res == 0 )
						continue;
					else
						return res;
				}
				
				if ( i1 == i2 )
					continue;
				else if ( i1 > i2 )
				{
					return 1;
				}
				else
				{
					return -1;
				}
			}
			return 1;
		}
		else if ( v1Numbers.length < v2Numbers.length )
		{
			for ( int i = 0; i < v1Numbers.length; i++ )
			{
				int i1 = 0;
				int i2 = 0;
				try {
					i1 = new Integer (v1Numbers[i]);
					i2 = new Integer (v2Numbers[i]);
				} catch (NumberFormatException e) {
					int res = charArrayComparer(v1Numbers[i], v2Numbers[i]);
					if ( res == 0 )
						continue;
					else
						return res;
				}
				
				if ( i1 == i2 )
					continue;
				else if ( i1 > i2 )
				{
					return 1;
				}
				else
				{
					return -1;
				}
			}
			return -1;
		}
		else
		{
			for( int i = 0; i < v1Numbers.length; i++ )
			{
				int i1 = 0;
				int i2 = 0;
				
				try {
					i1 = new Integer (v1Numbers[i]);
					i2 = new Integer (v2Numbers[i]);
				} catch (NumberFormatException e) {
					int res = charArrayComparer(v1Numbers[i], v2Numbers[i]);
					if ( res == 0 )
						continue;
					else
						return res;
				}
				
				if ( i1 == i2 )
					continue;
				else if ( i1 > i2 )
				{
					return 1;
				}
				else
				{
					return -1;
				}
			}
			return 0;
		}
	}
	
	private static int charArrayComparer(String s1, String s2)
	{
		if ( s1.equals(s2) )
			return 0;
		
		char[] c1 = s1.toCharArray();
		char[] c2 = s2.toCharArray();
		
		if (c1.length >= c2.length)
		{
			for(int i = 0; i < c2.length; i++)
			{
				if ( c1[i] == c2[i] )
					continue;
				if ( Character.isDigit( c1[i]) )
				{
					if ( Character.isDigit( c2[i]) )
					//2 cyfry
					{
						if ( c1[i] > c2[i] )
							return 1;
						else if (c1[i] < c2[i])
							return -1;
					}
					else
					//c1 cyfra c2 litera
					{
						return 1;
					}
				}
				else
				{
					if ( Character.isDigit( c2[i]) )
						//c1 litera c2 cyfra
						return -1;
					else
						//Obie nie sa cyframi
						return 0;
				}
			}
			if (!(c1.length == c2.length) )
				return 1; 
		}
		else	//c2.length > c1.length
		{
			for(int i = 0; i < c1.length; i++)
			{
				if ( c1[i] == c2[i] )
					continue;
				if ( Character.isDigit( c1[i]) )
				{
					if ( Character.isDigit( c2[i]) )
					//2 cyfry
					{
						if ( c1[i] > c2[i] )
							return 1;
						else if (c1[i] < c2[i])
							return -1;
					}
					else
					//c1 cyfra c2 litera
					{
						return 1;
					}
				}
				else
				{
					if ( Character.isDigit( c2[i]) )
						//c1 litera c2 cyfra
						return -1;
					else
						//Obie nie sa cyframi
						return 0;
				}
			}
			
			return -1;
		}
		
		return 0;
	}
}
