package pl.wroc.pwr.qualityspy.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.wroc.pwr.qualityspy.model.File;

import java.util.List;

@Repository
public interface FileRepository extends CrudRepository<File, Long> {

    List<File> findAllById(long id);
}
