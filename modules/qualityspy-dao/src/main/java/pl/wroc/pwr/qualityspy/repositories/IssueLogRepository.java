package pl.wroc.pwr.qualityspy.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.wroc.pwr.qualityspy.model.IssueLog;

import java.util.List;

@Repository
public interface IssueLogRepository extends CrudRepository<IssueLog, Long> {

    List<IssueLog> findAllById(long id);
}
