package pl.wroc.pwr.qualityspy.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.wroc.pwr.qualityspy.model.Revision;

import java.util.List;

@Repository
public interface RevisionRepository extends CrudRepository<Revision, Long> {

    List<Revision> findAllById(long id);
}
