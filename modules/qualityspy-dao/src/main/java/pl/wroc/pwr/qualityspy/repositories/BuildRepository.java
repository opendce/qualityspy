package pl.wroc.pwr.qualityspy.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.wroc.pwr.qualityspy.model.Build;

import java.util.List;

@Repository
public interface BuildRepository extends CrudRepository<Build, Long> {

    List<Build> findAllById(long id);
}
