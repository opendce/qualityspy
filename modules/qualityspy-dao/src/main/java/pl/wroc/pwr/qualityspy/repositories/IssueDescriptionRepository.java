package pl.wroc.pwr.qualityspy.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.wroc.pwr.qualityspy.model.IssueDescription;

import java.util.List;

@Repository
public interface IssueDescriptionRepository extends CrudRepository<IssueDescription, Long> {

    List<IssueDescription> findAllById(long id);
}
