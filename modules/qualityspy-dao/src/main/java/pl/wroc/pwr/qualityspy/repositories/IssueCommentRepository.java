package pl.wroc.pwr.qualityspy.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.wroc.pwr.qualityspy.model.IssueComment;

import java.util.List;

@Repository
public interface IssueCommentRepository extends CrudRepository<IssueComment, Long> {

    List<IssueComment> findAllById(long id);
}
