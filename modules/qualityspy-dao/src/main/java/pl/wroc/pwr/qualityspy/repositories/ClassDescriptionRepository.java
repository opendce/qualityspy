package pl.wroc.pwr.qualityspy.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.wroc.pwr.qualityspy.model.ClassDescription;

import java.util.List;

@Repository
public interface ClassDescriptionRepository extends CrudRepository<ClassDescription, Long> {

    List<ClassDescription> findAllById(long id);
}
