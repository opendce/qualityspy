package pl.wroc.pwr.qualityspy.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.wroc.pwr.qualityspy.model.*;
import pl.wroc.pwr.qualityspy.repositories.ClassDescriptionRepository;
import pl.wroc.pwr.qualityspy.repositories.IssueDescriptionRepository;

import java.util.Set;

@Service
public class IssueDescriptionServiceImpl implements IssueDescriptionService {

    @Autowired
    IssueDescriptionRepository issueDescriptionRepository;

    @Override
    public Set<IssueLog> getIssueLogs(IssueDescription issue) {
        issue = issueDescriptionRepository.save(issue);
        return issue.getIssueLogs();
    }

    @Override
    public Set<IssueComment> getIssueComments(IssueDescription issue) {
        issue = issueDescriptionRepository.save(issue);
        return issue.getIssueComments();
    }

    @Override
    public Set<IssueChange> getIssueHistory(IssueDescription issue) {
        issue = issueDescriptionRepository.save(issue);
        return issue.getIssueHistory();
    }
}