package pl.wroc.pwr.qualityspy.services;

import org.springframework.stereotype.Service;
import pl.wroc.pwr.qualityspy.model.*;

import java.util.Set;

@Service
public interface IssueDescriptionService {

    Set<IssueLog> getIssueLogs(IssueDescription issue);
    Set<IssueComment> getIssueComments(IssueDescription issue);
    Set<IssueChange> getIssueHistory(IssueDescription issue);
}
