package pl.wroc.pwr.qualityspy.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.wroc.pwr.qualityspy.common.ConfigurationManager;
import pl.wroc.pwr.qualityspy.common.Properties;
import pl.wroc.pwr.qualityspy.model.Project;
import pl.wroc.pwr.qualityspy.model.ProjectHistory;
import pl.wroc.pwr.qualityspy.persistence.utils.ProjectExtensions;
import pl.wroc.pwr.qualityspy.repositories.ProjectHistoryRepository;
import pl.wroc.pwr.qualityspy.repositories.ProjectRepository;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
public class ProjectServiceImpl implements ProjectService {

    @Autowired
    ProjectExtensions projExt;

    @Autowired
    ProjectHistoryRepository projectHistoryRepository;

    @Autowired
    ProjectRepository projectRepository;

    @Override
    public Project getProject(String name) {
        return projectRepository.findOneByProjectName(name);
    }

    @Override
    public Project getOrCreateProject(Properties config) {
        Project project = getProject(config.getProject());
        if (project == null) {
            project = new Project();
            project.setProjectName(config.getProject());
            project.setCurrentVersion(config.getVersion());
            project = projectRepository.save(project);
        }
        return project;
    }

    @Override
    @Transactional
    public ProjectHistory getOrCreateProjectHistory(Properties config, Project project) {
        return projExt.findProjectHistoryInProject(project, config.getVersion())
                .orElseGet(() -> {
                    ProjectHistory newProjectHistory = new ProjectHistory();
                    newProjectHistory.setVersion(config.getVersion());
                    newProjectHistory = projectHistoryRepository.save(newProjectHistory);
                    project.getProjectHistories().add(newProjectHistory);
                    projectRepository.save(project);
                    return newProjectHistory;
                });
    }

    @Override
    public ProjectHistory getProjectHistory(String projectName, String version) {
        final Project project = projectRepository.findByProjectNameAndProjectHistories_Version(projectName, version);
        final Optional<ProjectHistory> projectHistory = projExt.findProjectHistoryInProject(project, version);
        if (projectHistory.isPresent()) {
            return projectHistory.get();
        }
        return null;
    }

    @Override
    public void deleteVersion(String projectName, String version, ConfigurationManager configurationManager) {
        ProjectHistory projectHistory = getProjectHistory(projectName, version);
        if(projectHistory!=null) {
            getProject(projectName).getProjectHistories().remove(projectHistory);
            projectHistoryRepository.delete(projectHistory);
        }
        configurationManager.removeConfiguration(projectName, version);
    }

    @Override
    public void addNewVersion(ProjectHistory newVersion, ProjectHistory version, Project project) {
        version = projectHistoryRepository.save(version);
        project = projectRepository.save(project);

        project.getProjectHistories().remove(version);
        project.getProjectHistories().add(newVersion);
    }


    @Override
    public List<Project> getAllProjects() {
        LinkedList<Project> result = new LinkedList<>();
        for (Project history : projectRepository.findAll()) {
            result.push(history);
        }
        return result;
    }

    @Override
    @Transactional
    public void deleteProject(final String projectName) {
        final Project project = projectRepository.findOneByProjectName(projectName);
        if (project != null) {
            projectRepository.delete(project);
        }
    }
}
