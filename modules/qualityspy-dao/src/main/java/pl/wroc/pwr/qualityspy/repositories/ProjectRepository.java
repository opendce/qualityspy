package pl.wroc.pwr.qualityspy.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.wroc.pwr.qualityspy.model.Project;


@Repository
public interface ProjectRepository extends CrudRepository<Project, Long> {

    Project findByProjectNameAndProjectHistories_Version(String projectName, String version);
    Project findOneByProjectName(String projectName);
}
