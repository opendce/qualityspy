package pl.wroc.pwr.qualityspy.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;
import pl.wroc.pwr.qualityspy.model.*;
import pl.wroc.pwr.qualityspy.repositories.ProjectHistoryRepository;
import pl.wroc.pwr.qualityspy.repositories.ProjectRepository;

import java.util.List;
import java.util.Set;

@Transactional
@Service
public class ProjectHistoryServiceImpl implements ProjectHistoryService {

    @Autowired
    ProjectHistoryRepository projectHistoryRepository;

    @Autowired
    private ProjectRepository projectRepository;

    @Override
    public Set<Job> getJobs(ProjectHistory history) {
        history = projectHistoryRepository.save(history);
        return history.getJobs();
    }

    @Override
    public Set<Revision> getRevisions(ProjectHistory history) {
        history = projectHistoryRepository.save(history);
        return history.getRevisions();
    }

    @Override
    public Set<MilestoneDescription> getMilestones(ProjectHistory history) {
        history = projectHistoryRepository.save(history);
        return history.getMilestones();
    }

    @Override
    public Set<ClassDescription> getClasses(ProjectHistory history) {
        history = projectHistoryRepository.save(history);
        return history.getClasses();
    }

    @Override
    @Transactional
    public void addAllIssuesAndMilestones(ProjectHistory detachedProjectHistory, List<IssueDescription> issues, List<MilestoneDescription> milestones) {
        final ProjectHistory projectHistory = projectHistoryRepository.findOne(detachedProjectHistory.getId());
        projectHistory.getIssues().clear();
        projectHistory.getIssues().addAll(issues);

        projectHistory.getMilestones().clear();
        projectHistory.getMilestones().addAll(milestones);
    }

    @Override
    public void addAllRevisions(ProjectHistory newVersion, ProjectHistory version) {

        if (version.getRevisions() != null) {
            version.getRevisions().clear();
            version.getRevisions().addAll(newVersion.getRevisions());
        } else {
            version.setRevisions(newVersion.getRevisions());
        }
        version.setEndRevision(newVersion.getEndRevision());
        version.setEndRevisionDate(newVersion.getEndRevisionDate());
        version.setStartRevision(newVersion.getStartRevision());
        version.setStartRevisionDate(newVersion.getStartRevisionDate());
        
        projectHistoryRepository.save(version);

    }

    public void addAllRevisions(ProjectHistory newVersion, ProjectHistory version, Project project) {

        if (version.getRevisions() != null) {
            version.getRevisions().clear();
            version.getRevisions().addAll(newVersion.getRevisions());
        } else {
            version.setRevisions(newVersion.getRevisions());
        }
        version.setEndRevision(newVersion.getEndRevision());
        version.setEndRevisionDate(newVersion.getEndRevisionDate());
        version.setStartRevision(newVersion.getStartRevision());
        version.setStartRevisionDate(newVersion.getStartRevisionDate());
        project.getProjectHistories().add(version);
        projectRepository.save(project);
       // projectHistory = projectHistoryRepository.save(projectHistory);

    }
    
    @Override
    public void persistClasses(List<ClassDescription> classes, ProjectHistory version) {
        version = projectHistoryRepository.save(version);

        version.getClasses().clear();
        version.getClasses().addAll(classes);
    }

    @Override
    public void deleteVersion(String projectName, String versionName) {
        Project project = projectRepository.findOneByProjectName(projectName);
        for ( ProjectHistory ph: project.getProjectHistories()) {
            if (ph.getVersion().equals(versionName)) {
                projectHistoryRepository.delete(ph);
            }
        }
    }

    @Override
    public void addAllJobs(List<Job> jobs, ProjectHistory version) {
        version = projectHistoryRepository.save(version);

        version.getJobs().clear();
        version.getJobs().addAll(jobs);
    }

    @Override
    public void addJob(Job job, ProjectHistory version) {
        version = projectHistoryRepository.save(version);
        version.getJobs().add(job);
    }
}