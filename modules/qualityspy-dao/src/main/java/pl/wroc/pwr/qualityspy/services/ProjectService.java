package pl.wroc.pwr.qualityspy.services;

import org.springframework.stereotype.Service;
import pl.wroc.pwr.qualityspy.common.ConfigurationManager;
import pl.wroc.pwr.qualityspy.common.Properties;
import pl.wroc.pwr.qualityspy.model.Project;
import pl.wroc.pwr.qualityspy.model.ProjectHistory;

import java.util.List;

public interface ProjectService {
    /**
     * When the project does not exist in the database a new one is created and persisted.
     */
    Project getOrCreateProject(Properties config);

    Project getProject(String name);

    /**
     * When the project history does not exist in the database a new one is created and persisted.
     */
    ProjectHistory getOrCreateProjectHistory(Properties config, Project project);

    /**
     * When the project history does not exist, null is returned.
     */
    ProjectHistory getProjectHistory(String projectName, String version);

    void deleteVersion(String projectName, String version, ConfigurationManager configurationManager);

    void addNewVersion(ProjectHistory newVersion, ProjectHistory version, Project project);

    List<Project> getAllProjects();


    void deleteProject(final String projectName);
}
