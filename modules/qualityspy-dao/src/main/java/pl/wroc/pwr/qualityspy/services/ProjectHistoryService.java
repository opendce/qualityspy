package pl.wroc.pwr.qualityspy.services;

import org.springframework.stereotype.Service;
import pl.wroc.pwr.qualityspy.model.*;

import java.util.Set;
import java.util.List;

@Service
public interface ProjectHistoryService {

    Set<Job> getJobs(ProjectHistory history);
    Set<Revision> getRevisions(ProjectHistory history);
    Set<MilestoneDescription> getMilestones(ProjectHistory history);
    Set<ClassDescription> getClasses(ProjectHistory history);
    void addAllIssuesAndMilestones(ProjectHistory version, List<IssueDescription> issues, List<MilestoneDescription> milestones);
    void addAllRevisions(ProjectHistory newVersion, ProjectHistory version);
    void addAllRevisions(ProjectHistory newVersion, ProjectHistory version, Project project);
    void persistClasses(List<ClassDescription> classes, ProjectHistory version);
    void deleteVersion(String projectName, String versionName);
    void addAllJobs(List<Job> jobs, ProjectHistory version);
    void addJob(Job job, ProjectHistory version);
}
