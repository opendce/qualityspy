package pl.wroc.pwr.qualityspy.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.wroc.pwr.qualityspy.model.MethodDescription;

import java.util.List;

@Repository
public interface MethodDescriptionRepository extends CrudRepository<MethodDescription, Long> {

    List<MethodDescription> findAllById(long id);
}
