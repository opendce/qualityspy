package pl.wroc.pwr.qualityspy.model;

import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.commons.collections.CollectionUtils;

@XmlRootElement
@Entity
@Table(name = "CLASS_DESCRIPTIONS")
public class ClassDescription implements IEntityWithId {

    @XmlTransient
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;
    @Column(name = "NAME", length = 900)
    private String name;
    private double amc;
    private int ca;
    private double cam;
    private int cbm;
    private int cbo;
    private int ce;
    private double dam;
    private int dit;
    private int ic;
    private int lcom;
    private double lcom3;
    private int loc;
    private double mfa;
    private int moa;
    private int noc;
    private int npm;
    private int rfc;
    private int wmc;
    private double averageCC;
    private int maxCC;
    @OneToMany(targetEntity = MethodDescription.class, cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    @JoinColumn(name = "CLASS_DESCRIPTIONS_ID", nullable = false)
    Set<MethodDescription> methods = new HashSet<MethodDescription>();

    @Override
    public int hashCode() {
        if (id == null) {
            return new Long(0).hashCode();
        }

        return id.hashCode();
    }

    @Override
    public boolean equals(Object anotherObject) {
        if (this == anotherObject) {
            return true;
        }

        if (!(anotherObject instanceof ClassDescription)) {
            return false;
        }

        ClassDescription anotherCD = (ClassDescription) anotherObject;

        if (this.getName() == anotherCD.getName()
                && this.getAmc() == anotherCD.getAmc()
                && this.getCa() == anotherCD.getCa()
                && this.getCam() == anotherCD.getCam()
                && this.getCbm() == anotherCD.getCbm()
                && this.getCbo() == anotherCD.getCbo()
                && this.getCe() == anotherCD.getCe()
                && this.getDam() == anotherCD.getDam()
                && this.getDit() == anotherCD.getDit()
                && this.getIc() == anotherCD.getIc()
                && this.getLcom() == anotherCD.getLcom()
                && this.getLcom3() == anotherCD.getLcom3()
                && this.getLoc() == anotherCD.getLoc()
                && this.getMfa() == anotherCD.getMfa()
                && this.getMoa() == anotherCD.getMoa()
                && this.getNoc() == anotherCD.getNoc()
                && this.getNpm() == anotherCD.getNpm()
                && this.getRfc() == anotherCD.getRfc()
                && this.getWmc() == anotherCD.getWmc()
                && this.getAverageCC() == anotherCD.getAverageCC()
                && this.getMaxCC() == anotherCD.getMaxCC()) {
            return CollectionUtils.isEqualCollection(this.methods, anotherCD.methods);
        } else {
            return false;
        }
    }

    public Set<MethodDescription> getMethods() {
        return methods;
    }

    public String getName() {
        return name;
    }

    public double getAmc() {
        return amc;
    }

    public int getCa() {
        return ca;
    }

    public double getCam() {
        return cam;
    }

    public int getCbm() {
        return cbm;
    }

    public int getCbo() {
        return cbo;
    }

    public int getCe() {
        return ce;
    }

    public double getDam() {
        return dam;
    }

    public int getDit() {
        return dit;
    }

    public int getIc() {
        return ic;
    }

    public int getLcom() {
        return lcom;
    }

    public double getLcom3() {
        return lcom3;
    }

    public int getLoc() {
        return loc;
    }

    public double getMfa() {
        return mfa;
    }

    public int getMoa() {
        return moa;
    }

    public int getNoc() {
        return noc;
    }

    public int getNpm() {
        return npm;
    }

    public int getRfc() {
        return rfc;
    }

    public int getWmc() {
        return wmc;
    }

    public double getAverageCC() {
        return averageCC;
    }

    public int getMaxCC() {
        return maxCC;
    }

    public void setName(String aName) {
        name = aName;
    }

    public void setAmc(double aAmc) {
        if (!Double.isNaN(aAmc)) {
            amc = aAmc;
        }
    }

    public void setCa(int aCa) {
        ca = aCa;
    }

    public void setCam(double aCam) {
        if (!Double.isNaN(aCam)) {
            cam = aCam;
        }
    }

    public void setCbm(int aCbm) {
        cbm = aCbm;
    }

    public void setCbo(int aCbo) {
        cbo = aCbo;
    }

    public void setCe(int aCe) {
        ce = aCe;
    }

    public void setDam(double aDam) {
        if (!Double.isNaN(aDam)) {
            dam = aDam;
        }
    }

    public void setDit(int aDit) {
        dit = aDit;
    }

    public void setIc(int aIc) {
        ic = aIc;
    }

    public void setLcom(int aLcom) {
        lcom = aLcom;
    }

    public void setLcom3(double aLcom3) {
        if (!Double.isNaN(aLcom3)) {
            lcom3 = aLcom3;
        }
    }

    public void setLoc(int aLoc) {
        loc = aLoc;
    }

    public void setMfa(double aMfa) {
        if (!Double.isNaN(aMfa)) {
            mfa = aMfa;
        }
    }

    public void setMoa(int aMoa) {
        moa = aMoa;
    }

    public void setNoc(int aNoc) {
        noc = aNoc;
    }

    public void setNpm(int aNpm) {
        npm = aNpm;
    }

    public void setRfc(int aRfc) {
        rfc = aRfc;
    }

    public void setWmc(int aWmc) {
        wmc = aWmc;
    }

    public void setAverageCC(double aAverageCC) {
        if (!Double.isNaN(aAverageCC)) {
            averageCC = aAverageCC;
        }
    }

    public void setMaxCC(int aMaxCC) {
        maxCC = aMaxCC;
    }

    public void setMethod(MethodDescription method) {
        methods.add(method);
    }

    public Long getId() {
        return id;
    }

    public void setMethods(Set<MethodDescription> methods) {
        this.methods = methods;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String toCSV() {
        return name + ";" + amc + ";" + averageCC + ";" + ca + ";" + cam + ";"
                + cbm + ";" + cbo + ";" + ce + ";" + dam + ";" + dit + ";"
                + ic + ";" + lcom + ";" + lcom3 + ";" + loc + ";" + maxCC + ";"
                + mfa + ";" + moa + ";" + noc + ";" + npm + ";" + rfc + ";" + wmc;
    }
    
    public ArrayList<String> toStringArray() {
        ArrayList<String> values = new ArrayList<String>();
        values.add(name);
        values.add(Double.toString(amc));
        values.add(Double.toString(averageCC));
        values.add(Integer.toString(ca));
        values.add(Double.toString(cam));
        values.add(Integer.toString(cbm));
        values.add(Integer.toString(cbo));
        values.add(Integer.toString(ce));
        values.add(Double.toString(dam));
        values.add(Integer.toString(dit));
        values.add(Integer.toString(ic));
        values.add(Integer.toString(lcom));
        values.add(Double.toString(lcom3));
        values.add(Integer.toString(loc));
        values.add(Integer.toString(maxCC));
        values.add(Double.toString(mfa));
        values.add(Integer.toString(moa));
        values.add(Integer.toString(noc));
        values.add(Integer.toString(npm));
        values.add(Integer.toString(rfc));
        values.add(Integer.toString(wmc));
        return values;
    }
}
