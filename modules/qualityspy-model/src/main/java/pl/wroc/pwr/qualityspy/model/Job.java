package pl.wroc.pwr.qualityspy.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author JacekBurzynski
 */
@Entity
@Table(name = "JOBS")
public class Job implements Serializable, IEntityWithId {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;
    private String name = null;
    private String url = null;
    private String color = null;
    
    @OneToMany(targetEntity = Build.class, cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    @JoinColumn(name = "JOBS_ID", nullable = false)
    private List<Build> builds = new LinkedList<Build>();
    
    /*@OneToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name="PROJECT_HISTORIES_ID", nullable = false)
    private ProjectHistory projectHistory;*/

    public Job() {
        super();
    }

    public Job(String name, String url, String color) {
        this.name = name;
        this.url = url;
        this.color = color;
    }

    @Override
    public int hashCode() {
        int nameHash = name.hashCode();
        if (id == null) {
            return new Long(0).hashCode() + 13 * nameHash;
        }

        return id.hashCode() + 13 * nameHash;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Job && ((Job) obj).getName().equals(name)
                && ((Job) obj).getUrl().equals(url)
                && ((Job) obj).getColor().equals(color) && ((Job) obj).getBuilds().equals(builds);
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getColor() {
        return this.color;
    }

    public void setColor(String color) {
        this.color = color;
    }
    
    /*public ProjectHistory getProjectHistory() {
        return projectHistory;
    }
    
    public void setProjectHistory(ProjectHistory projectHistory) {
        this.projectHistory = projectHistory;
    }*/

    public void setBuilds(List<Build> builds) {
        this.builds = builds;
    }
    
    public List<Build> getBuilds() {
        return builds;
    }
    
    @Override
    public String toString() {
        return name;
    }
    
    public ArrayList<String> toStringArray() {
        ArrayList<String> values = new ArrayList<String>();
        values.add(name);
        values.add(url);
        values.add(color);
        return values;
    }
    
    public String toCSV() {
        return name+";"+url+";"+color;
    }
}
