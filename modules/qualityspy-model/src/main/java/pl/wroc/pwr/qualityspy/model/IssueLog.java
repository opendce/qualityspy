package pl.wroc.pwr.qualityspy.model;

import java.time.LocalDateTime;
import javax.persistence.*;

/**
 * Przechowuje worklog
 *
 * @author BugtrackerTeam
 */
@Entity
@Table(name = "ISSUE_LOGS")
public class IssueLog implements Comparable, IEntityWithId {

    @Transient
    private final int LONGTEXT_SIZE = 9000;
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;
    private String who;
    private LocalDateTime logDate;
    private String status;
    /** In seconds */
    private long timeSpent;
    @Column(name = "comment", length = LONGTEXT_SIZE)
    private String comment;

    @Override
    public String toString() {
        return "Log{" + "who=" + who + ", date=" + logDate + ", comment=" + status + timeSpent + comment + '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final IssueLog other = (IssueLog) obj;
        if ((this.who == null) ? (other.who != null) : !this.who.equals(other.who)) {
            return false;
        }
        if (this.logDate != other.logDate && (this.logDate == null || !this.logDate.equals(other.logDate))) {
            return false;
        }
        if ((this.status == null) ? (other.status != null) : !this.status.equals(other.status)) {
            return false;
        }
        if (this.timeSpent != other.timeSpent) {
            return false;
        }
        if ((this.comment == null) ? (other.comment != null) : !this.comment.equals(other.comment)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 13 * hash + (this.who != null ? this.who.hashCode() : 0);
        hash = 13 * hash + (this.logDate != null ? this.logDate.hashCode() : 0);
        hash = 13 * hash + (this.status != null ? this.status.hashCode() : 0);
        hash = 13 * hash + (int)this.timeSpent;
        hash = 13 * hash + (this.comment != null ? this.comment.hashCode() : 0);
        return hash;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LocalDateTime getDate() {
        return logDate;
    }

    public void setDate(LocalDateTime date) {
        this.logDate = date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /*
     * public IssueDescription getIssueDescription() { return issueDescription;
     * }
     *
     * public void setIssueDescription(IssueDescription issueDescription) {
     * this.issueDescription = issueDescription;
    }
     */
    public IssueLog(String who, LocalDateTime logDate, String status, long timeSpent, String comment) {
        this.who = who;
        this.logDate = logDate;
        this.status = status;
        this.timeSpent = timeSpent;
        this.comment = comment;
    }

    public String getWho() {
        return who;
    }

    public void setWho(String who) {
        this.who = who;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public long getTimeSpent() {
        return timeSpent;
    }

    public void setTimeSpent(long timeSpent) {
        this.timeSpent = timeSpent;
    }

    public IssueLog() {
    }

    @Override
    public int compareTo(Object o) {
        if (this.equals(o)) {
            return 0;
        } else {
            return -1;
        }
    }
}
