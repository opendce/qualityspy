package pl.wroc.pwr.qualityspy.model;

import org.apache.commons.collections.CollectionUtils;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Radek
 */
@Entity
@Table(name = "PROJECTS")
@XmlRootElement
public class Project implements IEntityWithId {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "ID")
    @XmlTransient
    private Long id;
    private String projectName;
    private String currentVersion;
    @OneToMany(targetEntity = ProjectHistory.class, cascade = CascadeType.ALL, orphanRemoval = true, fetch= FetchType.EAGER)
    @JoinColumn(name = "PROJECTS_ID", nullable = true)
    private Set<ProjectHistory> projectHistories = new HashSet<>();

    public Project(){
    }

    public Project(String name) {
        this.projectName = name;
    }

    @Override
    public int hashCode() {
        if (id == null) {
            return new Long(0).hashCode();
        }

        return id.hashCode();
    }

    @Override
    public boolean equals(Object anotherObject) {
        if (this == anotherObject) {
            return true;
        }

        if (!(anotherObject instanceof Project)) {
            return false;
        }

        Project anotherP = (Project) anotherObject;

        if (((this.getCurrentVersion() == null && anotherP.getCurrentVersion() == null) || this.getCurrentVersion().equals(anotherP.getCurrentVersion()))
                && ((this.getProjectName() == null && anotherP.getProjectName() == null) || this.getProjectName().equals(anotherP.getProjectName()))) {
            return CollectionUtils.isEqualCollection(this.projectHistories, anotherP.projectHistories);
        } else {
            return false;
        }
    }

    public String getProjectName() {
        return projectName;
    }

    public String getCurrentVersion() {
        return currentVersion;
    }

    public void setProjectName(String name) {
        projectName = name;
    }

    public void setCurrentVersion(String version) {
        currentVersion = version;
    }

    @XmlTransient
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @XmlTransient
    public Set<ProjectHistory> getProjectHistories() {
        return projectHistories;
    }

    public void setProjectHistories(Set<ProjectHistory> projectHistories) {
        this.projectHistories = projectHistories;
    }

    public String toCSV() {
        return projectName + ";" + currentVersion + ";" + getAllVersions();
    }
    
    public ArrayList<String> toStringArray() {
        ArrayList<String> values = new ArrayList<String>();
        values.add(projectName);
        values.add(currentVersion);
        values.add(getAllVersions());
        return values;
    }

    @XmlElement
    public String getAllVersions() {
        String ret = "";

        for (ProjectHistory ph : getProjectHistories()) {
            ret += ph.getVersion() + ",";
        }

        if (getProjectHistories() != null && getProjectHistories().size() > 0) {
            return ret.substring(0, ret.length() - 1);
        }

        return ret;
    }
}
