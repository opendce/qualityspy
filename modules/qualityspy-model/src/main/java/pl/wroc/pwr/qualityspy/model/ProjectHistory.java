package pl.wroc.pwr.qualityspy.model;

import com.google.common.collect.Sets;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "PROJECT_HISTORIES")
public class ProjectHistory implements Serializable, IEntityWithId {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;
    private String version;
    private String startRevision;
    private LocalDateTime startRevisionDate;
    private String endRevision;
    private LocalDateTime endRevisionDate;
    private String tag;
    private String branch;
    
    @OneToMany(targetEntity = Revision.class, cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    @JoinColumn(name = "PROJECT_HISTORIES_ID", nullable = false)
    private Set<Revision> revisions = new HashSet<>();
    
    @OneToMany(targetEntity = ClassDescription.class, cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    @JoinColumn(name = "PROJECT_HISTORIES_ID", nullable = false)
    private Set<ClassDescription> classes = new HashSet<>();
    
    @OneToMany(targetEntity = IssueDescription.class, cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "PROJECT_HISTORIES_ID", nullable = false)
    private Set<IssueDescription> issues = new HashSet<>();
    
    @OneToMany(targetEntity = MilestoneDescription.class, cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "PROJECT_HISTORIES_ID", nullable = false)
    private Set<MilestoneDescription> milestones = new HashSet<>();

    @OneToMany(targetEntity = Job.class, cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "PROJECT_HISTORIES_ID", nullable = false)
    private Set<Job> jobs = new HashSet<>();

    @Override
    public int hashCode() {
        if (id == null) {
            return new Long(0).hashCode();
        }

        return id.hashCode();
    }

    @Override
    public boolean equals(Object anotherObject) {
        if (this == anotherObject) {
            return true;
        }

        if (!(anotherObject instanceof ProjectHistory)) {
            return false;
        }

        ProjectHistory anotherPH = (ProjectHistory) anotherObject;
        return Objects.equals(version, ((ProjectHistory) anotherObject).version);
//        if (((this.getVersion() == null && anotherPH.getVersion() == null) || this.getVersion().equals(anotherPH.getVersion()))
//                && ((this.getStartRevision() == null && anotherPH.getStartRevision() == null) || this.getStartRevision().equals(anotherPH.getStartRevision()))
//                && ((this.getStartRevisionDate() == null && anotherPH.getStartRevisionDate() == null) || this.getStartRevisionDate().getTime() == anotherPH.getStartRevisionDate().getTime())
//                && ((this.getEndRevision() == null && anotherPH.getEndRevision() == null) || this.getEndRevision().equals(anotherPH.getEndRevision()))
//                && ((this.getEndRevisionDate() == null && anotherPH.getEndRevisionDate() == null) || this.getEndRevisionDate().getTime() == anotherPH.getEndRevisionDate().getTime())) {
//            return CollectionUtils.isEqualCollection(this.getRevisions(), anotherPH.getRevisions())
//                    && CollectionUtils.isEqualCollection(this.getClasses(), anotherPH.getClasses());
//        } else {
//            return false;
//        }
    }

    public String getVersion() {
        return version;
    }

    public String getStartRevision() {
        return startRevision;
    }

    public LocalDateTime getStartRevisionDate() {
        return startRevisionDate;
    }

    public String getEndRevision() {
        return endRevision;
    }

    public Set<Revision> getRevisions() {
        return revisions;
    }
    
    public LocalDateTime getEndRevisionDate() {
        return endRevisionDate;
    }

    public void setTag(String tagName)
    {
        tag = tagName;
    }
    
    public void setBranch(String branchName)
    {
        branch = branchName;
    }
    
    public String getTag()
    {
        return tag;
    }
    
    public String getBranch()
    {
        return branch;
    }
    
    
    public void setVersion(String aVersion) {
        version = aVersion;
    }

    public void setStartRevision(String aStartRevision) {
        startRevision = aStartRevision;
    }

    public void setStartRevisionDate(LocalDateTime aStartRevisionDate) {
        startRevisionDate = aStartRevisionDate;
    }

    public void setEndRevision(String aEndRevision) {
        endRevision = aEndRevision;
    }

    public void setEndRevisionDate(LocalDateTime aEndRevisionDate) {
        endRevisionDate = aEndRevisionDate;
    }

    public void addRevision(Revision classhistory) {
        getRevisions().add(classhistory);
    }

    public int countRevisions() {
        return getRevisions().size();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the classes
     */
    public Set<ClassDescription> getClasses() {
        return classes;
    }

    /**
     * @param classes the classes to set
     */
    public void setClasses(Set<ClassDescription> classes) {
        this.classes = classes;
    }

    public void addClass(ClassDescription clazz) {
        this.classes.add(clazz);
    }

    /**
     * @return the issues
     */
    public Set<IssueDescription> getIssues() {
        return issues;
    }
    
    public Set<MilestoneDescription> getMilestones() {
        return milestones;
    }

    /**
     * @param issues the issues to set
     */
    public void setIssues(Set<IssueDescription> issues) {
        this.issues = issues;
    }
        
    public void setMilestones(Set<MilestoneDescription> milestones) {
        this.milestones = milestones;
    }
    
    public void setRevisions(Set<Revision> revisions) {
        this.revisions = revisions;
    }

    public Set<Job> getJobs() {
        return jobs;
    }
    
    public void setJobs(Set<Job> jobs) {
        this.jobs = jobs;
    }
    
    public void addJob(Job job) {
        this.jobs.add(job);
    }
    
    /*public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }*/

    public Set<Revision> getRevisionsByFileName(String name) {
        Set<Revision> revisionsWithItem = Sets.newHashSet();
        for (Revision rev : getRevisions()) {
            if (rev.containsFile(name)) {
                revisionsWithItem.add(rev);
            }
        }
        return revisionsWithItem;
    }
}
