package pl.wroc.pwr.qualityspy.model;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * Przechowuje pojedynczy komentarz
 *
 * @author BugtrackerTeam
 */
@Entity
@Table(name = "ISSUE_COMMENTS")
public class IssueComment implements IEntityWithId {

    @Transient
    private final int LONGTEXT_SIZE = 9000;
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;
    private String author;
    private LocalDateTime commentDate;
    @Column(name = "COMMENT_TEXT", length = LONGTEXT_SIZE)
    private String commentText;

    public IssueComment() {
    }

    public IssueComment(String who, LocalDateTime when, String txt) {
        this.author = who;
        this.commentDate = when;
        this.commentText = txt;
    }

    public String getAuthor() {
        return this.author;
    }

    public LocalDateTime getDate() {
        return this.commentDate;
    }

    public String getCommentText() {
        return this.commentText;
    }

    public Long getId() {
        return id;
    }

    /*
     * public IssueDescription getIssueDescription() { return issueDescription;
     * }
     */
    public void setAuthor(String who) {
        this.author = who;
    }

    public void setDate(LocalDateTime when) {
        this.commentDate = when;
    }

    public void setCommentText(String txt) {
        this.commentText = txt;
    }

    public void setId(Long id) {
        this.id = id;
    }


    @Override
    public String toString() {
        return "Komentarz: " + commentText + "\n     Data: " + commentDate.toString()
                + "\n    Autor: " + author;
    }

    @Override
    public boolean equals(Object anotherObject) {
        if (this == anotherObject) {
            return true;
        }
        if (!(anotherObject instanceof IssueComment)) {
            return false;
        }
        IssueComment another = (IssueComment) anotherObject;
        return Objects.equals(author, another.author)
                && Objects.equals(commentText, another.commentText)
                && Objects.equals(commentDate, another.commentDate);
    }

    @Override
    public int hashCode() {
        int commentTextHash = commentText == null ? 0 : commentText.hashCode();
        int commentDateHash = commentDate == null ? 0 : commentDate.hashCode();
        int authorHash = author == null ? 0 : author.hashCode();
        return authorHash + 17 * commentDateHash + 31 * commentTextHash;
    }
}
