package pl.wroc.pwr.qualityspy.model;

import lombok.ToString;

import javax.persistence.*;
import java.util.*;

@ToString
@Entity
@Table(name = "ISSUE_DESCRIPTIONS")
public class  IssueDescription implements IEntityWithId {

    private final static int LONGTEXT_SIZE = 9000;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;
    private boolean defect;
    private String severity;
    @Column(name = "summary", length = LONGTEXT_SIZE)
    private String summary;
    @Column(name = "description", length = LONGTEXT_SIZE)
    private String description;
    private String reporter;
    private String assignTo;
    private String issueKey; // użyto 'issueKey', bo 'key' powoduje błąd w MySQLu
    private Long issueId;
    private String type;
    private String priority;
    private String status;
    private String resolution;
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar created;
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar updated;
    private String component;
    private String affectsVersion;
    private String fixVersion;
    @Column(name = "environment", length = LONGTEXT_SIZE)
    private String environment;
    @Column(name = "attachment", length = LONGTEXT_SIZE)
    private String attachment;
    @Column(name = "labels", length = LONGTEXT_SIZE)
    private String labels;
    private long originalEstimate;
    private long remainingEstimate;
    private long timeSpend;
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar resolved;
    private String project;
    @Column(name = "value", length = LONGTEXT_SIZE)
    private String value;
    private String complexity;
    private boolean isParent;
    private String parentKey;
    @Column(name = "watchers", length = LONGTEXT_SIZE)
    private String watchers;

    @OneToMany(targetEntity = IssueChange.class, cascade = CascadeType.ALL, orphanRemoval = true, fetch= FetchType.EAGER)
    @JoinColumn(name = "ISSUE_DESCRIPTIONS_ID", nullable = false)
    private Set<IssueChange> issueHistory = new LinkedHashSet<IssueChange>();
    @OneToMany(targetEntity = IssueComment.class, cascade = CascadeType.ALL, orphanRemoval = true, fetch= FetchType.EAGER)
    @JoinColumn(name = "ISSUE_DESCRIPTIONS_ID", nullable = false)
    private Set<IssueComment> issueComments = new LinkedHashSet<IssueComment>();
    @OneToMany(targetEntity = IssueLog.class, cascade = CascadeType.ALL, orphanRemoval = true, fetch= FetchType.EAGER)
    @JoinColumn(name = "ISSUE_DESCRIPTIONS_ID", nullable = false)
    private Set<IssueLog> issueLogs = new LinkedHashSet<IssueLog>();
    @OneToMany(targetEntity = IssueAttachment.class, cascade = CascadeType.ALL, orphanRemoval = true, fetch= FetchType.EAGER)
    @JoinColumn(name = "ISSUE_DESCRIPTIONS_ID", nullable = false)
    private Set<IssueAttachment> issueAttachments = new LinkedHashSet<IssueAttachment>();
    @Transient
    private final List<String> subtasks = new LinkedList<>();

    public IssueDescription() {
    }

    public IssueDescription(String summary, String description, String reporter, String assignTo, String issueKey, String type, String priority,
                            String status, String component, String affectsVersion, String fixVersion, String environment,
                            String attachment, String labels) {
        this.summary = summary;
        this.description = description;
        this.reporter = reporter;
        this.assignTo = assignTo;
        this.issueKey = issueKey;
        this.type = type;
        this.priority = priority;
        this.status = status;
        this.component = component;
        this.affectsVersion = affectsVersion;
        this.fixVersion = fixVersion;
        this.environment = environment;
        this.attachment = attachment;
        this.labels = labels;
    }

    public IssueDescription(IssueDescription issDesc) {
        this.affectsVersion = issDesc.getAffectsVersion();
        this.assignTo = issDesc.getAssignTo();
        this.attachment = issDesc.getAttachment();
        this.component = issDesc.getComponent();
        this.created = issDesc.getCreated();
        this.description = issDesc.getDescription();
        this.environment = issDesc.getEnvironment();
        this.fixVersion = issDesc.getFixVersion();
        this.id = issDesc.getId();
        this.issueComments = issDesc.getIssueComments();
        this.issueAttachments = issDesc.getIssueAttachments();
        this.issueHistory = issDesc.getIssueHistory();
        this.issueId = issDesc.getIssueId();
        this.issueKey = issDesc.getIssueKey();
        this.labels = issDesc.getLabels();
        this.originalEstimate = issDesc.getOriginalEstimate();
        this.priority = issDesc.getPriority();
        this.remainingEstimate = issDesc.getRemainingEstimate();
        this.reporter = issDesc.getReporter();
        this.resolution = issDesc.getResolution();
        this.status = issDesc.getStatus();
        this.severity = issDesc.getSeverity();
        this.summary = issDesc.getSummary();
        this.timeSpend = issDesc.getTimeSpend();
        this.type = issDesc.getType();
        this.updated = issDesc.getUpdated();
        this.resolved = issDesc.getResolved();
        this.project = issDesc.getProject();
        this.complexity = issDesc.getComplexity();
        this.value = issDesc.getValue();
        this.issueLogs = issDesc.getIssueLogs();
        this.watchers = issDesc.getWatchers();
    }

    @Override
    public boolean equals(Object anotherObject) {
        if (this == anotherObject) {
            return true;
        }
        if (!(anotherObject instanceof IssueDescription)) {
            return false;
        }
        IssueDescription another = (IssueDescription) anotherObject;
        boolean fieldEquals = defect == another.isDefect()
                && Objects.equals(severity, another.severity)
                && Objects.equals(summary, another.summary)
                && Objects.equals(description, another.description)
                && Objects.equals(reporter, another.reporter)
                && Objects.equals(assignTo, another.assignTo)
                && Objects.equals(issueKey, another.issueKey)
                && Objects.equals(type, another.type)
                && Objects.equals(priority, another.priority)
                && Objects.equals(resolution, another.resolution)
                && Objects.equals(status, another.status);
        boolean dateEquals = Objects.equals(getCreated(), another.getCreated())
                && Objects.equals(getUpdated(), another.getUpdated())
                && Objects.equals(getResolved(), another.getResolved());

        if (!(dateEquals && fieldEquals)) {
            return false;
        }
        Set<IssueChange> ownIssueChangeSet = getIssueHistory();
        Set<IssueChange> anotherIssueChangeSet = another.getIssueHistory();
        if (ownIssueChangeSet.size() != anotherIssueChangeSet.size()) {
            return false;
        }
        if (!ownIssueChangeSet.equals(anotherIssueChangeSet)) {
            return false;
        }

        Set<IssueLog> ownIssueLogSet = getIssueLogs();
        Set<IssueLog> anotherIssueLogSet = another.getIssueLogs();
        return ownIssueLogSet.size() == anotherIssueLogSet.size()
                && ownIssueLogSet.equals(anotherIssueLogSet);
    }

    @Override
    public int hashCode() {
        //Rzeczy, które najczęsciej się zmieniają w Opisie
        int severityTextHash = severity == null ? 0 : severity.hashCode();
        int summaryDateHash = summary == null ? 0 : summary.hashCode();
        int descriptionHash = description == null ? 0 : description.hashCode();
        return severityTextHash + 31 * summaryDateHash * 97 + descriptionHash;
    }

    public boolean isDefect() {
        return defect;
    }

    public String getSeverity() {
        return severity;
    }

    public String getSummary() {
        return summary;
    }

    public String getDescription() {
        return description;
    }

    public String getReporter() {
        return reporter;
    }

    public String getAssignTo() {
        return assignTo;
    }

    public Long getIssueId() {
        return issueId;
    }

    public String getStatus() {
        return status;
    }

    public String getResolution() {
        return resolution;
    }

    public String getType() {
        return type;
    }

    public String getPriority() {
        return priority;
    }

    public Calendar getCreated() {
        //if (created != null)
        //    created.set(Calendar.MILLISECOND, 0);
        return created;
    }

    public Calendar getUpdated() {
        if (updated != null) {
            updated.set(Calendar.MILLISECOND, 0);
        }
        return updated;
    }

    public Calendar getResolved() {
        if (resolved != null) {
            resolved.set(Calendar.MILLISECOND, 0);
        }
        return resolved;
    }

    public String getAffectsVersion() {
        return affectsVersion;
    }

    public String getAttachment() {
        return attachment;
    }

    public String getComponent() {
        return component;
    }

    public String getEnvironment() {
        return environment;
    }

    public String getFixVersion() {
        return fixVersion;
    }

    public String getIssueKey() {
        return issueKey;
    }

    public String getLabels() {
        return labels;
    }

    public long getOriginalEstimate() {
        return originalEstimate;
    }

    public long getRemainingEstimate() {
        return remainingEstimate;
    }

    public long getTimeSpend() {
        return timeSpend;
    }

    public Long getId() {
        return id;
    }

    public Set<IssueChange> getIssueHistory() {
        return issueHistory;
    }

    public IssueChange getIssueChange(int index) {
        Iterator<IssueChange> it = issueHistory.iterator();
        IssueChange a = new IssueChange();

        int i = -1;
        do {
            if (it.hasNext()) {
                a = it.next();
            } else {
                break;
            }
            i++;
        } while (i < index);

        if (i == index) {
            return a;
        } else {
            return null;
        }
    }

    public int getHistorySize() {
        return issueHistory.size();
    }

    public Set<IssueComment> getIssueComments() {
        return issueComments;
    }

    public Set<IssueAttachment> getIssueAttachments() {
        return issueAttachments;
    }

    public IssueComment getIssueComment(int index) {
        Iterator<IssueComment> it = issueComments.iterator();
        IssueComment a = new IssueComment();

        int i = -1;
        do {
            if (it.hasNext()) {
                a = it.next();
            } else {
                break;
            }
            i++;
        } while (i < index);

        if (i == index) {
            return a;
        } else {
            return null;
        }
    }

    public int getCommentsSize() {
        return issueComments.size();
    }

    public Set<IssueLog> getIssueLogs() {
        return issueLogs;
    }

    public IssueLog getIssueLog(int index) {
        Iterator<IssueLog> it = issueLogs.iterator();
        IssueLog a = new IssueLog();

        int i = -1;
        do {
            if (it.hasNext()) {
                a = it.next();
            } else {
                break;
            }
            i++;
        } while (i < index);

        if (i == index) {
            return a;
        } else {
            return null;
        }
    }

    public int getLogsSize() {
        return issueLogs.size();
    }

    public void setDefect(boolean aDefect) {
        defect = aDefect;
    }

    public void setSeverity(String aSeverity) {
        severity = aSeverity;
    }

    public void setSummary(String aSummary) {
        summary = aSummary;
    }

    public void setDescription(String aDescription) {
        description = aDescription;
    }

    public void setReporter(String aReporter) {
        reporter = aReporter;
    }

    public void setAssignTo(String aAssignTo) {
        assignTo = aAssignTo;
    }

    public void setId(Long aId) {
        id = aId;
    }

    public void setType(String aType) {
        type = aType;
    }

    public void setPriority(String aPriority) {
        priority = aPriority;
    }

    public void setAffectsVersion(String affectsVersion) {
        this.affectsVersion = affectsVersion;
    }

    public void setAttachments(String attachment) {
        this.attachment = attachment;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public void setFixVersion(String fixVersion) {
        this.fixVersion = fixVersion;
    }

    public void setIssueId(Long issueId) {
        this.issueId = issueId;
    }

    public void setIssueKey(String issueKey) {
        this.issueKey = issueKey;
    }

    public void setLabels(String labels) {
        this.labels = labels;
    }

    public void setOriginalEstimate(long originalEstimate) {
        this.originalEstimate = originalEstimate;
    }

    public void setRemainingEstimate(long remainingEstimate) {
        this.remainingEstimate = remainingEstimate;
    }

    public void setTimeSpend(long timeSpend) {
        this.timeSpend = timeSpend;
    }

    public void setIssueId(long aIssueId) {
        issueId = aIssueId;
    }

    public void setStatus(String aStatus) {
        status = aStatus;
    }

    public void setResolution(String aResolution) {
        resolution = aResolution;
    }

    public void setCreated(Calendar aCreated) {
        created = aCreated;
    }

    public void setUpdated(Calendar aUpdated) {
        updated = aUpdated;
    }

    public void setResolved(Calendar aResolved) {
        resolved = aResolved;
    }

    public void setIssueHistory(Set<IssueChange> history) {
        issueHistory = history;
    }

    public void addIssueChange(IssueChange change) {
        issueHistory.add(change);
    }

    public void setIssueComments(Set<IssueComment> comments) {
        issueComments = comments;
    }

    public void addIssueComment(IssueComment comment) {
        issueComments.add(comment);
    }

    public void setIssueAttachments(Set<IssueAttachment> attachments) {
        issueAttachments = attachments;
    }

    public void addIssueAttachment(IssueAttachment attachment) {
        issueAttachments.add(attachment);
    }

    public void setIssueLogs(Set<IssueLog> logs) {
        issueLogs = logs;
    }

    public void addIssueLog(IssueLog log) {
        issueLogs.add(log);
    }

    public String getProject() {
        return project;
    }

    public String getComplexity() {
        return complexity;
    }

    public String getValue() {
        return value;
    }

    public void setProject(String projectName) {
        project = projectName;
    }

    public void setComplexity(String complex) {
        complexity = complex;
    }

    public void setValue(String val) {
        value = val;
    }

    public String getWatchers() {
        return watchers;
    }

    public void setWatchers(String watchers) {
        this.watchers = watchers;
    }

    public String toCSV() {                                                                                              //+ originalEstimate +";"+ remainingEstimate +";"+ timeSpend +";"
        return id + ";" + defect + ";" + severity +";"+ summary
                +";"+ description +";"+ reporter +";"+
                assignTo +";"+ issueKey +";"+ issueId +";"+
                type +";"+ priority +";"+ status +";"+ resolved +";"+
                resolution +";"+ created +";"+ updated+";"+ component +";"+
                affectsVersion+";"+ fixVersion +";"+ environment +";"+ attachment
                +";"+ labels + ";" + project +";"+ value +";"+
                getComplexity() +";"+ issueHistory +";"+ issueComments +";"+ issueAttachments +";"+ issueLogs;
    }


    public ArrayList<String> toStringArray() {
        ArrayList<String> values = new ArrayList<String>();
        values.add(id.toString());
        if(defect){             //        values.add(defect);
            values.add("True");
        }else{
            values.add("False");
        }
        values.add(severity);
        values.add(summary);
        values.add(description);
        values.add(assignTo);
        values.add(issueKey);
        values.add(issueId.toString());
        values.add(type);
        values.add(priority);
        values.add(status);
        values.add(resolved.toString());
        values.add(resolution);
        values.add(created.toString());
        values.add(updated.toString());
        values.add(component);
        values.add(affectsVersion);
        values.add(fixVersion);
        values.add(environment);
        values.add(attachment);
        values.add(labels);
        values.add(project);
        values.add(value);
        values.add(getComplexity());
        values.add(issueHistory.toString());
        values.add(issueComments.toString());
        values.add(issueAttachments.toString());
        values.add(issueLogs.toString());
        return values;
    }

    /**
     * @return the isParent
     */
    public boolean isIsParent() {
        return isParent;
    }

    /**
     * @param isParent the isParent to set
     */
    public void setIsParent(boolean isParent) {
        this.isParent = isParent;
    }

    /**
     * @return the parentKey
     */
    public String getParentKey() {
        return parentKey;
    }

    /**
     * @param parentKey the parentKey to set
     */
    public void setParentKey(String parentKey) {
        this.parentKey = parentKey;
    }

    public List<String> getSubtasks() {
        return subtasks;
    }

    public void addSubtask(String subtaskKey) {
        getSubtasks().add(subtaskKey);
    }
}
