package pl.wroc.pwr.qualityspy.model.metric_definition;

import pl.wroc.pwr.qualityspy.model.IEntityWithId;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Model for definition of metric
 */
@Entity
@Table(name = "metric_definitions")
@XmlRootElement(name = "result")
@XmlAccessorType(XmlAccessType.FIELD)
public class MetricDefinition implements IEntityWithId {

    private static final int SYMBOL_MAX_LENGTH = 128;
    private static final int DEFINITION_MAX_LENGTH = 10000;
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "ID")
    @XmlElement(name = "id", defaultValue = "")
    private Long id;

    @XmlElement(name = "symbol", defaultValue = "")
    @Column(name = "symbol", unique = true, length = SYMBOL_MAX_LENGTH)
    private String symbol;
    @XmlElement(name = "metric_name", defaultValue = "")
    @Column(name = "metric_name", unique = true)
    private String metricName;
    @XmlElement(name = "definition", defaultValue = "")
    @Column(name = "definition", length = DEFINITION_MAX_LENGTH)
    private String definition;
    @Column(name = "prepared_definition", length = DEFINITION_MAX_LENGTH)
    private String preparedDefinition;

    public MetricDefinition(String name, String symbol, String definition) {
        this.metricName = name;
        this.symbol = symbol;
        this.definition = definition;
    }

    public MetricDefinition() {

    }


    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getMetricName() {
        return metricName;
    }

    public void setMetricName(String metricName) {
        this.metricName = metricName;
    }

    public void setDefinition(String definition) {
        this.definition = definition;
    }

    public void setPreparedDefinition(String preparedDefinition) {
        this.preparedDefinition = preparedDefinition;
    }

    public String getPreparedDefinition() {
        return preparedDefinition;
    }

    public String getDefinition() {
        return definition;
    }
}
