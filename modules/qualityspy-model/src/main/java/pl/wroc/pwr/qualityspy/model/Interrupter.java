/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wroc.pwr.qualityspy.model;

/**
 *
 * @author Mich
 */
public class Interrupter {
    private volatile Boolean isInterrupted;
    public Interrupter(){
        isInterrupted = false;
    }
    
    public boolean getIsInterrupted(){
        return isInterrupted;
    }
    
    public void setIsInterrupted(){
        isInterrupted = true;
    }
    
    public Interrupter(Boolean arg){
        isInterrupted = arg;
    }
    
    public void throwIntrerrupted() throws IsInterruptedException
    {
        throw new IsInterruptedException("Interrupted");
    }
}
