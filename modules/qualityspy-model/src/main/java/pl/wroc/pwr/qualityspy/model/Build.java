package pl.wroc.pwr.qualityspy.model;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author JacekBurzynski
 */
@Entity
@Table(name = "BUILDS")
public class Build implements Serializable, IEntityWithId {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;
    private Integer number;
    private LocalDateTime date;
    private String result;
    private Integer totalCount;
    private Integer totalDiff;
    private Integer failCount;
    private Integer failDiff;

    public Build() {
        super();
    }

    public Build(Integer number, LocalDateTime date, String result, Integer totalCount, Integer totalDiff, Integer failCount, Integer failDiff) {
        this.number = number;
        this.date = date;
        this.result = result;
        this.totalCount = totalCount;
        this.totalDiff = totalDiff;
        this.failCount = failCount;
        this.failDiff = failDiff;
    }

    @Override
    public int hashCode() {
        if (id == null) {
            return new Long(0).hashCode();
        }

        return id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return ((Build) obj).getNumber().equals(number) && ((Build) obj).getDate().equals(date) && ((Build) obj).getResult().equals(result) && ((Build) obj).getTotalCount().equals(totalCount) && ((Build) obj).getTotalDiff().equals(totalDiff) && ((Build) obj).getFailCount().equals(failCount) && ((Build) obj).getFailDiff().equals(failDiff);
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public Integer getNumber() {
        return this.number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public LocalDateTime getDate() {
        return this.date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getResult() {
        return this.result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Integer getTotalCount() {
        return this.totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public Integer getTotalDiff() {
        return this.totalDiff;
    }

    public void setTotalDiff(Integer totalDiff) {
        this.totalDiff = totalDiff;
    }

    public Integer getFailCount() {
        return this.failCount;
    }

    public void setFailCount(Integer failCount) {
        this.failCount = failCount;
    }

    public Integer getFailDiff() {
        return this.failDiff;
    }

    public void setFailDiff(Integer failDiff) {
        this.failDiff = failDiff;
    }
}
