package pl.wroc.pwr.qualityspy.model;

import java.util.Objects;
import javax.persistence.*;

@Entity
@Table(name = "METHOD_DESCRIPTIONS")
public class MethodDescription implements IEntityWithId {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "ID")
	private Long id;
	@Column(name = "NAME", length=9000)
	private String name;
	private long cyclomaticComplexity;

	public MethodDescription() {
		name = "";
		cyclomaticComplexity = 0;
	}

	@Override
	public int hashCode() {
		if (id != null) {
			return id.hashCode();
		} else {
			return Long.valueOf(0).hashCode();
		}
	}

	@Override
	public boolean equals(Object anotherObject) {
		if (this == anotherObject)
			return true;

		if (!(anotherObject instanceof MethodDescription))
			return false;

		MethodDescription anotherMD = (MethodDescription) anotherObject;
		if (Objects.equals(name, anotherMD.getName())
				&& cyclomaticComplexity == anotherMD.getCyclomaticComplexity())
			return true;
		else
			return false;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getCyclomaticComplexity() {
		return cyclomaticComplexity;
	}

	public void setCyclomaticComplexity(long cyclomaticComplexity) {
		this.cyclomaticComplexity = cyclomaticComplexity;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
        
        @Override
        public String toString() {
            return name + "," + cyclomaticComplexity + ",";
        }
}
