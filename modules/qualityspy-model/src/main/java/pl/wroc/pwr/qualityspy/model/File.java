package pl.wroc.pwr.qualityspy.model;

import java.io.Serializable;
import javax.persistence.*;
import pl.wroc.pwr.qualityspy.model.enums.FileType;
import java.util.ArrayList;

@Entity
@Table(name = "FILE")
public class File implements Serializable, IEntityWithId {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;
    @Column(name = "OLD_CONTENT", length = 90000)
    private String oldContent;
    @Column(name = "NEW_CONTENT", length = 90000)
    private String newContent;
    private String name;
    private String oldPath;
    private String newPath;
    private String repoPath;
    @Enumerated(EnumType.STRING)
    private ChangeType changeType;
    private long numberOfAddedLines;
    private long numberOfRemovedLines;
    private FileType fileType;
    private String fileExtension;

//    public String toCSV() {
//        return  id.toString()+";"+oldContent+";"+newContent+
//                ";"+name+";"+oldPath+
//                ";"+newPath+";"+repoPath+
//                ";"+changeType.toString()+";"+numberOfAddedLines+
//               ";"+numberOfRemovedLines+";"+fileType.toString();
//   }
	
	public ArrayList<String> toCSV() {
		ArrayList<String> values = new ArrayList<String>();
		//values.add(id.toString());
		values.add(oldContent);
		values.add(newContent);
		values.add(name);
		values.add(oldPath);
		values.add(newPath);
		values.add(getRepoPath());
		values.add(changeType.toString());
		values.add(Long.toString(getNumberOfAddedLines()));
		values.add(Long.toString(getNumberOfRemovedLines()));
		values.add(getFileType().toString());
                values.add(fileExtension);
		
		return values;
	}

        
    /**
    * @param fileExtension the fileExtension to set
     */
    public void setFileExtension(String fileExtension) {
        this.fileExtension = fileExtension;
    }

    /**
    * @return the fileExtension
     */
    public String getFileExtension() {
        return fileExtension;
    }

    /**
     * @return the repoPath
     */
    public String getRepoPath() {
        return repoPath;
    }

    /**
     * @param repoPath the repoPath to set
     */
    public void setRepoPath(String repoPath) {
        this.repoPath = repoPath;
    }

    /**
     * @return the numberOfAddedLines
     */
    public long getNumberOfAddedLines() {
        return numberOfAddedLines;
    }

    /**
     * @param numberOfAddedLines the numberOfAddedLines to set
     */
    public void setNumberOfAddedLines(long numberOfAddedLines) {
        this.numberOfAddedLines = numberOfAddedLines;
    }

    /**
     * @return the numberOfRemovedLines
     */
    public long getNumberOfRemovedLines() {
        return numberOfRemovedLines;
    }

    /**
     * @param numberOfRemovedLines the numberOfRemovedLines to set
     */
    public void setNumberOfRemovedLines(long numberOfRemovedLines) {
        this.numberOfRemovedLines = numberOfRemovedLines;
    }

    /**
     * @return the fileType
     */
    public FileType getFileType() {
        return fileType;
    }

    /**
     * @param fileType the fileType to set
     */
    public void setFileType(FileType fileType) {
        this.fileType = fileType;
    }
    
    public enum ChangeType {
        DELETION,
        CREATION,
        CHANGED_PATH,
        CHANGED_CONTENT;
    };

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOldPath() {
        return this.oldPath;
    }

    public void setOldPath(String path) {
        this.oldPath = path;
    }

    public String getNewPath() {
        return this.newPath;
    }

    public void setNewPath(String path) {
        this.newPath = path;
    }

    public String getOldContent() {
        return this.oldContent;
    }

    public void setOldContent(String content) {
        this.oldContent = content;
    }

    public String getNewContent() {
        return this.newContent;
    }

    public void setNewContent(String content) {
        this.newContent = content;
    }

    public void setChangeType(ChangeType type) {
        this.changeType = type;
    }

    public ChangeType getChangeType() {
        return this.changeType;
    }
}
