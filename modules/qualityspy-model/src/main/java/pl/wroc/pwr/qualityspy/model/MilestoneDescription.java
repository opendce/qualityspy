/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wroc.pwr.qualityspy.model;

import java.util.*;
import javax.persistence.*;
import java.util.ArrayList;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "MILESTONES")
@EqualsAndHashCode
public class MilestoneDescription implements IEntityWithId {

    @Transient
    private final int LONGTEXT_SIZE = 9000;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;
    private String title;
    private String creator;
    private String description;
    private String state;
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar created;
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar deadline;

    public MilestoneDescription(){
    }

    public MilestoneDescription(MilestoneDescription milestone){
        this.id = milestone.getId();
        this.title = milestone.getTitle();
        this.creator = milestone.getCreator();
        this.description = milestone.getDescription();
        this.state = milestone.getState();
        this.created = milestone.getCreated();
        this.deadline = milestone.getDeadline();
    }

    @Override
    public String toString() {
        return "Milestone{" + "id=" + id + ", title=" + title + ", creator=" + creator + ", description=" + description
                            + ", state+" + state + ", created=" + created + ", deadline=" + deadline + "}";
    }
    
    public String toCSV() {                                                                                              //+ originalEstimate +";"+ remainingEstimate +";"+ timeSpend +";"
        return id + ";" + title + ";" + creator +";"+ description
                +";"+ state +";"+ created +";"+ deadline;
    }
    
    
    public ArrayList<String> toStringArray() {
        ArrayList<String> values = new ArrayList<>();
        values.add(id.toString());
        values.add(title);
        values.add(creator);
        values.add(description);
        values.add(state);
        values.add(created.toString());
        values.add(deadline.toString());
        return values;
    }

    public Long getId(){
        return this.id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle(){
        return this.title;
    }
    
    public String getCreator(){
        return this.creator;
    }
    
    public String getDescription(){
        return this.description;
    }
    
    public String getState(){
        return this.state;
    }
    
    public Calendar getCreated(){
        if (created != null){
            created.set(Calendar.MILLISECOND, 0);
        }
        return created;
    }
    
    public Calendar getDeadline(){
        if (deadline != null){
            deadline.set(Calendar.MILLISECOND, 0);
        }
        return deadline;
    }
    
    public void setTitle(String title){
        this.title = title;
    }
    
    public void setCreator(String creator){
        this.creator = creator;
    }
    
    public void setDescription(String description){
        this.description = description;
    }
    
    public void setState(String state){
        this.state = state;
    }
    
    public void setCreated(Calendar created){
        this.created = created;
    }
    
    public void setDeadline(Calendar deadline){
        this.deadline = deadline;
    }
}

