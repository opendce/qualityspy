package pl.wroc.pwr.qualityspy.model;

import com.google.common.collect.Sets;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "REVISIONS")
public class Revision implements Serializable, IEntityWithId {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;
    private String number;
    @Column(name = "REVISION_DATE")
    private LocalDateTime date;
    private String tags;
    private String branch;
    private String author;
    @Column(name = "COMMENT", length = 5000)
    private String comment;
    @OneToMany(targetEntity = File.class, cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    @JoinColumn(name = "REVISIONS_ID")
    private Set<File> files = Sets.newHashSet();

    @Override
    public int hashCode() {
        if (id == null) {
            return new Long(0).hashCode();
        }

        return id.hashCode();
    }

    @Override
    public boolean equals(Object anotherObject) {

        if (this == anotherObject) {
            return true;
        }

        if (!(anotherObject instanceof Revision)) {
            return false;
        }

        Revision anotherRevision = (Revision) anotherObject;
        return Objects.equals(number, anotherRevision.number) && Objects.equals(date, anotherRevision.date) && Objects.equals(comment, anotherRevision.comment);
//        if (((this.getNumber() == null && anotherRevision.getNumber() == null) || this.getNumber().equals(anotherRevision.getNumber()))
//                && ((this.getDate() == null && anotherRevision.getDate() == null) || this.getDate().getTime() == anotherRevision.getDate().getTime())
//                && ((this.getTags() == null && anotherRevision.getTags() == null) || this.getTags().equals(anotherRevision.getTags()))
//                && ((this.getBranch() == null && anotherRevision.getBranch() == null) || this.getBranch().equals(anotherRevision.getBranch()))
//                && ((this.getAuthor() == null && anotherRevision.getAuthor() == null) || this.getAuthor().equals(anotherRevision.getAuthor()))
//                && ((this.getComment() == null && anotherRevision.getComment() == null) || this.getComment().equals(anotherRevision.getComment()))
//                && ((CollectionUtils.isEqualCollection(this.getFiles(), anotherRevision.getFiles())))) {
    }

    public String getNumber() {
        return number;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public String getTags() {
        return tags;
    }

    public String getBranch() {
        return branch;
    }

    public String getAuthor() {
        return author;
    }
    
    public Set<File> getFiles() {
        return files;
    }
    
    public void setFiles( Set<File> files ) {
        this.files = files;
    }

    public void setNumber(String aNumber) {
        number = aNumber;
    }

    public void setDate(LocalDateTime aDate) {
        date = aDate;
    }

    public void setTags(String aTags) {
        tags = aTags;
    }

    public void setBranch(String aBranch) {
        branch = aBranch;
    }

    public void setAuthor(String aAuthor) {
        author = aAuthor;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void addFile(File f) {
        getFiles().add(f);
    }

    public boolean containsFile(String fileName) {
        if (fileName == null) {
            return false;
        }
        for (File f : getFiles()) {
            if (fileName.equals(f.getName())) {
                return true;
            }
        }
        return false;
    }

    public File getFileByName(String name) {
        if (name == null) {
            return null;
        }
        for (File f : getFiles()) {
            if (name.equals(f.getName())) {
                return f;
            }
        }
        return null;
    }
}
