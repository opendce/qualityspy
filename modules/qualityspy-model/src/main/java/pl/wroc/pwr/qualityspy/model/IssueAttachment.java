package pl.wroc.pwr.qualityspy.model;


import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@ToString
@EqualsAndHashCode

@Entity
@Table(name = "ISSUE_ATTACHMENTS")
public class IssueAttachment {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "ATTACHMENT_ID")
    private Long id;
    @Column(name = "MIME_TYPE")
    private String mimeType;
    @Column(name = "FILE_NAME", length = 500)
    private String fileName;
    @Column(name = "SIZE")
    private int size;
    @Column(name = "CREATION_DATE")
    private LocalDateTime creationDate;

    private IssueAttachment() {
        /* default constructor used by Hibernate */
    }

    public IssueAttachment(String mimeType, String fileName, int size, LocalDateTime creationDate) {
        this.mimeType = mimeType;
        this.fileName = fileName;
        this.size = size;
        this.creationDate = creationDate;

    }

    public String getMimeType() {
        return mimeType;
    }

    public String getFileName() {
        return fileName;
    }

    public int getSize() {
        return size;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }
}
