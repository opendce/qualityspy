package pl.wroc.pwr.qualityspy.model;

import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.*;
import javax.persistence.Column;

/**
 * Przechowuje informacje o pojedynczej zmianie zagadnienia
 *
 * @author BugtrackerTeam
 */
@Entity
@Table(name = "ISSUE_CHANGES")
public class IssueChange implements IEntityWithId {

    private final static int LONGTEXT_SIZE = 9000;
    private static final String DATE_FORMAT = "dd-MM-yyyy HH:mm:ss";

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;
    private String whoChanged;
    private LocalDateTime dateOfChange;
    @Column(name = "WHAT_CHANGED", length = LONGTEXT_SIZE)
    private String whatChanged;
    @Column(name = "OLD_VALUE", length = LONGTEXT_SIZE)
    private String oldValue="";
    @Column(name = "NEW_VALUE", length = LONGTEXT_SIZE)
    private String newValue="";
    private String type;
    private static final String Type_Insertion = "Dodanie";
    private static final String Type_Modification = "Modyfikacja";
    private static final String Type_Deletion = "Usuniecie";

    public IssueChange() {
    }

    public IssueChange(String who, LocalDateTime when, String what, String oldV,
            String newV) {
        this.whoChanged = who;
        this.dateOfChange = when;
        this.whatChanged = what;
        this.oldValue = oldV;
        this.newValue = newV;

        if (oldValue.compareTo("") == 0 && newValue.compareTo("") != 0) {
            type = Type_Insertion;
        } else if (oldValue.compareTo("") != 0 && newValue.compareTo("") == 0) {
            type = Type_Deletion;
        } else {
            type = Type_Modification;
        }
    }

    public String getWhoChanged() {
        return this.whoChanged;
    }

    public LocalDateTime getDateOfChange() {
        return this.dateOfChange;
    }

    public String getWhatChanged() {
        return this.whatChanged;
    }

    public String getOldValue() {
        return this.oldValue;
    }

    public String getNewValue() {
        return this.newValue;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setWhoChanged(String whoChanged) {
        this.whoChanged = whoChanged;
    }

    public void setDateOfChange(LocalDateTime dateOfChange) {
        this.dateOfChange = dateOfChange;
    }

    public void setWhatChanged(String whatChanged) {
        this.whatChanged = whatChanged;
    }

    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
        if (oldValue.compareTo("") == 0 && newValue.compareTo("") != 0) {
            type = Type_Insertion;
        } else if (oldValue.compareTo("") != 0 && newValue.compareTo("") == 0) {
            type = Type_Deletion;
        } else {
            type = Type_Modification;
        }
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
        if (oldValue.compareTo("") == 0 && newValue.compareTo("") != 0) {
            type = Type_Insertion;
        } else if (oldValue.compareTo("") != 0 && newValue.compareTo("") == 0) {
            type = Type_Deletion;
        } else {
            type = Type_Modification;
        }
    }

    @Override
    public String toString() {
        return "Typ zmiany:\t" + type + "\nData:\t\t" + dateOfChange.toString()
                + "\nAutor:\t\t" + whoChanged + "\nDotyczy:\t" + whatChanged
                + "\nStara wart.:\t" + oldValue + "\nNowa wart.:\t" + newValue;
    }

    @Override
    public boolean equals(Object anotherObject) {
        if (this == anotherObject) {
            return true;
        }
        if (!(anotherObject instanceof IssueChange)) {
            return false;
        }
        IssueChange anotherID = (IssueChange) anotherObject;
        return Objects.equals(whoChanged, anotherID.whoChanged)
                && Objects.equals(dateOfChange, anotherID.dateOfChange)
                && Objects.equals(whatChanged, anotherID.whatChanged)
                && Objects.equals(oldValue, anotherID.oldValue)
                && Objects.equals(newValue, anotherID.newValue);
    }

    @Override
    public int hashCode() {
        int whoChangedHash = whoChanged == null ? 0 : whoChanged.hashCode();
        int dateOfChangeHash = dateOfChange == null ? 0 : dateOfChange.hashCode();
        int whatChangedHash = whatChanged == null ? 0 : whatChanged.hashCode();
        int oldValueHash = oldValue == null ? 0 : oldValue.hashCode();
        int newValueHash = newValue == null ? 0 : newValue.hashCode();
        return newValueHash + 11 * whatChangedHash + 31 * oldValueHash + 67 * dateOfChangeHash + 97 * whoChangedHash;
    }
}
