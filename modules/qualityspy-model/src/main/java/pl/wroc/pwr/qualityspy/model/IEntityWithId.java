package pl.wroc.pwr.qualityspy.model;

/**
 *
 * @author marian
 */
public interface IEntityWithId {

    public Long getId();
    public void setId(Long id);
}
