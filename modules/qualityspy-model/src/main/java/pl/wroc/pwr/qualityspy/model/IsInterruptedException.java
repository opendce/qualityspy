/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wroc.pwr.qualityspy.model;

/**
 *
 * @author Mich
 */
public class IsInterruptedException extends RuntimeException {
    public IsInterruptedException(){}
    public IsInterruptedException(String msg){
        super(msg);
    }
    boolean getInterruption(){
        return true;
    }
}
