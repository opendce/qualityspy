package pl.wroc.pwr.qualityspy.model.enums;

/**
 * Types of files from version control repository.
 */
public enum FileType {

    
    /**
     * Executable files.
     */
    BINARY,
    
    /**
     * Text files.
     */
    TEXT,
    
    /**
     * File types other than the aforementioned.
     */
    OTHER
    
    
   
}
