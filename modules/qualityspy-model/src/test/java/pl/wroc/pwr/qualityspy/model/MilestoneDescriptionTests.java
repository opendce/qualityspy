package pl.wroc.pwr.qualityspy.model;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by Verloren on 08.05.2017.
 */
public class MilestoneDescriptionTests {

    @Test
    public void equals_when_objects_have_the_same_bodies() {
        // Given
        MilestoneDescription milestoneDescription1 = newMilestoneDescription(100, "First", "Creator", "Description", "State");
        MilestoneDescription milestoneDescription2 = milestoneDescription1;

        // When
        boolean result = milestoneDescription1.equals(milestoneDescription2);

        // Then
        assertTrue(result);
    }

    @Test
    public void does_not_equal_when_objects_have_different_bodies() {
        // Given
        MilestoneDescription milestoneDescription1 = newMilestoneDescription(100, "First", "Creator", "Description", "State");
        MilestoneDescription milestoneDescription2 = newMilestoneDescription(200, "Second", "Completely", "New", "Body");

        // When
        boolean result = milestoneDescription1.equals(milestoneDescription2);

        // Then
        assertFalse(result);
    }

    private MilestoneDescription newMilestoneDescription(int id, String title, String creator, String description, String state) {
        MilestoneDescription milestoneDescription = new MilestoneDescription();

        milestoneDescription.setTitle(title);
        milestoneDescription.setId((long)id);
        milestoneDescription.setCreator(creator);
        milestoneDescription.setDescription(description);
        milestoneDescription.setState(state);
        milestoneDescription.setCreated(null);
        milestoneDescription.setDeadline(null);

        return milestoneDescription;
    }
}