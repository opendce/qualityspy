package pl.wroc.pwr.qualityspy.model;

import pl.wroc.pwr.qualityspy.model.Revision;
import pl.wroc.pwr.qualityspy.model.ClassDescription;
import pl.wroc.pwr.qualityspy.model.ProjectHistory;
import pl.wroc.pwr.qualityspy.model.Project;
import pl.wroc.pwr.qualityspy.model.IssueDescription;
import pl.wroc.pwr.qualityspy.model.MethodDescription;
import org.junit.Test;
import org.junit.Before;
import static org.junit.Assert.*;
import java.time.LocalDateTime;

public class UnitTestsTransport {
	
	//Needed objects
	private ClassDescription classDescription;
	private ClassDescription equalClassDescription;
	private ClassDescription notEqualClassDescription;
	
	private IssueDescription issueDescription;
	private IssueDescription equalIssueDescription;
	private IssueDescription notEqualIssueDescription;
	
	private MethodDescription methodDescription;
	private MethodDescription equalMethodDescription;
	private MethodDescription notEqualMethodDescription;
	
	private Project project;
	private Project equalProject;
	private Project notEqualProject;
	
	private ProjectHistory projectHistory;
	private ProjectHistory equalProjectHistory;
	private ProjectHistory notEqualProjectHistory;
	
	private Revision revision;
	private Revision equalRevision;
	private Revision notEqualRevision;
	
	//Preparing objects for tests
	@Before
	public void prepareObjects() {
		
		//Project and its components
		//IssueDescription
		issueDescription = new IssueDescription();
		equalIssueDescription = new IssueDescription();
		notEqualIssueDescription = new IssueDescription();
		issueDescription.setSummary("Summary");
		issueDescription.setReporter("Reporter");
		issueDescription.setAssignTo("AssignTo");
		issueDescription.setIssueKey("key");
		equalIssueDescription.setSummary("Summary");
		equalIssueDescription.setReporter("Reporter");
		equalIssueDescription.setAssignTo("AssignTo");
		equalIssueDescription.setIssueKey("key");
		notEqualIssueDescription.setSummary("AnotherSummary");
		notEqualIssueDescription.setReporter("AnotherReporter");
		notEqualIssueDescription.setAssignTo("AnotherAssignTo");
		notEqualIssueDescription.setIssueKey("Anotherkey");
		
		//Revision
		revision = new Revision();
		equalRevision = new Revision();
		notEqualRevision = new Revision();
		revision.setNumber("10");
		revision.setDate(LocalDateTime.of(70,1,16,13,47,57));
		revision.setTags("Tags");
		revision.setBranch("Branch");
		revision.setAuthor("Author");
		revision.setComment("Comment");
		equalRevision.setNumber("10");
		equalRevision.setDate(LocalDateTime.of(70,1,16,13,47,57));
		equalRevision.setTags("Tags");
		equalRevision.setBranch("Branch");
		equalRevision.setAuthor("Author");
		equalRevision.setComment("Comment");
		notEqualRevision.setNumber("100");
		notEqualRevision.setDate(LocalDateTime.of(70,1,16,13,47,58));
		notEqualRevision.setTags("AnotherTags");
		notEqualRevision.setBranch("AnotherBranch");
		notEqualRevision.setAuthor("AnotherAuthor");
		notEqualRevision.setComment("AnotherComment");

		
		//ProjectHistory
		projectHistory = new ProjectHistory();
		equalProjectHistory = new ProjectHistory();
		notEqualProjectHistory = new ProjectHistory();
		projectHistory.setVersion("Version");
		projectHistory.setStartRevision("10");
		projectHistory.setStartRevisionDate(LocalDateTime.of(70,1,16,13,47,57));
		projectHistory.setEndRevision("11");
		projectHistory.setEndRevisionDate(LocalDateTime.of(70,1,16,13,47,58));
		projectHistory.addRevision(revision);
		equalProjectHistory.setVersion("Version");
		equalProjectHistory.setStartRevision("10");
		equalProjectHistory.setStartRevisionDate(LocalDateTime.of(70,1,16,13,47,57));
		equalProjectHistory.setEndRevision("11");
		equalProjectHistory.setEndRevisionDate(LocalDateTime.of(70,1,16,13,47,58));
		equalProjectHistory.addRevision(revision);
		notEqualProjectHistory.setVersion("AnotherVersion");
		notEqualProjectHistory.setStartRevision("20");
		notEqualProjectHistory.setStartRevisionDate(LocalDateTime.of(70,1,16,13,47,58));
		notEqualProjectHistory.setEndRevision("21");
		notEqualProjectHistory.setEndRevisionDate(LocalDateTime.of(70,1,16,13,47,59));
		notEqualProjectHistory.addRevision(notEqualRevision);
		
		//Project
		project = new Project();
		equalProject = new Project();
		notEqualProject = new Project();
		project.setProjectName("Name");
		project.setCurrentVersion("Version");
		project.getProjectHistories().add(projectHistory);
		equalProject.setProjectName("Name");
		equalProject.setCurrentVersion("Version");
		equalProject.getProjectHistories().add(projectHistory);
		notEqualProject.setProjectName("AnotherName");
		notEqualProject.setCurrentVersion("AnotherVersion");
		notEqualProject.getProjectHistories().add(notEqualProjectHistory);
		
		
		//Metrics and its components
		//MethodDescription
		methodDescription = new MethodDescription();
		equalMethodDescription = new MethodDescription();
		notEqualMethodDescription = new MethodDescription();
		methodDescription.setName("MD");
		methodDescription.setCyclomaticComplexity(10);
		equalMethodDescription.setName("MD");
		equalMethodDescription.setCyclomaticComplexity(10);
		notEqualMethodDescription.setName("AnotherMD");
		notEqualMethodDescription.setCyclomaticComplexity(100);
		
		//ClassDescription
		classDescription = new ClassDescription();
		equalClassDescription = new ClassDescription();
		notEqualClassDescription = new ClassDescription();
		classDescription.setName("CD");
		classDescription.setAmc(10);
		classDescription.setCa(10);
		classDescription.setCam(10);
		classDescription.setCbm(10);
		classDescription.setCbo(10);
		classDescription.setCe(10);
		classDescription.setDam(10);
		classDescription.setDit(10);
		classDescription.setIc(10);
		classDescription.setLcom(10);
		classDescription.setLcom3(10);
		classDescription.setLoc(10);
		classDescription.setMfa(10);
		classDescription.setMoa(10);
		classDescription.setNoc(10);
		classDescription.setNpm(10);
		classDescription.setRfc(10);
		classDescription.setWmc(10);
		classDescription.setAverageCC(10);
		classDescription.setMaxCC(10);
		classDescription.setMethod(methodDescription);		
		equalClassDescription.setName("CD");
		equalClassDescription.setAmc(10);
		equalClassDescription.setCa(10);
		equalClassDescription.setCam(10);
		equalClassDescription.setCbm(10);
		equalClassDescription.setCbo(10);
		equalClassDescription.setCe(10);
		equalClassDescription.setDam(10);
		equalClassDescription.setDit(10);
		equalClassDescription.setIc(10);
		equalClassDescription.setLcom(10);
		equalClassDescription.setLcom3(10);
		equalClassDescription.setLoc(10);
		equalClassDescription.setMfa(10);
		equalClassDescription.setMoa(10);
		equalClassDescription.setNoc(10);
		equalClassDescription.setNpm(10);
		equalClassDescription.setRfc(10);
		equalClassDescription.setWmc(10);
		equalClassDescription.setAverageCC(10);
		equalClassDescription.setMaxCC(10);
		equalClassDescription.setMethod(methodDescription);
		notEqualClassDescription.setName("AnotherCD");
		notEqualClassDescription.setAmc(100);
		notEqualClassDescription.setCa(100);
		notEqualClassDescription.setCam(100);
		notEqualClassDescription.setCbm(100);
		notEqualClassDescription.setCbo(100);
		notEqualClassDescription.setCe(100);
		notEqualClassDescription.setDam(100);
		notEqualClassDescription.setDit(100);
		notEqualClassDescription.setIc(100);
		notEqualClassDescription.setLcom(100);
		notEqualClassDescription.setLcom3(100);
		notEqualClassDescription.setLoc(100);
		notEqualClassDescription.setMfa(100);
		notEqualClassDescription.setMoa(100);
		notEqualClassDescription.setNoc(100);
		notEqualClassDescription.setNpm(100);
		notEqualClassDescription.setRfc(100);
		notEqualClassDescription.setWmc(100);
		notEqualClassDescription.setAverageCC(100);
		notEqualClassDescription.setMaxCC(100);
		notEqualClassDescription.setMethod(notEqualMethodDescription);
	
	}
	
	//Equal Objects test
	@Test
	public void testClassDescriptionEquals() throws Exception {
		assertEquals(classDescription, equalClassDescription);
	}
	
	@Test
	public void testIssueDescriptionEquals() throws Exception {
		assertEquals(issueDescription, equalIssueDescription);
	}
	
	@Test
	public void testMethodDescriptionEquals() throws Exception {
		assertEquals(methodDescription, equalMethodDescription);
        }
	
	@Test
	public void testProjectEquals() throws Exception {
		assertEquals(project, equalProject);
	}
	
	@Test
	public void testProjectHistoryEquals() throws Exception {
		assertEquals(projectHistory, equalProjectHistory);
	}
	
	@Test
	public void testRevisionEquals() throws Exception {
		assertEquals(revision, equalRevision);
	}
	
	//Not Equal Objects test
	@Test
	public void testClassDescriptionEqualsError() throws Exception {
		assertFalse(classDescription.equals(notEqualClassDescription));
	}
	
	@Test
	public void testIssueDescriptionEqualsError() throws Exception {
		assertFalse(issueDescription.equals(notEqualIssueDescription));
	}
	
	@Test
	public void testMethodDescriptionEqualsError() throws Exception {
		assertFalse(methodDescription.equals(notEqualMethodDescription));
	}
	
	
	@Test
	public void testProjectEqualsError() throws Exception {
		assertFalse(project.equals(notEqualProject));
	}
	
	@Test
	public void testProjectHistoryEqualsError() throws Exception {
		assertFalse(projectHistory.equals(notEqualProjectHistory));
	}
	
	@Test
	public void testRevisionEqualsError() throws Exception {
		assertFalse(revision.equals(notEqualRevision));
	}
}
