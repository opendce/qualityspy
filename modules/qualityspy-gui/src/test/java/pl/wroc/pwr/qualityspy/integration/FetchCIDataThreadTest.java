/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wroc.pwr.qualityspy.integration;

import org.easymock.EasyMock;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import pl.wroc.pwr.qualityspy.ci.connector.ICIConnector;
import pl.wroc.pwr.qualityspy.ci.factory.CIFactory;
import pl.wroc.pwr.qualityspy.common.CIProperties;
import pl.wroc.pwr.qualityspy.common.Properties;
import pl.wroc.pwr.qualityspy.model.Build;
import pl.wroc.pwr.qualityspy.model.Job;
import pl.wroc.pwr.qualityspy.model.ProjectHistory;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import pl.wroc.pwr.qualityspy.model.Interrupter;

/**
 *
 * @author root
 */
public class FetchCIDataThreadTest {
    
    public FetchCIDataThreadTest() {
    }

    private MockIntegrationServer mockIntegrationServer;
    private Properties mProperties;
    
    @Before
    public void setUp() throws IOException {
        mockIntegrationServer = new MockIntegrationServer();
        mockIntegrationServer.setUp();
        mProperties = EasyMock.createNiceMock(Properties.class);
    }
    
    @After
    public void tearDown() throws IOException {
        mockIntegrationServer.tearDown();
    }

    /**
     * Test of fetcherRun method, of class FetchCIDataThread.
     */
    @Test
    public void testFetcherRun() throws Exception {
        mockIntegrationServer.prepareResponse(Arrays.asList("hudson/hudson_ok_job.txt", "hudson/hudson_failed_build.txt", "hudson/hudson_ok_build.txt", "hudson/hudson_unstable_build.txt"));

        EasyMock.expect(mProperties.getCIServerType()).andReturn(CIProperties.CIServerType.Hudson);
        EasyMock.expect(mProperties.getCIJobName()).andReturn("QualitySpy");
        EasyMock.expect(mProperties.getCIServerLogin()).andReturn("");
        EasyMock.expect(mProperties.getCIServerPassword()).andReturn("");
        String uri = "http://localhost:8282";
        EasyMock.expect(mProperties.getCIServerUrl()).andReturn(uri);
        
        EasyMock.replay(mProperties);
        ICIConnector connector = new CIFactory().Create(mProperties);
        Job jobActual = connector.getJob(mProperties, new Interrupter());

        Job jobExpected = prepareJob();
        
        Assert.assertEquals(jobExpected.toString(), jobActual.toString());
        
        ProjectHistory version = new ProjectHistory();
        version.addJob(jobActual);
        Assert.assertEquals(version.getJobs().toArray()[0], jobActual);
    }

    public static Job prepareJob() {
        String uri = String.format("http://localhost:%d", 8282);
        Job jobExpected = new Job("QualitySpy", String.format("%s/job/QualitySpy/", uri), "red");
        List<Build> builds = new ArrayList<>();
        builds.add(new Build(175,
                LocalDateTime.ofEpochSecond(1457780161762L, 0, ZoneOffset.UTC),
                "FAILURE",
                125,
                0,
                0,
                0));

        builds.add(new Build(174,
                LocalDateTime.ofEpochSecond(1435649705000L, 0, ZoneOffset.UTC),
                "SUCCESS",
                125,
                1,
                0,
                -1));
        builds.add(new Build(148,
                LocalDateTime.ofEpochSecond(1432751214000L, 0, ZoneOffset.UTC),
                "UNSTABLE",
                124,
                0,
                1,
                0));
        jobExpected.setBuilds(builds);
        return jobExpected;
    }
}