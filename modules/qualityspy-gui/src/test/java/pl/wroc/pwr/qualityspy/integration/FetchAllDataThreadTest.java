/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wroc.pwr.qualityspy.integration;

import org.easymock.EasyMock;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import pl.wroc.pwr.qualityspy.ci.connector.ICIConnector;
import pl.wroc.pwr.qualityspy.ci.factory.CIFactory;
import pl.wroc.pwr.qualityspy.common.CIProperties;
import pl.wroc.pwr.qualityspy.common.Properties;
import pl.wroc.pwr.qualityspy.model.Interrupter;
import pl.wroc.pwr.qualityspy.model.Job;
import pl.wroc.pwr.qualityspy.model.Project;
import pl.wroc.pwr.qualityspy.model.ProjectHistory;

import java.io.IOException;
import java.util.Arrays;

/**
 *
 * @author root
 */
public class FetchAllDataThreadTest {
    
    public FetchAllDataThreadTest() {
    }

    private final static int CIsocket = 8282;
    private MockIntegrationServer mockIntegrationServer;
    private Properties mProperties;

    @Before
    public void setUp() throws IOException {
        mockIntegrationServer = new MockIntegrationServer();
        mockIntegrationServer.setUp();
        mProperties = EasyMock.createNiceMock(Properties.class);
    }

    @After
    public void tearDown() throws IOException {
        mockIntegrationServer.tearDown();
    }

    /**
     * Test of fetcherRun method, of class FetchAllDataThread.
     */
    @Test
    public void testFetcherRun() throws Exception {
        // Set mock return values
        EasyMock.expect(mProperties.getCIServerType()).andReturn(CIProperties.CIServerType.Hudson);
        EasyMock.expect(mProperties.getCIJobName()).andReturn("QualitySpy");
        EasyMock.expect(mProperties.getCIServerLogin()).andReturn("");
        EasyMock.expect(mProperties.getCIServerPassword()).andReturn("");
        String uri = "http://localhost:" + CIsocket;
        EasyMock.expect(mProperties.getCIServerUrl()).andReturn(uri);

        // Replay mock - all above settings will affect
        EasyMock.replay(mProperties);
        ProjectHistory newVersion = new ProjectHistory();
        Project project = new Project();
        
        // TODO: IRepositoryConnector test...
        
        // TODO: MetricConnectorStarter test...
        
        // TODO: IBugTrackerConnector test...

        mockIntegrationServer.prepareResponse(Arrays.asList("hudson/hudson_ok_job.txt", "hudson/hudson_failed_build.txt", "hudson/hudson_ok_build.txt", "hudson/hudson_unstable_build.txt"));

        ICIConnector connector = new CIFactory().Create(mProperties);
        Job jobActual = connector.getJob(mProperties, new Interrupter());

        Job jobExpected = FetchCIDataThreadTest.prepareJob();
        
        Assert.assertEquals(jobExpected.toString(), jobActual.toString());
        
        newVersion.addJob(jobActual);
        Assert.assertEquals(jobActual, newVersion.getJobs().toArray()[0]);
        
        
        project.getProjectHistories().add(newVersion);
        Assert.assertTrue(project.getProjectHistories().contains(newVersion));
    }
}