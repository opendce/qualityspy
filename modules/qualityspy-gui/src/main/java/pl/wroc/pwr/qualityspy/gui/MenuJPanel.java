/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.wroc.pwr.qualityspy.gui;

import org.apache.log4j.Logger;
import pl.wroc.pwr.qualityspy.common.ConfigurationManager;
import pl.wroc.pwr.qualityspy.common.Properties;
import pl.wroc.pwr.qualityspy.context.AppContextProvider;
import pl.wroc.pwr.qualityspy.integration.FetchCIDataThread;
import pl.wroc.pwr.qualityspy.integration.FetchCkjmDataThread;
import pl.wroc.pwr.qualityspy.integration.FetchDataDialog;
import pl.wroc.pwr.qualityspy.integration.FetchDataThread;
import pl.wroc.pwr.qualityspy.integration.FetchGitDataThread;
import pl.wroc.pwr.qualityspy.integration.FetchJiraDataThread;
import pl.wroc.pwr.qualityspy.integration.FetchSvnDataThread;
import pl.wroc.pwr.qualityspy.model.Interrupter;
import pl.wroc.pwr.qualityspy.services.ProjectService;

import javax.swing.*;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.Arrays;

/**
 * @author Pit
 */
public class MenuJPanel extends javax.swing.JPanel {
    static final String separator = "/";
    static final String PATH = separator + "images" + separator;
    private ProjectJPanel projectPanel;
    private ConfigJPanel configPanel;
    private String selectedProject;
    private String selectedVersion;
    private JMenu project;
    private JMenu version;
    private JMenu fetch;
    private JMenu settings;
    private JMenu help;
    private JMenuItem newProject;
    private JMenuItem copyProject;
    private JMenuItem deleteProject;
    private JMenuItem importProject;
    private JMenuItem exitProject;
    private JMenuItem newVersion;
    private JMenuItem saveVersion;
    private JMenuItem copyVersion;
    private JMenuItem deleteVersion;
    private JMenuItem vcFetch;
    private JMenuItem metricsFetch;
    private JMenuItem issueFetch;
    private JMenuItem ciFetch;
    private JMenuItem dbConnectionSettings;
    private JMenuItem qualityHelp;
    private JMenuItem aboutHelp;

    /**
     * Creates new form MenuJPanel
     */
    public MenuJPanel() {
        initComponents();
        init();
    }

    private void init() {
        initMenu();
        createMenu();
        addProjectListener();
        addVersionListener();
        addSettingsListener();
        addRepositoryListener();
        addHelpListener();
        setToolTipButton();
        enableButton(false);
        setImageButton();
    }

    private void initMenu() {
        project = new JMenu("Project");
        version = new JMenu("Version");
        fetch = new JMenu("Fetch");
        settings = new JMenu("Settings");
        help = new JMenu("Help");
        newProject = new JMenuItem("New");
        copyProject = new JMenuItem("Copy");
        deleteProject = new JMenuItem("Delete");
        importProject = new JMenuItem("Import");
        exitProject = new JMenuItem("Exit");
        newVersion = new JMenuItem("New");
        saveVersion = new JMenuItem("Save");
        copyVersion = new JMenuItem("Copy");
        deleteVersion = new JMenuItem("Delete");
        vcFetch = new JMenuItem("Version Control");
        metricsFetch = new JMenuItem("Metrics (CKJM)");
        issueFetch = new JMenuItem("Issue Tracker");
        ciFetch = new JMenuItem("Continous Integration");
        dbConnectionSettings = new JMenuItem("Database Connection");
        qualityHelp = new JMenuItem("Quality Spy Help");
        aboutHelp = new JMenuItem("About");
    }

    private void setImageButton() {
        URL url = getClass().getResource(PATH + "NewProjectIcon.png");
        if (url != null) {
            ImageIcon imgNewProject = new javax.swing.ImageIcon(url);
            newProjectButton.setText("");
            newProjectButton.setIcon(imgNewProject);
        }

        url = getClass().getResource(PATH + "CopyProjectIcon.png");
        if (url != null) {
            ImageIcon imgCopyProject = new javax.swing.ImageIcon(url);
            copyProjectButton.setText("");
            copyProjectButton.setIcon(imgCopyProject);
        }

        url = getClass().getResource(PATH + "deleteIcon.png");
        if (url != null) {
            ImageIcon imgDeleteProject = new javax.swing.ImageIcon(url);
            deleteProjectButton.setText("");
            deleteProjectButton.setIcon(imgDeleteProject);
        }

        url = getClass().getResource(PATH + "NewVersionIcon.png");
        if (url != null) {
            ImageIcon imgVersionProject = new javax.swing.ImageIcon(url);
            newVersionButton.setText("");
            newVersionButton.setIcon(imgVersionProject);
        }

        url = getClass().getResource(PATH + "CopyVersionIcon.png");
        if (url != null) {
            ImageIcon imgCopyVersion = new javax.swing.ImageIcon(url);
            copyVersionButton.setText("");
            copyVersionButton.setIcon(imgCopyVersion);
        }

        url = getClass().getResource(PATH + "DeleteVersionIcon.png");
        if (url != null) {
            ImageIcon imgDeleteVersion = new javax.swing.ImageIcon(url);
            deleteVersionButton.setText("");
            deleteVersionButton.setIcon(imgDeleteVersion);
        }

        url = getClass().getResource(PATH + "SaveIcon.png");
        if (url != null) {
            ImageIcon imgSaveVersion = new javax.swing.ImageIcon(url);
            saveVersionButton.setText("");
            saveVersionButton.setIcon(imgSaveVersion);
        }

        url = getClass().getResource(PATH + "FetchSvnIcon.png");
        if (url != null) {
            ImageIcon imgSvnFetch = new javax.swing.ImageIcon(url);
            svnFetchButton.setText("");
            svnFetchButton.setIcon(imgSvnFetch);
        }

        url = getClass().getResource(PATH + "FetchCkjmIcon.png");
        if (url != null) {
            ImageIcon imgCkjmFetch = new javax.swing.ImageIcon(url);
            ckjmFetchButton.setText("");
            ckjmFetchButton.setIcon(imgCkjmFetch);
        }

        url = getClass().getResource(PATH + "FetchJiraIcon.png");
        if (url != null) {
            ImageIcon imgJiraFetch = new javax.swing.ImageIcon(url);
            jiraFetchButton.setText("");
            jiraFetchButton.setIcon(imgJiraFetch);
        }

        url = getClass().getResource(PATH + "FetchCiIcon.png");
        if (url != null) {
            ImageIcon imgCiFetch = new javax.swing.ImageIcon(url);
            ciFetchButton.setText("");
            ciFetchButton.setIcon(imgCiFetch);
        }

        url = getClass().getResource(PATH + "HelpIcon.png");
        if (url != null) {
            ImageIcon imgHelp = new javax.swing.ImageIcon(url);
            helpButton.setText("");
            helpButton.setIcon(imgHelp);
        }
    }

    private void createMenu() {
        JMenuBar menuBar = new JMenuBar();
        menuBar.add(project);
        menuBar.add(version);
        menuBar.add(fetch);
        menuBar.add(settings);
        menuBar.add(help);

        project.add(newProject);
        project.add(copyProject);
        project.add(deleteProject);
        project.add(importProject);
        project.add(exitProject);

        version.add(newVersion);
        version.add(saveVersion);
        version.add(copyVersion);
        version.add(deleteVersion);

        fetch.add(vcFetch);
        fetch.add(metricsFetch);
        fetch.add(issueFetch);
        fetch.add(ciFetch);

        settings.add(dbConnectionSettings);

        help.add(qualityHelp);
        help.add(aboutHelp);

        jPanel3.setLayout(new BorderLayout());
        jPanel3.add(menuBar, BorderLayout.NORTH);
    }

    private void enableButton(boolean enable) {
        deleteProject.setEnabled(enable);
        copyProject.setEnabled(enable);
        newVersion.setEnabled(enable);
        saveVersion.setEnabled(enable);
        copyVersion.setEnabled(enable);
        deleteVersion.setEnabled(enable);
        setEnabledRepositoryButton(enable);
    }

    private void setToolTipButton() {
        newProjectButton.setToolTipText("Create new project");
        copyProjectButton.setToolTipText("Copy selected project");
        deleteProjectButton.setToolTipText("Delete selected project");
        newVersionButton.setToolTipText("Create new version");
        copyVersionButton.setToolTipText("Copy version");
        saveVersionButton.setToolTipText("Save version");
        deleteVersionButton.setToolTipText("Delete version");
        svnFetchButton.setToolTipText("Fetch SVN/GIT");
        ckjmFetchButton.setToolTipText("Fetch CKJM");
        jiraFetchButton.setToolTipText("Fetch JIRA");
        ciFetchButton.setToolTipText("Fetch CI");
        helpButton.setToolTipText("Help");
    }

    private void addHelpListener() {
        qualityHelp.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {

                helpButtonActionPerformed(evt);
            }
        });

        aboutHelp.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showAbout();
            }
        });
    }

    private void addSettingsListener() {
        dbConnectionSettings.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showDatabaseConnection();
                //throw new UnsupportedOperationException("Not yet implemented");
            }
        });
    }

    private void addVersionListener() {
        newVersion.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newVersionButtonActionPerformed(evt);
            }
        });

        saveVersion.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveVersionButtonActionPerformed(evt);
            }
        });

        copyVersion.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                copyVersionButtonActionPerformed(evt);
            }
        });

        deleteVersion.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteVersionButtonActionPerformed(evt);
            }
        });
    }

    public void addProjectListener() {
        newProject.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newProjectButtonActionPerformed(evt);
            }
        });

        copyProject.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                copyProjectButtonActionPerformed(evt);
            }
        });

        deleteProject.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteProjectButtonActionPerformed(evt);
            }
        });

        importProject.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                importActionPerformed(evt);
            }
        });

        exitProject.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitActionPerformed(evt);

            }
        });
    }

    private void addRepositoryListener() {
        vcFetch.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                svnFetchButtonActionPerformed(evt);
            }
        });

        metricsFetch.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ckjmFetchButtonHandler(evt);
            }
        });

        issueFetch.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jiraFetchButtonActionPerformed(evt);
            }
        });

        ciFetch.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ciFetchButtonActionPerformed(evt);
            }
        });
    }

    void setconfigJPanel(ConfigJPanel configJPanel1) {
        this.configPanel = configJPanel1;
    }

    private void selectedProject() {
        copyProjectButton.setEnabled(true);
        copyProject.setEnabled(true);

        deleteProjectButton.setEnabled(true);
        deleteProject.setEnabled(true);

        configPanel.enableConfiguration(false);

        selectedProject = projectPanel.getProjectTree().getSelectionPath().getLastPathComponent().toString();

        configPanel.createConfigTabs();

        newVersionButton.setEnabled(true);
        newVersion.setEnabled(true);
        copyVersion.setEnabled(false);
        copyVersionButton.setEnabled(false);

        saveVersionButton.setEnabled(false);
        saveVersion.setEnabled(false);

        deleteVersionButton.setEnabled(false);
        deleteVersion.setEnabled(false);
    }

    private void selectedVersion() {
        copyProjectButton.setEnabled(false);
        copyProject.setEnabled(false);

        deleteProjectButton.setEnabled(false);
        deleteProject.setEnabled(false);

        configPanel.enableConfiguration(true);

        selectedProject = projectPanel.getProjectTree().getSelectionPath().getParentPath().getLastPathComponent().toString();
        selectedVersion = projectPanel.getProjectTree().getSelectionPath().getLastPathComponent().toString();

        newVersionButton.setEnabled(true);
        newVersion.setEnabled(true);
        copyVersion.setEnabled(true);
        copyVersionButton.setEnabled(true);

        saveVersionButton.setEnabled(true);
        saveVersion.setEnabled(true);

        deleteVersionButton.setEnabled(true);
        deleteVersion.setEnabled(true);
    }

    private static final String[] browsers = {"firefox", "opera", "konqueror", "epiphany",
            "seamonkey", "galeon", "kazehakase", "mozilla", "netscape"};

    private static void openURL(String url) {
        String osName = System.getProperty("os.name");

        try {
            if (osName.startsWith("Mac OS")) {
                Class<?> fileMgr = Class.forName("com.apple.eio.FileManager");
                Method openURL = fileMgr.getDeclaredMethod("openURL", String.class);
                openURL.invoke(null, url);
            } else if (osName.startsWith("Windows")) {
                Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + url);
            } else { // assume Unix or Linux
                boolean found = false;
                for (String browser : browsers) {
                    if (!found) {
                        found = Runtime.getRuntime().exec(new String[]{"which", browser}).waitFor() == 0;
                        if (found) {
                            Runtime.getRuntime().exec(new String[]{browser, url});
                        }
                    }
                }

                if (!found) {
                    throw new Exception(Arrays.toString(browsers));
                }
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error attempting to launch web browser\n" + e.toString());
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        menuToolBar = new javax.swing.JToolBar();
        menuSeparator = new javax.swing.JToolBar.Separator();
        newProjectButton = new javax.swing.JButton();
        copyProjectButton = new javax.swing.JButton();
        deleteProjectButton = new javax.swing.JButton();
        projectVersionSeparator = new javax.swing.JToolBar.Separator();
        newVersionButton = new javax.swing.JButton();
        saveVersionButton = new javax.swing.JButton();
        copyVersionButton = new javax.swing.JButton();
        deleteVersionButton = new javax.swing.JButton();
        versionFetchSeparator = new javax.swing.JToolBar.Separator();
        svnFetchButton = new javax.swing.JButton();
        ckjmFetchButton = new javax.swing.JButton();
        jiraFetchButton = new javax.swing.JButton();
        ciFetchButton = new javax.swing.JButton();
        versionFetchSeparator1 = new javax.swing.JToolBar.Separator();
        helpButton = new javax.swing.JButton();

        jMenu1.setText("File");
        jMenuBar1.add(jMenu1);

        jMenu2.setText("Edit");
        jMenuBar1.add(jMenu2);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
                jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
                jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 100, Short.MAX_VALUE)
        );

        jPanel3.setLayout(new java.awt.BorderLayout());

        menuToolBar.setFloatable(false);
        menuToolBar.setRollover(true);

        menuSeparator.setBorder(javax.swing.BorderFactory.createCompoundBorder());
        menuToolBar.add(menuSeparator);

        newProjectButton.setText("New");
        newProjectButton.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        newProjectButton.setFocusable(false);
        newProjectButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        newProjectButton.setMaximumSize(new java.awt.Dimension(30, 30));
        newProjectButton.setMinimumSize(new java.awt.Dimension(30, 30));
        newProjectButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        newProjectButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newProjectButtonActionPerformed(evt);
            }
        });
        menuToolBar.add(newProjectButton);

        copyProjectButton.setText("Copy");
        copyProjectButton.setBorder(null);
        copyProjectButton.setEnabled(false);
        copyProjectButton.setMaximumSize(new java.awt.Dimension(30, 30));
        copyProjectButton.setMinimumSize(new java.awt.Dimension(40, 40));
        copyProjectButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                copyProjectButtonActionPerformed(evt);
            }
        });
        menuToolBar.add(copyProjectButton);

        deleteProjectButton.setText("Delete");
        deleteProjectButton.setBorder(null);
        deleteProjectButton.setEnabled(false);
        deleteProjectButton.setFocusable(false);
        deleteProjectButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        deleteProjectButton.setMaximumSize(new java.awt.Dimension(30, 30));
        deleteProjectButton.setMinimumSize(new java.awt.Dimension(30, 30));
        deleteProjectButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        deleteProjectButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteProjectButtonActionPerformed(evt);
            }
        });
        menuToolBar.add(deleteProjectButton);

        projectVersionSeparator.setBorder(javax.swing.BorderFactory.createCompoundBorder());
        menuToolBar.add(projectVersionSeparator);

        newVersionButton.setText("New");
        newVersionButton.setActionCommand("Nadaj wersję\\nużywanemu zestawowi...");
        newVersionButton.setBorder(null);
        newVersionButton.setEnabled(false);
        newVersionButton.setMaximumSize(new java.awt.Dimension(30, 30));
        newVersionButton.setMinimumSize(new java.awt.Dimension(30, 30));
        newVersionButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newVersionButtonActionPerformed(evt);
            }
        });
        menuToolBar.add(newVersionButton);

        saveVersionButton.setText("Save");
        saveVersionButton.setBorder(null);
        saveVersionButton.setEnabled(false);
        saveVersionButton.setMaximumSize(new java.awt.Dimension(30, 30));
        saveVersionButton.setMinimumSize(new java.awt.Dimension(30, 30));
        saveVersionButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveVersionButtonActionPerformed(evt);
            }
        });
        menuToolBar.add(saveVersionButton);

        copyVersionButton.setText("Copy");
        copyVersionButton.setBorder(null);
        copyVersionButton.setEnabled(false);
        copyVersionButton.setFocusable(false);
        copyVersionButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        copyVersionButton.setMaximumSize(new java.awt.Dimension(30, 30));
        copyVersionButton.setMinimumSize(new java.awt.Dimension(30, 30));
        copyVersionButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        copyVersionButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                copyVersionButtonActionPerformed(evt);
            }
        });
        menuToolBar.add(copyVersionButton);

        deleteVersionButton.setText("Delete");
        deleteVersionButton.setBorder(null);
        deleteVersionButton.setEnabled(false);
        deleteVersionButton.setFocusable(false);
        deleteVersionButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        deleteVersionButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        deleteVersionButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteVersionButtonActionPerformed(evt);
            }
        });
        menuToolBar.add(deleteVersionButton);

        versionFetchSeparator.setBorder(javax.swing.BorderFactory.createCompoundBorder());
        menuToolBar.add(versionFetchSeparator);

        svnFetchButton.setText("SVN/GIT");
        svnFetchButton.setActionCommand("svnButton");
        svnFetchButton.setBorder(null);
        svnFetchButton.setEnabled(false);
        svnFetchButton.setFocusable(false);
        svnFetchButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        svnFetchButton.setMaximumSize(new java.awt.Dimension(30, 30));
        svnFetchButton.setMinimumSize(new java.awt.Dimension(30, 30));
        svnFetchButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        svnFetchButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                svnFetchButtonActionPerformed(evt);
            }
        });
        menuToolBar.add(svnFetchButton);

        ckjmFetchButton.setText("CKJM");
        ckjmFetchButton.setBorder(null);
        ckjmFetchButton.setEnabled(false);
        ckjmFetchButton.setFocusable(false);
        ckjmFetchButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ckjmFetchButton.setMaximumSize(new java.awt.Dimension(30, 30));
        ckjmFetchButton.setMinimumSize(new java.awt.Dimension(30, 30));
        ckjmFetchButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        ckjmFetchButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ckjmFetchButtonHandler(evt);
            }
        });
        menuToolBar.add(ckjmFetchButton);

        jiraFetchButton.setText("JIRA");
        jiraFetchButton.setBorder(null);
        jiraFetchButton.setEnabled(false);
        jiraFetchButton.setFocusable(false);
        jiraFetchButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jiraFetchButton.setMaximumSize(new java.awt.Dimension(30, 30));
        jiraFetchButton.setMinimumSize(new java.awt.Dimension(30, 30));
        jiraFetchButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jiraFetchButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jiraFetchButtonActionPerformed(evt);
            }
        });
        menuToolBar.add(jiraFetchButton);

        ciFetchButton.setText("CI");
        ciFetchButton.setToolTipText("Fetch data from continuous integration server");
        ciFetchButton.setBorder(null);
        ciFetchButton.setEnabled(false);
        ciFetchButton.setFocusable(false);
        ciFetchButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        ciFetchButton.setMaximumSize(new java.awt.Dimension(30, 30));
        ciFetchButton.setMinimumSize(new java.awt.Dimension(30, 30));
        ciFetchButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        ciFetchButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ciFetchButtonActionPerformed(evt);
            }
        });
        menuToolBar.add(ciFetchButton);

        versionFetchSeparator1.setBorder(javax.swing.BorderFactory.createCompoundBorder());
        menuToolBar.add(versionFetchSeparator1);

        helpButton.setText("Help");
        helpButton.setBorder(null);
        helpButton.setFocusable(false);
        helpButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        helpButton.setMaximumSize(new java.awt.Dimension(30, 30));
        helpButton.setMinimumSize(new java.awt.Dimension(30, 30));
        helpButton.setVerticalTextPosition(javax.swing.SwingConstants.TOP);
        helpButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                helpButtonActionPerformed(evt);
            }
        });
        menuToolBar.add(helpButton);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(menuToolBar, javax.swing.GroupLayout.DEFAULT_SIZE, 486, Short.MAX_VALUE)
                        .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(menuToolBar, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void projectsTreeValueChanged(TreeSelectionEvent evt) {
        newVersionButton.setEnabled(false);
        newVersion.setEnabled(false);

        if (projectPanel.getProjectTree().getSelectionPath() != null && (projectPanel.getlastSelectedProjectPath() == null ||
                !projectPanel.getProjectTree().getSelectionPath().toString().equals(projectPanel.getlastSelectedProjectPath().toString()))) {

            Object[] path = projectPanel.getProjectTree().getSelectionPath().getPath();
            selectedProject = null;

            if (path.length == 2) { // selected project
                selectedProject();
            } else if (path.length == 3) { //selected project version
                selectedVersion();
            }

            String selectedVersion = path[path.length - 1].toString();
            String selectedProject = path[path.length - 2].toString();

            if (configPanel.getconfigTableChanged()) {
                if (configPanel.questionBoxIfSavingChanges(3)) {
                    configPanel.saveChangesComponent();
                }
            }

            projectPanel.setlastSelectedProjectPath();

            if (configPanel.getConfigTabPaneCount() == 0) {
                configPanel.createConfigTabs();
            }

            if (selectedProject == null || selectedProject.equals("rootz")) {
                try {
                    configPanel.noConfiguration();
                } catch (Exception e) {
                    // TODO: Empty catch block
                }

                configPanel.setVisibleConfigTable(false);
                setEnabledRepositoryButton(false);
            } else {
                configPanel.setVisibleConfigTable(true);
                configPanel.loadConfiguration(selectedProject, selectedVersion);
                configPanel.loadPropertiesToGrid();
                configPanel.getCollectedDataTree().setVisible(true);
                configPanel.collectedManager.BuildTree(configPanel.getCollectedDataTree(), selectedProject, selectedVersion);
                setEnabledRepositoryButton(true);
            }
        }
    }

    private void setEnabledRepositoryButton(boolean enable) {
        svnFetchButton.setEnabled(enable);
        ckjmFetchButton.setEnabled(enable);
        jiraFetchButton.setEnabled(enable);
        ciFetchButton.setEnabled(enable);
        vcFetch.setEnabled(enable);
        metricsFetch.setEnabled(enable);
        issueFetch.setEnabled(enable);
        ciFetch.setEnabled(enable);
    }

    void setProjectJPanel(ProjectJPanel projectPanel) {
        this.projectPanel = projectPanel;
        javax.swing.event.TreeSelectionListener tsl = new javax.swing.event.TreeSelectionListener() {

            @Override
            public void valueChanged(TreeSelectionEvent evt) {
                projectsTreeValueChanged(evt);
            }
        };

        projectPanel.addProjectTreeListener(tsl);
    }

    private void showAbout() {
        About about = new About();
        about.setVisible(true);
    }

    private void showDatabaseConnection() {
        try {
            DatabaseConnection dbConnection = new DatabaseConnection();
            dbConnection.setModal(true);
            dbConnection.setVisible(true);
        } catch (IOException ex) {
            Logger.getLogger(MenuJPanel.class.getName()).fatal(null, ex);
        }
    }

    private void importActionPerformed(java.awt.event.ActionEvent evt) {
        JFileChooser chooser = new JFileChooser();
        chooser.setCurrentDirectory(new java.io.File("."));
        chooser.setDialogTitle("select Project");
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.showOpenDialog(this);
        String dir = chooser.getSelectedFile().getParent();

        try {
            configPanel.getConfigurationManager().importProject(dir, chooser.getSelectedFile().getName());
            configPanel.loadListOfConfigs();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this.getRootPane(), "Selected project name exist!");
        }
    }

    private void exitActionPerformed(java.awt.event.ActionEvent evt) {
        Runtime.getRuntime().exit(0);
    }

    private void newProjectButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newProjectButtonActionPerformed
        if (configPanel.getconfigTableChanged()) {
            if (configPanel.questionBoxIfSavingChanges(2)) {
                configPanel.saveChangesComponent();
            }
        }

        String newNameProject = JOptionPane.showInputDialog(this.getRootPane(), "Please enter project name", "");

        if (newNameProject != null && newNameProject.length() > 0) {
            String projectVersion = JOptionPane.showInputDialog(this.getRootPane(), "Please enter version", "");

            if (projectVersion != null && !projectVersion.equals("")) {
                configPanel.configureManager(newNameProject, projectVersion);
            }
        }
    }//GEN-LAST:event_newProjectButtonActionPerformed

    private void copyProjectButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_copyProjectButtonActionPerformed
        if (configPanel.getconfigTableChanged()) {
            if (configPanel.questionBoxIfSavingChanges(2)) {
                configPanel.saveChangesOfComponent();
            }
        }

        String newProjectName = JOptionPane.showInputDialog(this.getRootPane(), "Enter name for the new project.", "");

        if (newProjectName != null && newProjectName.length() > 0) {
            if (selectedProject != null) {
                for (String versionProject : configPanel.getConfigurationManager().getListOfVersions(selectedProject)) {

                    ConfigurationManager configurationManager = new ConfigurationManager();
                    configurationManager.setSelectedProjAndVer(selectedProject, versionProject);

                    try {
                        configurationManager.savePropertiesAs(newProjectName, versionProject);
                    } catch (IOException ex) {
                        // TODO: Empty catch block
                    }
                }

                configPanel.loadListOfConfigs();

                JOptionPane.showMessageDialog(this.getRootPane(), "The copy was created!");
            }
        }
    }//GEN-LAST:event_copyProjectButtonActionPerformed

    private void deleteProjectButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteProjectButtonActionPerformed
        final TreePath path = projectPanel.getProjectTree().getSelectionPath();

        if (path.getPathCount() >= 2) {
            if (!path.getPathComponent(path.getPathCount() - 1).toString().contains("<u>")) {

                if (JOptionPane.showConfirmDialog(this.getRootPane(),
                        "Erasing this element will also erase group: \""
                                + path.getPathComponent(1) + "\".\nDo you want to proceed?",
                        "Confirmation",
                        JOptionPane.YES_NO_OPTION) == 1) {
                } else {
                    final ProjectService projectService =
                            AppContextProvider.getContext().getBean(ProjectService.class);
                    projectService.deleteProject(selectedProject);
                    configPanel.getConfigurationManager().removeProject(selectedProject);
                    configPanel.loadListOfConfigs();
                    configPanel.loadPropertiesToGrid();
                }
            } else {
                JOptionPane.showMessageDialog(this.getRootPane(), "You can not delete this set of properties while using it!",
                        "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_deleteProjectButtonActionPerformed

    private void newVersionButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newVersionButtonActionPerformed
        if (configPanel.getconfigTableChanged()) {
            if (configPanel.questionBoxIfSavingChanges(2)) {
                configPanel.saveChangesOfComponent();
            }
        }

        String version = JOptionPane.showInputDialog(this.getRootPane(), "Enter number of version:", "");

        if (version != null && !version.equals("")) {
            try {
                configPanel.getConfigurationManager().addVersion(selectedProject, version);
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(this.getRootPane(), ex);
            }

            selectedProject = null;
            configPanel.loadListOfConfigs();
            configPanel.loadPropertiesToGrid();
        }
    }//GEN-LAST:event_newVersionButtonActionPerformed

    private void saveVersionButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveVersionButtonActionPerformed
        configPanel.saveChangesOfComponent();
        configPanel.remoweRowConfigPanel(false);
        configPanel.setConfigTab();
    }//GEN-LAST:event_saveVersionButtonActionPerformed

    private void svnFetchButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_svnFetchButtonActionPerformed
        Interrupter interrupted = new Interrupter();
        Properties conf = configPanel.getConfiguration();
        String repoType = conf.getRepotype().toLowerCase();
        FetchDataThread thread;

        try {
            conf.validateRepoParameters();
            conf.getRepoendrev();
            conf.getRepostartrev();

            if (repoType.equals("svn")) {
                thread = new FetchSvnDataThread(conf, interrupted);
            } else {
                thread = new FetchGitDataThread(conf, interrupted);
            }

            FetchDataDialog fetchRevisionDialog = new FetchDataDialog(thread);
            fetchRevisionDialog.startFetchingThread();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,
                    e.getMessage(),
                    "Caught Exception",
                    JOptionPane.WARNING_MESSAGE);
        }
        //}
    }//GEN-LAST:event_svnFetchButtonActionPerformed

    private void ckjmFetchButtonHandler(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ckjmFetchButtonHandler
        Interrupter interrupted = new Interrupter();
        FetchDataDialog fetchRevisionDialog = new FetchDataDialog(new FetchCkjmDataThread(configPanel.getConfiguration(), interrupted));
        fetchRevisionDialog.startFetchingThread();
    }//GEN-LAST:event_ckjmFetchButtonHandler

    private void jiraFetchButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jiraFetchButtonActionPerformed
        Interrupter interrupted = new Interrupter();
        FetchDataDialog fetchRevisionDialog = new FetchDataDialog(new FetchJiraDataThread(configPanel.getConfiguration(), interrupted));
        fetchRevisionDialog.startFetchingThread();
    }//GEN-LAST:event_jiraFetchButtonActionPerformed

    private void ciFetchButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ciFetchButtonActionPerformed
        Interrupter interrupted = new Interrupter();

        try {
            FetchDataDialog fetchRevisionDialog = new FetchDataDialog(new FetchCIDataThread(configPanel.getConfiguration(), interrupted));
            fetchRevisionDialog.startFetchingThread();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this.getRootPane(), ex.toString(), "Fetching failed!", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_ciFetchButtonActionPerformed

    private void copyVersionButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_copyVersionButtonActionPerformed
        if (configPanel.getconfigTableChanged()) {
            if (configPanel.questionBoxIfSavingChanges(2)) {
                configPanel.saveChangesOfComponent();
            }
        }

        String version = JOptionPane.showInputDialog(this.getRootPane(), "Enter number of version:", "");

        if (version != null && !version.equals("")) {
            try {
                configPanel.getConfigurationManager().savePropertiesAs(selectedProject, version);
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(this.getRootPane(), ex);
            }

            selectedProject = null;
            configPanel.loadListOfConfigs();
            configPanel.loadPropertiesToGrid();
        }
    }//GEN-LAST:event_copyVersionButtonActionPerformed

    private void helpButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_helpButtonActionPerformed
        openURL("https://opendce.atlassian.net/wiki/display/QS");
    }//GEN-LAST:event_helpButtonActionPerformed

    private void deleteVersionButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteVersionButtonActionPerformed
        ProjectService projService = AppContextProvider.getContext().getBean(ProjectService.class);

        if (JOptionPane.showConfirmDialog(this.getRootPane(),
                "Do you want to erase version " + selectedVersion + " of project " + selectedProject + "?",
                "Confirmation",
                JOptionPane.YES_NO_OPTION) == 0) {
                projService.deleteVersion(selectedProject, selectedVersion,configPanel.getConfigurationManager());
                configPanel.loadListOfConfigs();
                configPanel.loadPropertiesToGrid();
        }
    }//GEN-LAST:event_deleteVersionButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton ciFetchButton;
    private javax.swing.JButton ckjmFetchButton;
    private javax.swing.JButton copyProjectButton;
    private javax.swing.JButton copyVersionButton;
    private javax.swing.JButton deleteProjectButton;
    private javax.swing.JButton deleteVersionButton;
    private javax.swing.JButton helpButton;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JButton jiraFetchButton;
    private javax.swing.JToolBar.Separator menuSeparator;
    private javax.swing.JToolBar menuToolBar;
    private javax.swing.JButton newProjectButton;
    private javax.swing.JButton newVersionButton;
    private javax.swing.JToolBar.Separator projectVersionSeparator;
    private javax.swing.JButton saveVersionButton;
    private javax.swing.JButton svnFetchButton;
    private javax.swing.JToolBar.Separator versionFetchSeparator;
    private javax.swing.JToolBar.Separator versionFetchSeparator1;
    // End of variables declaration//GEN-END:variables
}
