package pl.wroc.pwr.qualityspy.integration;

import pl.wroc.pwr.qualityspy.common.Properties;
import pl.wroc.pwr.qualityspy.model.Interrupter;
import pl.wroc.pwr.qualityspy.vc.IRepositoryConnector;
import pl.wroc.pwr.qualityspy.vc.RepositoryConnector;
import pl.wroc.pwr.qualityspy.model.Project;
import pl.wroc.pwr.qualityspy.model.ProjectHistory;

public class FetchGitDataThread extends FetchDataThread {

    public FetchGitDataThread(Properties p) {
        super(p);
        this.interrupted = new Interrupter();
    }
    
    public FetchGitDataThread(Properties p, Interrupter interrupted) {
        super(p);
        this.interrupted = interrupted;
    }
    
    @Override
    protected void fetcherRun(Project project, ProjectHistory version) throws Exception {
        
        IRepositoryConnector git = new RepositoryConnector();
        ProjectHistory newVersion = git.fetchData(config, interrupted);
        if(this.interrupted.getIsInterrupted()) 
            return;
        projectHistoryService.addAllRevisions(newVersion, version, project);
    }
    
}