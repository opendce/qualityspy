/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wroc.pwr.qualityspy.integration;

import pl.wroc.pwr.qualityspy.common.Properties;
import pl.wroc.pwr.qualityspy.model.Interrupter;
import pl.wroc.pwr.qualityspy.vc.IRepositoryConnector;
import pl.wroc.pwr.qualityspy.vc.RepositoryConnector;
import pl.wroc.pwr.qualityspy.model.Project;
import pl.wroc.pwr.qualityspy.model.ProjectHistory;

/**
 * To fetch data only from the version control system.
 */
public class FetchSvnDataThread extends FetchDataThread {
    
    public FetchSvnDataThread(Properties p) {
        super(p);
        this.interrupted = new Interrupter();
    }
    
    public FetchSvnDataThread(Properties p, Interrupter interrupted) {
        super(p);
        this.interrupted = interrupted;
    }
    
    @Override
    protected void fetcherRun(Project project, ProjectHistory version) throws Exception {
        
        IRepositoryConnector svn = new RepositoryConnector();
        ProjectHistory newVersion = svn.fetchData(config, interrupted);
                
        projectHistoryService.addAllRevisions(newVersion, version, project);
    }
}
