/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wroc.pwr.qualityspy.tocheck;

import javax.swing.table.DefaultTableModel;
import javax.swing.tree.DefaultMutableTreeNode;

import pl.wroc.pwr.qualityspy.context.AppContextProvider;
import pl.wroc.pwr.qualityspy.model.*;

/**
 *
 * @author Vlad
 */
public class ProjectDefaultTreeNode extends DefaultMutableTreeNode{
    
    public Object object;
    private Type whatType;
    private CollectedData collectedData = AppContextProvider.getContext().getBean(CollectedData.class);
    
    public enum Type{
        IssueChange, IssueComments, IssueLogs, Job, Revisions, Classes, Issues
    }
    
    public ProjectDefaultTreeNode(){
        super();
    }
    
    public ProjectDefaultTreeNode(Object userObject){
        super(userObject);
        object = userObject;
    }
    
    public ProjectDefaultTreeNode(String name, Object userObject){
        super(name);
        object = userObject;
    }
    
    public ProjectDefaultTreeNode(Object userObject, boolean allowsChildren){
        super(userObject, allowsChildren);
    }
    
    public DefaultTableModel whatToShow(ProjectDefaultTreeNode root) throws Exception{
        if(root.object.getClass() == Job.class){
            return collectedData.getBuilds((Job)root.object, root);
        }
        if(root.object.getClass() == Revision.class){
            return collectedData.getFiles((Revision)root.object, root);
        }
        if(root.object.getClass() == ClassDescription.class){
            return collectedData.getMethods((ClassDescription)root.object, root);
        }
        if(root.object.getClass() == IssueDescription.class){
            if(root.whatType.equals(Type.IssueChange)){
                return collectedData.getIssueChange((IssueDescription)root.object, root);
            }
            if(root.whatType.equals(Type.IssueComments)){
                return collectedData.getIssueComments((IssueDescription)root.object, root);
            }
            if(root.whatType.equals(Type.IssueLogs)){
                return collectedData.getIssueLogs((IssueDescription)root.object, root);
            }
        }
        if(root.object.getClass() == ProjectHistory.class){
            if(root.whatType.equals(Type.Job)){
                return collectedData.getJob((ProjectHistory)root.object, root);
            }
            if(root.whatType.equals(Type.Revisions)){
                return collectedData.getRevisions((ProjectHistory)root.object, root);
            }
            if(root.whatType.equals(Type.Issues)){
                return collectedData.getIssues((ProjectHistory)root.object, root);
            }
            if(root.whatType.equals(Type.Classes)){
                return collectedData.getClasses((ProjectHistory)root.object, root);
            }
        }
        return null;
    }
    public void setType(Type type){
        whatType = type;
    }
    
    

}
