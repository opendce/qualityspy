package pl.wroc.pwr.qualityspy.tocheck;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.wroc.pwr.qualityspy.model.*;
import pl.wroc.pwr.qualityspy.repositories.ProjectHistoryRepository;
import pl.wroc.pwr.qualityspy.services.IssueDescriptionService;
import pl.wroc.pwr.qualityspy.services.ProjectHistoryService;
import pl.wroc.pwr.qualityspy.services.ProjectService;

import javax.swing.table.DefaultTableModel;
import java.util.Iterator;
import java.util.Set;

/**
 * TODO: split building view and fetching data into two different classes and  move the later one to dao package.
 *
 * @author VAIO
 */
@Component
public class CollectedData {

    @Autowired
    private ProjectService projectService;
    @Autowired
    private IssueDescriptionService issueDescriptionService;
    @Autowired
    private ProjectHistoryService projectHistoryService;
    @Autowired
    private ProjectHistoryRepository projectHistoryRepository;


    @Transactional
    public void getProjectHistories(String projectName, String version, ProjectDefaultTreeNode root) {
        ProjectHistory history = projectService.getProjectHistory(projectName, version);

        if (history == null) {
            root.add(new ProjectDefaultTreeNode("Brak danych o historii projektu!"));
        } else {
            root.add(new ProjectDefaultTreeNode("Id: " + history.getId()));
            root.add(new ProjectDefaultTreeNode("Start revision: " + history.getStartRevision()));
            root.add(new ProjectDefaultTreeNode("Date start revision: " + history.getStartRevisionDate()));
            root.add(new ProjectDefaultTreeNode("End revision: " + history.getEndRevision()));
            root.add(new ProjectDefaultTreeNode("Date end revision: " + history.getEndRevisionDate()));


            ProjectDefaultTreeNode subnodeJob = new ProjectDefaultTreeNode("Job", history);
            subnodeJob.setType(ProjectDefaultTreeNode.Type.Job);
            subnodeJob.add(new ProjectDefaultTreeNode());
            root.add(subnodeJob);

            ProjectDefaultTreeNode subnodeRevision = new ProjectDefaultTreeNode("Revisions", history);
            subnodeRevision.setType(ProjectDefaultTreeNode.Type.Revisions);
            subnodeRevision.add(new ProjectDefaultTreeNode());
            root.add(subnodeRevision);

            ProjectDefaultTreeNode subnodeClasses = new ProjectDefaultTreeNode("Classes", history);
            subnodeClasses.setType(ProjectDefaultTreeNode.Type.Classes);
            subnodeClasses.add(new ProjectDefaultTreeNode());
            root.add(subnodeClasses);

            ProjectDefaultTreeNode subnodeIssue = new ProjectDefaultTreeNode("Issues", history);
            subnodeIssue.setType(ProjectDefaultTreeNode.Type.Issues);
            subnodeIssue.add(new ProjectDefaultTreeNode());
            root.add(subnodeIssue);
        }
    }

    @Transactional
    public DefaultTableModel getJob(ProjectHistory history, ProjectDefaultTreeNode root) {
        root.removeAllChildren();
        Set<Job> jobs = projectHistoryService.getJobs(history);
        if (jobs.isEmpty()) {
            root.add(new ProjectDefaultTreeNode("Brak danych o job projektu!"));
        }

        for (Job job : jobs) {
            ProjectDefaultTreeNode node = new ProjectDefaultTreeNode("Name of jobs: " + job.getName());//history.getJob().getName());
            root.add(node);
            node.add(new ProjectDefaultTreeNode("Id: " + job.getId()));
            node.add(new ProjectDefaultTreeNode("Color: " + job.getColor()));
            node.add(new ProjectDefaultTreeNode("Url: " + job.getUrl()));

            ProjectDefaultTreeNode subnode = new ProjectDefaultTreeNode("Builds", job);
            subnode.add(new ProjectDefaultTreeNode());
            node.add(subnode);
        }
        return null;
    }

    @Transactional
    public DefaultTableModel getRevisions(ProjectHistory history, ProjectDefaultTreeNode root) {
        root.removeAllChildren();
        Set<Revision> revisons = projectHistoryService.getRevisions(history);
        if (revisons.isEmpty()) {
            root.add(new ProjectDefaultTreeNode("Brak danych o revision projektu!"));
        }

        for (Revision revision : revisons) {
            ProjectDefaultTreeNode node = new ProjectDefaultTreeNode("Revision number: " + revision.getNumber());
            root.add(node);
            node.add(new ProjectDefaultTreeNode("Id: " + revision.getId()));
            node.add(new ProjectDefaultTreeNode("Author: " + revision.getAuthor()));
            node.add(new ProjectDefaultTreeNode("Branch: " + revision.getBranch()));
            node.add(new ProjectDefaultTreeNode("Comment: " + revision.getComment()));
            node.add(new ProjectDefaultTreeNode("Date: " + revision.getDate()));
            node.add(new ProjectDefaultTreeNode("Tags: " + revision.getTags()));

            ProjectDefaultTreeNode subnode = new ProjectDefaultTreeNode("Files", revision);
            subnode.add(new ProjectDefaultTreeNode());
            node.add(subnode);

        }
        return null;
    }

    @Transactional
    public DefaultTableModel getClasses(ProjectHistory history, ProjectDefaultTreeNode root) {
        root.removeAllChildren();
        Set<ClassDescription> classes = projectHistoryService.getClasses(history);
        if (classes.isEmpty()) {
            root.add(new ProjectDefaultTreeNode("Brak danych o classes projektu!"));
        }

        for (ClassDescription class_description : classes) {
            ProjectDefaultTreeNode node = new ProjectDefaultTreeNode("Class description id: " + class_description.getId());
            root.add(node);
            node.add(new ProjectDefaultTreeNode("Amc: " + class_description.getAmc()));
            node.add(new ProjectDefaultTreeNode("AverageCC: " + class_description.getAverageCC()));
            node.add(new ProjectDefaultTreeNode("Ca: " + class_description.getCa()));
            node.add(new ProjectDefaultTreeNode("Cam: " + class_description.getCam()));
            node.add(new ProjectDefaultTreeNode("Cbm: " + class_description.getCbm()));
            node.add(new ProjectDefaultTreeNode("Cbo: " + class_description.getCbo()));
            node.add(new ProjectDefaultTreeNode("Ce: " + class_description.getCe()));
            node.add(new ProjectDefaultTreeNode("Dam: " + class_description.getDam()));
            node.add(new ProjectDefaultTreeNode("Dit: " + class_description.getDit()));
            node.add(new ProjectDefaultTreeNode("Ic: " + class_description.getIc()));
            node.add(new ProjectDefaultTreeNode("Lcom: " + class_description.getLcom()));
            node.add(new ProjectDefaultTreeNode("Lcom3: " + class_description.getLcom3()));
            node.add(new ProjectDefaultTreeNode("Loc: " + class_description.getLoc()));
            node.add(new ProjectDefaultTreeNode("MaxCC: " + class_description.getMaxCC()));
            node.add(new ProjectDefaultTreeNode("Mfa: " + class_description.getMfa()));
            node.add(new ProjectDefaultTreeNode("Moa: " + class_description.getMoa()));
            node.add(new ProjectDefaultTreeNode("Noc: " + class_description.getNoc()));
            node.add(new ProjectDefaultTreeNode("Npm: " + class_description.getNpm()));
            node.add(new ProjectDefaultTreeNode("Rfc: " + class_description.getRfc()));
            node.add(new ProjectDefaultTreeNode("Wmc: " + class_description.getWmc()));

            ProjectDefaultTreeNode subnode = new ProjectDefaultTreeNode("Methods", class_description);
            subnode.add(new ProjectDefaultTreeNode());
            node.add(subnode);
        }
        return null;
    }

    @Transactional
    public DefaultTableModel getIssues(ProjectHistory history, ProjectDefaultTreeNode root) {
        root.removeAllChildren();
        history = projectHistoryRepository.findOne(history.getId());
        Set<IssueDescription> issues = history.getIssues();
        if (issues.isEmpty()) {
            root.add(new ProjectDefaultTreeNode("Brak danych o issue projektu!"));
        }

        for (IssueDescription issue_description : issues) {
            ProjectDefaultTreeNode node = new ProjectDefaultTreeNode("Issue description id: " + issue_description.getId());
            root.add(node);
            node.add(new ProjectDefaultTreeNode("Affects version" + issue_description.getAffectsVersion()));
            node.add(new ProjectDefaultTreeNode("Assign to: " + issue_description.getAssignTo()));
            node.add(new ProjectDefaultTreeNode(FormatAttachmentsList.formatAttachmentsList(issue_description)));
            node.add(new ProjectDefaultTreeNode("Complexity: " + issue_description.getComplexity()));
            node.add(new ProjectDefaultTreeNode("Component: " + issue_description.getComponent()));
            node.add(new ProjectDefaultTreeNode("Created: " + issue_description.getCreated()));
            node.add(new ProjectDefaultTreeNode("Description: " + issue_description.getDescription()));
            node.add(new ProjectDefaultTreeNode("Environment: " + issue_description.getEnvironment()));
            node.add(new ProjectDefaultTreeNode("Fix version: " + issue_description.getFixVersion()));
            node.add(new ProjectDefaultTreeNode("Issue key: " + issue_description.getIssueKey()));
            node.add(new ProjectDefaultTreeNode("Labels: " + issue_description.getLabels()));
            node.add(new ProjectDefaultTreeNode("Original estimate: " + issue_description.getOriginalEstimate()));
            node.add(new ProjectDefaultTreeNode("Parent key: " + issue_description.getParentKey()));
            node.add(new ProjectDefaultTreeNode("Priority: " + issue_description.getPriority()));
            node.add(new ProjectDefaultTreeNode("Project: " + issue_description.getProject()));
            node.add(new ProjectDefaultTreeNode("Remaining estimate: " + issue_description.getRemainingEstimate()));
            node.add(new ProjectDefaultTreeNode("Reporter: " + issue_description.getReporter()));
            node.add(new ProjectDefaultTreeNode("Resolution: " + issue_description.getResolution()));
            node.add(new ProjectDefaultTreeNode("Resolved: " + issue_description.getResolved()));
            node.add(new ProjectDefaultTreeNode("Severity: " + issue_description.getSeverity()));
            node.add(new ProjectDefaultTreeNode("Status: " + issue_description.getStatus()));
            node.add(new ProjectDefaultTreeNode("Summary: " + issue_description.getSummary()));
            node.add(new ProjectDefaultTreeNode("Time spend: " + issue_description.getTimeSpend()));
            node.add(new ProjectDefaultTreeNode("Type: " + issue_description.getType()));
            node.add(new ProjectDefaultTreeNode("Updated: " + issue_description.getUpdated()));
            node.add(new ProjectDefaultTreeNode("Value: " + issue_description.getValue()));

            ProjectDefaultTreeNode subnodeIssueChange = new ProjectDefaultTreeNode("Issue Change", issue_description);
            subnodeIssueChange.setType(ProjectDefaultTreeNode.Type.IssueChange);
            subnodeIssueChange.add(new ProjectDefaultTreeNode());
            node.add(subnodeIssueChange);
            ProjectDefaultTreeNode subnodeIssueComments = new ProjectDefaultTreeNode("Issue Comments", issue_description);
            subnodeIssueComments.setType(ProjectDefaultTreeNode.Type.IssueComments);
            subnodeIssueComments.add(new ProjectDefaultTreeNode());
            node.add(subnodeIssueComments);
            ProjectDefaultTreeNode subnodeIssueLogs = new ProjectDefaultTreeNode("Issue Logs", issue_description);
            subnodeIssueLogs.setType(ProjectDefaultTreeNode.Type.IssueLogs);
            subnodeIssueLogs.add(new ProjectDefaultTreeNode());
            node.add(subnodeIssueLogs);
        }
        return null;
    }

    private static class FormatAttachmentsList extends CollectedData {
        private static String formatAttachmentsList(IssueDescription issue_description) {
            String attachmentsList = "", attachment, row = "</td><td>";
            IssueAttachment issueAttachment;
            if (issue_description.getIssueAttachments().isEmpty()) {
                attachmentsList = "Attachments: none";
            } else {
                attachmentsList = "<html>Attachments:<table><tr><td>type:</td><td>name:</td><td>size [B]:</td><td>created:</td>";
                Iterator<IssueAttachment> iterator = issue_description.getIssueAttachments().iterator();
                while (iterator.hasNext()) {
                    issueAttachment = iterator.next();
                    attachment = "<tr><td>" + issueAttachment.getMimeType() +
                            row + issueAttachment.getFileName() +
                            row + issueAttachment.getSize() +
                            row + issueAttachment.getCreationDate() +
                            "</td></tr>";
                    attachmentsList += attachment;
                }
                attachmentsList += "</table></html>";
            }
            return attachmentsList;
        }
    }


    //TABELE-----------------------------------------------------------------------------------------------------------------------------------    

    @Transactional
    public DefaultTableModel getBuilds(Job job, ProjectDefaultTreeNode root) {
        String[] names = {"Id", "Date", "Build number", "Fail count", "Fail diff", "Result", "Total count", "Total diff"};
        DefaultTableModel model = new DefaultTableModel(new Object[][]{}, names);
        for (Build build : job.getBuilds()) {
            model.addRow(new Object[]{
                    build.getId().toString(),
                    build.getDate().toString(),
                    build.getNumber().toString(),
                    build.getFailCount().toString(),
                    build.getFailDiff().toString(),
                    build.getResult(),
                    build.getTotalCount().toString(),
                    build.getTotalDiff().toString()
            });
        }
        return model;
    }

    @Transactional
    public DefaultTableModel getFiles(Revision revision, ProjectDefaultTreeNode root) {
        String[] names = {"Id", "Change type", "File Extension", "File type", "New content", "New path",
                "Number of added lines", "Number of removed lines", "Old content", "Old path"};
        DefaultTableModel model = new DefaultTableModel(new Object[][]{}, names);
        for (File file : revision.getFiles()) {
            model.addRow(new Object[]{
                    revision.getId().toString(),
                    file.getChangeType().toString(),
                    file.getFileExtension(),
                    file.getFileType().toString(),
                    file.getNewContent(),
                    file.getNewPath(),
                    Long.valueOf(file.getNumberOfAddedLines()).toString(),
                    Long.valueOf(file.getNumberOfRemovedLines()).toString(),
                    file.getOldContent(),
                    file.getOldPath()
            });
        }
        return model;
    }

    @Transactional
    public DefaultTableModel getMethods(ClassDescription class_description, ProjectDefaultTreeNode root) {
        String[] names = {"Id", "Method description name", "Cyclomatic complexity"};
        DefaultTableModel model = new DefaultTableModel(new Object[][]{}, names);
        for (MethodDescription method : class_description.getMethods()) {
            model.addRow(new Object[]{
                    method.getId().toString(),
                    method.getName(),
                    Long.valueOf(method.getCyclomaticComplexity()).toString()
            });
        }
        return model;
    }

    @Transactional
    public DefaultTableModel getIssueChange(IssueDescription issue_description, ProjectDefaultTreeNode root) {
        Set<IssueChange> issueChanges = issueDescriptionService.getIssueHistory(issue_description);
        String[] names = {"Id", "Date of change", "New value", "Old value", "Type", "What changed", "Who changed"};
        DefaultTableModel model = new DefaultTableModel(new Object[][]{}, names);
        for (IssueChange issueChange : issueChanges) {
            model.addRow(new Object[]{
                    issueChange.getId().toString(),
                    issueChange.getDateOfChange().toString(),
                    issueChange.getNewValue(),
                    issueChange.getOldValue(),
                    issueChange.getType(),
                    issueChange.getWhatChanged(),
                    issueChange.getWhoChanged()
            });
        }
        return model;
    }

    @Transactional
    public DefaultTableModel getIssueComments(IssueDescription issue_description, ProjectDefaultTreeNode root) {
        Set<IssueComment> issueComments = issueDescriptionService.getIssueComments(issue_description);
        String[] names = {"Id", "Author", "Date", "Comment text"};
        DefaultTableModel model = new DefaultTableModel(new Object[][]{}, names);
        for (IssueComment issueComment : issueComments) {
            model.addRow(new Object[]{
                    issueComment.getId().toString(),
                    issueComment.getAuthor(),
                    issueComment.getDate().toString(),
                    issueComment.getCommentText()
            });
        }
        return model;
    }

    @Transactional
    public DefaultTableModel getIssueLogs(IssueDescription issue_description, ProjectDefaultTreeNode root) {
        Set<IssueLog> issueLogs = issueDescriptionService.getIssueLogs(issue_description);
        String[] names = {"Id", "Comment", "Date", "Status", "Time spent", "Who"};
        DefaultTableModel model = new DefaultTableModel(new Object[][]{}, names);
        for (IssueLog issueLog : issueLogs) {
            model.addRow(new Object[]{
                    issueLog.getId().toString(),
                    issueLog.getComment(),
                    issueLog.getDate().toString(),
                    issueLog.getStatus(),
                    Long.valueOf(issueLog.getTimeSpent()).toString(),
                    issueLog.getWho()
            });
        }
        return model;
    }

}
