/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wroc.pwr.qualityspy.integration;

import org.apache.log4j.Logger;
import pl.wroc.pwr.qualityspy.common.Properties;
import pl.wroc.pwr.qualityspy.metrics.IMetricsConnector;
import pl.wroc.pwr.qualityspy.metrics.MetricConnectorStarter;
import pl.wroc.pwr.qualityspy.model.ClassDescription;
import pl.wroc.pwr.qualityspy.model.Interrupter;
import pl.wroc.pwr.qualityspy.model.Project;
import pl.wroc.pwr.qualityspy.model.ProjectHistory;

import java.util.List;

/**
 *
 */
public class FetchCkjmDataThread extends FetchDataThread {

    private static final Logger LOG = Logger.getLogger(FetchCkjmDataThread.class);
    
    public FetchCkjmDataThread(Properties p) {
        super(p);
        this.interrupted = new Interrupter();
    }
    
    public FetchCkjmDataThread(Properties p, Interrupter interrupted) {
        super(p);
        this.interrupted = interrupted;
    }

    @Override
    protected void fetcherRun(Project project, ProjectHistory version) throws Exception {
            MetricConnectorStarter starter = new MetricConnectorStarter(config, interrupted);
            IMetricsConnector metrics = starter.getMetricConnector();
            List<ClassDescription> result = metrics.getMetric();
            LOG.info("Number of collected classes: " + result.size());
            if(interrupted.getIsInterrupted()) return;
            projectHistoryService.persistClasses(result, version);
    }

    

    
}
