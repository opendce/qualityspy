/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wroc.pwr.qualityspy.integration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import pl.wroc.pwr.qualityspy.common.Properties;
import pl.wroc.pwr.qualityspy.common.SupportedException;
import pl.wroc.pwr.qualityspy.context.AppContextProvider;
import pl.wroc.pwr.qualityspy.errorHandling.ErrorWindow;
import pl.wroc.pwr.qualityspy.errorHandling.Errors;
import pl.wroc.pwr.qualityspy.model.Project;
import pl.wroc.pwr.qualityspy.model.ProjectHistory;
import pl.wroc.pwr.qualityspy.repositories.ProjectRepository;
import pl.wroc.pwr.qualityspy.services.ProjectHistoryService;
import pl.wroc.pwr.qualityspy.services.ProjectService;

import javax.swing.*;
import pl.wroc.pwr.qualityspy.model.Interrupter;

/**
 * Class that should be extended by the threads that cordinate the process of
 * fetching data from different sources e.g. Jira, SVN.
 */

@Slf4j
public abstract class FetchDataThread extends Thread {

    private volatile boolean running = true;
    
    protected JProgressBar progressBar;
    protected JLabel status;
    protected JButton apply;
    protected JButton cancel;

    protected ProjectHistoryService projectHistoryService =
            AppContextProvider.getContext().getBean(ProjectHistoryService.class);
    protected ProjectService projService = AppContextProvider.getContext().getBean(ProjectService.class);
    protected ProjectRepository projectRepository = AppContextProvider.getContext().getBean(ProjectRepository.class);
    protected Properties config;
    
    protected Interrupter interrupted;
    
    public void cancelling() {
        interrupted.setIsInterrupted();
    }

    public FetchDataThread(Properties props) {config = props;}

    public void setGuiComponents(JProgressBar progressBar, JLabel status, JButton apply, JButton cancel) {
        this.progressBar = progressBar;
        this.status = status;
        this.apply = apply;
        this.cancel = cancel;
    }

    @Override
    public void run() {
        while (running) {
            try {
                Project project = projService.getOrCreateProject(config);
                ProjectHistory version = projService.getOrCreateProjectHistory(config, project);

                if (progressBar != null) {
                    progressBar.setValue(25);
                }

                fetcherRun(project, version);

                if (progressBar != null) {
                    progressBar.setValue(75);
                }

                if(!interrupted.getIsInterrupted()) {
                    updateProjectInDatabase(project);
                }

                status.setText("Collected!");
                running=false;

                if (progressBar != null) {
                    progressBar.setValue(100);
                }

            } catch(SupportedException ex) {
                log.error(ex.getMessage(), ex);
                running=false;
            } catch (final Exception ex) {
               log.error(ex.getMessage(), ex);
               ErrorWindow.getInstance().handleError(ex,new Errors() {
                    @Override
                    public void tryAgain() {
                        log.error(ex.getMessage(), ex);
                        run();
                    }

                    @Override
                    public void endProccess() {
                        log.error(ex.getMessage(), ex);
                        progressBar.setValue(0);
                        status.setText("Error occurred");
                        running=false;
                    }
                });
            }

            //deleted "finally {}" with progressBar.setValue(100), it was always performed, even when exception occurred
            if (apply != null) {
                apply.setEnabled(true);
            }
        }
    }

    abstract protected void fetcherRun(Project project, ProjectHistory version) throws Exception;

    private void handleError(Exception e) {
        log.error(e.getMessage(), e);

        if (status != null) {
            String msg = e.getMessage();

            if (msg == null || "".equals(msg)) {
                msg = e.toString();
            }

            status.setText("Error: " + msg);
        }
    }

    @Transactional
    private void updateProjectInDatabase(Project project) {
        try {
        	projectRepository.save(project);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
    }
}

    
