/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wroc.pwr.qualityspy.integration;

import au.com.bytecode.opencsv.CSVWriter;
import com.atlassian.jira.rest.client.internal.async.AsynchronousJiraRestClientFactory;
import org.apache.log4j.Logger;
import pl.wroc.pwr.qualityspy.bugtracker.IBugTrackerConnector;
import pl.wroc.pwr.qualityspy.bugtracker.github.GithubConnector;
import pl.wroc.pwr.qualityspy.bugtracker.jira.JiraConnector;
import pl.wroc.pwr.qualityspy.bugtracker.jira.ParentKeysSetter;
import pl.wroc.pwr.qualityspy.common.Properties;
import pl.wroc.pwr.qualityspy.model.*;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.List;

/**
 *
 */
public class FetchJiraDataThread extends FetchDataThread {

    private static final Logger LOG = Logger.getLogger(FetchJiraDataThread.class);
    
    public FetchJiraDataThread(Properties p) {
        super(p);
        this.interrupted = new Interrupter();
    }
    
    public FetchJiraDataThread(Properties p, Interrupter interrupted) {
        super(p);
        this.interrupted = interrupted;
    }

    @Override
    protected void fetcherRun(Project project, ProjectHistory projectHistory) throws Exception {
        IBugTrackerConnector issuesConnector;

        if("JiRa".equalsIgnoreCase(config.getBugtracker())) {
            issuesConnector = new JiraConnector(config, interrupted, new ParentKeysSetter(),
                    new AsynchronousJiraRestClientFactory());
        } else if("GiThUb".equalsIgnoreCase(config.getBugtracker())) {
            issuesConnector = new GithubConnector(config,interrupted);
        } else {
            throw new IllegalArgumentException("Supported bugtrackers: Jira and Github");
        }
        

        List<IssueDescription> issues = issuesConnector.getIssuesWithFullDescription();
        List<MilestoneDescription> milestones = issuesConnector.getMilestones();
        save(issues, milestones);

        LOG.info( "No of collected issues: " + issues.size());
        
        projectHistoryService.addAllIssuesAndMilestones(projectHistory, issues, milestones);
    }

    private void save(List<IssueDescription> issues, List<MilestoneDescription> milestones) {
        try {
            Writer fDescWriter = new FileWriter("descriptions.csv");
            CSVWriter descWriter = new CSVWriter(fDescWriter, ',');
            String[] descHeader = {"severity", "summar", "description", "reporter", "assignTo", "key", "issueId", "type", "priority", "resolution", "created", "updated", "component", "affectsVersion", "fixVersion", "environment", "attachement", "labels", "originalEstimate", "remainingEstimate", "timeSpend", "project", "value", "complexity", "isParent", "parentKey"};
            descWriter.writeNext(descHeader);
            String[] descRow = new String[descHeader.length];

            Writer fChanWriter = new FileWriter("changes.csv");
            CSVWriter chanWriter = new CSVWriter(fChanWriter, ',');
            String[] chanHeader = {"issue", "whoChanged", "dateOfChange", "whatChanged", "oldValue", "newValue", "type"};
            chanWriter.writeNext(chanHeader);
            String[] chanRow = new String[chanHeader.length];

            Writer fComWriter = new FileWriter("comments.csv");
            CSVWriter comWriter = new CSVWriter(fComWriter, ',');
            String[] conHeader = {"issue", "author", "date", "text"};
            comWriter.writeNext(conHeader);
            String[] comRow = new String[conHeader.length];

            Writer fLogWriter = new FileWriter("logs.csv");
            CSVWriter logWriter = new CSVWriter(fLogWriter, ',');
            String[] logHeader = {"issue", "who", "date", "status", "timeSpent", "comment"};
            logWriter.writeNext(logHeader);
            String[] logRow = new String[logHeader.length];
            
            Writer fMilestoneWriter = new FileWriter("milestones.csv");
            CSVWriter milestoneWriter = new CSVWriter(fMilestoneWriter, ',');
            String[] milestoneHeader = {"title", "creator", "description", "state", "date", "deadline"};
            milestoneWriter.writeNext(milestoneHeader);
            String[] milestoneRow = new String[milestoneHeader.length];

            for (IssueDescription issue : issues) {
                copyDescriptionToTab(descRow, issue);
                descWriter.writeNext(descRow);
                if (issue.getIssueHistory() != null) {
                    for (IssueChange change : issue.getIssueHistory()) {
                        copyChangesToTab(chanRow, issue, change);
                        chanWriter.writeNext(chanRow);
                    }
                }
                if (issue.getIssueComments() != null) {
                    for (IssueComment comment : issue.getIssueComments()) {
                        comRow[0] = issue.getIssueKey();
                        comRow[1] = comment.getAuthor();
                        comRow[2] = dateToString(comment.getDate());
                        comRow[3] = comment.getCommentText();
                        comWriter.writeNext(comRow);
                    }
                }
                if (issue.getIssueLogs() != null) {
                    for(IssueLog log : issue.getIssueLogs()){
                        logRow[0] = issue.getIssueKey();
                        logRow[1] = log.getWho();
                        logRow[2] = dateToString(log.getDate());
                        logRow[3] = log.getStatus();
                        logRow[4] = String.valueOf(log.getTimeSpent());
                        logRow[5] = log.getComment();
                        logWriter.writeNext(logRow);
                    }
                }
            }

            for(MilestoneDescription milestoneDescription : milestones) {
                copyMilestoneToTab(milestoneRow, milestoneDescription);
                milestoneWriter.writeNext(milestoneRow);
            }
            
            descWriter.close();
            chanWriter.close();
            comWriter.close();
            logWriter.close();
            milestoneWriter.close();
            
            
        } catch (IOException ex) {
            LOG.fatal( null, ex);
        }

    }

    private void copyChangesToTab(String[] chanRow, IssueDescription issue, IssueChange change) {
        chanRow[0] = issue.getIssueKey();
        chanRow[1] = change.getWhoChanged();
        chanRow[2] = dateToString(change.getDateOfChange());
        chanRow[3] = change.getWhatChanged();
        chanRow[4] = change.getOldValue();
        chanRow[5] = change.getNewValue();
        chanRow[6] = change.getType();
    }
    
    private void copyMilestoneToTab(String[] milestoneRow, MilestoneDescription milestone) {
        milestoneRow[0] = milestone.getTitle();
        milestoneRow[1] = milestone.getCreator();
        milestoneRow[2] = milestone.getDescription();
        milestoneRow[3] = milestone.getState();
        milestoneRow[4] = calendarToString(milestone.getCreated());
        milestoneRow[5] = calendarToString(milestone.getDeadline());
    }

    private void copyDescriptionToTab(String[] descRow, IssueDescription issue) {
        descRow[0] = issue.getSeverity();
        descRow[1] = issue.getSummary();
        descRow[2] = issue.getDescription();
        descRow[3] = issue.getReporter();
        descRow[4] = issue.getAssignTo();
        descRow[5] = issue.getIssueKey();
        descRow[6] = String.valueOf(issue.getIssueId());
        descRow[7] = issue.getType();
        descRow[8] = issue.getPriority();
        descRow[9] = issue.getResolution();
        descRow[10] = calendarToString(issue.getCreated());
        descRow[11] = calendarToString(issue.getUpdated());
        descRow[12] = issue.getComponent();
        descRow[13] = issue.getAffectsVersion();
        descRow[14] = issue.getFixVersion();
        descRow[15] = issue.getEnvironment();
        descRow[16] = issue.getAttachment();
        descRow[17] = issue.getLabels();
        descRow[18] = String.valueOf(issue.getOriginalEstimate());
        descRow[19] = String.valueOf(issue.getRemainingEstimate());
        descRow[20] = String.valueOf(issue.getTimeSpend());
        descRow[21] = issue.getPriority();
        descRow[22] = issue.getValue();
        descRow[23] = issue.getComplexity();
        descRow[24] = String.valueOf(issue.isIsParent());
        descRow[25] = issue.getParentKey();
    }

    private String calendarToString(Calendar calendar) {
        String date = "";
        if (calendar != null) {
            date = calendar.getTime().toString();
        }
        return date;
    }

    protected String dateToString(LocalDateTime calendar) {
        String date = "";
        if (calendar != null) {
            date = calendar.toString();
        }
        return date;
    }
}
