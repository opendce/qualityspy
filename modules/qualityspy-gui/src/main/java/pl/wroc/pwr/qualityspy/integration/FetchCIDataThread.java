/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wroc.pwr.qualityspy.integration;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import pl.wroc.pwr.qualityspy.ci.connector.ICIConnector;
import pl.wroc.pwr.qualityspy.ci.factory.CIFactory;
import pl.wroc.pwr.qualityspy.common.Properties;
import pl.wroc.pwr.qualityspy.model.Interrupter;
import pl.wroc.pwr.qualityspy.model.Job;
import pl.wroc.pwr.qualityspy.model.Project;
import pl.wroc.pwr.qualityspy.model.ProjectHistory;

import java.util.List;

/**
 * Fetch data from continuous integration server
 */
public class FetchCIDataThread extends FetchDataThread {

    private static final Log LOG = LogFactory.getLog(FetchCIDataThread.class);

    public FetchCIDataThread(Properties p) {
        super(p);
        this.interrupted = new Interrupter();
    }
    
    public FetchCIDataThread(Properties p, Interrupter interrupted) {
        super(p);
        this.interrupted = interrupted;
    }

    @Override
    protected void fetcherRun(Project project, ProjectHistory version) throws Exception {
        ICIConnector connector = new CIFactory().Create(config);
        if (config.getCIJobName().compareTo("") == 0 || config.getCIJobName().compareTo("UNKNOWN") == 0) {
            List<Job> jobs = connector.getJobs(config.getCIServerUrl(), config.getCIServerLogin(), config.getCIServerPassword(), interrupted);
            LOG.info("Number of collected jobs: " + jobs.size());
            if(interrupted.getIsInterrupted()) return;
            projectHistoryService.addAllJobs(jobs, version);
        } else {
            Job job = connector.getJob(config, interrupted);
            if(interrupted.getIsInterrupted()) return;
            projectHistoryService.addJob(job, version);
        }
    }
}
