


package pl.wroc.pwr.qualityspy.gui;

import com.jhlabs.image.CellularFilter;
import java.awt.Dimension;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import org.apache.log4j.Logger;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.renderer.DefaultTableRenderer;
import org.jdesktop.swingx.renderer.StringValue;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import pl.wroc.pwr.qualityspy.common.*;
import pl.wroc.pwr.qualityspy.common.ConfigurationManager;
import pl.wroc.pwr.qualityspy.common.JTableX;
import pl.wroc.pwr.qualityspy.common.Properties;
import pl.wroc.pwr.qualityspy.common.RowEditorModel;
import pl.wroc.pwr.qualityspy.context.AppContextProvider;
import pl.wroc.pwr.qualityspy.services.IssueDescriptionService;
import pl.wroc.pwr.qualityspy.services.ProjectHistoryService;
import pl.wroc.pwr.qualityspy.services.ProjectService;
import pl.wroc.pwr.qualityspy.tocheck.CollectedManager;
import pl.wroc.pwr.qualityspy.tocheck.ProjectDefaultTreeNode;

/**
 *
 * @author Pit
 */
public class ConfigJPanel extends javax.swing.JPanel {
    private boolean configTableChanged = false;
    private ConfigurationManager cfgManager = new ConfigurationManager();
    private  ProjectJPanel projectPanel;
    private  TreePath lastSelectedProjectPath = null;
    private boolean dontLookAtChangesCosImDeletingRows = false;
    private boolean dummyGUI = false;
    private int lastSelectedComponentIndex;
    private String selectedRowValue;
    private RowEditorModel rm = new RowEditorModel();
    private DefaultTableModel model = null;
        // tell the JTableX which RowEditorModel we are using
    public CollectedManager collectedManager = new CollectedManager();
    IssueDescriptionService issueDescriptionService;
    ProjectHistoryService projectHistoryService;
    ProjectService projectService;

    /**
     * Creates new form configJPanel
     */
    public ConfigJPanel(){
       
        File defaultConfig = new File("config/.default");
        if (!defaultConfig.exists()) {
            if (JOptionPane.showConfirmDialog(this.getRootPane(), "'config/.default' was not found. Continue?",
                "Confirmation", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == 1) {
                System.exit(0);
            }
        }
        initComponents();
        createConfigTabs();
        try {
            ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("persistance-context.xml");
            issueDescriptionService = AppContextProvider.getContext().getBean(IssueDescriptionService.class);
            projectHistoryService = AppContextProvider.getContext().getBean(ProjectHistoryService.class);
            projectService = AppContextProvider.getContext().getBean(ProjectService.class);
        } catch (Exception ex) {
            Logger.getLogger(ConfigJPanel.class.getName()).fatal(null, ex);
        }

//        CollectedManager cm = new CollectedManager();
//       cm.BuildTree(collectedDataTree, table);
       
        cfgManager = new ConfigurationManager();
    }
    public JTree getCollectedDataTree(){
        return collectedDataTree;
    }
    
        public void setTableModel(DefaultTableModel mod){
            table.setModel(mod);
        }
        private void  configureCommonTableProperties(JXTable table) {
        table.setColumnControlVisible(true);
        StringValue toString = new StringValue() {

            public String getString(Object value) {
                if (value instanceof CellularFilter.Point) {
                    CellularFilter.Point p = (CellularFilter.Point) value;
                    return createString(p.x, p.y);
                } else if (value instanceof Dimension) {
                    Dimension dim = (Dimension) value;
                    return createString(dim.width, dim.height);
                }
               return "";
            }

            private String createString(float width, float height) {
                return "(" + width + ", " + height + ")";
            }
            
        };
        TableCellRenderer renderer = new DefaultTableRenderer(toString);
        table.setDefaultRenderer(CellularFilter.Point.class, renderer);
        table.setDefaultRenderer(Dimension.class, renderer);
    }
    
    
    
    
    
    
 public boolean getconfigTableChanged(){
       return  configTableChanged;
   }
   public boolean questionBoxIfSavingChanges(int saveReason) {
        configTableChanged = false;   
        if (JOptionPane.showConfirmDialog(this.getRootPane(), "The configuration has been changed.\n Do you want to save them?",
                "Confirmation", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == 0) {
            return true;
        } else {
            return false;
        }
    }
    public void setVisibleConfigTable(boolean enable){
    configTable.setVisible(enable);
    }
     public void createConfigTabs() {
        configTable.setVisible(false);
        configCollectedPane.add(configTable);
        configTabPane.removeAll();
        configTable.removeAll();

        configTabPane.addTab("CKJM", new JPanel());
        configTabPane.addTab("Repository", new JPanel());
        configTabPane.addTab("Issue Tracker", new JPanel());
        configTabPane.addTab("CI", new JPanel());
        
    }
   public void setProjectPanel(ProjectJPanel projectPanel)
   {
        this.projectPanel = projectPanel;
        loadListOfConfigs();
   }
     public Properties getConfiguration()
    {
        return cfgManager.getConfiguration();
    }
      public  ConfigurationManager  getConfigurationManager(){
       return  cfgManager;
    }
      
      public void removeSelectedConfiguration() throws IOException {
        //cfgManager.removeSelectedConfiguration("", "");
    }
     public void  saveProperties(){
        try
        {
        cfgManager.saveProperties();
        } catch (IOException ex) {
      //          log.fatal(null, ex);
                JOptionPane.showMessageDialog(this.getRootPane(), "Unable to save configuration");
            }
    }
     public void savePropertiesAs(String version){
        try
        {
            cfgManager.savePropertiesAs(version);
        } catch (IOException ex) {
         //  log.fatal( null, ex);
            JOptionPane.showMessageDialog(this.getRootPane(), "Unable to save configuration for version " + version);
        }
    }
      public void loadListOfConfigs() {
        Map<String, List<String>> configurations = new TreeMap<String, List<String>>();

        DefaultMutableTreeNode root = new DefaultMutableTreeNode("rootz");

        for (String project : cfgManager.getListOfProjects()) {
            List<String> versions = cfgManager.getListOfVersions(project);
            configurations.put(project, versions);
        }

        for (String project : configurations.keySet()) {
            DefaultMutableTreeNode node = new DefaultMutableTreeNode(project);
            root.add(node);
            for (String version : configurations.get(project)) {
                DefaultMutableTreeNode subNode = new DefaultMutableTreeNode(version);
                subNode.setAllowsChildren(false);
                node.add(subNode);
            }
        }

        DefaultTreeModel tmodel = ((DefaultTreeModel) projectPanel.getProjectTree().getModel());
        tmodel.setRoot(root);
        
        if (!dummyGUI) {
          // CollectedManager.expandAll(projectsTree, new TreePath(root), true); 
        }
    }
         public void importListOfConfigs(ConfigurationManager cm,String selectedProject) 
         {
            Map<String, List<String>> configurations = new TreeMap<String, List<String>>();
           
            DefaultMutableTreeNode root =  (DefaultMutableTreeNode)projectPanel.getProjectTree().getModel().getRoot();
            
            for (String project : cm.getListOfProjects()) {
                if(project.equals(selectedProject))
                {
                    List<String> versions = cm.getListOfVersions(project);
                    configurations.put(project, versions);
                    break;
                }
            }

            for (String project : configurations.keySet()) {
                DefaultMutableTreeNode node = new DefaultMutableTreeNode(project);
                root.add(node);
                for (String version : configurations.get(project)) {
                    DefaultMutableTreeNode subNode = new DefaultMutableTreeNode(version);
                    subNode.setAllowsChildren(false);
                    node.add(subNode);
                }
            }

            DefaultTreeModel tmodel = ((DefaultTreeModel) projectPanel.getProjectTree().getModel());
            tmodel.setRoot(root);

    }

     public void setConfigTab(){
         try {
           
            int selectedConfigTab = configTabPane.getSelectedIndex();
            Properties configProperties = cfgManager.getConfiguration();
            if (configProperties == null) {
                return;
            }
            switch(selectedConfigTab)
            {
                case 0:
              //      log.info("Loading ckjm properties");
                    addRowCkjmToConfigTable(configProperties);
                    addRowCkjmEditorRendererToConfigTable(configProperties);
                    break;
                case 1:
                //    log.info("Loading repository properties");
                    addRowSvnToConfigTable(configProperties);    
                    addRowRepositoryEditorRendererToConfigTable(configProperties);
                    break;
                case 2:
                  //  log.info("Loading Jira properties");
                    addRowJiraToConfigTable(configProperties);
                    addRowJiraEditorRendererToConfigTable(configProperties);
                   break;
                case 3:
                    //log.info("Loading continuous integration properties");
                    addRowCiToConfigTable(configProperties);
                    addRowCiEditorRendererToConfigTable(configProperties);
                 break;       
                default:
                    //log.warn(String.format("There is no content for tab \"%s\"!",
                            configTabPane.getTitleAt(selectedConfigTab);
                    break;                
            }
            configTableChanged = false;
        
            configTable.repaint();
        } catch (Exception ex) {      
        //    log.fatal( ex.getMessage(), ex);
        }
     }
     private void addRowToConfigTable(String name, String value) {
        DefaultTableModel defModel = (DefaultTableModel) configTable.getModel();
        if (name != null || name.length() > 0) {
            if (value == null || value.length() == 0) {
                value = "";
            }
            defModel.addRow(new Object[]{name, value});

        }
    }
     public void configureManager(String newNameProject,String projectVersion) {
      cfgManager.clearConfiguration();
      try{
          
             cfgManager.savePropertiesAs(newNameProject, projectVersion);
       } catch (IOException ex) {
                JOptionPane.showMessageDialog(this.getRootPane(), "IO error! Unable to create new project!");
       }
      cfgManager.loadConfiguration(newNameProject, projectVersion);
      loadListOfConfigs();
      createConfigTabs();
      loadPropertiesToGrid();
   }
     
      public javax.swing.JTable getConfigTable(){
        return getConfigTable();
     }
      public void remoweRowConfigPanel(boolean refresh){
           DefaultTableModel defConfigTableModel = (DefaultTableModel) configTable.getModel();
 
        while (configTable.getRowCount() > 0) {
            defConfigTableModel.removeRow(0);
        }
       if(refresh == true)
            configTable.repaint();
      }
     public synchronized void loadPropertiesToGrid(){ 
        dontLookAtChangesCosImDeletingRows = true;
       remoweRowConfigPanel(false);
        
        if (cfgManager.getSelectedVersion() == null) {
            return;
        }
        setConfigTab();
            
      
        dontLookAtChangesCosImDeletingRows = false;
    }

      public void loadConfiguration(String selectedProject,String selectedVersion){
        cfgManager.loadConfiguration(selectedProject, selectedVersion);
        }
      public void noConfiguration()
     {
        cfgManager.noConfiguration();
            loadPropertiesToGrid();
     }
       private void changeConfigTab(int newIndex) {

        if (newIndex < 0) {
            return;
        }

        JPanel newConfigurationPanel = (JPanel) configTabPane.getComponentAt(newIndex);
        newConfigurationPanel.removeAll();

        JScrollPane scrollConfigurateTable = new JScrollPane();
        scrollConfigurateTable.setViewportView(configTable);
         
        org.jdesktop.layout.GroupLayout newLyout = new org.jdesktop.layout.GroupLayout(newConfigurationPanel);
        newConfigurationPanel.setLayout(newLyout);
        
        newLyout.setHorizontalGroup(
                newLyout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(newLyout.createSequentialGroup().addContainerGap().add(scrollConfigurateTable, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 326, Short.MAX_VALUE).addContainerGap()));
        
        newLyout.setVerticalGroup(
                newLyout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING).add(newLyout.createSequentialGroup().addContainerGap().add(scrollConfigurateTable, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 201, Short.MAX_VALUE).addContainerGap()));
  
        configTable.setVisible(true);

        if (lastSelectedComponentIndex != newIndex) {
            if (configTableChanged) {
                if (questionBoxIfSavingChanges(0)) {  // 0 = zmiana komponentu, 1 = wyj�cie z okna
                    //zapisujemy zmiany
                    saveChangesOfComponent();
                }
            }
        }

        if (lastSelectedComponentIndex == newIndex) {
            return;
        } else {
            lastSelectedComponentIndex = newIndex;
        }
        loadPropertiesToGrid();
    }
      public int getConfigTabPaneCount(){
            return configTabPane.getTabCount();
     }
        public void saveChangesOfComponent() {
///        log.info("saving Changes Of Component...");

        Properties proper = cfgManager.getConfiguration();
        try {
                DefaultTableModel defModel = (DefaultTableModel)configTable.getModel();
                for (int i = 0; i < defModel.getRowCount(); i++)
                {
                    
                    String[] a = String.valueOf(defModel.getValueAt(i, 0)).split("\\*");
                    proper.setProperty(a[0], String.valueOf(defModel.getValueAt(i, 1)));
                }

                try {
                    cfgManager.saveProperties();
                } catch (FileNotFoundException ex) {
              //      log.fatal( null, ex);
                }

        } catch (IOException ex) {
      //      log.fatal( null, ex);L 
                   
        }
        configTableChanged = false;
    }
       public void saveChangesComponent(){
        configTableChanged = false;
        saveChangesOfComponent();
    }
       public void enableConfiguration(boolean enable){
            if(mainConfigPane!= null){
                mainConfigPane.getComponentAt(0).setEnabled(enable);
                configCollectedPane.setEnabled(enable);
                configCollectedPane.getComponents()[0].setEnabled(enable);
                configCollectedPane.getComponents()[1].setEnabled(enable);
                configTable.setEnabled(enable);
            }
       }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jFrame1 = new javax.swing.JFrame();
        jFrame2 = new javax.swing.JFrame();
        jFrame3 = new javax.swing.JFrame();
        mainConfigPane = new javax.swing.JTabbedPane();
        configCollectedPane = new javax.swing.JPanel();
        configTabPane = new javax.swing.JTabbedPane();
        configCollectedScrollPane = new javax.swing.JScrollPane();
        configTable = new JTableX();
        jiraPanel = new javax.swing.JPanel();
        jSplitPane1 = new javax.swing.JSplitPane();
        jScrollPane4 = new javax.swing.JScrollPane();
        collectedDataTree = new javax.swing.JTree();
        jScrollPane1 = new javax.swing.JScrollPane();
        table = new javax.swing.JTable();

        javax.swing.GroupLayout jFrame1Layout = new javax.swing.GroupLayout(jFrame1.getContentPane());
        jFrame1.getContentPane().setLayout(jFrame1Layout);
        jFrame1Layout.setHorizontalGroup(
            jFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        jFrame1Layout.setVerticalGroup(
            jFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jFrame2Layout = new javax.swing.GroupLayout(jFrame2.getContentPane());
        jFrame2.getContentPane().setLayout(jFrame2Layout);
        jFrame2Layout.setHorizontalGroup(
            jFrame2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        jFrame2Layout.setVerticalGroup(
            jFrame2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jFrame3Layout = new javax.swing.GroupLayout(jFrame3.getContentPane());
        jFrame3.getContentPane().setLayout(jFrame3Layout);
        jFrame3Layout.setHorizontalGroup(
            jFrame3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        jFrame3Layout.setVerticalGroup(
            jFrame3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        configCollectedPane.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createTitledBorder("")), "Configuration editor"));

        configTabPane.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                configTabPaneStateChanged(evt);
            }
        });

        configCollectedScrollPane.setMinimumSize(new java.awt.Dimension(0, 0));

        configTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Parameter", "Value"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        configTable.setFocusable(false);
        configTable.getTableHeader().setReorderingAllowed(false);
        configTable.setUpdateSelectionOnSort(false);
        configCollectedScrollPane.setViewportView(configTable);

        javax.swing.GroupLayout configCollectedPaneLayout = new javax.swing.GroupLayout(configCollectedPane);
        configCollectedPane.setLayout(configCollectedPaneLayout);
        configCollectedPaneLayout.setHorizontalGroup(
            configCollectedPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(configCollectedPaneLayout.createSequentialGroup()
                .addGroup(configCollectedPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(configCollectedPaneLayout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(configCollectedScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 252, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(configCollectedPaneLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(configTabPane, javax.swing.GroupLayout.DEFAULT_SIZE, 824, Short.MAX_VALUE)))
                .addContainerGap())
        );
        configCollectedPaneLayout.setVerticalGroup(
            configCollectedPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(configCollectedPaneLayout.createSequentialGroup()
                .addComponent(configCollectedScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 0, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(configTabPane, javax.swing.GroupLayout.DEFAULT_SIZE, 489, Short.MAX_VALUE)
                .addContainerGap())
        );

        mainConfigPane.addTab("Configuration", configCollectedPane);

        jiraPanel.setBackground(new java.awt.Color(255, 255, 255));
        jiraPanel.setForeground(new java.awt.Color(255, 255, 255));

        jSplitPane1.setDividerLocation(200);
        jSplitPane1.setDividerSize(1);

        javax.swing.tree.DefaultMutableTreeNode treeNode1 = new javax.swing.tree.DefaultMutableTreeNode("root");
        javax.swing.tree.DefaultMutableTreeNode treeNode2 = new javax.swing.tree.DefaultMutableTreeNode("aaa");
        javax.swing.tree.DefaultMutableTreeNode treeNode3 = new javax.swing.tree.DefaultMutableTreeNode("v1");
        treeNode2.add(treeNode3);
        treeNode3 = new javax.swing.tree.DefaultMutableTreeNode("v2");
        treeNode2.add(treeNode3);
        treeNode1.add(treeNode2);
        treeNode2 = new javax.swing.tree.DefaultMutableTreeNode("bbb");
        treeNode3 = new javax.swing.tree.DefaultMutableTreeNode("v1");
        treeNode2.add(treeNode3);
        treeNode1.add(treeNode2);
        treeNode2 = new javax.swing.tree.DefaultMutableTreeNode("ccc");
        treeNode1.add(treeNode2);
		collectedDataTree.setVisible(false);
        collectedDataTree.setModel(new javax.swing.tree.DefaultTreeModel(treeNode1));
        collectedDataTree.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        collectedDataTree.setRootVisible(false);
        collectedDataTree.setShowsRootHandles(true);
        collectedDataTree.addTreeWillExpandListener(new javax.swing.event.TreeWillExpandListener() {
            public void treeWillCollapse(javax.swing.event.TreeExpansionEvent evt)throws javax.swing.tree.ExpandVetoException {
            }
            public void treeWillExpand(javax.swing.event.TreeExpansionEvent evt)throws javax.swing.tree.ExpandVetoException {
                collectedDataTreeTreeWillExpand(evt);
            }
        });
        collectedDataTree.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                collectedDataTreeMouseClicked(evt);
            }
        });
        collectedDataTree.addTreeSelectionListener(new javax.swing.event.TreeSelectionListener() {
            public void valueChanged(javax.swing.event.TreeSelectionEvent evt) {
                collectedDataTreeValueChanged(evt);
            }
        });
        jScrollPane4.setViewportView(collectedDataTree);

        jSplitPane1.setLeftComponent(jScrollPane4);

        table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null}
            },
            new String [] {
                ""
            }
        ));
		table.setVisible(false);
        jScrollPane1.setViewportView(table);

        jSplitPane1.setRightComponent(jScrollPane1);

        javax.swing.GroupLayout jiraPanelLayout = new javax.swing.GroupLayout(jiraPanel);
        jiraPanel.setLayout(jiraPanelLayout);
        jiraPanelLayout.setHorizontalGroup(
            jiraPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 856, Short.MAX_VALUE)
        );
        jiraPanelLayout.setVerticalGroup(
            jiraPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 532, Short.MAX_VALUE)
        );

        mainConfigPane.addTab("Collected data", jiraPanel);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(mainConfigPane)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(mainConfigPane)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void configTabPaneStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_configTabPaneStateChanged
        changeConfigTab(configTabPane.getSelectedIndex());
    }//GEN-LAST:event_configTabPaneStateChanged

    private void collectedDataTreeValueChanged(javax.swing.event.TreeSelectionEvent evt) {//GEN-FIRST:event_collectedDataTreeValueChanged
        // TODO add your handling code here:
        //collectedDataTree.repaint();
        collectedDataTree.invalidate();
        collectedDataTree.validate();
        collectedDataTree.repaint();
    }//GEN-LAST:event_collectedDataTreeValueChanged

    private void collectedDataTreeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_collectedDataTreeMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_collectedDataTreeMouseClicked

    private void collectedDataTreeTreeWillExpand(javax.swing.event.TreeExpansionEvent evt) throws javax.swing.tree.ExpandVetoException {//GEN-FIRST:event_collectedDataTreeTreeWillExpand
        // TODO add your handling code here:
                ProjectDefaultTreeNode nodeToShowInTable = (ProjectDefaultTreeNode)((DefaultMutableTreeNode)evt.getPath().getLastPathComponent());
                        ProjectDefaultTreeNode p = (ProjectDefaultTreeNode)nodeToShowInTable.getParent();
                        //System.out.println(p.object);
                        //System.out.println(nodeToShowInTable.object.getClass());
                       // if(p.object != null)
                           // System.out.println(p.object.getClass());
                        //evt.getPath().getLastPathComponent();
                        //nodeToShowInTable.getParent() getPreviousNode().add(new ProjectDefaultTreeNode("tutaj"));

                        
        try {
            model = nodeToShowInTable.whatToShow(nodeToShowInTable);
        } catch (Exception ex) {
            Logger.getLogger(ConfigJPanel.class.getName()).fatal(null, ex);
        }
        if(model != null)
        {
            table.setModel(model);
            table.setVisible(true);
            table.repaint();
        }
    }//GEN-LAST:event_collectedDataTreeTreeWillExpand


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTree collectedDataTree;
    private javax.swing.JPanel configCollectedPane;
    private javax.swing.JScrollPane configCollectedScrollPane;
    private javax.swing.JTabbedPane configTabPane;
    private JTableX configTable;
    private javax.swing.JFrame jFrame1;
    private javax.swing.JFrame jFrame2;
    private javax.swing.JFrame jFrame3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JPanel jiraPanel;
    private javax.swing.JTabbedPane mainConfigPane;
    private javax.swing.JTable table;
    // End of variables declaration//GEN-END:variables

 
    
    private void addRowCiToConfigTable(Properties configProperties) {
           addRowToConfigTable(configProperties.getCIServerTypeKey()+"*",
           configProperties.getCIServerTypeName(configProperties.getCIServerType()));
           
           addRowToConfigTable(configProperties.getCIServerUrlKey()+"*", configProperties.getCIServerUrl());
           addRowToConfigTable(configProperties.getCIServerLoginKey(), configProperties.getCIServerLogin());
           addRowToConfigTable(configProperties.getCIServerPasswordKey(), configProperties.getCIServerPassword());
           addRowToConfigTable(configProperties.getCIServerJobNameKey()+"*", configProperties.getCIJobName());
    }
      private void addRowCiEditorRendererToConfigTable(Properties configProperties) {
          CiParametrRenderer ciPr = new CiParametrRenderer(configProperties);
          CiValueRenderer ciVr = new CiValueRenderer(configProperties);
          configTable.getColumnModel().getColumn(0).setCellRenderer(ciPr);//renderer kolumny "Parametr"
          configTable.getColumnModel().getColumn(1).setCellRenderer(ciVr);//renderer kolumny "Value"
          configTable.removeMyEditor();
          configTable.removeEditor();
         // String[] anchor_values = { "Jenkins","Hudson"};
        //  JComboBox cb = new JComboBox(anchor_values); 
        //  DefaultCellEditor dCe = new DefaultCellEditor(cb);
          try{
       //  configTable.setEditorForRow(0,dCe);// ustawienie edytora dla pierwszego wiersza
          }catch(Exception e){
          JOptionPane.showMessageDialog(this.getRootPane(), e);
          }
    }

    private void addRowJiraToConfigTable(Properties configProperties) {
      addRowToConfigTable(configProperties.getBugtrackerKey()+"*", configProperties.getBugtracker());
      addRowToConfigTable(configProperties.getJiraUrlKey()+"*", configProperties.getJiraUrl());
      addRowToConfigTable(configProperties.getJiraLoginKey(), configProperties.getJiraLogin());
      addRowToConfigTable(configProperties.getJiraPasswordKey(), configProperties.getJiraPassword());
      addRowToConfigTable(configProperties.getJiraTimeoutKey(), configProperties.getJiraTimeout());
      addRowToConfigTable(configProperties.getJiraJQLStringKey(), configProperties.getJiraJQLString());
      
      addRowToConfigTable(configProperties.getGithubRepoKey(),configProperties.getGithubRepo());             //Repository
      addRowToConfigTable(configProperties.getGithubRepoOwnerKey(), configProperties.getGithubRepoOwner());  //Github owner
      addRowToConfigTable(configProperties.getGithubIssueStateKey(), configProperties.getGithubIssueState());                 //State
      addRowToConfigTable(configProperties.getGithubIssueTitleKey(), configProperties.getGithubIssueTitle());                 //Title
      addRowToConfigTable(configProperties.getGithubIssueNumberKey(), configProperties.getGithubIssueNumber());               //Number
      addRowToConfigTable(configProperties.getGithubIssueHtmlUrlKey(), configProperties.getGithubIssueHtmlUrl());             //Reporter
      addRowToConfigTable(configProperties.getGithubIssueAssigneeKey(), configProperties.getGithubIssueAssignee());           //Assignee     
      addRowToConfigTable(configProperties.getGithubIssueLabelsKey(), configProperties.getGithubIssueLabels());              //labels
      addRowToConfigTable(configProperties.getGithubIssueCreatedAtKey(), configProperties.getGithubIssueCreatedAt());        //Created at
      addRowToConfigTable(configProperties.getGithubIssueUpdatedAtKey(), configProperties.getGithubIssueUpdatedAt());        //updated at
      addRowToConfigTable(configProperties.getGithubIssueMentionedKey(), configProperties.getGithubIssueMentioned());        //Description   
                      
    }
    
  private void addRowJiraEditorRendererToConfigTable(Properties configProperties) {
          JiraParametrRenderer jiraPr = new JiraParametrRenderer(configProperties);
          JiraValueRenderer jiraVr = new JiraValueRenderer(configProperties);
          configTable.getColumnModel().getColumn(0).setCellRenderer(jiraPr);//renderer kolumny "Parametr"
          configTable.getColumnModel().getColumn(1).setCellRenderer(jiraVr);//renderer kolumny "Value"
          configTable.removeMyEditor();
          configTable.removeEditor();
  }
    private void addRowRepositoryEditorRendererToConfigTable(Properties configProperties) {
          RepositoryParametrRenderer repositoryPr = new RepositoryParametrRenderer(configProperties);
          RepositoryValueRenderer repositoryVr = new RepositoryValueRenderer(configProperties);
          configTable.getColumnModel().getColumn(0).setCellRenderer(repositoryPr);//renderer kolumny "Parametr"
          configTable.getColumnModel().getColumn(1).setCellRenderer(repositoryVr);//renderer kolumny "Value"
          configTable.removeMyEditor();
          configTable.removeEditor();
         
  }
    private void addRowSvnToConfigTable(Properties configProperties) {
        addRowToConfigTable(configProperties.getRepoSvnPassKey(), configProperties.getRepoSvnPass());
        addRowToConfigTable(configProperties.getRepoSvnUserKey(), configProperties.getRepoSvnUser());
        Character res = configProperties.getRepofileseparator();
        addRowToConfigTable(configProperties.getRepofileseparatorKey()+"*", (res == null ? "" : res.toString()));
        addRowToConfigTable(configProperties.getReposrcpathpostfixregexKey()+"*", configProperties.getReposrcpathpostfixregex());
        addRowToConfigTable(configProperties.getReposrcpathprefixregexKey()+"*", configProperties.getReposrcpathprefixregex());
        addRowToConfigTable(configProperties.getRepotypeKey()+"*", configProperties.getRepotype());
        addRowToConfigTable(configProperties.getRepourlKey()+"*", configProperties.getRepourl());
        addRowToConfigTable(configProperties.getRepopathKey(), configProperties.getRepopath());
        addRowToConfigTable(configProperties.getRepoContentRegexKey(), configProperties.getRepoContentRegex());
        addRowToConfigTable(configProperties.getRepoDiffRegexKey(), configProperties.getRepoDiffRegex());
        addRowToConfigTable(configProperties.getRepoworkingcopyKey(), configProperties.getRepoworkingcopy());
                    
        String type = configProperties.getRepotype();
        if (type.equalsIgnoreCase("git")){
                SimpleDateFormat dataFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                addRowToConfigTable(configProperties.getRepostartrevKey()+"* ", dataFormat.format(configProperties.getRepostartrev()));
                addRowToConfigTable(configProperties.getRepoendrevKey()+"* ", dataFormat.format(configProperties.getRepoendrev())); 
        } else {
                addRowToConfigTable(configProperties.getRepostartrevKey()+"*", configProperties.getRepostartrev().toString());
                addRowToConfigTable(configProperties.getRepoendrevKey()+"*", configProperties.getRepoendrev().toString());   
        }  
    }

    private void addRowCkjmToConfigTable(Properties configProperties) {
       addRowToConfigTable(configProperties.getCkjmSourceTypeKey()+"*", configProperties.getCkjmSourceType().toString());
       addRowToConfigTable(configProperties.getCkjmJarKey(), configProperties.getCkjmJar());
       addRowToConfigTable(configProperties.getCkjmClassKey(), configProperties.getCkjmClass());
       addRowToConfigTable(configProperties.getCkjmLibrariesSeparatorKey(), configProperties.getCkjmLibrariesSeparator());
       addRowToConfigTable(configProperties.getCkjmLibrariesKey(), configProperties.getCkjmLibraries());
       addRowToConfigTable(configProperties.getCkjmMavenProjectDirectoryKey(), configProperties.getCkjmMavenProjectDirectory());
       addRowToConfigTable(configProperties.getCkjmMavenHomeKey(), configProperties.getCkjmMavenHome());
       addRowToConfigTable(configProperties.getCkjmJavaHomeKey(), configProperties.getCkjmJavaHome());
       addRowToConfigTable(configProperties.getaddTestHomeKey(), configProperties.getaddTestHome());
    }
     private void addRowCkjmEditorRendererToConfigTable(Properties configProperties) {
          CkjmParametrRenderer ckjmPr = new CkjmParametrRenderer(configProperties);
          CkjmValueRenderer ckjmVr = new CkjmValueRenderer(configProperties);
          configTable.getColumnModel().getColumn(0).setCellRenderer(ckjmPr);//renderer kolumny "Parametr"
          configTable.getColumnModel().getColumn(1).setCellRenderer(ckjmVr);//renderer kolumny "Value"
          configTable.removeMyEditor();
          configTable.removeEditor();
       //   String[] trueFalse = { "true","false"};
     
        //  JComboBox cbRow1 = new JComboBox(trueFalse); 
        //  JComboBox cbRow2 = new JComboBox(trueFalse); 
    
       //   DefaultCellEditor row1 = new DefaultCellEditor(cbRow1);
       //   DefaultCellEditor row2 = new DefaultCellEditor(cbRow2);
          try{
      //   configTable.setEditorForRow(1,row1);
      //   configTable.setEditorForRow(2,row2);
          }catch(Exception e){
          JOptionPane.showMessageDialog(this.getRootPane(), e);
          }
    }    

   
}
