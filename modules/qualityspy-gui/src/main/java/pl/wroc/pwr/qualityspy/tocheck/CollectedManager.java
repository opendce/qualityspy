/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wroc.pwr.qualityspy.tocheck;

import org.apache.log4j.Logger;
import pl.wroc.pwr.qualityspy.context.AppContextProvider;

import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

public class CollectedManager {
    private static final Logger LOG = Logger.getLogger(CollectedManager.class);
    public void BuildTree(JTree tree, String selectedProject, String selectedVersion){

        ProjectDefaultTreeNode root = new ProjectDefaultTreeNode("ProjectHistories");
        CollectedData collectedData = AppContextProvider.getContext().getBean(CollectedData.class);
        try{
            tree.setRootVisible(true);
            collectedData.getProjectHistories(selectedProject, selectedVersion, root);
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
            DefaultMutableTreeNode node = new DefaultMutableTreeNode("Brak danych w bazie!");
            root.add(node);
        }
                    
         DefaultTreeModel tmodel = ((DefaultTreeModel) tree.getModel());
         tmodel.setRoot(root);
        
    }

}

