/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wroc.pwr.qualityspy.tocheck;

import java.util.List;
import pl.wroc.pwr.qualityspy.common.ConfigurationManager;
import pl.wroc.pwr.qualityspy.common.Properties;
import pl.wroc.pwr.qualityspy.model.Project;

/**
 *
 * @author VAIO
 */

public class CollectedProjectTreeData {
    private ConfigurationManager cfgManager= new ConfigurationManager();
    private Properties properties = cfgManager.getConfiguration();
    
//    public DefaultMutableTreeNode getData(List projects){
//        DefaultMutableTreeNode root = getProjectAndVersion(projects);
//        return root;
//    }
    
    public void getProjectAndVersion(List<Project> projects, ProjectDefaultTreeNode root){
//        DefaultMutableTreeNode root = new DefaultMutableTreeNode("From hard disc");
        for (String project : cfgManager.getListOfProjects()) {
            if(projects != null && projects.contains(project)){
                continue;
            }
            else{
                ProjectDefaultTreeNode node = new ProjectDefaultTreeNode(project);
                root.add(node);
                for (String version : cfgManager.getListOfVersions(project)) {
                    ProjectDefaultTreeNode subnode = new ProjectDefaultTreeNode(version);
                    node.add(subnode);
            }
            
//                cfgManager.loadConfiguration(project, version);
//                properties = cfgManager.getConfiguration();
//                DefaultMutableTreeNode subnodeCKJM = getCKJM();
//                subnode.add(subnodeCKJM);
//                DefaultMutableTreeNode subnodeRepository = getRepository();
//                subnode.add(subnodeRepository);
//                DefaultMutableTreeNode subnodeJira = getJira();
//                subnode.add(subnodeJira);
//                DefaultMutableTreeNode subnodeCl = getCl();
//                subnode.add(subnodeCl);
            }
        }
//        return root;
    }
//    private DefaultMutableTreeNode getCKJM(){
//        DefaultMutableTreeNode root = new DefaultMutableTreeNode("CKJM");
//        root.add(new DefaultMutableTreeNode(properties.getCkjmSourceTypeKey()+ ": " +properties.getCkjmSourceType()));
//        root.add(new DefaultMutableTreeNode(properties.getCkjmJarKey() + ": " + properties.getCkjmJar()));
//        root.add(new DefaultMutableTreeNode(properties.getCkjmClassKey() + ": " + properties.getCkjmClass()));
//        root.add(new DefaultMutableTreeNode(properties.getCkjmLibrariesSeparatorKey() + ": " + properties.getCkjmLibrariesSeparator()));
//        root.add(new DefaultMutableTreeNode(properties.getCkjmLibrariesKey() + ": " + properties.getCkjmLibraries()));
//        return root;
//    }
//    
//    private DefaultMutableTreeNode getRepository(){
//        DefaultMutableTreeNode root = new DefaultMutableTreeNode("Repository");
//        Character res = properties.getRepofileseparator();
//        root.add(new DefaultMutableTreeNode(properties.getRepoSvnPassKey()+ ": " +properties.getRepoSvnPass()));
//        root.add(new DefaultMutableTreeNode(properties.getRepoSvnUserKey() + ": " + properties.getRepoSvnUser()));
//        root.add(new DefaultMutableTreeNode(properties.getRepofileseparatorKey() + ": " + (res == null ? "" : res.toString())));
//        root.add(new DefaultMutableTreeNode(properties.getReposrcpathpostfixregexKey() + ": " + properties.getReposrcpathpostfixregex()));
//        root.add(new DefaultMutableTreeNode(properties.getReposrcpathprefixregexKey() + ": " + properties.getReposrcpathprefixregex()));
//        root.add(new DefaultMutableTreeNode(properties.getRepotypeKey() + ": " + properties.getRepotype()));
//        root.add(new DefaultMutableTreeNode(properties.getRepourlKey() + ": " + properties.getRepourl()));
//        root.add(new DefaultMutableTreeNode(properties.getRepopathKey() + ": " + properties.getRepopath()));
//        root.add(new DefaultMutableTreeNode(properties.getRepoContentRegexKey() + ": " + properties.getRepoContentRegex()));
//        root.add(new DefaultMutableTreeNode(properties.getRepoDiffRegexKey() + ": " + properties.getRepoDiffRegex()));
//        root.add(new DefaultMutableTreeNode(properties.getRepoworkingcopyKey() + ": " + properties.getRepoworkingcopy()));
//
//        String type = properties.getRepotype();
//        if (type != null && type.equalsIgnoreCase ("git")) {
//            SimpleDateFormat dataFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
//            root.add(new DefaultMutableTreeNode(properties.getRepostartrevKey() + ": " + dataFormat.format(properties.getRepostartrev())));
//            root.add(new DefaultMutableTreeNode(properties.getRepoendrevKey() + ": " + dataFormat.format(properties.getRepoendrev())));
//        } 
//        else {
//            root.add(new DefaultMutableTreeNode(properties.getRepostartrevKey() + ": " + properties.getRepostartrev()));
//            root.add(new DefaultMutableTreeNode(properties.getRepoendrevKey() + ": " + properties.getRepoendrev()));
//        }
//        return root;                 
//    }
//    
//    private DefaultMutableTreeNode getJira(){
//        DefaultMutableTreeNode root = new DefaultMutableTreeNode("Jira");
//        root.add(new DefaultMutableTreeNode(properties.getJiraUrlKey()+ ": " +properties.getJiraUrl()));
//        root.add(new DefaultMutableTreeNode(properties.getJiraLoginKey() + ": " + properties.getJiraLogin()));
//        root.add(new DefaultMutableTreeNode(properties.getJiraPasswordKey() + ": " + properties.getJiraPassword()));
//        root.add(new DefaultMutableTreeNode(properties.getJiraTimeoutKey() + ": " + properties.getJiraTimeout()));
//        root.add(new DefaultMutableTreeNode(properties.getJiraJQLStringKey() + ": " + properties.getJiraJQLString()));
//        return root;
//    }
//    
//    private DefaultMutableTreeNode getCl() {
//        DefaultMutableTreeNode root = new DefaultMutableTreeNode("Cl");
//        root.add(new DefaultMutableTreeNode(properties.getCIServerTypeKey() + ": " + properties.getCIServerTypeName(properties.getCIServerType())));
//        root.add(new DefaultMutableTreeNode(properties.getCIServerUrlKey() + ": " + properties.getCIServerUrl()));
//        root.add(new DefaultMutableTreeNode(properties.getCIServerLoginKey() + ": " + properties.getCIServerLogin()));
//        root.add(new DefaultMutableTreeNode(properties.getCIServerPasswordKey() + ": " + properties.getCIServerPassword()));
//        root.add(new DefaultMutableTreeNode(properties.getCIServerJobNameKey() + ": " + properties.getCIJobName()));
//        return root;
//    }
}
