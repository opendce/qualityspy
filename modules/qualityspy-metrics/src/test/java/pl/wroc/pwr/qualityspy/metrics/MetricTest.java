/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.wroc.pwr.qualityspy.metrics;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author mateusz
 */
public class MetricTest {
    /** logger **/
    private static final Logger logger = Logger.getLogger(MetricTest.class.getName());
    
    public MetricTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of addClassMetric method, of class Metric.
     */
    @Test
    public void testAddClassMetric() {
        logger.info("addClassMetric");
        String klasa = "Klasa";
        String metryka = "metryka";
        double value = 0.0;
        Metric instance = new Metric(klasa);
        boolean expResult = true;
        boolean result = instance.addClassMetric(metryka, value);
        assertEquals(expResult, result);
    }

    /**
     * Test of addMethodMetric method, of class Metric.
     */
    @Test
    public void testAddMethodMetric() {
        logger.info("addMethodMetric");
        String klasa = "Klasa";
        String metryka = "metryka";
        double value = 0.0;
        Metric instance = new Metric(klasa);
        boolean expResult = true;
        boolean result = instance.addMethodMetric(metryka, value);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Metric.
     */
    @Test
    public void testToString() {
        logger.info("toString");
        String klasa = "Klasa";
        String metoda1 = "metoda1";
        String metoda2 = "metoda2";
        double value = 5.0;

        Metric instance = new Metric(klasa);
        instance.addMethodMetric(metoda1, value);
        instance.addMethodMetric(metoda2, value);

        String expResult = klasa + "\n";
        String toStringMetoda1 = metoda1 + " : " + value + "\n";
        String toStringMetoda2 = metoda2 + " : " + value + "\n";
        expResult += toStringMetoda1;
        expResult += toStringMetoda2;

        String result = instance.toString();
        assertEquals(expResult, result);

        /*
        String s = class_name + "\n";
        for(Element e : metric_values)
        {
            s += e.toString() + "\n";
        }
        for(Element e : method_metrics)
        {
            s += e.toString() + "\n";
        }
         */



    }

}