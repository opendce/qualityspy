/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.wroc.pwr.qualityspy.metrics;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author mateusz
 */
public class ElementTest {
    /** logger **/
    private static final Logger logger = Logger.getLogger(ElementTest.class.getName());
//    private static final Logger logger = Logger.getLogger(ElementTest.class.getName());
    
    public ElementTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getName method, of class Element.
     */
    @Test
    public void testGetName() {
        logger.info("getName");
        Element instance = new Element();
        String expResult = "Nazwa";
        instance.setName(expResult);
        String result = instance.getName();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getValue method, of class Element.
     */
    @Test
    public void testGetValue() {
        logger.info("getValue");
        Element instance = new Element();
        double expResult = 0.0;
        double result = instance.getValue();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setName method, of class Element.
     */
    @Test
    public void testSetName() {
        logger.info("setName");
        String new_name = "Nazwa";
        Element instance = new Element();
        instance.setName(new_name);
        assertEquals(new_name , instance.getName());

        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setValue method, of class Element.
     */
    @Test
    public void testSetValue() {
        logger.info("setValue");
        double new_value = 0.0;
        Element instance = new Element();
        instance.setValue(new_value);
        assertEquals(new_value , instance.getValue(),0);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of toString method, of class Element.
     */
    @Test
    public void testToString() {
        logger.info("toString");
        Element instance = new Element();
        String expResult = instance.getName() + " : " + instance.getValue();
        String result = instance.toString();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");


        //return name + " : " + value;
    }

}