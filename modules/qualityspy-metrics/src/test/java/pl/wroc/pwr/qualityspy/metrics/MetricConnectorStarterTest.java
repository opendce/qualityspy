/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.wroc.pwr.qualityspy.metrics;

import java.lang.reflect.Field;
import java.util.ArrayList;
import org.apache.log4j.Logger;
import org.apache.maven.shared.invoker.MavenInvocationException;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import pl.wroc.pwr.qualityspy.common.ConfigurationManager;
import pl.wroc.pwr.qualityspy.common.Properties;
import pl.wroc.pwr.qualityspy.model.Interrupter;

/**
 *
 * @author mateusz
 */
public class MetricConnectorStarterTest {
    /** logger **/
    private static final Logger logger = Logger.getLogger(MetricConnectorStarterTest.class.getName());
    
    public MetricConnectorStarterTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getMetricConnector method, of class MetricConnectorStarter.
     */
    @Test
    public void testGetMetricConnector() {
        logger.info("getMetricConnector");
//        MetricConnectorStarter instance = null;
//        MetricConnector expResult = null;
//        MetricConnector result = instance.getMetricConnector();
//        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of toString method, of class MetricConnectorStarter.
     */
    @Test
    public void testToString() {
        logger.info("toString");
//        MetricConnectorStarter instance = null;
//        String expResult = "";
//        String result = instance.toString();
//        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of main method, of class MetricConnectorStarter.
     */
    @Test
    public void testMain() {
        logger.info("main");
//        String[] args = null;
//        MetricConnectorStarter.main(args);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    /*
    Test jest ignorowany.
    Test nie przechodzi w środowisku Hudson
    Stacktrace błędu testu:
    org.apache.maven.shared.invoker.MavenInvocationException: Maven invocation failed: Error while executing process.
	at java.lang.UNIXProcess.forkAndExec(Native Method)
	at java.lang.UNIXProcess.<init>(UNIXProcess.java:248)
	at java.lang.ProcessImpl.start(ProcessImpl.java:134)
	at java.lang.ProcessBuilder.start(ProcessBuilder.java:1029)
	at java.lang.Runtime.exec(Runtime.java:620)
	at org.codehaus.plexus.util.cli.Commandline.execute(Commandline.java:670)
	at org.codehaus.plexus.util.cli.CommandLineUtils.executeCommandLineAsCallable(CommandLineUtils.java:134)
	at org.codehaus.plexus.util.cli.CommandLineUtils.executeCommandLine(CommandLineUtils.java:105)
	at org.codehaus.plexus.util.cli.CommandLineUtils.executeCommandLine(CommandLineUtils.java:74)
	at org.apache.maven.shared.invoker.DefaultInvoker.executeCommandLine(DefaultInvoker.java:159)
	at org.apache.maven.shared.invoker.DefaultInvoker.execute(DefaultInvoker.java:112)
	at pl.wroc.pwr.qualityspy.metrics.MetricConnectorStarter.resolveDependenciesWithMaven(MetricConnectorStarter.java:221)
	at pl.wroc.pwr.qualityspy.metrics.MetricConnectorStarter.addLibraries(MetricConnectorStarter.java:171)
	at pl.wroc.pwr.qualityspy.metrics.MetricConnectorStarter.<init>(MetricConnectorStarter.java:73)
	at pl.wroc.pwr.qualityspy.metrics.MetricConnectorStarterTest.testAcceptanceMaven(MetricConnectorStarterTest.java:108)
    */
    @Ignore
    @Test
    public void testAcceptanceMaven() throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException, MavenInvocationException {
        Properties metricsProperties = new ConfigurationManager(getClass().getResource("/").getPath()).loadConfiguration("", "maven_prop");
        
        String testPath = getClass().getResource("/").getPath();
        String mavenHomePath = testPath + "maven_home";
        String jarPath = testPath + "qualityspy-metrics-0.4-SNAPSHOT.jar";
        String mavenPrjPath = testPath + "maven_prj";
        
        System.out.println(testPath);
        System.out.println(mavenHomePath);
        System.out.println(jarPath);
        System.out.println(mavenPrjPath);
        
        metricsProperties.setCkjmJar(jarPath);
        metricsProperties.setCkjmMavenHome(mavenHomePath);
        metricsProperties.setCkjmMavenProjectDirectory(mavenPrjPath);
        
        System.out.println("before starter");
        MetricConnectorStarter starter = new MetricConnectorStarter(metricsProperties, new Interrupter());
        System.out.println("after starter");
        
        // Use reflection to access private field
        Field projectLibsField = MetricConnectorStarter.class.getDeclaredField("project_libs");
        projectLibsField.setAccessible(true);
        ArrayList<String> projectLibs = (ArrayList<String>) projectLibsField.get(starter);
        
        ArrayList<String> projectLibsNames = new ArrayList<String>(projectLibs.size());
        System.out.println("Lib names");
        for(String s : projectLibs) {
            // Platform independend path splitting
            String[] split = s.replaceAll("\\\\", "/").split("/");
            projectLibsNames.add(split[split.length - 1]);
        }
        
        assertTrue(projectLibsNames.contains("ant-1.7.1.jar"));
        assertTrue(projectLibsNames.contains("hibernate-annotations-3.5.6-Final.jar"));
        assertTrue(projectLibsNames.contains("plexus-utils-3.0.20.jar"));
        assertTrue(projectLibsNames.contains("maven-invoker-2.2.jar"));
          
    }
   

}