package pl.wroc.pwr.qualityspy.metrics;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;


/** Klasa pzowalajaca na dodawanie dodatkowych bibliotek do classpatha podczas dzialania programu */
public class ClassPathHacker {

    private static final Logger logger = Logger.getLogger(ClassPathHacker.class.getName());
    private static final String separator = System.getProperty("path.separator");

    public static void addFile(String s) throws IOException {
        System.out.println("ClassPathHacker: addFIle: " + s);
        File f = new File(s);
        addFile(f);
    }

    public static void addFile(File f) throws IOException {
         String newResource = f.getAbsolutePath();
         
         String classPath = System.getProperty("java.class.path");
        
        classPath += separator + newResource;
        System.setProperty("java.class.path", classPath);
        //classPath = System.getProperty("java.class.path");
    }
    
}