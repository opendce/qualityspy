package pl.wroc.pwr.qualityspy.metrics;

import java.io.IOException;
import java.io.OutputStream;

/** Klasa pomocnicza ułatwiająca pobranie danych ze strumienia wyjścioego z CKJM'a oraz przejmowanie bledow wystepujacyhc podczas jego pracy */
class StringOutputStream extends OutputStream
{

    StringBuilder buffer = new StringBuilder();

    public String getString()
    {
        return buffer.toString();
    }

    public void write(int b) throws IOException
    {
        buffer.append((char) b);
    }
}