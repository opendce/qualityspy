package pl.wroc.pwr.qualityspy.metrics;

import org.apache.log4j.Logger;
import pl.wroc.pwr.qualityspy.model.ClassDescription;
import pl.wroc.pwr.qualityspy.model.MethodDescription;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashSet;
import pl.wroc.pwr.qualityspy.model.Interrupter;

/** program główny odpowiedzialny za weryfikacje danych wejściowych, wywołanie klasy
 *  która uruchmomi CKJM'a oraz pobierze dane otrzymane  zniego,a następnie wyśle je do warstwy transportowej */
public class MetricsConnector implements IMetricsConnector{

    private static final Logger LOG = Logger.getLogger(MetricsConnector.class);
            
    private String project_name = null;
    private String project_version = null;
    //obiekty klas zewnetrznych
    /** obiekt klasy Starter, ktory przeprowadza obliczenia metryk za pomoca CKJM'a  dla zadanych plikow */
    private Starter starter = null;
    /** obiekt klasy Loader, ktory przeprowadza odczyt wyjscia CKJM'a i odpowiednia jego interpretacji */
    private Loader loader = null;
    //parametry dzialania
    /** sciezka dla ktorej zostana wyszukane pliki z klasami */
    private String path;
    /** lista plikow ktore udalo sie odnalezc w zadanej sciezce */
    private String files_list;
    /** zapamietuje czy poprawnie udalo sie wykonac program obliczanie metryk */
    private boolean success = false;
    /** Lista z danymi zapakowanymi w kontenery ktore mogą zostać pobrane */
    private ArrayList<ClassDescription> class_metrics = new ArrayList();
    private boolean missingClass = false;
    private boolean countMetrics = false;
    private boolean saveMetrics = false;
    private boolean addMissingClasses = false;
    private Interrupter interrupted;
    /** logger **/

    public MetricsConnector( String fl, Interrupter interrupted) {
        this.interrupted = interrupted;
        files_list = fl;
        starter = new Starter(files_list);
        while (starter != null && !interrupted.getIsInterrupted()) {
            if (starter.startCKJM(interrupted) == true) {
                loader = new Loader(starter.getResult());
                starter = null;
                countMetrics = true;
            } else {
                if (analizeErrors(starter.getErrors()) == true) {
                    starter = null;
                } else {
                    starter = null;
                }
            }
        }
        if (loader != null) {
            if (loader.readMetrics() == true) {
                this.saveMetrics = true;
                unpackMetrics(loader.getMetric());
                success = true;
            }
        }
    }

    public boolean isMissingClass() {
        return this.missingClass;
    }

    public boolean isCountMetrics() {
        return this.countMetrics;
    }

    public boolean isSaveMetrics() {
        return this.saveMetrics;
    }

    public boolean isAddClasses() {
        return this.addMissingClasses;
    }

    /** Funkcja sprawdza czy zadana sciazka istnieje */
    private boolean checkPath(String path) {
        File file = new File(path);
        if (file.exists()) {
            return true;
        } else {
            LOG.warn("Ścieżka niepoprawna.");
            return false;
        }
    }

    /** Funkcja azalizuje bledy ktore wystapily podczas pracy CKJM'a */
    private boolean analizeErrors(String errors) {
        String line;
        BufferedReader reader = new BufferedReader(new StringReader(errors));
        HashSet<String> classes = new HashSet();
        try {
            while ((line = reader.readLine()) != null && !interrupted.getIsInterrupted()) {
                line = line.replace(":", "");
                String[] words = line.split(" ");
                if (words[0].equals("java.lang.ClassNotFoundException")) {
                    classes.add(words[6]);
                } else {
                    LOG.warn("Wystąpił inny błąd niż brak klasy");
                    return false;
                }
            }
        } catch (IOException e) {
            LOG.warn("Wystąpił błąd podczas odczytu strumienia ze String'a z błędami");
            return false;
        }
        LOG.warn("Brakuje następujących klas: ");
        String k = "";
        for (String s : classes) {
            k += s + "\n";
            LOG.info(s);
        }
        if (!k.equals("")) {
            String msg = "The following classes are missing:\n" + k;
            JOptionPane.showMessageDialog(null, msg);
            throw new RuntimeException(msg);
        }
        return true;
    }

    public ArrayList<ClassDescription> getMetric() {
        if (success == true) {
            return class_metrics;
        } else {
            return null;
        }
    }

    public String getProjectName() {
        return project_name;
    }

    public String getProjektVersion() {
        return project_version;
    }

    public boolean getSuccess() {
        return success;
    }

    private void unpackMetrics(ArrayList<Metric> metrics) {
        for (Metric met : metrics) {
            if (interrupted.getIsInterrupted()) break;
            ClassDescription des = new ClassDescription();
            des.setAmc(met.getMetricValue("AMC"));
            des.setCa((int) met.getMetricValue("Ca"));
            des.setCam(met.getMetricValue("CAM"));
            des.setCbm((int) met.getMetricValue("CBM"));
            des.setCbo((int) met.getMetricValue("CBO"));
            des.setCe((int) met.getMetricValue("Ce"));
            des.setDam(met.getMetricValue("DAM"));
            des.setDit((int) met.getMetricValue("DIT"));
            des.setIc((int) met.getMetricValue("IC"));
            des.setLcom((int) met.getMetricValue("LCOM"));
            des.setLcom3(met.getMetricValue("LCOM3"));
            des.setLoc((int) met.getMetricValue("LOC"));
            des.setMfa(met.getMetricValue("MFA"));
            des.setMoa((int) met.getMetricValue("MOA"));
            des.setNoc((int) met.getMetricValue("NOC"));
            des.setNpm((int) met.getMetricValue("NPM"));
            des.setRfc((int) met.getMetricValue("RFC"));
            des.setWmc((int) met.getMetricValue("WMC"));
            des.setName(met.getClassName());
            MethodDescription md = null;
            int maxCC = 0;
            double avarageCC = 0.0;
            for (Element e : met.getMethodMetrics()) {
                md = new MethodDescription();
                md.setName(e.getName());
                md.setCyclomaticComplexity((long) e.getValue());
                avarageCC += e.getValue();
                if ((long) e.getValue() > maxCC) {
                    maxCC = (int) e.getValue();
                }
                des.setMethod(md);
            }
            avarageCC /= met.getMethodMetrics().size();
            des.setMaxCC(maxCC);
            des.setAverageCC(avarageCC);
            class_metrics.add(des);
        }
    }
}
