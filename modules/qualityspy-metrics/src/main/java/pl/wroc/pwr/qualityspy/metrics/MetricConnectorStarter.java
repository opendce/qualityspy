package pl.wroc.pwr.qualityspy.metrics;

import org.apache.log4j.Logger;
import pl.wroc.pwr.qualityspy.common.CkjmProperties;
import pl.wroc.pwr.qualityspy.common.SourceTypeEnum;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import org.apache.maven.shared.invoker.DefaultInvocationRequest;
import org.apache.maven.shared.invoker.DefaultInvoker;
import org.apache.maven.shared.invoker.InvocationRequest;
import org.apache.maven.shared.invoker.InvocationResult;
import org.apache.maven.shared.invoker.Invoker;
import org.apache.maven.shared.invoker.MavenInvocationException;
import org.apache.maven.shared.invoker.SystemOutLogger;
import pl.wroc.pwr.qualityspy.model.Interrupter;

public class MetricConnectorStarter {

    private static final Logger LOG = Logger.getLogger(MetricConnectorStarter.class);

    private boolean JAR = false;
    private boolean CLASS = false;
    private boolean fileExists = false;
    private boolean classExists = false;
    private boolean goodPath = false;
    private boolean useMaven = false;
    private boolean useMavenHomePath = false;
    private boolean useJavaHomePath = false;
    private String mavenPrjDirectory = null;
    private String mavenHome = null;
    private String javaHome = null;
    private ArrayList<String> project_paths = new ArrayList<String>();
    private ArrayList<String> project_libs = new ArrayList<String>();
    private ArrayList<String> project_files = new ArrayList<String>();
    private MetricsConnector mc = null;

    public boolean isJAR() {
        return this.JAR;
    }

    public boolean isCLASS() {
        return this.CLASS;
    }

    public boolean isGoodPath() {
        return !this.project_paths.isEmpty();
    }

    public boolean isClassExists() {
        return this.classExists;
    }

    public boolean isFileExists() {
        return this.fileExists;
    }
    
    public boolean isMavenInUse() {
        return this.useMaven;
    }
    ////////////////////////////////////////////////////////////////////////////

    public MetricConnectorStarter(CkjmProperties config, Interrupter interrupted) throws MavenInvocationException {
        if (readProperties(config)) {
            if (!project_paths.isEmpty()) {

                if (analizePaths()) {
                    if (addLibraries()) {
                        mc = new MetricsConnector(getFileList(), interrupted);
                    }
                } else {
                    //System.out.println("Nie udaĹ‚o siÄ™ znaleĹşÄ‡ ĹĽadnych plikĂłw z klasami");
                    LOG.warn("Nie udaĹ‚o siÄ™ znaleĹşÄ‡ ĹĽadnych plikĂłw z klasami");
                    throw new RuntimeException("CKJM - no class files found, looking in: " + project_paths);
                }
            } else {
                //System.out.println("Brak poprawnych Ĺ›cieĹĽek w pliku z wĹ‚aĹ›ciwoĹ›ciami");
                LOG.warn("Brak poprawnych Ĺ›cieĹĽek w pliku z wĹ‚aĹ›ciwoĹ›ciami");
                throw new RuntimeException("CKJM: no files on the configured paths (ckjmJar or ckjmClass parameter).");
            }
        }
    }

    private boolean readProperties(CkjmProperties props) {
        String mavenDirectory = props.getCkjmMavenProjectDirectory();
        if(mavenDirectory != null && !mavenDirectory.isEmpty()) {
            useMaven = true;
            mavenPrjDirectory = mavenDirectory;
            //LOG.debug("Maven directory: " + mavenDirectory);
            System.out.println("Maven directory: " + mavenDirectory);
        }
        String mavenHome = props.getCkjmMavenHome();
        if(mavenHome != null && !mavenHome.isEmpty()) {
            this.mavenHome = mavenHome;
            this.useMavenHomePath = true;
        }
        String javaHome = props.getCkjmJavaHome();
        if(javaHome != null && !javaHome.isEmpty()) {
            this.javaHome = javaHome;
            this.useJavaHomePath = true;
        }
        if (props.getCkjmSourceType() == SourceTypeEnum.JAR) {
            JAR = true;
            CLASS = false;
            project_paths.add(props.getCkjmJar());
        } else if (props.getCkjmSourceType() == SourceTypeEnum.CLASS) {
            JAR = false;
            CLASS = true;
            project_paths.add(props.getCkjmClass());
        } else {
            throw new RuntimeException("CKJM: unknown source format.");
        }

        String separator = props.getCkjmLibrariesSeparator();
        if (separator == null || "".equals(separator)) {
            separator = File.pathSeparator;
        }
        if (props.getCkjmLibraries() != null) {
            for (String lib : props.getCkjmLibraries().split(separator)) {
                project_libs.add(lib);
            }
        }
        
        this.fileExists = true;
        return true;

    }

    private boolean makeFileList(String path) {
        FileFinder.clear();
        if (JAR == true) {
            FileFinder.find(path, "jar");
        }
        if (CLASS == true) {
            FileFinder.find(path, "class");
        }
        if (FileFinder.fileList.isEmpty()) {
            LOG.warn("Nie odnaleziono ĹĽadnych plikĂłw, z klasami w Ĺ›cieĹĽce " + path);
            return false;
        } else {
            project_files.addAll(FileFinder.fileList);
            return true;
        }
    }

    private boolean analizePaths() {
        for (String s : project_paths) {
            makeFileList(s);
        }
        for (String s : project_files) {
            if (FileFinder.check(new File(s), "jar")) {
                project_libs.add(s);
            }
        }
        if (project_files.isEmpty()) {
            this.classExists = false;
            return false;
        } else {
            this.classExists = true;
            return true;
        }
    }

    private boolean addLibraries() throws MavenInvocationException {
        if(useMaven)
            resolveDependenciesWithMaven();
        for (String path_jar : project_libs) {
            try {
                ClassPathHacker.addFile(path_jar);
            } catch (IOException ex) {
                LOG.warn("WystÄ…piĹ‚ bĹ‚Ä…d podczas dodawania bibliotek");
                return false;
            }
        }
        return true;
    }
    
    private boolean checkInvocationResult(InvocationResult result) throws MavenInvocationException {
        if ( result.getExitCode() != 0 )
        {
            if ( result.getExecutionException() != null )
            {
                System.out.println("Maven invocation failed: " +
                                            result.getExecutionException().getMessage());
                throw new MavenInvocationException( "Maven invocation failed: " +
                                            result.getExecutionException().getMessage(), result.getExecutionException());
            }
            else
            {
                System.out.println("Maven exited with non-zero exit code. Exit code: " + 
                                             result.getExitCode());
                throw new MavenInvocationException( "Maven exited with non-zero exit code. Exit code: " + 
                                             result.getExitCode() );
            }
        } 
        return true;
    }
    
    private boolean resolveDependenciesWithMaven() throws MavenInvocationException {
        
        System.out.println("resolveDependenciesWithMaven");
        InvocationRequest request = new DefaultInvocationRequest();
        request.setPomFile( new File( mavenPrjDirectory + FileSystems.getDefault().getSeparator() + "pom.xml" ) );
        request.setGoals( Collections.singletonList( "dependency:resolve" ) );

        Invoker invoker = new DefaultInvoker();
        
        invoker.setLogger(new SystemOutLogger());

        if(useMavenHomePath)
            invoker.setMavenHome(new File(mavenHome));
        if(useJavaHomePath)
            request.setJavaHome(new File(javaHome));

        System.out.println("Maven: Invoking dependency:resolve");
        InvocationResult result = invoker.execute( request );
        checkInvocationResult(result);

        request.setGoals(Collections.singletonList( "dependency:build-classpath -Dmdep.outputFile=classes.classpath"));
        System.out.println("Maven: Invokind dependency:build-classpath");
        result = invoker.execute( request );
        checkInvocationResult(result);
            
        readMavenGeneratedClassPath();
        
        return true;
    }

    private void readMavenGeneratedClassPath() throws RuntimeException {
        /* Testy generujÄ… Ĺ›cieĹĽkÄ™ bezwzglÄ™dnÄ… przy pomocy metody getResource
        Ĺ›ciezka ta rozpoczna siÄ™ od znaku "/", powoduje to problem w klasie
        FileSystems. Dla systemĂłw windows poczÄ…tkowy znak "/" musi zostaÄ‡
        obciÄ™ty
        */
        String classPathFilePath = mavenPrjDirectory + "/classes.classpath";
        String osApproprioratePath;
        if(System.getProperty( "os.name" ).contains( "indow" ) && classPathFilePath.startsWith("/")) {
            osApproprioratePath = classPathFilePath.substring(1);
        } else {
            osApproprioratePath = classPathFilePath;
        }
        
        Path path = FileSystems.getDefault().getPath(osApproprioratePath);
        
        String classPath;
        try {
            classPath = new String(Files.readAllBytes(path));
            System.out.println("Maven: classPath: " + classPath);
        } catch (IOException ex) {
            throw new RuntimeException("CKJM - Maven: could not open class path file!");
        }
        
        for(String s : classPath.split(";")) {
            if(!s.isEmpty())
                project_libs.add(s);
        }
    }

    private String getFileList() {
        String fl = "";
        for (String file : project_files) {
            fl += file + File.pathSeparator;
        }
        if (fl.length() > 1) {
            return fl.substring(0, fl.length() - 1);
        }
        return fl;
    }

    public IMetricsConnector getMetricConnector() {
        return mc;
    }

    public String toString() {
        String result = "";
        result += JAR + "\n";
        result += CLASS + "\n";
        result += "PLIKI Z KLASAMI" + "\n";
        for (String s : project_files) {
            result += s + "\n";
        }
        result += "BIBLIOTEKI" + "\n";
        for (String s : project_libs) {
            result += s + "\n";
        }
        return result;
    }
}
