package pl.wroc.pwr.qualityspy.metrics;

import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;

/**
 * Klasa służąca do załadowania pobranych z CKJM'a danych do konterów
 * przechowujących dane poszczeólnych klas
 */
public class Loader {

    private static final Logger LOG = Logger.getLogger(Loader.class);
    
    /**
     * zawiera dane wejsciowe otrzymane z CKJM'a
     */
    private String data;
    /**
     * zawiera upakowane w odpowiednie obiekty dane odczytane z wyjscia CKJM'a
     */
    private ArrayList<Metric> class_metrics = new ArrayList<Metric>();

    /**
     * konstruktor ktory jako argument pobiera String zawierajacy wyjscie z
     * CKJM'a
     */
    Loader(String in) {
        data = in;
    }

    /**
     * fukcja odczytujaca dane z String'a ktory jest wyjsciem CKJM'a i
     * zapisujaca je w liscie obiektow typu Metric
     */
    public boolean readMetrics() {
        BufferedReader reader;
        String[] metric_names = {null, "WMC", "DIT", "NOC", "CBO", "RFC", "LCOM", "Ca", "Ce", "NPM",
            "LCOM3", "LOC", "DAM", "MOA", "MFA", "CAM", "IC", "CBM", "AMC"};
        try {
            reader = new BufferedReader(new StringReader(data));
            String line = "";
            while ((line = reader.readLine()) != null) {
                String[] metric_tab = line.split(" ");
                Metric metric = new Metric(metric_tab[0]);
                for (int i = 1; i < metric_tab.length; i++) {
                    metric_tab[i] = metric_tab[i].replace(',', '.');
                    metric.addClassMetric(metric_names[i], Double.parseDouble(metric_tab[i]));
                }
                while ((line = reader.readLine()) != null && !line.isEmpty()) {
                    line = line.replaceFirst(" ~ ", "");
                    String[] method = line.split(": ");
                    method[0] = method[0].trim();
                    method[1] = method[1].trim();
                    method[1] = method[1].replace(',', '.');
                    metric.addMethodMetric(method[0], Double.parseDouble(method[1]));
                }
                class_metrics.add(metric);
            }
            reader.close();
        } catch (IOException ex) {
            LOG.info("Wystąpił błąd podczas odczytu danych ze strumienia");
            return false;
        }
        return true;
    }

    /**
     * funkcja wysylajace metryki w kontenerze przez nas stworzonym
     */
    public ArrayList<Metric> getMetric() {
        return class_metrics;
    }
}
