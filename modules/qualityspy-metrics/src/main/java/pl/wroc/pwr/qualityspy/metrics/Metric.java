package pl.wroc.pwr.qualityspy.metrics;

import java.util.ArrayList;

/**
 * Kontener do danych o klasie wczytanych z CKJM'a
 */
public class Metric {

    /**
     * nazwa klasy o ktorej dane sa przechowywane w obiekcie
     */
    private String class_name;
    /**
     * lista zawierajaca dane o wyliczonych metrykach dla klasy
     */
    private ArrayList<Element> metric_values = new ArrayList<Element>();
    /**
     * lista zawierajaca dane o wyliczonych metrykach dla metod klasy
     */
    private ArrayList<Element> method_metrics = new ArrayList<Element>();

    /**
     * argumentem funkcji jest nazwa klasy o ktorej dane ma przechowywac obiekt
     */
    public Metric(String s) {
        class_name = s;
    }

    /**
     * funkcja dodaje do listy z metrykami klasy element o zadanym nazwie
     * (kluczu) i wartosci
     */
    public boolean addClassMetric(String name, double value) {
        return metric_values.add(new Element(name, value));
    }

    /**
     * funkcja dodaje do listy z metrykami metod klasy element o zadanym nazwie
     * (prototypie funkcji) i wartosci
     */
    public boolean addMethodMetric(String name, double value) {
        return method_metrics.add(new Element(name, value));
    }

    public String getClassName() {
        return class_name;
    }

    public double getMetricValue(String name) {
        for (Element e : metric_values) {
            if ((e.getName()).equalsIgnoreCase(name)) {
                return e.getValue();
            }
        }
        return -1;
    }

    public ArrayList<Element> getMethodMetrics() {
        return method_metrics;
    }

    /**
     * standardowa funkcja zmieniajaca obiekt klasy na String
     */
    public String toString() {
        String s = class_name + "\n";
        for (Element e : metric_values) {
            s += e.toString() + "\n";
        }
        for (Element e : method_metrics) {
            s += e.toString() + "\n";
        }
        return s;
    }
}
