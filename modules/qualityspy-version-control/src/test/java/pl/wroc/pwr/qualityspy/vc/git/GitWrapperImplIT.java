/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wroc.pwr.qualityspy.vc.git;

import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevWalk;
import org.junit.*;

import static org.junit.Assert.*;

import pl.wroc.pwr.qualityspy.common.ConfigurationManager;
import pl.wroc.pwr.qualityspy.common.Properties;
import pl.wroc.pwr.qualityspy.model.ProjectHistory;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.easymock.EasyMock;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.JGitInternalException;
import org.eclipse.jgit.errors.IncorrectObjectTypeException;
import org.eclipse.jgit.lib.AnyObjectId;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;

/**
 * @author Piotrek
 */
public class GitWrapperImplIT {
    private Repository repo;
    private RevWalk revWalk;
    private Git git;
    private GitWrapperImpl gitWrapperImpl;

    String repoURI;
    String repoDir;


    @Before
    public void setUp() throws IOException, GitAPIException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException {

        repoURI = "https://github.com/florent37/MaterialViewPager.git";
        repoDir = "/tmp/QualitySpy";

        gitWrapperImpl = new GitWrapperImpl();
        gitWrapperImpl.setURI(repoURI);
        gitWrapperImpl.setDir(repoDir);

        gitWrapperImpl.buildRepository();

        repo = gitWrapperImpl.getRepository();

        revWalk = new RevWalk(repo);
        revWalk.markStart(revWalk.parseCommit(repo.resolve("HEAD")));

        // Java Reflection for set private field repository in GitWrapperImpl class
        //Field repository = GitWrapperImpl.class.getDeclaredField("repository");
        //repository.setAccessible(true);
        //repository.set(gitWrapperImpl, repo);
    }

    /**
     * Test of getRevTag method, of class GitWrapperImpl.
     *
     * @throws org.eclipse.jgit.errors.IncorrectObjectTypeException
     */
    @Test
    public void testGetRevTag() throws IncorrectObjectTypeException, IOException {
        System.out.println("getRevTag");

        // Arrange
        String expResult1 = "1.0.2";
        String expResult2 = "1.0.0";
        String expResult3 = "";
        String commitName1 = "73fe0e8abd6db074122cb5c4a2d6ea39f7bcda8e";
        String commitName2 = "179c1dbc8feafbed408e2ab52a9bc64ac589baf3";
        String commitName3 = "191c2c99d2fab9d786d794c63b031e6bf71b00d4";
        RevCommit revCommit1 = getRevCommit(commitName1);
        RevCommit revCommit2 = getRevCommit(commitName2);
        RevCommit revCommit3 = getRevCommit(commitName3);

        // Action
        String result1 = gitWrapperImpl.getRevTag(revCommit1);
        String result2 = gitWrapperImpl.getRevTag(revCommit2);
        String result3 = gitWrapperImpl.getRevTag(revCommit3);

        // Assert
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
        assertEquals(expResult3, result3);
    }

    /**
     * Test of getRevBranch method, of class GitWrapperImpl.
     *
     * @throws org.eclipse.jgit.errors.IncorrectObjectTypeException
     */
    @Test
    public void testGetRevBranch() throws IncorrectObjectTypeException, IOException {
        System.out.println("getRevBranch");

        //Arrange
        String expResult1 = "refs/heads/master";
        String expResult2 = "refs/heads/master";
        String commitName1 = "191c2c99d2fab9d786d794c63b031e6bf71b00d4";
        String commitName2 = "191c2c99d2fab9d786d794c63b031e6bf71b00d4";
        RevCommit revCommit1 = getRevCommit(commitName1);
        RevCommit revCommit2 = getRevCommit(commitName2);

        //Action
        String result1 = gitWrapperImpl.getRevBranch(revCommit1);
        String result2 = gitWrapperImpl.getRevBranch(revCommit2);

        //Assert
        assertNotNull(result1);
        assertNotNull(result2);
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
    }

    private RevCommit getRevCommit(String commitName) {
        Iterator<RevCommit> i = revWalk.iterator();
        RevCommit revCommit = null;

        while (i.hasNext()) {
            revCommit = i.next();
            if (commitName == null || revCommit.getId().getName().equals(commitName))
                break;
        }

        return revCommit;
    }
}