/*
 * Added javadoc code.
 * Krzysiek C.
 */

package pl.wroc.pwr.qualityspy.vc;

import java.io.IOException;

import org.junit.Ignore;
import pl.wroc.pwr.qualityspy.model.File;
import pl.wroc.pwr.qualityspy.model.ProjectHistory;
import pl.wroc.pwr.qualityspy.model.File.ChangeType;
import pl.wroc.pwr.qualityspy.common.Properties;
import org.easymock.EasyMock;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.tmatesoft.svn.core.SVNLogEntry;
import org.tmatesoft.svn.core.SVNLogEntryPath;

import static org.junit.Assert.*;
import org.tmatesoft.svn.core.SVNNodeKind;
import pl.wroc.pwr.qualityspy.vc.svn.SVNLogListener;

/**
 * Class for testing SVNLogListener @see SVNLogListener
 */
public class SVNLogListenerIT {

	static final String mURL = "http://svn.apache.org/repos/asf/tomcat/trunk";
	static String mBugfixPattern = ".*(([bB][uU][gG][fF][iI][xX])|([bB][uU][gG][zZ][iI][lL][lL][aA])).*(\n.*)*";
	// static final String mBugfixPattern = ".*(Fix).*";
	static final long rev = 719983;
	SVNLogListener mSvnLogListener;
	ProjectHistory mRes;
	static char mSep = '/';
	Properties mProperties;

	/**
	 * Sets up repository properties.
	 */
	public SVNLogListenerIT() {
		mProperties = EasyMock.createMock(Properties.class);
	}

	/**
	 * Sets up repository history.
	 */
	@Before
	public void setUp() {
		mRes = new ProjectHistory();
	}

	@After
	public void tearDown() {

	}

	/**
	 * Testing is path correct.
	 */
	@Test
	public void testIsPathToClass1() {
		String clas = "java.class";

		configurePropertiesMock(null, null, null, mSep);
		mSvnLogListener = new SVNLogListener(mRes, mProperties, null, null);
		assertTrue(mSvnLogListener.isPathToClass(clas));
	}

	/**
	 * Testing is path correct.
	 */
	@Test
	public void testIsPathToClass2() {
		String pre = "pre";
		String post = "post";
		String clas = "java.class";

		configurePropertiesMock(null, pre, post, mSep);
		mSvnLogListener = new SVNLogListener(mRes, mProperties, null, null);
		assertFalse(mSvnLogListener.isPathToClass(clas));
		assertFalse(mSvnLogListener.isPathToClass(pre + clas));
		assertFalse(mSvnLogListener.isPathToClass(clas + post));
		assertTrue(mSvnLogListener.isPathToClass(pre + clas + post));
	}

	/**
	 * Testing converting path to svn class name.
	 */
	@Test
	public void testConvertPathToClassName1() {
		String pre = "/tomcat/";
		String post = "java";
		String path = "/tomcat/tomcat/trunk/modules/jdbc-pool/java/org/apache/tomcat/jdbc/pool/ConnectionPool.java";

		configurePropertiesMock(null, pre, post, mSep);
		mSvnLogListener = new SVNLogListener(mRes, mProperties, null, null);
		String className = mSvnLogListener.convertPathToClassName(path);
		assertEquals(
				0,
				"tomcat.trunk.modules.jdbc-pool.java.org.apache.tomcat.jdbc.pool.ConnectionPool."
						.compareTo(className));
	}

	/**
	 * Testing converting path to svn class name.
	 */
	@Test
	public void testConvertPathToClassName2() {
		String pre = ".*/java/";
		String post = ".java";
		String path = "/tomcat/tomcat/trunk/modules/jdbc-pool/java/org/apache/tomcat/jdbc/pool/ConnectionPool.java";

		configurePropertiesMock(null, pre, post, mSep);
		mSvnLogListener = new SVNLogListener(mRes, mProperties, null, null);
		String className = mSvnLogListener.convertPathToClassName(path);
		assertEquals(0, "org.apache.tomcat.jdbc.pool.ConnectionPool"
				.compareTo(className));
	}
	
//	@Test
//	public void testCreateFileDescription() throws IOException {
//	//given
//		String pre = ".*/java/";
//		String post = ".java";
//		String path = "";
//		String url = "http://svn.apache.org/repos/asf/tomcat/trunk/";
//                String repoContentRegex = "";
//                File file1, file2, file3, file4;
//		// initialize mocks
//		SVNLogEntryPath entryPath = EasyMock.createMock(SVNLogEntryPath.class);
//		SVNLogEntry logEntry = EasyMock.createMock(SVNLogEntry.class);
//		String name = "org.apache.tomcat.util.net.AbstractEndpoint";
//		// replay mocks
//		EasyMock.expect(mProperties.getReposrcpathprefixregex()).andReturn(pre).anyTimes();
//		EasyMock.expect(mProperties.getReposrcpathpostfixregex()).andReturn(post).anyTimes();
//		EasyMock.expect(mProperties.getRepofileseparator()).andReturn(mSep).anyTimes();
//		EasyMock.expect(mProperties.getRepostartrev()).andReturn(rev).anyTimes();
//		EasyMock.expect(mProperties.getRepourl()).andReturn(url).anyTimes();
//                EasyMock.expect(mProperties.getRepoContentRegex()).andReturn(repoContentRegex).times(1);
//                EasyMock.expect(mProperties.getRepoContentRegex()).andReturn("").times(1);
//                EasyMock.expect(mProperties.getRepoContentRegex()).andReturn(".*").times(1);
//                EasyMock.expect(mProperties.getRepoContentRegex()).andReturn("[.*][.txt]+").times(1);
//		EasyMock.expect(logEntry.getRevision()).andReturn(rev).anyTimes();
//		EasyMock.expect(entryPath.getPath()).andReturn(path).anyTimes();
//		EasyMock.expect(entryPath.getCopyPath()).andReturn("hello/" + name); //changed path
//		EasyMock.expect(entryPath.getCopyPath()).andReturn(null).times(4);
//                EasyMock.expect(entryPath.getKind()).andReturn(SVNNodeKind.FILE).times(4);
//		EasyMock.expect(entryPath.getType()).andReturn('D'); // deletion
//		EasyMock.expect(entryPath.getType()).andReturn('A').times(2); //creation
//		EasyMock.expect(entryPath.getType()).andReturn('M').times(2); //changed content
//		EasyMock.replay(entryPath, logEntry, mProperties);
//
//	//when
//		SVNLogListener listener = new SVNLogListener(mRes, mProperties);
//		file1 = listener.createFileDescription(entryPath, logEntry, name);
//		file2 = listener.createFileDescription(entryPath, logEntry, name);
//		file3 = listener.createFileDescription(entryPath, logEntry, name);
//		file4 = listener.createFileDescription(entryPath, logEntry, name);
//
//	//then
//		assertNotNull(file1);
//		assertEquals(ChangeType.CHANGED_PATH, file1.getChangeType());
//                assertNotNull(file1.getNewContent());
//		assertNotNull(file2);
//		assertEquals(ChangeType.DELETION, file2.getChangeType());
//                assertNotNull(file2.getNewContent());
//		assertNotNull(file3);
//		assertEquals(ChangeType.CREATION, file3.getChangeType());
//                assertNotNull(file3.getNewContent());
//		assertNotNull(file4);
//		assertEquals(ChangeType.CHANGED_CONTENT, file4.getChangeType());
//                assertEquals(null,file4.getNewContent());
//	}

	/**
	 * Configures all needed stuff for repository.
	 * 
	 * @param bugfixPattern
	 *            Bugfix pattern.
	 * @param pre
	 *            Regex prefix.
	 * @param post
	 *            Regex postfix.
	 * @param fileSeparator
	 *            File separator character.
	 */
	private void configurePropertiesMock(String bugfixPattern, String pre,
			String post, char fileSeparator) {

		EasyMock.expect(mProperties.getReposrcpathprefixregex()).andReturn(pre).anyTimes();
		EasyMock.expect(mProperties.getReposrcpathpostfixregex()).andReturn(
				post).anyTimes();
		EasyMock.expect(mProperties.getRepofileseparator()).andReturn(
				fileSeparator).anyTimes();
		EasyMock.replay(mProperties);
	}
}