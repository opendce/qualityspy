package pl.wroc.pwr.qualityspy.vc;

import java.io.IOException;
import java.net.URISyntaxException;
import static org.junit.Assert.*;
import org.junit.Before;

import org.junit.Test;
import org.tmatesoft.svn.core.SVNLogEntryPath;
import pl.wroc.pwr.qualityspy.model.File;
import pl.wroc.pwr.qualityspy.model.enums.FileType;
import pl.wroc.pwr.qualityspy.vc.FileAnalyzer;

public class FileAnalyzerTest {

    private FileAnalyzer FA;

    @Before
    public void setUp() throws URISyntaxException, IOException {
        FA = new FileAnalyzer();
    }

    @Test
    public void testCheckFileType() {
        
        String[] texts = {"java", "h", "c", "cpp", "txt", "asc", "md", "pas", "xml", "xsd"};

        for (String text : texts) {
            assertEquals(FileType.TEXT, FA.checkFileType(text));
        }

        String[] binaries = {"exe", "jar", "rar", "zip", "gz", "tgz", "jpg", "jpeg", "gif", "bmp", "pcx", "png", "mpg", "mpeg", "mp4", "avi", "mp3", "wav", "au", "mid"};
        
        for (String binarie : binaries) {
            assertEquals(FileType.BINARY, FA.checkFileType(binarie));
        }

        String other = "other";
        assertEquals(FileType.OTHER, FA.checkFileType(other));

    }

    @Test
    public void testCheckFileExtension() {
        
        String path = "/tomcat/trunk/test/org/apache/catalina/mbeans/MBeanDumper.java";
        assertEquals("java", FA.checkFileExtension(path));
    }

    @Test
    public void testGetChangeTypeGit() {

        assertEquals(File.ChangeType.DELETION, FA.getChangeTypeGit("deleted test", "/test/test2/" , "/test/test2/"));
        assertNotEquals(File.ChangeType.DELETION, FA.getChangeTypeGit("test deleted test", "/test/test2/" , "/test/test2/"));

        assertEquals(File.ChangeType.CREATION, FA.getChangeTypeGit("new file test", "/test/test2/" , "/test/test2/"));
        assertNotEquals(File.ChangeType.CREATION, FA.getChangeTypeGit("test new file test", "/test/test2/" , "/test/test2/"));

        assertEquals(File.ChangeType.CHANGED_PATH, FA.getChangeTypeGit("test", "/test/test2/" , "/test/test3/"));
        assertNotEquals(File.ChangeType.CHANGED_PATH, FA.getChangeTypeGit("test", "/test/test2/" , "/test/test2/"));

        assertEquals(File.ChangeType.CHANGED_CONTENT, FA.getChangeTypeGit("test", "/test/test2/" , "/test/test2/"));
        assertEquals(File.ChangeType.CHANGED_CONTENT, FA.getChangeTypeGit("","",""));
    }

    @Test
    public void testGetChangeTypeSVN() {

        SVNLogEntryPath ChangedPath = new SVNLogEntryPath("/test/test2/", '0', "/test/test3/", 123456);
        assertEquals(File.ChangeType.CHANGED_PATH, FA.getChangeTypeSvn(ChangedPath));

        SVNLogEntryPath ChangedPath2 = new SVNLogEntryPath(null, '0', "/test/test3/", 123456);
        assertEquals(File.ChangeType.CHANGED_PATH, FA.getChangeTypeSvn(ChangedPath2));

        SVNLogEntryPath Deletion = new SVNLogEntryPath("/test/test2/", 'D', null, 123456);
        assertEquals(File.ChangeType.DELETION, FA.getChangeTypeSvn(Deletion));

        SVNLogEntryPath Creation = new SVNLogEntryPath("/test/test2/", 'A', null, 123456);
        assertEquals(File.ChangeType.CREATION, FA.getChangeTypeSvn(Creation));

        SVNLogEntryPath ChangedContent = new SVNLogEntryPath("/test/test2/", '0', null, 123456);
        assertEquals(File.ChangeType.CHANGED_CONTENT, FA.getChangeTypeSvn(ChangedContent));

        SVNLogEntryPath Null = new SVNLogEntryPath(null, '0', null, 123456);
        assertEquals(File.ChangeType.CHANGED_CONTENT, FA.getChangeTypeSvn(Null));


    }
}
