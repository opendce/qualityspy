/**
 * @author Mateusz Adamiak
 */
package pl.wroc.pwr.qualityspy.vc;

import com.google.common.collect.Sets;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import static org.junit.Assert.*;

import java.util.Set;

import org.easymock.EasyMock;
import org.easymock.IAnswer;
import org.junit.Before;
import org.junit.Test;
import org.tmatesoft.svn.core.SVNDepth;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.wc.SVNClientManager;
import org.tmatesoft.svn.core.wc.SVNDiffClient;
import org.tmatesoft.svn.core.wc.SVNRevision;

import pl.wroc.pwr.qualityspy.model.File;
import pl.wroc.pwr.qualityspy.model.Interrupter;
import pl.wroc.pwr.qualityspy.model.Revision;
import pl.wroc.pwr.qualityspy.model.ProjectHistory;
import pl.wroc.pwr.qualityspy.vc.svn.SVNDiffService;

public class SVNDiffServiceIT {

    //declarations
    SVNDiffService mSvn;
    private SVNClientManager mSvnClientManager;
    private String mRepositoryUrl = "http://svn.apache.org/repos/asf/tomcat/trunk/";
    private Revision mRev;
    private SVNURL mDiffURL;
    private SVNDiffClient mClient;

    @Before
    public void setUp() throws Exception {
        mSvn = new SVNDiffService();
    }

    @Test
    public void testDoDiff() throws SVNException {
        //given
        //creating mocks
        mSvnClientManager = EasyMock.createMock(SVNClientManager.class);
        Revision rev = EasyMock.createMock(Revision.class);
        mClient = EasyMock.createMock(SVNDiffClient.class);
        String mRepoDiffRegex = ".*[txt]+";

        //initialize
        SVNDiffClient dif = SVNClientManager.newInstance().getDiffClient();
        ProjectHistory mResult = new ProjectHistory();
        Set<Revision> revisions1 = mResult.getRevisionsByFileName(
                "org.apache.tomcat.util.threads.ThreadPool");
        Set<Revision> revisions2 = mResult.getRevisionsByFileName(
                "org.apache.tomcat.util.threads.Constants");
        Set<Revision> revisions = Sets.newHashSet();
        revisions.addAll(revisions1);
        revisions.addAll(revisions2);

        for (Revision revTmp : revisions) {
            for (File f : revTmp.getFiles()) {
                f.setRepoPath(mRepositoryUrl);
            }
        }
        
        
        EasyMock.expect(rev.getNumber()).andReturn("719983").anyTimes();
        EasyMock.expect(mSvnClientManager.getDiffClient()).andReturn(dif);
        

        //replay
        EasyMock.replay(rev, mSvnClientManager);

        //when
        mSvn.doDiff(mSvnClientManager, mRepositoryUrl, revisions, mRepoDiffRegex, "/", new Interrupter());
        
        //then
        EasyMock.verify(rev, mSvnClientManager);
       
        File file1 = new File();
        file1.setName("sciezka/test.txt");
        file1.setNewContent("test");
        file1.setChangeType(File.ChangeType.CREATION);
        file1.setOldPath(mRepositoryUrl);
        file1.setRepoPath(mRepositoryUrl);
        
        File file2 = new File();
        file2.setName("sciezka/test2.csv");
        file2.setNewContent("test");
        file2.setChangeType(File.ChangeType.CREATION);
        file2.setOldPath(mRepositoryUrl);
        file2.setRepoPath(mRepositoryUrl);
        
        Revision revision = new Revision();
        revision.addFile(file1);
        revision.addFile(file2);
        Set<Revision> revisionsTest = Sets.newHashSet();
        revisionsTest.add(revision);
        
        SVNClientManager mSvnClientManager2 = EasyMock.createMock(SVNClientManager.class);
        EasyMock.expect(mSvnClientManager2.getDiffClient()).andReturn(mClient);

        //when
        mSvn.doDiff(mSvnClientManager2, mRepositoryUrl, revisionsTest, mRepoDiffRegex, "/",new Interrupter());
        
        assertEquals(1,file1.getNumberOfAddedLines());
        assertEquals(0,file1.getNumberOfRemovedLines());
        assertEquals(-1,file2.getNumberOfAddedLines());
        assertEquals(-1,file2.getNumberOfRemovedLines());
        
        SVNClientManager mSvnClientManager3 = EasyMock.createMock(SVNClientManager.class);
        EasyMock.expect(mSvnClientManager3.getDiffClient()).andReturn(mClient);
        
        file1.setNumberOfAddedLines(0);
        file1.setNumberOfRemovedLines(0);
        file2.setNumberOfAddedLines(0);
        file2.setNumberOfRemovedLines(0);
        
        mSvn.doDiff(mSvnClientManager3, mRepositoryUrl, revisionsTest, "", "/", new Interrupter());
        
        assertEquals(1,file1.getNumberOfAddedLines());
        assertEquals(0,file1.getNumberOfRemovedLines());
        assertEquals(1,file2.getNumberOfAddedLines());
        assertEquals(0,file2.getNumberOfRemovedLines());
    }

    @Test
    public void testUpdateRevision() throws SVNException {
        //given
        mClient = EasyMock.createMock(SVNDiffClient.class);

        //creating mocks
        mRev = EasyMock.createMock(Revision.class);
        File file = EasyMock.createMock(File.class);

        //initialize
        mDiffURL = mSvn.generateUrlForClass(mRepositoryUrl,
                "java/org/apache/tomcat/util/threads/Constants.java");

        EasyMock.expect(mRev.getNumber()).andReturn("1187781").anyTimes();
        EasyMock.expect(file.getChangeType()).andReturn(File.ChangeType.CHANGED_CONTENT);
        EasyMock.expect(file.getRepoPath()).andReturn("/java/org/apache/tomcat/util/threads/Constants.java").anyTimes();
        EasyMock.expect(file.getNewPath()).andReturn("/java/org/apache/tomcat/util/threads/Constants.java").anyTimes();
        EasyMock.expect(file.getOldPath()).andReturn(null).anyTimes();
        file.setNumberOfAddedLines(3);
        file.setNumberOfRemovedLines(2);

        mClient.doDiff(EasyMock.eq(mDiffURL), EasyMock.eq(SVNRevision.UNDEFINED), EasyMock.eq(SVNRevision.create(1187780)), EasyMock.eq(SVNRevision.create(1187781)),
                EasyMock.eq(SVNDepth.INFINITY), EasyMock.eq(true), EasyMock.anyObject(OutputStream.class));
        EasyMock.expectLastCall().andAnswer(new IAnswer() {

            @Override
            public Object answer() throws Throwable {
                ByteArrayOutputStream os = (ByteArrayOutputStream) EasyMock.getCurrentArguments()[6];
                String content = "+\n+\n+\n+\n-\n-\n-\n";
                os.write(content.getBytes(), 0, content.length());
                return null;
            }
        });

        //replay
        EasyMock.replay(mRev, file, mClient);

        //when
        mSvn.updateRevision(mRev, file, mRepositoryUrl, mClient);

        //then
        EasyMock.verify(mRev, file, mClient);
    }

    @Test
    public void testGenerateUrlForClass() throws SVNException {
        //given
        String equal;
        String notEqual;
        String result = "/repos/asf/tomcat/trunk/java/"
                + "org/apache/tomcat/util/threads/Constants.java";

        String mPathE = "java/org/apache/tomcat/util/threads/Constants.java";
        String mPathNE = "org/apache/tomcat/util/threads/Constants.java";

        //when
        equal = mSvn.generateUrlForClass(mRepositoryUrl, mPathE).getPath();
        notEqual = mSvn.generateUrlForClass(mRepositoryUrl, mPathNE).getPath();

        //then
        assertNotNull(mSvn.generateUrlForClass(mRepositoryUrl, mPathE));
        assertNotSame(result, notEqual);
        assertEquals(result, equal);
    }

    @Test
    public void testGenerateUrlForClassChangedPath() throws SVNException {
        String expected = "/repos/asf/tomcat/branches/test/java/org/apache/tomcat/util/threads/Constants.java";
        String changedPath = "tomcat/branches/test/java/org/apache/tomcat/util/threads/Constants.java";

        String result = mSvn.generateUrlForClass(mRepositoryUrl, changedPath).getPath();

        assertNotSame(expected, result);
    }
}
