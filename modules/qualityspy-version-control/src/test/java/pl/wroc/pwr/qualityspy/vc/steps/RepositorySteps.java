package pl.wroc.pwr.qualityspy.vc.steps;

import cucumber.api.PendingException;
import cucumber.api.java.Before;
import cucumber.api.java.en.*;

import pl.wroc.pwr.qualityspy.common.ConfigurationManager;
import pl.wroc.pwr.qualityspy.model.File;
import pl.wroc.pwr.qualityspy.model.Interrupter;
import pl.wroc.pwr.qualityspy.model.ProjectHistory;
import pl.wroc.pwr.qualityspy.model.Revision;
import pl.wroc.pwr.qualityspy.vc.IRepositoryConnector;
import pl.wroc.pwr.qualityspy.vc.RepositoryConnector;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Set;
import static org.junit.Assert.*;



public class RepositorySteps {
    private ProjectHistory mProjectHistory;
    private pl.wroc.pwr.qualityspy.common.Properties mConfig;
    private String mRepositoryUrl;
    private IRepositoryConnector mRepositoryConnector;
    private String bugfix = ".*(([bB][uU][gG][fF][iI][xX])|([bB][uU][gG][zZ][iI][lL][lL][aA])).*(\n.*)*";


    private Revision rev;
    private File f;

    @Before
    public void beforeScenario() {
        mConfig = new ConfigurationManager("src/").loadConfiguration("test", "resources");
        mProjectHistory = new ProjectHistory();
        mRepositoryUrl = "http://svn.apache.org/repos/asf/tomcat/trunk/";
        mRepositoryConnector = new RepositoryConnector();
    }

    @Given("^Configured properties$")
    public void configureProperties() throws Exception {

    }
    @Given("^Revision number from \"([^\"]*)\" to \"([^\"]*)\" and URL \"([^\"]*)\"$")
    public void revisionNumberFromToAndURL(String arg0, String arg1, String arg2) throws Throwable {
        configure(mConfig, "svn", mRepositoryUrl, "1336800", "1336884",
                bugfix, ".*/java/", ".java", "/", "user", "pass","","");
    }

    private void configure(
            pl.wroc.pwr.qualityspy.common.Properties p,
            String type,
            String url,
            String start,
            String end,
            String bugfix,
            String prefix,
            String postfix,
            String separator,
            String user,
            String pass,
            String repocontentregex,
            String repodiffregex)
            throws NullPointerException, IOException, URISyntaxException {

        p.setProperty("repotype", type);
        p.setProperty("repourl", url);
        p.setProperty("repostartrev", start);
        p.setProperty("repoendrev", end);
        p.setProperty("repobugfixregex", bugfix);
        p.setProperty("reposrcpathprefixregex", prefix);
        p.setProperty("reposrcpathpostfixregex", postfix);
        p.setProperty("repofileseparator", separator);
        p.setProperty("reposvnpass", pass);
        p.setProperty("reposvnuser", user);
        p.setProperty("repocontentregex", repocontentregex);
        p.setProperty("repodiffregex", repodiffregex);
        p.saveProperties();
    }




    @When("^I fetch project history data$")
    public void fetchProjectHistoryData() throws Exception {
        mProjectHistory = mRepositoryConnector.fetchData(mConfig, new Interrupter());
    }

    @And("^I get Revisions from file \"([^\"]*)\"$")
    public void iGetRevisionsFromFile(String arg0) throws Throwable {
        Set<Revision> revisions = mProjectHistory.getRevisionsByFileName("JIoEndpoint.java");
        rev = revisions.iterator().next();
        f = rev.getFileByName("JIoEndpoint.java");
    }

    @Then("^Author should match \"markt\"")
    public void authorShouldMatch() throws Exception {
        assertEquals("markt", rev.getAuthor());
    }

    @And("^revision number should be equal to \"1336884\"")
    public void revisionNumber() throws Exception {
        assertEquals("1336884", rev.getNumber());
    }
    @And("^File change type should be defined with \"CHANGED_CONTENT\"")
    public void fileChangeType() throws Exception {
        assertEquals("CHANGED_CONTENT", f.getChangeType().toString());
    }

    @And("^Comment should contain \"([^\"]*)\"$")
    public void commentShouldContain(String arg0) throws Throwable {
        assertTrue(rev.getComment().contains("Fix https://issues.apache.org/bugzilla/show_bug.cgi?id=53063"));
    }

    @And("^New file content should contain \"([^\"]*)\"$")
    public void newFileContentShouldContain(String arg0) throws Throwable {
        assertTrue(f.getNewContent().contains("setMaxConnections(getMaxThreadsExecutor(true));"));
    }

    @And("^New file content should not contain \"([^\"]*)\"$")
    public void newFileContentShouldNotContain(String arg0) throws Throwable {
        assertFalse(f.getNewContent().contains("setMaxConnections(getMaxThreads());"));
    }

    @And("^Old file content should contain \"([^\"]*)\"$")
    public void oldFileContentShouldContain(String arg0) throws Throwable {
        assertTrue(f.getOldContent().contains("setMaxConnections(getMaxThreads());"));
    }

    @And("^Old file content should not contain \"([^\"]*)\"$")
    public void oldFileContentShouldNotContain(String arg0) throws Throwable {
        assertFalse(f.getOldContent().contains("setMaxConnections(getMaxThreadsExecutor(true));"));
    }
}