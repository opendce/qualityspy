/*
 * Added javadoc code.
 * Krzysiek C.
 */
package pl.wroc.pwr.qualityspy.vc;

import org.junit.Ignore;
import pl.wroc.pwr.qualityspy.vc.RepoWrapper;
import pl.wroc.pwr.qualityspy.vc.RepoWrapperFactory;
import pl.wroc.pwr.qualityspy.vc.svn.SVNWrapperImpl;
import pl.wroc.pwr.qualityspy.common.Properties;
import java.io.IOException;
import java.net.URISyntaxException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import pl.wroc.pwr.qualityspy.common.ConfigurationManager;

/**
 * Class for testing RepoWrapperFactory
 *
 * @see RepoWrapperFactory
 */
public class RepoWrapperFactoryIT {

    private String repoURI = "https://github.com/florent37/MaterialViewPager.git";
    private String repoDir = "/tmp/QualitySpy";
    private String bugfix = ".*(([bB][uU][gG][fF][iI][xX])|([bB][uU][gG][zZ][iI][lL][lL][aA])).*(\\n.*)*";
    private String startDate = "2015/05/08 21:08:00";
    private String endDate = "2015/05/08 21:10:00";
    
    public RepoWrapperFactoryIT() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    /**
     * Test for creating SVN repository.
     *
     * @throws IOException
     */
    @Test
    public void testCreateRepoWrapperSVN() throws IOException, URISyntaxException {

        Properties pr = new ConfigurationManager("src/").loadConfiguration("test", "resources");
        configureProperties(pr, "svn", repoURI, startDate, endDate,bugfix ,".*/java/", ".java", "/", "user", "pass", repoDir);

        RepoWrapper svnRepo = RepoWrapperFactory.createWrapper(pr);

        assertTrue(svnRepo instanceof SVNWrapperImpl);

        checkRepoProps(pr, svnRepo);

        assertEquals("user", ((SVNWrapperImpl) svnRepo).getUser());
        assertEquals("pass", ((SVNWrapperImpl) svnRepo).getPass());
    }

    /**
     * Test for repository properties.
     *
     * @param pr	Repository properties.
     * @param repo	Repository wrapper object.
     */
    private void checkRepoProps(Properties pr, RepoWrapper repo) {
        assertEquals(pr.getReposrcpathprefixregex(), repo.getSrcPathPrefix());
        assertEquals(pr.getReposrcpathpostfixregex(), repo.getSrcPathPostfix());
        assertEquals((char) pr.getRepofileseparator(), repo.getFileSeparator());
    }
    
      private void configureProperties(
            pl.wroc.pwr.qualityspy.common.Properties p,
            String type,
            String url,
            String start,
            String end,
            String bugfix,
            String prefix,
            String postfix,
            String separator,
            String user,
            String pass,
            String repoworkcopy)
            throws URISyntaxException, IOException {

        p.setProperty("repotype", type);
        p.setProperty("repourl", url);
        p.setProperty("repostartrev", start);
        p.setProperty("repoendrev", end);
        p.setProperty("repobugfixregex", bugfix);
        p.setProperty("reposrcpathprefixregex", prefix);
        p.setProperty("reposrcpathpostfixregex", postfix);
        p.setProperty("repofileseparator", separator);
        p.setProperty("reposvnuser", user);
        p.setProperty("reposvnpass", pass);
        p.setProperty("repoworkingcopy", repoworkcopy);

        p.saveProperties();
    }
}