package pl.wroc.pwr.qualityspy.vc;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        monochrome = true,
        format = { "pretty", "html:target/cucumber" },
        glue = "pl.wroc.pwr.qualityspy.vc.steps",
        features = "classpath:cucumber/Repository.feature"
)
public class AcceptanceTestRepositoryCucumber {


}
