/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wroc.pwr.qualityspy.vc.git;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Set;
import org.apache.log4j.Logger;
import org.easymock.EasyMock;
import org.junit.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import pl.wroc.pwr.qualityspy.common.ConfigurationManager;
import pl.wroc.pwr.qualityspy.common.Properties;
import pl.wroc.pwr.qualityspy.model.File;
import pl.wroc.pwr.qualityspy.model.Interrupter;
import pl.wroc.pwr.qualityspy.model.ProjectHistory;
import pl.wroc.pwr.qualityspy.model.Revision;
import pl.wroc.pwr.qualityspy.model.enums.FileType;
import pl.wroc.pwr.qualityspy.vc.FileAnalyzer;
import pl.wroc.pwr.qualityspy.vc.RepositoryException;

/**
 *
 * @author Maks
 */
public class AcceptanceGitTest {
    
    private static final Logger logger = Logger.getLogger(AcceptanceGitTest.class.getName());
    private String repoURI = "https://github.com/florent37/MaterialViewPager.git";
    private String repoDir = "/tmp/QualitySpy";
    private String bugfix = ".*(([bB][uU][gG][fF][iI][xX])|([bB][uU][gG][zZ][iI][lL][lL][aA])).*(\\n.*)*";
    private String startDate = "2015/08/03 22:25:00";
    private String endDate = "2015/08/04 23:25:00";

    private GitWrapperImpl gitWrapperImpl;  
    private Properties config;
    private ProjectHistory history;
    private Set<Revision> revisions;
    
    @BeforeClass
    public static void setUpClass() {
        
    }
   
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() throws URISyntaxException, IOException {
        config = new ConfigurationManager("src/").loadConfiguration("test", "resources");

        configureProperties(config, "git", repoURI, startDate, endDate,bugfix ,".*/java/", ".java", "/", "user", "pass", repoDir);

        gitWrapperImpl = new GitWrapperImpl ();
        history = new ProjectHistory();

    }
   
    @After
    public void tearDown() {
    }
    
    @Ignore
    @Test
    public void testFetchProjectHistory(){
        
        // Arrange
        String expBranchName = "refs/heads/master";
        String expAuthor = "Florent, champigny.florent@gmail.com";
        String expTagName="1.0.3";
        String expComment="sent version 1.0.3";
        String expNumber="82e743a42d44414d628c46b99b516dc1ebff370a";
        String expDate="2015-05-08T21:09:08";

        Long expID = null;
        
        String resultBranch = "";
        String resultAuthor = "";
        String resultComment = "";
        String resultNumber = "";
        String resultDate = "";
        Long resultID = null;
        String resultTag = "";
        
        // Action
        history = gitWrapperImpl.fetchProjectHistory(config, new Interrupter());
        revisions = history.getRevisions();
        
        for(Revision a : revisions) {
            resultBranch = a.getBranch();
            resultTag = a.getTags();
            resultAuthor = a.getAuthor();
            resultComment = a.getComment();
            resultNumber = a.getNumber();
            resultDate = a.getDate().withNano(0).toString();
            resultID = a.getId();
        }

        // Assert
        assertEquals(expBranchName, resultBranch);
        assertEquals(expTagName, resultTag);
        assertEquals(expAuthor, resultAuthor);
        assertEquals(expComment, resultComment); 
        assertEquals(expNumber, resultNumber);
        assertEquals(expDate, resultDate);
        assertEquals(expID, resultID); 
    }

    @Test
    @Ignore
    public void  testCheckFileTypeAndExtension() throws NullPointerException, IOException, URISyntaxException, RepositoryException {

        FileAnalyzer FA = new FileAnalyzer();
        Boolean isOpenRevision = false;
        Boolean isSomeFiles = false;

        Properties config3 = new ConfigurationManager("src/").loadConfiguration("test", "resources");

        configureProperties(config3, "git", repoURI, startDate, endDate,bugfix ,".*/java/", ".java", "/", "user", "pass", repoDir);

        GitWrapperImpl gitWrapperImpl3 = new GitWrapperImpl ();
        ProjectHistory history3 = gitWrapperImpl3.fetchProjectHistory(config3, new Interrupter());
        Set<Revision> testRevisions2 = history3.getRevisions();

        for (Revision r : testRevisions2) {
            Set<File> mFile = r.getFiles();
            isOpenRevision = true;
            if (r.getFiles().iterator().hasNext()) {
                for (File f : mFile) {
                    if (f.getFileExtension().equals("java")) {
                        isSomeFiles = true;
                        String extension = FA.checkFileExtension(f.getNewPath());
                        FileType type = FA.checkFileType(extension);
                        assertEquals("java", extension);
                        assertEquals(FileType.TEXT, type);
                    }
                }
                assertEquals(true, isSomeFiles);   //Rewizja ma "jakieś" pliki
            }
            else {   //Przebieg alternatywny 5a. Nie ma plików w rewizji
                assertNull(r.getFiles().iterator().next());
                assertEquals(false, isSomeFiles);
            }
        }
        assertEquals(true, isOpenRevision);
    }

    @Test
    public void  testGetChangeTypeGit() throws NullPointerException, IOException, URISyntaxException, RepositoryException {
        Properties config2 = new ConfigurationManager("src/").loadConfiguration("test", "resources");

        configureProperties(config2, "git", repoURI, "2015/08/03 20:00:00", endDate,bugfix ,".*/java/", ".java", "/", "user", "pass", repoDir);

        GitWrapperImpl gitWrapperImpl2 = new GitWrapperImpl ();
        ProjectHistory history2;
        FileAnalyzer FA = new FileAnalyzer();

        history2 = gitWrapperImpl2.fetchProjectHistory(config2, new Interrupter());
        Set<Revision> testRevisions;
        File f;

        testRevisions = history2.getRevisionsByFileName("MainActivity.java");
        f = testRevisions.iterator().next().getFileByName("MainActivity.java");

        assertEquals(File.ChangeType.CHANGED_CONTENT,f.getChangeType());

        testRevisions = history2.getRevisionsByFileName("ApplicationTest.java");
        f = testRevisions.iterator().next().getFileByName("ApplicationTest.java");

        assertEquals(File.ChangeType.CREATION,f.getChangeType());


        File fileMock = EasyMock.createMock(File.class);
        EasyMock.expect(fileMock.getNewPath()).andReturn("/test/test2/");
        EasyMock.expect(fileMock.getOldPath()).andReturn("/test/test3/");
        EasyMock.replay(fileMock);

        f.setChangeType(FA.getChangeTypeGit("testCode",fileMock.getOldPath(),fileMock.getNewPath()));
        assertEquals(File.ChangeType.CHANGED_PATH, f.getChangeType());

        File fileMock2 = EasyMock.createMock(File.class);
        EasyMock.expect(fileMock2.getNewPath()).andReturn("/test/test2/");
        EasyMock.expect(fileMock2.getOldPath()).andReturn("/test/test2/");
        EasyMock.replay(fileMock2);

        f.setChangeType(FA.getChangeTypeGit("deleted testCode",fileMock2.getOldPath(),fileMock2.getNewPath()));
        assertEquals(File.ChangeType.DELETION, f.getChangeType());
    }

    private void configureProperties(
            pl.wroc.pwr.qualityspy.common.Properties p,
            String type,
            String url,
            String start,
            String end,
            String bugfix,
            String prefix,
            String postfix,
            String separator,
            String user,
            String pass,
            String repoworkcopy)
            throws URISyntaxException, IOException {

        p.setProperty("repotype", type);
        p.setProperty("repourl", url);
        p.setProperty("repostartrev", start);
        p.setProperty("repoendrev", end);
        p.setProperty("repobugfixregex", bugfix);
        p.setProperty("reposrcpathprefixregex", prefix);
        p.setProperty("reposrcpathpostfixregex", postfix);
        p.setProperty("repofileseparator", separator);
        p.setProperty("reposvnuser", user);
        p.setProperty("reposvnpass", pass);
        p.setProperty("repoworkingcopy", repoworkcopy);

        p.saveProperties();
    }



}
