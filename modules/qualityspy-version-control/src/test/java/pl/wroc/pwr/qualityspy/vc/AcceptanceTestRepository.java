package pl.wroc.pwr.qualityspy.vc;

import org.easymock.EasyMock;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.tmatesoft.svn.core.SVNLogEntryPath;
import org.tmatesoft.svn.core.wc.SVNClientManager;
import pl.wroc.pwr.qualityspy.common.ConfigurationManager;
import pl.wroc.pwr.qualityspy.errorHandling.ErrorWindow;
import pl.wroc.pwr.qualityspy.model.File;
import pl.wroc.pwr.qualityspy.model.File.ChangeType;
import pl.wroc.pwr.qualityspy.model.Interrupter;
import pl.wroc.pwr.qualityspy.model.ProjectHistory;
import pl.wroc.pwr.qualityspy.model.Revision;
import pl.wroc.pwr.qualityspy.model.enums.FileType;
import pl.wroc.pwr.qualityspy.vc.svn.SVNDiffService;
import pl.wroc.pwr.qualityspy.vc.svn.SVNWrapperImpl;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

import static org.junit.Assert.*;

public class AcceptanceTestRepository {

    private static final String END_REVISION = "390120";
    private static final String START_REVISION = "390055";
    private ProjectHistory mProjectHistory;
    private SVNWrapperImpl mRepository;
    private pl.wroc.pwr.qualityspy.common.Properties mConfig;
    private SVNDiffService mClient;
    private SVNClientManager mSvnClientManager;
    private String mRepositoryUrl;
    private IRepositoryConnector mRepositoryConnector;
    String bugfix = ".*(([bB][uU][gG][fF][iI][xX])|([bB][uU][gG][zZ][iI][lL][lL][aA])).*(\n.*)*";

    @Before
    public void setUp() throws IOException, NullPointerException, URISyntaxException {

        mProjectHistory = new ProjectHistory();
        mRepository = new SVNWrapperImpl();
        mConfig = new ConfigurationManager("src/").loadConfiguration("test", "resources");
        mClient = new SVNDiffService();
        mRepositoryUrl = "http://svn.apache.org/repos/asf/tomcat/trunk/";
        mRepositoryConnector = new RepositoryConnector();
//        configureProperties(mConfig, "svn", mRepositoryUrl, START_REVISION, END_REVISION,
//                bugfix, ".*/java/", ".java", "/", "user", "pass");
    }

    @After
    public void tearDown() throws NullPointerException, IOException, URISyntaxException {
//        configureProperties(mConfig, "svn", mRepositoryUrl, START_REVISION, END_REVISION,
//                bugfix, ".*/java/", ".java", "/", "user", "pass");
    }

    @Test
    @Ignore
    public void testFileDescription() throws NullPointerException, IOException, URISyntaxException, RepositoryException {
        //given
        Iterator<Revision> itr = null;
        Revision rev = null;

        //when
        // fetching revisions, where is changed path
        configureProperties(mConfig, "svn", mRepositoryUrl, "1229549", "1229549",
                bugfix, ".*/java/", ".java", "/", "user", "pass","","");
        mProjectHistory = mRepositoryConnector.fetchData(mConfig, new Interrupter());
        Set<Revision> revisions = mProjectHistory.getRevisionsByFileName("org.apache.catalina.mbeans.MBeanDumper");
        rev = revisions.iterator().next();

        //then
        assertNotNull(rev);
        assertEquals(4, rev.getFiles().size());
        File f = rev.getFileByName("org.apache.catalina.mbeans.MBeanDumper");
        assertEquals(ChangeType.CHANGED_PATH, f.getChangeType());
        assertEquals(FileType.TEXT, f.getFileType());
        assertNotNull(f.getOldPath());
        assertEquals("/tomcat/trunk/test/org/apache/catalina/mbeans/MBeanDumper.java", f.getOldPath());
        assertEquals("/tomcat/trunk/java/org/apache/catalina/mbeans/MBeanDumper.java", f.getNewPath());
        assertEquals("", f.getOldContent());

        f = rev.getFileByName("/tomcat/trunk/test/org/apache/catalina/mbeans/MBeanDumper.java");
        assertEquals(ChangeType.DELETION, f.getChangeType());
        assertEquals(FileType.TEXT, f.getFileType());
        assertNull(f.getOldPath());
        assertEquals("/tomcat/trunk/test/org/apache/catalina/mbeans/MBeanDumper.java", f.getNewPath());
        assertEquals("", f.getNewContent());
        assertTrue(f.getOldContent().contains("private static Log log = LogFactory.getLog(MBeanDumper.class);"));

        //when
        // testing modified file
        revisions = mProjectHistory.getRevisionsByFileName("org.apache.catalina.manager.JMXProxyServlet");
        rev = revisions.iterator().next();

        //then
        assertEquals(4, rev.getFiles().size());
        f = rev.getFileByName("org.apache.catalina.manager.JMXProxyServlet");
        assertEquals(ChangeType.CHANGED_CONTENT, f.getChangeType());
        assertEquals(FileType.TEXT, f.getFileType());
        assertEquals(4, f.getNumberOfAddedLines());
        assertEquals(121, f.getNumberOfRemovedLines());

        //when
        // fetching revisions, where file was added 
        configureProperties(mConfig, "svn", mRepositoryUrl, "1230950", "1230960",
                bugfix, ".*/java/", ".java", "/", "user", "pass","","");
        mProjectHistory = mRepositoryConnector.fetchData(mConfig, new Interrupter());
        revisions = mProjectHistory.getRevisionsByFileName("org.apache.catalina.realm.NullRealm");
        rev = revisions.iterator().next();

        //then
        f = rev.getFileByName("org.apache.catalina.realm.NullRealm");
        assertEquals(ChangeType.CREATION, f.getChangeType());
        assertEquals(FileType.TEXT, f.getFileType());
        assertEquals("", f.getOldContent());
        assertNotSame("", f.getNewContent());

        //when
        // fetching revisions, where file was deleted
        configureProperties(mConfig, "svn", mRepositoryUrl, "1235200", "1235210",
                bugfix, ".*/java/", ".java", "/", "user", "pass","","");
        mProjectHistory = mRepositoryConnector.fetchData(mConfig, new Interrupter());
        revisions = mProjectHistory.getRevisionsByFileName("org.apache.jasper.runtime.PerThreadTagHandlerPool");
        rev = revisions.iterator().next();

        //then
        f = rev.getFileByName("org.apache.jasper.runtime.PerThreadTagHandlerPool");
        assertEquals(ChangeType.DELETION, f.getChangeType());
        assertEquals(FileType.TEXT, f.getFileType());
        assertNotSame("", f.getOldContent());
        assertEquals("", f.getNewContent());
    }


    @Test
    public void testData() throws NullPointerException, IOException, URISyntaxException, RepositoryException {
        //given
        configureProperties(mConfig, "svn", mRepositoryUrl, "1336800", "1336884",
                bugfix, ".*/java/", ".java", "/", "user", "pass","","");
        Revision rev;

        //when
        mProjectHistory = mRepositoryConnector.fetchData(mConfig, new Interrupter());
        Set<Revision> revisions = mProjectHistory.getRevisionsByFileName("JIoEndpoint.java");
        rev = revisions.iterator().next();

        //then
        assertEquals("markt", rev.getAuthor());
        assertTrue(rev.getComment().contains("Fix https://issues.apache.org/bugzilla/show_bug.cgi?id=53063"));
//        Calendar cal = Calendar.getInstance(); TODO timezone
//        cal.set(2012, Calendar.MAY, 10, 23, 7, 22);
//        cal.setTimeZone(TimeZone.getTimeZone("GMT"));
//        assertEquals(cal.getTimeInMillis() / 1000, rev.getDate().getTime() / 1000);
        File f = rev.getFileByName("JIoEndpoint.java");
        assertEquals("JIoEndpoint.java", f.getName());
        assertTrue(f.getNewContent().contains("setMaxConnections(getMaxThreadsExecutor(true));"));
        assertFalse(f.getNewContent().contains("setMaxConnections(getMaxThreads());"));
        assertTrue(f.getOldContent().contains("setMaxConnections(getMaxThreads());"));
        assertFalse(f.getOldContent().contains("setMaxConnections(getMaxThreadsExecutor(true));"));
        assertTrue(f.getNewPath().contains("JIoEndpoint.java"));
        assertEquals("1336884", rev.getNumber());
        assertEquals("CHANGED_CONTENT", f.getChangeType().toString());
    }

    @Test
    public void testGetContents() throws NullPointerException, IOException, URISyntaxException {
        //given
        configureProperties(mConfig, "svn", mRepositoryUrl, "1306713", "1306720",
                bugfix, ".*/java/", ".java", "/", "user", "pass","","");

        //Two different file contents
        long endRev = 1306715L;
        String path1 = "java/org/apache/tomcat/spdy/SpdyStream.java";
        long startRev = (Long) mConfig.getRepostartrev();

        //Two the same file contents
        String path2 = "java/org/apache/tomcat/jni/socket/AprSocket.java";

        //Deleted file
        String path3 = "java/org/apache/tomcat/spdy/SpdyContextJni.java";

        //Added file
        String path4 = "java/org/apache/tomcat/spdy/NetSupportOpenSSL.java";

        //File previous context in revision older than limiting one
        String path5 = "java/org/apache/coyote/spdy/SpdyProcessor.java";

        mRepository.setErrorWindow(createIgnoringAllErrorWindowMock());
        String[] result1 = mRepository.getContents(endRev, path1, startRev, mRepositoryUrl, false);
        //then
        //Testing getting two different file contents
        assertNotNull(result1);
        assertNotSame(result1[0], result1[1]);
        assertTrue(result1[0].contains("public SpdyStream(SpdyConnection spdy)"));

        //Testing getting two the same file contents (file without changes)
        String[] result2 = mRepository.getContents(endRev, path2, startRev, mRepositoryUrl, false);
        assertNotNull(result2);
        assertEquals(result2[0], result2[1]);
        assertTrue(result2[0].contains("public AprSocketContext getContext()"));

        //Testing getting file contents from removed file
        String[] result3 = mRepository.getContents(endRev, path3, startRev, mRepositoryUrl, false);
        assertNotNull(result3);
        assertNotSame(result3[0], result3[1]);
        assertEquals("", result3[0]);
        assertNotSame("", result3[1]);

        //Testing getting file contents from added file
        String[] result4 = mRepository.getContents(endRev, path4, startRev, mRepositoryUrl, false);
        assertNotNull(result4);
        assertNotSame(result4[0], result4[1]);
        assertNotSame("", result4[0]);
        assertEquals("", result4[1]);

        //Testing getting file content with old content from revision older than limiting one
        String[] result5 = mRepository.getContents(endRev, path5, startRev, mRepositoryUrl, false);
        assertNotNull(result5);
        assertNotSame(result5[0], result5[1]);
        assertTrue(result5[0].contains("int rd = Math.min(inFrame.remaining(), bchunk.getBytes().length)"));
        assertFalse(result5[0].contains("int rd = Math.min(inFrame.endData, bchunk.getBytes().length);"));
        assertTrue(result5[1].contains("int rd = Math.min(inFrame.endData, bchunk.getBytes().length);"));
        assertFalse(result5[1].contains("int rd = Math.min(inFrame.remaining(), bchunk.getBytes().length)"));
    }

    @Ignore //not implemented yet
    @Test
    public void testInnersClasses() throws IOException {
        try {
            //given
            String s5 = readFileAsString("src\\test\\resources\\testAnonymousAndInnerClass1.txt");
            String s6 = readFileAsString("src\\test\\resources\\testAnonymousAndInnerClass2.txt");
            String s7 = readFileAsString("src\\test\\resources\\testAnonymousAndInnerClass3.txt");
            String s8 = readFileAsString("src\\test\\resources\\testAnonymousAndInnerClass4.txt");
            String s9 = readFileAsString("src\\test\\resources\\testAnonymousAndInnerClass5.txt");
            String s10 = readFileAsString("src\\test\\resources\\testAnonymousAndInnerClass6.txt");
            String s11 = readFileAsString("src\\test\\resources\\testAnonymousAndInnerClass7.txt");

            //when
            ArrayList<String> innerClass5 = SVNWrapperImpl.inner(s5);
            ArrayList<String> innerClass6 = SVNWrapperImpl.inner(s6);
            ArrayList<String> innerClass7 = SVNWrapperImpl.inner(s7);
            ArrayList<String> innerClass8 = SVNWrapperImpl.inner(s8);
            ArrayList<String> innerClass9 = SVNWrapperImpl.inner(s9);
            ArrayList<String> innerClass10 = SVNWrapperImpl.inner(s10);
            ArrayList<String> innerClass11 = SVNWrapperImpl.inner(s11);


            //then
            assertTrue(innerClass5.isEmpty());
            assertTrue(innerClass6.isEmpty());
            assertEquals("public class testing", innerClass7.get(0));
            assertTrue(innerClass8.isEmpty());
            assertEquals("public class testing", innerClass9.get(0));
            assertEquals("class testowaDrugaKlasa", innerClass9.get(0));
            assertEquals("public class testing", innerClass10.get(0));
            assertEquals("class testowaDrugaKlasa", innerClass10.get(0));
            assertEquals("public class testing", innerClass11.get(0));
            assertEquals("class testowaDrugaKlasa", innerClass11.get(0));
            assertEquals("class testowaTrzeciaKlasa", innerClass11.get(0));

        } catch (IOException e) {
        }
    }

    @Test
    public void testAnnonymousClasses() {
        try {
            //given
            String s5 = readFileAsString("src\\test\\resources\\testAnonymousAndInnerClass1.txt");
            String s6 = readFileAsString("src\\test\\resources\\testAnonymousAndInnerClass2.txt");
            String s7 = readFileAsString("src\\test\\resources\\testAnonymousAndInnerClass3.txt");
            String s8 = readFileAsString("src\\test\\resources\\testAnonymousAndInnerClass4.txt");
            String s9 = readFileAsString("src\\test\\resources\\testAnonymousAndInnerClass5.txt");
            String s10 = readFileAsString("src\\test\\resources\\testAnonymousAndInnerClass6.txt");
            String s11 = readFileAsString("src\\test\\resources\\testAnonymousAndInnerClass7.txt");

            //when
            ArrayList<String> anonymousClass5 = SVNWrapperImpl.anonymous(s5);
            ArrayList<String> anonymousClass6 = SVNWrapperImpl.anonymous(s6);
            ArrayList<String> anonymousClass7 = SVNWrapperImpl.anonymous(s7);
            ArrayList<String> anonymousClass8 = SVNWrapperImpl.anonymous(s8);
            ArrayList<String> anonymousClass9 = SVNWrapperImpl.anonymous(s9);
            ArrayList<String> anonymousClass10 = SVNWrapperImpl.anonymous(s10);
            ArrayList<String> anonymousClass11 = SVNWrapperImpl.anonymous(s11);

            //then
            assertTrue(anonymousClass5.isEmpty());
            assertTrue(anonymousClass6.isEmpty());
            assertTrue(anonymousClass7.isEmpty());
            assertEquals("new klasaAnonimowa (stringVar, intVal) ", anonymousClass8.get(0));
            assertEquals("new klasaAnonimowa (stringVar, intVal) ", anonymousClass9.get(0));
            assertEquals("new klasaAnonimowa2 (new String(\"\"),stringVar, intVal) ", anonymousClass9.get(1));
            assertEquals("new klasaAnonimowa (stringVar, intVal) ", anonymousClass10.get(0));

            assertEquals("new klasaAnonimowa (stringVar, intVal) ", anonymousClass11.get(0));
            assertEquals("new klasaAnonimowa2 (new String(\"\"),stringVar, intVal) ", anonymousClass11.get(1));

        } catch (Exception e) {
        }
    }

    private void configureProperties(
            pl.wroc.pwr.qualityspy.common.Properties p,
            String type,
            String url,
            String start,
            String end,
            String bugfix,
            String prefix,
            String postfix,
            String separator,
            String user,
            String pass,
            String repocontentregex,
            String repodiffregex)
            throws NullPointerException, IOException, URISyntaxException {

        p.setProperty("repotype", type);
        p.setProperty("repourl", url);
        p.setProperty("repostartrev", start);
        p.setProperty("repoendrev", end);
        p.setProperty("repobugfixregex", bugfix);
        p.setProperty("reposrcpathprefixregex", prefix);
        p.setProperty("reposrcpathpostfixregex", postfix);
        p.setProperty("repofileseparator", separator);
        p.setProperty("reposvnuser", user);
        p.setProperty("reposvnpass", pass);
        p.setProperty("repocontentregex", repocontentregex);
        p.setProperty("repodiffregex", repodiffregex);

        //p.storeProperties("buginfoSVN.properties", "Repository");
        p.saveProperties();
    }

    private static String readFileAsString(String filePath)
            throws java.io.IOException {
        StringBuffer fileData = new StringBuffer(1000);
        BufferedReader reader = new BufferedReader(
                new FileReader(filePath));
        char[] buf = new char[1024];
        int numRead = 0;
        while ((numRead = reader.read(buf)) != -1) {
            String readData = String.valueOf(buf, 0, numRead);
            fileData.append(readData);
            buf = new char[1024];
        }
        reader.close();
        return fileData.toString();
    }

    @Test
    public void testContentRegexDiffRegex()throws NullPointerException, IOException, URISyntaxException, RepositoryException {
        Revision rev = null;
        
        //Start Pusty repoContentRegex i repoDiffRegex
        configureProperties(mConfig, "svn", mRepositoryUrl, "1578812", "1578812",
                bugfix, ".*/java/", ".java", "/", "user", "pass","","");
        mProjectHistory = mRepositoryConnector.fetchData(mConfig, new Interrupter());
        Set<Revision> revisions = mProjectHistory.getRevisionsByFileName("TestAscii.java");
        rev = revisions.iterator().next();
        
        assertNotNull(rev);
        assertEquals(3, rev.getFiles().size());
        File f = rev.getFileByName("TestAscii.java");
        assertEquals(ChangeType.CREATION, f.getChangeType());
        assertEquals(FileType.TEXT, f.getFileType());
        assertNull(f.getOldPath());
        assertEquals("/tomcat/trunk/test/org/apache/tomcat/util/buf/TestAscii.java", f.getNewPath());
        assertNotNull(f.getOldContent());
        assertNotNull(f.getNewContent());
        assertEquals(65, f.getNumberOfAddedLines());
        assertEquals(0, f.getNumberOfRemovedLines());
        
        //Koniec poprawny repoContentRegex i pusty repoDiffRegex
        
        //Start poprawny repoContentRegex i pusty repoDiffRegex
        configureProperties(mConfig, "svn", mRepositoryUrl, "1578812", "1578812",
                bugfix, ".*/java/", ".java", "/", "user", "pass",".*java+","");
        mProjectHistory = mRepositoryConnector.fetchData(mConfig, new Interrupter());
        revisions = mProjectHistory.getRevisionsByFileName("TestAscii.java");
        rev = revisions.iterator().next();
        
        assertNotNull(rev);
        assertEquals(3, rev.getFiles().size());
        f = rev.getFileByName("TestAscii.java");
        assertEquals(ChangeType.CREATION, f.getChangeType());
        assertEquals(FileType.TEXT, f.getFileType());
        assertNull(f.getOldPath());
        assertEquals("/tomcat/trunk/test/org/apache/tomcat/util/buf/TestAscii.java", f.getNewPath());
        assertNotNull(f.getOldContent());
        assertNotNull(f.getNewContent());
        assertEquals(65, f.getNumberOfAddedLines());
        assertEquals(0, f.getNumberOfRemovedLines());
        
        //Koniec poprawny repoContentRegex i pusty repoDiffRegex
        
        //Start niepoprawny repoContentRegex i pusty repoDiffRegex
        configureProperties(mConfig, "svn", mRepositoryUrl, "1578812", "1578812",
                bugfix, ".*/java/", ".java", "/", "user", "pass",".*txt+","");
        mProjectHistory = mRepositoryConnector.fetchData(mConfig, new Interrupter());
        revisions = mProjectHistory.getRevisionsByFileName("TestAscii.java");
        rev = revisions.iterator().next();
        
        assertNotNull(rev);
        assertEquals(3, rev.getFiles().size());
        f = rev.getFileByName("TestAscii.java");
        assertEquals(ChangeType.CREATION, f.getChangeType());
        assertEquals(FileType.TEXT, f.getFileType());
        assertNull(f.getOldPath());
        assertEquals("/tomcat/trunk/test/org/apache/tomcat/util/buf/TestAscii.java", f.getNewPath());
        assertNull(f.getOldContent());
        assertNull(f.getNewContent());
        assertEquals(0, f.getNumberOfAddedLines());
        assertEquals(0, f.getNumberOfRemovedLines());
        
        //Koniec niepoprawny repoContentRegex i pusty repoDiffRegex
        
        //Start pusty repoContentRegex i poprawny repoDiffRegex
        configureProperties(mConfig, "svn", mRepositoryUrl, "1578812", "1578812",
                bugfix, ".*/java/", ".java", "/", "user", "pass","",".*java+");
        mProjectHistory = mRepositoryConnector.fetchData(mConfig, new Interrupter());
        revisions = mProjectHistory.getRevisionsByFileName("TestAscii.java");
        rev = revisions.iterator().next();
        
        assertNotNull(rev);
        assertEquals(3, rev.getFiles().size());
        f = rev.getFileByName("TestAscii.java");
        assertEquals(ChangeType.CREATION, f.getChangeType());
        assertEquals(FileType.TEXT, f.getFileType());
        assertNull(f.getOldPath());
        assertEquals("/tomcat/trunk/test/org/apache/tomcat/util/buf/TestAscii.java", f.getNewPath());
        assertNotNull(f.getOldContent());
        assertNotNull(f.getNewContent());
        assertEquals(65, f.getNumberOfAddedLines());
        assertEquals(0, f.getNumberOfRemovedLines());
        
        //Koniec pusty repoContentRegex i poprawny repoDiffRegex
        
        //Start poprawny repoContentRegex i poprawny repoDiffRegex
        configureProperties(mConfig, "svn", mRepositoryUrl, "1578812", "1578812",
                bugfix, ".*/java/", ".java", "/", "user", "pass",".*java+",".*java+");
        mProjectHistory = mRepositoryConnector.fetchData(mConfig, new Interrupter());
        revisions = mProjectHistory.getRevisionsByFileName("TestAscii.java");
        rev = revisions.iterator().next();
        
        assertNotNull(rev);
        assertEquals(3, rev.getFiles().size());
        f = rev.getFileByName("TestAscii.java");
        assertEquals(ChangeType.CREATION, f.getChangeType());
        assertEquals(FileType.TEXT, f.getFileType());
        assertNull(f.getOldPath());
        assertEquals("/tomcat/trunk/test/org/apache/tomcat/util/buf/TestAscii.java", f.getNewPath());
        assertNotNull(f.getOldContent());
        assertNotNull(f.getNewContent());
        assertEquals(65, f.getNumberOfAddedLines());
        assertEquals(0, f.getNumberOfRemovedLines());
        
        //Koniec poprawny repoContentRegex i poprawny repoDiffRegex
        
        //Start niepoprawny repoContentRegex i poprawny repoDiffRegex
        configureProperties(mConfig, "svn", mRepositoryUrl, "1578812", "1578812",
                bugfix, ".*/java/", ".java", "/", "user", "pass",".*txt+",".*java+");
        mProjectHistory = mRepositoryConnector.fetchData(mConfig, new Interrupter());
        revisions = mProjectHistory.getRevisionsByFileName("TestAscii.java");
        rev = revisions.iterator().next();
        
        assertNotNull(rev);
        assertEquals(3, rev.getFiles().size());
        f = rev.getFileByName("TestAscii.java");
        assertEquals(ChangeType.CREATION, f.getChangeType());
        assertEquals(FileType.TEXT, f.getFileType());
        assertNull(f.getOldPath());
        assertEquals("/tomcat/trunk/test/org/apache/tomcat/util/buf/TestAscii.java", f.getNewPath());
        assertNull(f.getOldContent());
        assertNull(f.getNewContent());
        assertEquals(0, f.getNumberOfAddedLines());
        assertEquals(0, f.getNumberOfRemovedLines());
        
        //Koniec niepoprawny repoContentRegex i poprawny repoDiffRegex
        
        //Start pusty repoContentRegex i niepoprawny repoDiffRegex
        configureProperties(mConfig, "svn", mRepositoryUrl, "1578812", "1578812",
                bugfix, ".*/java/", ".java", "/", "user", "pass","",".*txt+");
        mProjectHistory = mRepositoryConnector.fetchData(mConfig, new Interrupter());
        revisions = mProjectHistory.getRevisionsByFileName("TestAscii.java");
        rev = revisions.iterator().next();
        
        assertNotNull(rev);
        assertEquals(3, rev.getFiles().size());
        f = rev.getFileByName("TestAscii.java");
        assertEquals(ChangeType.CREATION, f.getChangeType());
        assertEquals(FileType.TEXT, f.getFileType());
        assertNull(f.getOldPath());
        assertEquals("/tomcat/trunk/test/org/apache/tomcat/util/buf/TestAscii.java", f.getNewPath());
        assertNotNull(f.getOldContent());
        assertNotNull(f.getNewContent());
        assertEquals(-1, f.getNumberOfAddedLines());
        assertEquals(-1, f.getNumberOfRemovedLines());
        
        //Koniec pusty repoContentRegex i niepoprawny repoDiffRegex
        
        //Start poprawny repoContentRegex i niepoprawny repoDiffRegex
        configureProperties(mConfig, "svn", mRepositoryUrl, "1578812", "1578812",
                bugfix, ".*/java/", ".java", "/", "user", "pass",".*java+",".*txt+");
        mProjectHistory = mRepositoryConnector.fetchData(mConfig, new Interrupter());
        revisions = mProjectHistory.getRevisionsByFileName("TestAscii.java");
        rev = revisions.iterator().next();
        
        assertNotNull(rev);
        assertEquals(3, rev.getFiles().size());
        f = rev.getFileByName("TestAscii.java");
        assertEquals(ChangeType.CREATION, f.getChangeType());
        assertEquals(FileType.TEXT, f.getFileType());
        assertNull(f.getOldPath());
        assertEquals("/tomcat/trunk/test/org/apache/tomcat/util/buf/TestAscii.java", f.getNewPath());
        assertNotNull(f.getOldContent());
        assertNotNull(f.getNewContent());
        assertEquals(-1, f.getNumberOfAddedLines());
        assertEquals(-1, f.getNumberOfRemovedLines());
        
        //Koniec pusty repoContentRegex i niepoprawny repoDiffRegex
        
        //Start niepoprawny repoContentRegex i niepoprawny repoDiffRegex
        configureProperties(mConfig, "svn", mRepositoryUrl, "1578812", "1578812",
                bugfix, ".*/java/", ".java", "/", "user", "pass",".*txt+",".*txt+");
        mProjectHistory = mRepositoryConnector.fetchData(mConfig, new Interrupter());
        revisions = mProjectHistory.getRevisionsByFileName("TestAscii.java");
        rev = revisions.iterator().next();
        
        assertNotNull(rev);
        assertEquals(3, rev.getFiles().size());
        f = rev.getFileByName("TestAscii.java");
        assertEquals(ChangeType.CREATION, f.getChangeType());
        assertEquals(FileType.TEXT, f.getFileType());
        assertNull(f.getOldPath());
        assertEquals("/tomcat/trunk/test/org/apache/tomcat/util/buf/TestAscii.java", f.getNewPath());
        assertNull(f.getOldContent());
        assertNull(f.getNewContent());
        assertEquals(-1, f.getNumberOfAddedLines());
        assertEquals(-1, f.getNumberOfRemovedLines());
        
        //Koniec pusty repoContentRegex i niepoprawny repoDiffRegex
        
    }

    
    @Test
    public void testGetBranch() throws NullPointerException, IOException, URISyntaxException, RepositoryException{
        
        // Arrange
        final String START_REVISION1     = "1679707";
        final String END_REVISION1       = "1679727";
        String mRepositoryUrl1 = "http://svn.apache.org/repos/asf/tomcat/tc6.0.x/branches/tomcat6-testing/";
        String bugfix1 = ".*(([bB][uU][gG][fF][iI][xX])|([bB][uU][gG][zZ][iI][lL][lL][aA])).*(\n.*)*";  
        pl.wroc.pwr.qualityspy.common.Properties mConfig1 = new ConfigurationManager("src/").loadConfiguration("test", "resources");
        
        configureProperties(mConfig1, "svn", mRepositoryUrl1, START_REVISION1, END_REVISION1,
                bugfix1, ".*/java/", ".java", "/", "user", "pass","","");
        
        SVNWrapperImpl mSvn = new SVNWrapperImpl();
        ProjectHistory fetchProjectHistory = mSvn.fetchProjectHistory(mConfig1, new Interrupter());
        
        // Action
        String branch1 = fetchProjectHistory.getBranch();
        String tag1 = fetchProjectHistory.getTag();
  
        // Assert
        assertEquals(branch1, "tomcat6-testing");
        assertEquals(tag1, "");
    }

    @Test
    public void testGetTag() throws NullPointerException, IOException, URISyntaxException, RepositoryException{
        
        // Arrange
        final String START_REVISION1     = "1679757";
        final String END_REVISION1       = "1680577";
        String mRepositoryUrl1 = "http://svn.apache.org/repos/asf/tomcat/tags/JDBC_POOL_1_0_5/test/";
        String bugfix1 = ".*(([bB][uU][gG][fF][iI][xX])|([bB][uU][gG][zZ][iI][lL][lL][aA])).*(\n.*)*";  
        pl.wroc.pwr.qualityspy.common.Properties mConfig1 = new ConfigurationManager("src/").loadConfiguration("test", "resources");

        configureProperties(mConfig1, "svn", mRepositoryUrl1, START_REVISION1, END_REVISION1,
                bugfix1, ".*/java/", ".java", "/", "user", "pass","","");
        
        SVNWrapperImpl mSvn = new SVNWrapperImpl();
        ProjectHistory fetchProjectHistory = mSvn.fetchProjectHistory(mConfig1, new Interrupter());
        
        // Action
        String branch1 = fetchProjectHistory.getBranch();
        String tag1 = fetchProjectHistory.getTag();
  
        // Assert
        assertEquals(branch1, "");
        assertEquals(tag1, "JDBC_POOL_1_0_5");
    }


    @Test
    public void  testGetFileExtension() throws NullPointerException, IOException, URISyntaxException, RepositoryException {
        // Arrange
        final String START_REVISION1     = "811778";
        final String END_REVISION1       = "811778";
        String mRepositoryUrl1 = "http://svn.apache.org/repos/asf/";
        String bugfix1 = ".*(([bB][uU][gG][fF][iI][xX])|([bB][uU][gG][zZ][iI][lL][lL][aA])).*(\n.*)*";
        pl.wroc.pwr.qualityspy.common.Properties mConfig1 = new ConfigurationManager("src/").loadConfiguration("test", "resources");

        configureProperties(mConfig1, "svn", mRepositoryUrl1, START_REVISION1, END_REVISION1,
                bugfix1, ".*/java/", ".java", "/", "user", "pass","","");

        SVNWrapperImpl mSvn = new SVNWrapperImpl();
        ProjectHistory fetchProjectHistory = mSvn.fetchProjectHistory(mConfig1, new Interrupter());

        boolean isOpenRevision = false;
        boolean isSomeFiles = false;
        FileAnalyzer FA = new FileAnalyzer();


        //Action
        Set<Revision> mRev = fetchProjectHistory.getRevisions();
        for (Revision r : mRev) {
            isOpenRevision = true;
                Set<File> mFile = r.getFiles();
                for (File f : mFile) {
                    isSomeFiles = true;
                    String extension = FA.checkFileExtension(f.getRepoPath());
                    assertEquals("java", extension);
                }
        }
        assertTrue(isSomeFiles);
        assertTrue(isOpenRevision);
    }

    @Test
    public void  testGetFileType() throws NullPointerException, IOException, URISyntaxException, RepositoryException {
        // Arrange
        final String START_REVISION1     = "811778";
        final String END_REVISION1       = "811778";
        String mRepositoryUrl1 = "http://svn.apache.org/repos/asf/";
        String bugfix1 = ".*(([bB][uU][gG][fF][iI][xX])|([bB][uU][gG][zZ][iI][lL][lL][aA])).*(\n.*)*";
        pl.wroc.pwr.qualityspy.common.Properties mConfig1 = new ConfigurationManager("src/").loadConfiguration("test", "resources");

        configureProperties(mConfig1, "svn", mRepositoryUrl1, START_REVISION1, END_REVISION1,
                bugfix1, ".*/java/", ".java", "/", "user", "pass","","");

        SVNWrapperImpl mSvn = new SVNWrapperImpl();
        ProjectHistory fetchProjectHistory = mSvn.fetchProjectHistory(mConfig1, new Interrupter());

        boolean isOpenRevision = false;
        boolean isSomeFiles = false;
        FileAnalyzer FA = new FileAnalyzer();


        //Action
        Set<Revision> mRev = fetchProjectHistory.getRevisions();
        for (Revision r : mRev){
            isOpenRevision = true;
            Set<File> mFile = r.getFiles();
            for (File f : mFile){
                isSomeFiles = true;
                String extension = FA.checkFileExtension(f.getRepoPath());
                FileType type = FA.checkFileType(extension);
                assertEquals(FileType.TEXT, type);
           }
        }
        assertTrue(isSomeFiles);
        assertTrue(isOpenRevision);
    }

    @Test
    public void  testGetChangeType() throws NullPointerException, IOException, URISyntaxException, RepositoryException {
        // Arrange
        final String START_REVISION1     = "811778";
        final String END_REVISION1       = "811778";
        String mRepositoryUrl1 = "http://svn.apache.org/repos/asf/";
        String bugfix1 = ".*(([bB][uU][gG][fF][iI][xX])|([bB][uU][gG][zZ][iI][lL][lL][aA])).*(\n.*)*";
        pl.wroc.pwr.qualityspy.common.Properties mConfig1 = new ConfigurationManager("src/").loadConfiguration("test", "resources");

        configureProperties(mConfig1, "svn", mRepositoryUrl1, START_REVISION1, END_REVISION1,
                bugfix1, ".*/java/", ".java", "/", "user", "pass","","");

        SVNWrapperImpl mSvn = new SVNWrapperImpl();
        ProjectHistory fetchProjectHistory = mSvn.fetchProjectHistory(mConfig1, new Interrupter());
        FileAnalyzer FA = new FileAnalyzer();

        SVNLogEntryPath mockLogEntryD = EasyMock.createMock(SVNLogEntryPath.class);
        EasyMock.expect(mockLogEntryD.getCopyPath()).andReturn(null);
        EasyMock.expect(mockLogEntryD.getType()).andReturn('D').anyTimes();
        EasyMock.replay(mockLogEntryD);

        SVNLogEntryPath mockLogEntryA = EasyMock.createMock(SVNLogEntryPath.class);
        EasyMock.expect(mockLogEntryA.getCopyPath()).andReturn(null);
        EasyMock.expect(mockLogEntryA.getType()).andReturn('A').anyTimes();
        EasyMock.replay(mockLogEntryA);

        SVNLogEntryPath mockLogEntryCP = EasyMock.createMock(SVNLogEntryPath.class);
        EasyMock.expect(mockLogEntryCP.getCopyPath()).andReturn("/test/test2/");
        EasyMock.replay(mockLogEntryCP);

        //Action
        Set<Revision> mRev = fetchProjectHistory.getRevisions();
        File f = mRev.iterator().next().getFileByName("IvyResolve.java");
        assertEquals(ChangeType.CHANGED_CONTENT, f.getChangeType());

        f.setChangeType(FA.getChangeTypeSvn(mockLogEntryD));
        assertEquals(ChangeType.DELETION, f.getChangeType());

        f.setChangeType(FA.getChangeTypeSvn(mockLogEntryA));
        assertEquals(ChangeType.CREATION, f.getChangeType());

        f.setChangeType(FA.getChangeTypeSvn(mockLogEntryCP));
        assertEquals(ChangeType.CHANGED_PATH, f.getChangeType());
    }

    private ErrorWindow createIgnoringAllErrorWindowMock() {
        ErrorWindow mock = EasyMock.createNiceMock(ErrorWindow.class);
        return mock;
    }
}
