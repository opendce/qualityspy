package pl.wroc.pwr.qualityspy.vc;

import org.apache.log4j.Logger;
import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.tmatesoft.svn.core.SVNException;
import pl.wroc.pwr.qualityspy.common.Properties;
import pl.wroc.pwr.qualityspy.errorHandling.ErrorWindow;
import pl.wroc.pwr.qualityspy.errorHandling.Errors;
import pl.wroc.pwr.qualityspy.model.ProjectHistory;
import pl.wroc.pwr.qualityspy.model.Revision;
import pl.wroc.pwr.qualityspy.vc.svn.SVNWrapperImpl;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Set;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import org.junit.Ignore;
import pl.wroc.pwr.qualityspy.model.Interrupter;

/**
 * Class for testing SVNWrapperImpl
 *
 * @see SVNWrapperImpl
 */
public class SVNWrapperImplIT {

    static final String URL = "http://svn.apache.org/repos/asf/";
    static final String PATH = "tomcat/trunk";
    static final long START_REV = 719096;//718818;
    SVNWrapperImpl mSvn;
    private final static long END_REV = 719120;
    private Properties mProperties;
    private static final Logger LOGGER = Logger.getLogger(SVNWrapperImplIT.class.getName());

    public SVNWrapperImplIT() {
    }

    /**
     * Initiate all repository stuff.
     */
    @Before
    public void setUp() throws Exception {
        mProperties = EasyMock.createMock(Properties.class);
        mSvn = new SVNWrapperImpl();
    }

    /**
     * Main method for testing SVNWrapperImpl - added/removed lines between
     * revisions, getting content of file with specified revision, etc.
     */
    @Ignore
    @Test
    public void testGetBugs() {
        String pre = ".*/java/";
        String post = ".java";

        configurePropertiesMock(URL, START_REV, END_REV, pre, post, PATH);
        ProjectHistory res = mSvn.fetchProjectHistory(mProperties, new Interrupter());
        EasyMock.verify(mProperties);
        assertNotNull(res);
        final String emptyEnumFile = "org.apache.tomcat.util.collections.EmptyEnumeration";
        final String stringCacheFile = "org.apache.tomcat.util.buf.StringCache";


        Set<Revision> emptyEnumRevisions = res.getRevisionsByFileName(emptyEnumFile);
        Revision rev = emptyEnumRevisions.iterator().next();

        assertEquals("markt", rev.getAuthor());
        assertNotNull(rev.getComment());
        assertEquals("719119", rev.getNumber());
        assertNotNull(rev.getDate());

        // Testing added/removed lines.
        pl.wroc.pwr.qualityspy.model.File file = rev.getFileByName(emptyEnumFile);
        long removedLines = file.getNumberOfRemovedLines();
        long addedLines = file.getNumberOfAddedLines();
        assertEquals(0, removedLines);
        assertEquals(3, addedLines);
        
        
        

        Set<Revision> stringCacheRevisions = res.getRevisionsByFileName(stringCacheFile);
        rev = stringCacheRevisions.iterator().next();

        assertEquals("markt", rev.getAuthor());
        assertNotNull(rev.getComment());
        assertEquals("719117", rev.getNumber());
        assertNotNull(rev.getDate());

        // Testing added/removed lines.
        file = rev.getFileByName(stringCacheFile);
        removedLines = file.getNumberOfRemovedLines();
        addedLines = file.getNumberOfAddedLines();
        assertEquals(24, removedLines);
        assertEquals(25, addedLines);

        String[] sArray;
        // Testing getting content of file with specified revision.
        try {
            String oldVer = readFileAsString("src\\test\\resources\\revisionTestMultiMapValuesEnumeration.java.old.txt");
            String newVer = readFileAsString("src\\test\\resources\\revisionTestMultiMapValuesEnumeration.java.new.txt");

            sArray = mSvn.getContents(719116, "java/org/apache/tomcat/util/collections/MultiMapValuesEnumeration.java", START_REV, URL, false);

            assertEquals(sArray[0], newVer);
            assertEquals(sArray[1], oldVer);
        } catch (Exception e) {
            LOGGER.warn("{0}", e);
        }

        assertEquals(0, res.getRevisionsByFileName("/tomcat/trunk/bin/setclasspath.sh").size());
    }
    
    @Test
    public void testGetBugsNoRepourl() {
        configurePropertiesMock("", START_REV, null, null, null,"");
        ErrorWindow errorWindow = EasyMock.createMock(ErrorWindow.class);
        errorWindow.handleError(EasyMock.isA(SVNException.class), EasyMock.isA(Errors.class));
        EasyMock.replay(errorWindow);
        mSvn.setErrorWindow(errorWindow);
        mSvn.fetchProjectHistory(mProperties, new Interrupter());
        EasyMock.verify(errorWindow);
        EasyMock.verify(mProperties);
   }
    
    @Test
    public void testGetBranchFromURL()
    {
        //Arrange
        String repoUrl = "https://svn.java.net/svn/qs/branches/b1/";
        String repoUrl1 = "https://svn.java.net/svn/qs/branches/thg/";
        String repoUrlB = "https://svn.java.net/svn/qs/tags/t1/";
        String repoUrlE = "";
        String repoUrlB1 = "///";
        
        //Action
        String res = mSvn.getBranchFromURL(repoUrl, new Interrupter());
        String res1 = mSvn.getBranchFromURL(repoUrl1, new Interrupter());
        String resB = mSvn.getBranchFromURL(repoUrlB, new Interrupter());
        String resE = mSvn.getBranchFromURL(repoUrlE, new Interrupter());
        String resB1 = mSvn.getBranchFromURL(repoUrlB1, new Interrupter());
        
        //Assert
        assertEquals("b1", res);
        assertEquals("thg", res1);
        assertEquals("", resB);
        assertEquals("", resE);
        assertEquals("", resB1);
    }
    
    @Test
    public void testGetTagFromURL()
    {
        //Arrange
        String repoUrl = "https://svn.java.net/svn/qs/tags/t1/";
        String repoUrl1 = "https://svn.java.net/svn/qs/tags/zzd/";
        String repoUrlB = "https://svn.java.net/svn/qs/branches/b1/";
        String repoUrlE = "";
        String repoUrlB1 = "///";
                
        //Action
        String res = mSvn.getTagFromURL(repoUrl, new Interrupter());
        String res1 = mSvn.getTagFromURL(repoUrl1, new Interrupter());
        String resB = mSvn.getTagFromURL(repoUrlB, new Interrupter());
        String resE = mSvn.getTagFromURL(repoUrlE, new Interrupter());
        String resB1 = mSvn.getTagFromURL(repoUrlB1, new Interrupter());
        
        //Assert
        assertEquals("t1", res);
        assertEquals("zzd", res1);
        assertEquals("", resB);
        assertEquals("", resE);
        assertEquals("", resB1);
    }
    
    /**
     * Read file and put content into String.
     *
     * @param filePath the name of the file to open. Not sure if it can accept
     * URLs or just filenames. Path handling could be better, and buffer sizes
     * are hardcoded
     * @return String containing file content.
     */
    private static String readFileAsString(String filePath)
            throws java.io.IOException {
        StringBuilder fileData = new StringBuilder(1000);
        try (BufferedReader reader = new BufferedReader(
                new FileReader(filePath))) {
            char[] buf = new char[1024];
            int numRead;
            while ((numRead = reader.read(buf)) != -1) {
                String readData = String.valueOf(buf, 0, numRead);
                fileData.append(readData);
                buf = new char[1024];
            }
        }
        return fileData.toString();
    }

    /**
     * Configures all needed stuff for repository.
     *
     * @param url	Repository URL address.
     * @param startRev	Starting at this revision.
     * @param endRev	Ending at this revision.
     * @param pre	Regex prefix.
     * @param post	Regex postfix.
     */
    private void configurePropertiesMock(String url, Object startRev, Object endRev, String pre, String post, String path) {
        EasyMock.expect(mProperties.getRepourl()).andReturn(url).anyTimes();
        EasyMock.expect(mProperties.getRepopath()).andReturn(path).anyTimes();
        EasyMock.expect(mProperties.getRepostartrev()).andReturn(startRev).anyTimes();
        EasyMock.expect(mProperties.getRepoendrev()).andReturn(endRev).anyTimes();

        EasyMock.expect(mProperties.getReposrcpathprefixregex()).andReturn(pre).anyTimes();
        EasyMock.expect(mProperties.getReposrcpathpostfixregex()).andReturn(post).anyTimes();
        EasyMock.expect(mProperties.getRepofileseparator()).andReturn('/').anyTimes();
        EasyMock.expect(mProperties.getRepoContentRegex()).andReturn("").anyTimes();
        EasyMock.expect(mProperties.getRepoDiffRegex()).andReturn("").anyTimes();
        EasyMock.replay(mProperties);
    }
}