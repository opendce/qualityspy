
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FindInnerClass {

/*


     class testowaDrugaKlasa {
     
     }
*/

	class testing {
	
	}

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		
		inner();
		//String s = new String("import java.util.regex.Matcher;\nimport java.util.regex.Pattern;\n class dupa   		 { \"\nclass paaaa \"\"");

	}

	public static void inner() throws IOException {
		String s = readFileAsString("testInnerClass1.txt");
		
		Pattern p = Pattern.compile("(\n[ "+System.getProperty("line.separator")+"\t]*[a-zA-Z]*[ "+System.getProperty("line.separator")+"\t]*class[ "+System.getProperty("line.separator")+"\t]+[a-zA-Z0-9]+)");
		Matcher m = p.matcher(s);
		boolean firstClass = true;
	
		while (m.find()) {
			// Do not count first class - it is not inner class. 
			if (firstClass) {
				firstClass = false;
			} else {
				System.out.println(m.group().trim());
			}
		}
	}
	
	/** 
     * Read file and put content into String.
     * 
     * @param filePath the name of the file to open. Not sure if it can accept URLs or just filenames. Path handling could be better, and buffer sizes are hardcoded
     * @return String containing file content.
     */ 
     private static String readFileAsString(String filePath)
     throws java.io.IOException{
         StringBuffer fileData = new StringBuffer(1000);
         BufferedReader reader = new BufferedReader(
                 new FileReader(filePath));
         char[] buf = new char[1024];
         int numRead=0;
         while((numRead=reader.read(buf)) != -1){
             String readData = String.valueOf(buf, 0, numRead);
             fileData.append(readData);
             buf = new char[1024];
         }
         reader.close();
         return fileData.toString();
     }
     
     class testowaDrugaKlasa {
     
     }
}
