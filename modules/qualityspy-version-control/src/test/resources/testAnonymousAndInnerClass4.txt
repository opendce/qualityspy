import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
public class FindInnerClass {
/**
* @param args
* @throws IOException
*/
public static void main(String[] args) throws IOException {
inner();
fun(
new klasaAnonimowa (stringVar, intVal) {
//something
}
);
//String s = new String("import java.util.regex.Matcher;\nimport java.util.regex.Pattern;\n class aaaaa { \"\nclass paaaa \"\"");
}
public static void inner() throws IOException {
String s = readFileAsString("testInnerClass1.txt");
Pattern p = Pattern.compile("(\n[ "+System.getProperty("line.separator")+"\t]*[a-zA-Z]*[ "+System.getProperty("line.separator")+"\t]*class[ "+System.getProperty("line.separator")+"\t]+[a-zA-Z0-9]+)");
Matcher m = p.matcher(s);
boolean firstClass = true;
while (m.find()) {
// Do not count first class - it is not inner class.
if (firstClass) {
firstClass = false;
} else {
System.out.println(m.group().trim());
}
}
}
}