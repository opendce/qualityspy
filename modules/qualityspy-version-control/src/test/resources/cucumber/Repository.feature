Feature: testData
  Scenario:
    Given Revision number from "1336800" to "1336884" and URL "http://svn.apache.org/repos/asf/tomcat/trunk/"
    When I fetch project history data
    And I get Revisions from file "JIoEndpoint.java"
    Then Author should match "markt"
    And Comment should contain "Fix https://issues.apache.org/bugzilla/show_bug.cgi?id=53063"
    And New file content should contain "setMaxConnections(getMaxThreadsExecutor(true));"
    And New file content should not contain "setMaxConnections(getMaxThreads());"
    And Old file content should contain "setMaxConnections(getMaxThreads());"
    And Old file content should not contain "setMaxConnections(getMaxThreadsExecutor(true));"
    And revision number should be equal to "1336884"
    And File change type should be defined with "CHANGED_CONTENT"