/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.wroc.pwr.qualityspy.vc;


import pl.wroc.pwr.qualityspy.common.Properties;
import pl.wroc.pwr.qualityspy.model.ProjectHistory;

/**
 * The listener abstract class for receiving repository events
 */
public abstract class AbstractRepoLogListener {
	protected ProjectHistory mResult;
	protected Properties mConfig;
	private String mPrefix;
	private String mPostfix;
	private char mFileSeparator;

	/**
	 * Creates an object of AbstractRepoLogListener class.
	 * 
	 * @param result
	 * @param config
	 *            Common Properties object
	 */
	protected AbstractRepoLogListener(ProjectHistory result, Properties config) {
		mResult = result;
		mConfig = config;

		if (config.getReposrcpathprefixregex() == null) {
			mPrefix = "";
		} else {
			mPrefix = config.getReposrcpathprefixregex();
		}
		if (config.getReposrcpathpostfixregex() == null) {
			mPostfix = "";
		} else {
			mPostfix = config.getReposrcpathpostfixregex();
		}
		mFileSeparator = config.getRepofileseparator();
	}

	/**
	 * Returns true, if path starts from prefix and ends with postfix (prefix
	 * and postfix comes from Properties file).
	 * 
	 * @param path
	 *            String to match with pattern
	 * @return bool
	 */
	public boolean isPathToClass(String path) {
		return path.matches(mPrefix + ".*" + mPostfix);
	}

	/**
	 * Converts path to class name.
	 * <p>
	 * Let path is: 
	 * "/tomcat/trunk/java/org/apache/tomcat/util/net/AbstractEndpoint.java". 
	 * After converting we have result "org.apache.tomcat.util.net.AbstractEndpoint".
	 * 
	 * @param path
	 *            String to convert
	 * @return Generated class name
	 */
	protected String convertPathToClassName(String path) {
		if (isPathToClass(path)) {
			String[] tab = path.split(mPrefix, 2);
			path = tab[1];
			StringBuilder sb = new StringBuilder();
			tab = path.split(mPostfix);
			sb.append(tab[0]);
			for (int i = 1; i < tab.length; i++) {
				sb.append(mPostfix);
				sb.append(tab[i]);
			}
			return convertFileSeparator(sb.toString());
		} else {
			return path;
		}
	}

	/**
	 * Converts separator of path to '.'.
	 * 
	 * @param path
	 *            String to convert
	 * @return Generated path with new separator
	 */
	private String convertFileSeparator(String path) {
		return path.replace(mConfig.getRepofileseparator(), '.');
	}

}