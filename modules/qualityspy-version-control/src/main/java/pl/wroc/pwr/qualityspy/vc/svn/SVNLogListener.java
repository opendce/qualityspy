/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wroc.pwr.qualityspy.vc.svn;

import com.google.common.collect.Sets;
import org.apache.log4j.Logger;
import org.tmatesoft.svn.core.ISVNLogEntryHandler;
import org.tmatesoft.svn.core.SVNLogEntry;
import org.tmatesoft.svn.core.SVNLogEntryPath;
import pl.wroc.pwr.qualityspy.common.Properties;
import pl.wroc.pwr.qualityspy.model.File;
import pl.wroc.pwr.qualityspy.model.ProjectHistory;
import pl.wroc.pwr.qualityspy.model.Revision;
import pl.wroc.pwr.qualityspy.vc.AbstractRepoLogListener;

import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Set;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.io.SVNRepository;

/**
 * The listener class for receiving for SVN events.
 */
public class SVNLogListener extends AbstractRepoLogListener implements
        ISVNLogEntryHandler {

    private static final Logger logger = Logger.getLogger(SVNLogListener.class);

    private final Set<Revision> revisions = Sets.newHashSet();

    private final SVNRepository repo;

    private SVNWrapperImpl wrapper;
    /**
     * Extends constructor of AbstractRepoLogListener class and sets number of
     * modifications, committers and bugs.
     *
     * @param result
     * @param config Data from Properties object.
     * @param repo Svn reository
     */
    public SVNLogListener(ProjectHistory result, Properties config, SVNRepository repo, SVNWrapperImpl wrapper) {
        super(result, config);
        this.repo = repo;
        this.wrapper = wrapper;
    }

    /**
     * Method adds information about revision according to log from SVN
     *
     * @param logEntry Encapsulates information about revision
     */
    @Override
    public void handleLogEntry(SVNLogEntry logEntry) {
        Set<String> keys = logEntry.getChangedPaths().keySet();
        File file;
        Revision rev = createRevisionDescription(logEntry);
        getRevisions().add(rev);

        for (String path : keys) {
            /*
             * String path must include prefix and postfix from Properties
             */
            SVNLogEntryPath entryPath = (SVNLogEntryPath) logEntry.getChangedPaths().get(path);

            if (wrapper.getWindowInterrupter().getIsInterrupted()){
                wrapper.getWindowInterrupter().throwIntrerrupted();
            }
            try {
                FileLogHelper flh = new FileLogHelper(mResult, mConfig, repo, wrapper);
                file = flh.createFileDescription(entryPath, logEntry, path);
                file.setRepoPath(path);
                rev.addFile(file);
            } catch (IOException | SVNException e) {
                logger.error(e.getMessage());
            }
        }
    }

    /**
     * Sets description of revision.
     *
     * @param logEntry Information about revision.
     *
     * @return Revision with setting information about author, date and the
     * number.
     */
    private Revision createRevisionDescription(SVNLogEntry logEntry) {
        Revision rev = new Revision();
        rev.setAuthor(logEntry.getAuthor());
        rev.setDate((logEntry.getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime()));
        rev.setNumber(Long.toString(logEntry.getRevision()));
        rev.setComment(logEntry.getMessage());
        return rev;
    }

    /**
     * @return the revisions
     */
    public Set<Revision> getRevisions() {
        return revisions;
    }
}