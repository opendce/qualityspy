package pl.wroc.pwr.qualityspy.vc;

import pl.wroc.pwr.qualityspy.common.Properties;
import pl.wroc.pwr.qualityspy.vc.git.GitWrapperImpl;
import pl.wroc.pwr.qualityspy.vc.svn.SVNWrapperImpl;

/**
 * Fabryka wraperow do repozytoriow. Aktualnie wspierany tylko svn
 */
public class RepoWrapperFactory {

	/**
	 *	Fetching the data from Properties object, checks the repository type,
	 *	creates the specified wrapper and sets Properties object with created
	 *	wrapper data.
	 *
	 *	@param pr	Data from Properties object
	 *
	 *	@return 	Created repository wrapper
	 */
    static public RepoWrapper createWrapper(Properties pr) {
        RepoWrapper repo = null;
        if (pr.getRepotype().compareToIgnoreCase("cvs") == 0) {
            throw new RuntimeException("Not supported");
        } else if (pr.getRepotype().compareToIgnoreCase("svn") == 0) {
            repo = createSVNWrapper(pr);
        } else if (pr.getRepotype().compareToIgnoreCase("git") == 0) {
            repo = createGitWrapper(pr);
        }
        setRepoProps(repo, pr);
        return repo;
    }

    /**
	 *	Fetching the data from Properties object and creates the SVN wrapper
	 *	setting password and user data.
	 *
	 *	@param pr	Data from Properties object
	 *
	 *	@return 	Created SVN repository wrapper
	 */
    private static RepoWrapper createSVNWrapper(Properties pr) {
        SVNWrapperImpl svn = new SVNWrapperImpl();
        svn.setPass(pr.getRepoSvnPass());
        svn.setUser(pr.getRepoSvnUser());
        return svn;
    }
    
    /**
     * 
     *	Fetching the data from Properties object and creates the Git wrapper.
     *
     *	@param pr	Data from Properties object
     *
     *	@return 	Created Git repository wrapper
     */
    private static RepoWrapper createGitWrapper(Properties pr) {
        GitWrapperImpl git = new GitWrapperImpl();
        return git;
    }

    /**
	 *	Sets properties for specified repository - bugfix pattern, file
	 *	separator, paths of prefix and postfix.
	 *
	 *	@param repoToConfigure	Which repository will be configured
	 *	@param propsSource		Data from Properties object
	 */
    private static void setRepoProps(RepoWrapper repoToConfigure, Properties propsSource) {
        repoToConfigure.setFileSeparator(propsSource.getRepofileseparator());
        repoToConfigure.setSrcPathPrefix(propsSource.getReposrcpathprefixregex());
        repoToConfigure.setSrcPathPostfix(propsSource.getReposrcpathpostfixregex());
    }
}
