package pl.wroc.pwr.qualityspy.vc.svn;

import java.io.ByteArrayOutputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.logging.Level;

import org.apache.log4j.Logger;

import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.internal.io.dav.DAVRepositoryFactory;
import org.tmatesoft.svn.core.internal.io.fs.FSRepositoryFactory;
import org.tmatesoft.svn.core.internal.io.svn.SVNRepositoryFactoryImpl;
import org.tmatesoft.svn.core.internal.wc.DefaultSVNOptions;
import org.tmatesoft.svn.core.wc.SVNClientManager;
import org.tmatesoft.svn.core.wc.SVNLogClient;
import org.tmatesoft.svn.core.wc.SVNRevision;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.SVNLogEntry;
import org.tmatesoft.svn.core.io.SVNRepository;
import org.tmatesoft.svn.core.io.SVNRepositoryFactory;
import org.tmatesoft.svn.core.wc.SVNWCUtil;
import org.tmatesoft.svn.core.auth.ISVNAuthenticationManager;
import org.tmatesoft.svn.core.SVNProperties;

import pl.wroc.pwr.qualityspy.common.Properties;
import pl.wroc.pwr.qualityspy.vc.AbstractRepoWrapperImpl;
import pl.wroc.pwr.qualityspy.vc.RepoWrapper;
import pl.wroc.pwr.qualityspy.model.ProjectHistory;

import pl.wroc.pwr.qualityspy.errorHandling.Errors;
import pl.wroc.pwr.qualityspy.errorHandling.ErrorWindow;

import pl.wroc.pwr.qualityspy.errorHandling.ExtendedErrors;
import pl.wroc.pwr.qualityspy.common.SupportedException;
import pl.wroc.pwr.qualityspy.model.Interrupter;
import pl.wroc.pwr.qualityspy.model.IsInterruptedException;

/**
 * This is a main class of SVN component. It contains all functionality which
 * provides SVN version control system.
 */
public class SVNWrapperImpl extends AbstractRepoWrapperImpl implements
        RepoWrapper {

    private String mUser;
    private String mPass;
    private final SVNDiffService diff = new SVNDiffService();
    private SVNRepository repository = null;
    private static boolean skipAllSVNException = false;
    private static boolean SkipAllEx = false;
    private Interrupter windowInterrupter = null;
    private ErrorWindow errorWindow = ErrorWindow.getInstance();

    /**
     * logger *
     */
    private static final Logger logger = Logger.getLogger(SVNWrapperImpl.class);

    public SVNWrapperImpl() {
        super();
    }

    /**
     * Method gets branch name from repository URL.
     *
     * @param repoURL Repository URL address from config
     * @return String with branch name.
     */
    public String getBranchFromURL(String repoURL, Interrupter interrupted) {
        String tempTab[] = repoURL.split("/");
        for (int index = 0; index < tempTab.length && !interrupted.getIsInterrupted(); index++) {
            if (tempTab[index].equals("branches") || tempTab[index].equals("branch") ) {
                return tempTab[++index];
            }
        }
        return "";
    }

    /**
     * Method gets tag name from repository URL.
     *
     * @param repoURL Repository URL address from config
     * @return String with tag name.
     */
    public String getTagFromURL(String repoURL, Interrupter interrupted) {
        String tempTab[] = repoURL.split("/");
        for (int index = 0; index < tempTab.length && !interrupted.getIsInterrupted(); index++) {
            if (tempTab[index].equals("tags") || tempTab[index].equals("tag")) {
                return tempTab[++index];
            }
        }
        return "";
    }

    /**
     * Method fetches svn project history.
     *
     * @param config Config with repository URL address, start/end revision,
     * etc.
     * @return
     */
    public ProjectHistory fetchProjectHistory(final Properties config, Interrupter interrupted) {
        /*
         * Setting data from Properties object
         */
        windowInterrupter = interrupted;
        Object startRevision = config.getRepostartrev();
        Object endRevision = config.getRepoendrev();
        long stRev = getStartRevision(startRevision);
        SVNRevision endRev = getEndRevision(endRevision);
        String repositoryURL = createRepositoryUrl(config.getRepourl(),
                config.getRepopath(), config.getRepofileseparator());
        /*
         * Initializes the library (it must be done before ever using the
         * library itself)
         */
        setupLibrary();

        /*
         * User's authentication information (name/password) is provided via an
         * ISVNAuthenticationManager instance. SVNWCUtil creates a default
         * authentication manager given user's name and password.
         */
        final SVNClientManager svnClientManager = createSVNClientManager();
        SVNLogClient svnLogClient = svnClientManager.getLogClient();

        final ProjectHistory result = new ProjectHistory();
        try {
            SVNURL mSvnUrl = SVNURL.parseURIEncoded(repositoryURL);

            logger.info("SVN client uses following parameter:\n-repository URL: "
                    + repositoryURL + "\n" + "-revision filter:" + stRev + ":"
                    + endRev.toString());

            /*
             * SVNLogListener class include implementing of handleLogEntry()
             * method which is used in doLog() method.
             */
            SVNLogListener logListener = new SVNLogListener(result, config, repository, this);

            String tag = getTagFromURL(repositoryURL, interrupted);
            String branch = getBranchFromURL(repositoryURL, interrupted);
            result.setTag(tag);
            result.setBranch(branch);
            
            if (tag != "")
               logger.info("Tag: " + tag);
            if (branch != "")
                logger.info("Branch: " + branch);
            
            /*
             * Fetching log with information as: revision number, datestamp of
             * commit, author of revision, commit log message and changed paths
             */
            svnLogClient.doLog(mSvnUrl, null, SVNRevision.UNDEFINED,
                    SVNRevision.create(stRev), endRev, false, true, 0,
                    logListener); //interrupt dodac
            /*
             * Calculating number of removed and added lines
             */
            diff.doDiff(svnClientManager, config.getRepourl(),
                    /*
                     * add
                     */ logListener.getRevisions(), config.getRepoDiffRegex(), String.valueOf(config.getRepofileseparator().charValue()), interrupted);
            if (interrupted.getIsInterrupted()) interrupted.throwIntrerrupted();
            result.setRevisions(logListener.getRevisions());
            result.setEndRevision(((Long) endRevision).toString());
            result.setStartRevision(Long.toString((long) stRev));

            return result;
        } catch (final SVNException ex) {

            logger.fatal(ex.getMessage());
            getErrorWindow().handleError(ex, new Errors() {

                @Override
                public void tryAgain() {           
                    ProjectHistory ob = fetchProjectHistory(config, interrupted);
                    result.setRevisions(ob.getRevisions());
                    result.setEndRevision(ob.getEndRevision());
                    result.setStartRevision(ob.getStartRevision());
                    // fetchProjectHistory(config);
                }

                @Override
                public void endProccess() {
                    logger.warn("Operation cancelled.");
                    throw new SupportedException(ex);
                }
            });
        } catch (IsInterruptedException ex) {
            java.util.logging.Logger.getLogger(SVNWrapperImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            svnClientManager.dispose();
        }
        return result;

    }

    /**
     * Sets up svn client.
     *
     * @return SVNClientManager object with set data.
     */
    SVNClientManager createSVNClientManager() {
        SVNClientManager newClientManager;
        if (mPass != null && mUser != null) {
            DefaultSVNOptions svnOptions = SVNWCUtil.createDefaultOptions(true);
            newClientManager = SVNClientManager.newInstance(svnOptions, mUser,
                    mPass);
        } else {
            newClientManager = SVNClientManager.newInstance();
        }
        return newClientManager;
    }

    /**
     * Creates SVNRevision object with first revision.
     *
     * @param startRevision Object with number of revision.
     * @return SVNRevision object with first revision.
     */
    private long getStartRevision(Object startRevision) {
        long result = 0;

        if (startRevision != null) {
            try {
                result = ((Long) startRevision).longValue();
            } catch (Exception e) {
                result = 0;
            }
        }
        return result;
    }

    /**
     * Creates SVNRevision object with last revision.
     *
     * @param endRevision Object with number of revision.
     * @return SVNRevision object with last revision.
     */
    private SVNRevision getEndRevision(Object endRevision) {
        SVNRevision result = SVNRevision.HEAD;

        if (endRevision != null) {
            try {
                long r = ((Long) endRevision).longValue();
                result = SVNRevision.create(r);
            } catch (Exception e) {
                // do nothing, result has already the default value
            }
        }
        return result;
    }

    /**
     * Gets two contents from specified file - with specified revision and it
     * previous version.
     *
     * @param revision Number of revision.
     * @param path Path to file.
     * @param startRev Start revision helps describe range of searching previous
     * revision.
     * @param url URL of repository
     * @param firstRevision
     * @return Files content or file content and empty string if there is no
     * previous version. First ([0]) one contains new content, second ([1]) one
     * contains old content.
     */
    public String[] getContents(final long revision, final String path,
            final long startRev, final String url, final boolean firstRevision) {

        // setup a libray to use
        setupLibrary();

        logger.info("Fetching contents of " + path + " in revision " + revision + ".");

        String[] result = new String[2];
        result[0] = "";
        result[0] = getContent(revision, path, url);
        result[1] = "";
        if (firstRevision) {
            return result;
        }

        SVNRepository repository = null;
        try {
            repository = SVNRepositoryFactory.create(SVNURL.parseURIDecoded(url));
        } catch (SVNException e) {
            /*
             * Malformed URL is the cause of this exception.
             */
            logger.warn("error while creating an SVNRepository for the location ''"
                    + url + "'': " + e.getMessage());
            System.exit(1);
        }
        ISVNAuthenticationManager authManager = SVNWCUtil.createDefaultAuthenticationManager(mUser, mPass);
        repository.setAuthenticationManager(authManager);
        // collection of file revisions
        Collection revisions = new LinkedList();
        try {
            // fetching log of changes of file
            String paths[] = new String[]{path};
            revisions = repository.log(paths, null, 0, revision - 1, true,
                    false);

            SVNLogEntry entry = null;
            if (!revisions.isEmpty()) {

                for (Iterator collectionItr = revisions.iterator(); collectionItr.hasNext();) {
                    entry = (SVNLogEntry) collectionItr.next();
                }
                result[1] = getContent(entry.getRevision(), path, url);
            }

        } catch (final SVNException e) {
            if (skipAllSVNException == false) {
                logger.warn("Unable to fetch ''" + path + "'' revisions : "
                        + e.getMessage());

                getErrorWindow().extendedHandleError(e,
                        new ExtendedErrors() {

                            @Override
                            public void tryAgain() {
                                getContents(revision, path, startRev, url, firstRevision);
                            }

                            @Override
                            public void endProccess() {
                                throw new SupportedException(e);
                            }

                            @Override
                            public void skip() {
                            }

                            public void skipAll() {
                                skipAllSVNException = true;
                                skip();
                            }
                        });
            } else {
                logger.fatal("Skip(SVNEx)>>:Unable to fetch ''" + path + "'' revisions : "
                        + e.getMessage());

            }
        }
        return result;
    }

    /**
     * Gets content from specified file with specified revision.
     *
     * @param revision Number of revision.
     * @param path Path to file.
     * @return File content.
     */
    private String getContent(final long revision, final String path,
            final String url) {
        /*
         * Initializes the library (it must be done before ever using the
         * library itself)
         */
        setupLibrary();

        try {
            /*
             * Creates an instance of SVNRepository to work with the repository.
             * All user's requests to the repository are relative to the
             * repository location used to create this SVNRepository. SVNURL is
             * a wrapper for URL strings that refer to repository locations.
             */
            repository = SVNRepositoryFactory.create(SVNURL.parseURIEncoded(url));
        } catch (SVNException svne) {
            /*
             * Malformed URL is the cause of this exception.
             */
            logger.warn("error while creating an SVNRepository for the location ''"
                    + url + "'': " + svne.getMessage());
            System.exit(1);
        }
        ISVNAuthenticationManager authManager = SVNWCUtil.createDefaultAuthenticationManager(mUser, mPass);
        repository.setAuthenticationManager(authManager);
        try {
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            SVNProperties svnProps = new SVNProperties();
            repository.getFile(path, revision, svnProps, os);
            return os.toString();
        } catch (final Exception ex) {
            if (windowInterrupter.getIsInterrupted()){
                windowInterrupter.throwIntrerrupted();
            }
            if (!SkipAllEx) {
                logger.warn("Unable to fetch " + path + " in revision " + revision
                        + ". QualitySpy will try do fetch the previous version.");
                getErrorWindow().extendedHandleError(ex,
                        new ExtendedErrors() {

                            @Override
                            public void tryAgain() {
                                getContent(revision, path, url);
                            }

                            @Override
                            public void endProccess() {
                                throw new SupportedException(ex);
                            }

                            @Override
                            public void skip() {
                            }

                            public void skipAll() {
                                SkipAllEx = true;
                                skip();
                            }
                        });
            } else {
                logger.fatal("Skip(ex)>>:Unable to fetch " + path + " in revision " + revision
                        + ". QualitySpy will try do fetch the previous version.");

            }
        }
        return "";
    }

    /**
     * Removes comments from source code.
     *
     * @param s String with source code.
     * @return String without comments.
     */
    public static String removeComments(String s) {
        String newString = "";
        for (int i = 0; i < s.length() - 1; i++) {

            if (s.charAt(i) == '/' && s.charAt(i + 1) == '/') {
                // searching // comment - remove to line separator.
                i = s.indexOf(System.getProperty("line.separator"), i + 2);
            } else if (s.charAt(i) == '/' && s.charAt(i + 1) == '*') {
                // // searching /* comment - remove when */ occur.
                i = s.indexOf("*/", i + 2) + 2;
            }
            if (i != s.length()) {
                newString += s.charAt(i);
            }
        }
        return newString;
    }

    /**
     * Gets all definitions of anonymous classes from string.
     *
     * @param s String which contain java file.
     * @return ArrayList with definitions of anonymous classes.
     */
    public static ArrayList<String> anonymous(String s) {
        s = removeComments(s);
        ArrayList<String> anonymousClasses = new ArrayList<String>();
        Pattern p = Pattern.compile("\n[ "
                + System.getProperty("line.separator") + "\t]*.*[ "
                + System.getProperty("line.separator")
                + "\t]*(new [a-zA-Z0-9]+)([ "
                + System.getProperty("line.separator") + "\t]*[(].*[)][ "
                + System.getProperty("line.separator") + "\t]*[{])");
        Matcher m = p.matcher(s);
        String s2 = "";
        int i = 0;
        while (m.find()) {
            // Check if it is not commented.
            s2 = m.group();
            if (!s2.contains("//")) {
                p = Pattern.compile("((new [a-zA-Z0-9]+)[ "
                        + System.getProperty("line.separator")
                        + "\t]*[(].*[)][ "
                        + System.getProperty("line.separator") + "\t]*)[{]");
                Matcher m2 = p.matcher(s2);
                while (m2.find()) {
                    anonymousClasses.add(cutNewObjects(m2.group(1)));
                    i++;
                }
            }
        }
        return anonymousClasses;
    }

    /**
     * Removes possibility of sequence like -> new String(""),new anonymousClass
     * ...
     *
     * @param s String with line which contains anonymous class.
     * @return Sequence like "new ClassName (parameters)".
     */
    public static String cutNewObjects(String s) {
        String result = "";
        int close = 0, open = 0;
        int i = s.length() - 1;
        int end = 0;
        String str;
        while ((close != open || close == 0) && i >= 0) {
            str = s.substring(i, i + 1);
            if (str.equals("(")) {
                open++;
            } else if (str.equals(")")) {
                close++;
            }
            result = str + result;
            i--;
        }
        while (end != 3) {
            str = s.substring(i, i + 1);
            if (str.equals("w")) {
                end = 1;
            } else if (str.equals("e") && end == 1) {
                end = 2;
            } else if (str.equals("n") && end == 2) {
                end = 3;
            }
            result = str + result;
            i--;
        }
        return result;
    }

    /**
     * Gets all definitions of inner classes from string.
     *
     * @param s String which contain java file.
     * @return ArrayList with definitions of inner classes.
     */
    public static ArrayList<String> inner(String s) {
        ArrayList<String> result = new ArrayList<String>();
        s = removeComments(s);
        Pattern p = Pattern.compile("(\n[ "
                + System.getProperty("line.separator") + "\t]*[a-zA-Z]*[ "
                + System.getProperty("line.separator") + "\t]*class[ "
                + System.getProperty("line.separator") + "\t]+[a-zA-Z0-9]+)");
        Matcher m = p.matcher(s);
        boolean firstClass = true;

        while (m.find()) {
            // Do not count first class - it is not inner class.
            if (firstClass) {
                firstClass = false;
            } else {
                result.add(m.group().trim());
            }
        }
        return result;
    }

    /**
     * Sets SVN user name.
     *
     * @param user SVN user name.
     */
    public void setUser(String user) {
        mUser = user;
    }

    /**
     * Gets SVN user name.
     *
     * @return SVN user name.
     */
    public String getUser() {
        return mUser;
    }

    /**
     * Gets variable Interrupter.
     *
     * @return variable Interrupter.
     */
    public Interrupter getWindowInterrupter() {
        return windowInterrupter;
    }

    /**
     * Sets SVN password.
     *
     * @param pass SVN password.
     */
    public void setPass(String pass) {
        mPass = pass;
    }

    /**
     * Gets SVN password
     *
     * @return SVN password.
     */
    public String getPass() {
        return mPass;
    }

    /**
     * Initializes the library to work with a repository via different
     * protocols.
     */
    private void setupLibrary() {

        /*
         * For using over http:// and https://
         */
        DAVRepositoryFactory.setup();
        /*
         * For using over svn:// and svn+xxx://
         */
        SVNRepositoryFactoryImpl.setup();
        /*
         * For using over file:///
         */
        FSRepositoryFactory.setup();

    }

    private String createRepositoryUrl(String repoUrl, String repoPath, char repoSeparator) {
        if (repoPath.startsWith(String.valueOf(repoSeparator))
                && !repoUrl.endsWith(String.valueOf(repoSeparator))
                || !repoPath.startsWith(String.valueOf(repoSeparator))
                && repoUrl.endsWith(String.valueOf(repoSeparator))) {
            repoUrl += repoPath;
        } else if (!repoPath.startsWith(String.valueOf(repoSeparator)) && !repoUrl.endsWith(String.valueOf(repoSeparator))) {
            repoUrl += repoSeparator + repoPath;
        } else if (repoPath.startsWith(String.valueOf(repoSeparator)) && repoUrl.endsWith(String.valueOf(repoSeparator))) {
            repoUrl += repoPath.substring(1);
        }

        return repoUrl;
    }

    /**
     * @return the errorWindow
     */
    public ErrorWindow getErrorWindow() {
        return errorWindow;
    }

    /**
     * @param errorWindow the errorWindow to set
     */
    public void setErrorWindow(ErrorWindow errorWindow) {
        this.errorWindow = errorWindow;
    }
}
