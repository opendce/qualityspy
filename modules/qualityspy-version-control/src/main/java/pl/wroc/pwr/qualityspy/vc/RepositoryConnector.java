/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wroc.pwr.qualityspy.vc;

import pl.wroc.pwr.qualityspy.common.Properties;
import pl.wroc.pwr.qualityspy.model.Interrupter;
import pl.wroc.pwr.qualityspy.model.ProjectHistory;

/**
 *
 * Implementacja Repository Connectora dla systemu kontroli wersji SVN. Do
 * prawidlowego dzialania konieczne jest zaladowanie zewnetrznej biblioteki
 * SVNKit. Przyjmujemy, ze kazdy plik .java zawiera osobna klase.
 *
 * @author Patryk Matuszewski
 */
public class RepositoryConnector implements IRepositoryConnector {
    /*
    @Override
    public ProjectHistory fetchData(Properties config) throws RepositoryException {
        RepoWrapper repo = RepoWrapperFactory.createWrapper(config);
        return repo.fetchProjectHistory(config, new Interrupter());
    }*/
    
    @Override
    public ProjectHistory fetchData(Properties config, Interrupter interrupted) throws RepositoryException {
        RepoWrapper repo = RepoWrapperFactory.createWrapper(config);
        return repo.fetchProjectHistory(config, interrupted);
    }
}
