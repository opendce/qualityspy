/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.wroc.pwr.qualityspy.vc;

import pl.wroc.pwr.qualityspy.common.Properties;
import pl.wroc.pwr.qualityspy.model.Interrupter;
import pl.wroc.pwr.qualityspy.model.ProjectHistory;

/**
 *	Interface which invokes fetching data
 */
public interface IRepositoryConnector {
	
	/**
	 *	Fetching data about revisions from repository
     * @param props
     * @param interrupted
     * @return 
     * @throws pl.wroc.pwr.qualityspy.vc.RepositoryException
	 */
        //ProjectHistory fetchData(Properties props) throws RepositoryException;
        ProjectHistory fetchData(Properties props, Interrupter interrupted) throws RepositoryException;
}
