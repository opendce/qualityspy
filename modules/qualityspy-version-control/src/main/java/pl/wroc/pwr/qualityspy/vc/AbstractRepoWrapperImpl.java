package pl.wroc.pwr.qualityspy.vc;

import pl.wroc.pwr.qualityspy.common.Properties;
import pl.wroc.pwr.qualityspy.model.Interrupter;
import pl.wroc.pwr.qualityspy.model.ProjectHistory;

/**
 *	Implements methods fetching history of project, setting and getting prefixes, postfixes and file separators
 */
public class AbstractRepoWrapperImpl implements RepoWrapper{

    String mPrefix;
    String mPostfix;
    char mFileSeparator='/';
    
    /**
     * 	Sets default prefix and postfix.
     */
    public AbstractRepoWrapperImpl(){
        mPrefix = "";
        mPostfix = "";
    }

    @Override    
    public ProjectHistory fetchProjectHistory( Properties config, Interrupter interrupted) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void setSrcPathPrefix(String regexPre) {
        if( regexPre==null ){
            return;
        }
        mPrefix = regexPre;
    }

    @Override
    public void setSrcPathPostfix(String regexPost) {
        if( regexPost==null ){
            return;
        }
        mPostfix = regexPost;
    }
    
    @Override
    public void setFileSeparator(Character sep) {
        if( sep == null ){
            return;
        }
        mFileSeparator = sep.charValue();
    }

    @Override
    public String getSrcPathPrefix() {
        return mPrefix;
    }

    @Override
    public String getSrcPathPostfix() {
        return mPostfix;
    }

    @Override
    public char getFileSeparator() {
        return mFileSeparator;
    }
}
