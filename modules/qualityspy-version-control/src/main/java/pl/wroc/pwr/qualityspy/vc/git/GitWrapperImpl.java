package pl.wroc.pwr.qualityspy.vc.git;

import com.google.common.collect.Sets;
import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.Map.Entry;
import java.util.logging.Level;
import org.apache.log4j.Logger;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.errors.MissingObjectException;
import org.eclipse.jgit.lib.Constants;
import org.eclipse.jgit.lib.PersonIdent;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.revwalk.filter.CommitTimeRevFilter;
import org.eclipse.jgit.revwalk.filter.RevFilter;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import pl.wroc.pwr.qualityspy.common.Properties;
import pl.wroc.pwr.qualityspy.common.SupportedException;
import pl.wroc.pwr.qualityspy.errorHandling.ErrorWindow;
import pl.wroc.pwr.qualityspy.errorHandling.Errors;
import pl.wroc.pwr.qualityspy.vc.AbstractRepoWrapperImpl;
import pl.wroc.pwr.qualityspy.model.ProjectHistory;
import pl.wroc.pwr.qualityspy.model.Revision;
import org.eclipse.jgit.api.ListBranchCommand.ListMode;
import org.eclipse.jgit.errors.IncorrectObjectTypeException;
import pl.wroc.pwr.qualityspy.model.Interrupter;
import pl.wroc.pwr.qualityspy.model.IsInterruptedException;

public class GitWrapperImpl extends AbstractRepoWrapperImpl {

    private static final Logger logger = Logger.getLogger(GitWrapperImpl.class);
    private String repoURI;
    private String repoDir;
    private Git git;
    private Repository repository;
    private final GitDiffService diff = new GitDiffService();

    /**
     * Method fetches git project history.
     *
     * @param config Config with repository URL address, start/end revision,
     * etc.
     * @return
     */
    @Override
    public ProjectHistory fetchProjectHistory(Properties config, Interrupter interrupted) {
        ProjectHistory history = new ProjectHistory();
        repoURI = config.getRepourl();
        repoDir = config.getRepoworkingcopy();
        if (repository == null) {
            fetchRepository(interrupted);      
        }
        try {
            if (interrupted.getIsInterrupted()) interrupted.throwIntrerrupted();
            RevCommit stRev = getStartRevision(config.getRepostartrev());
            RevCommit endRev = getEndRevision(config.getRepoendrev());
            Set<RevCommit> commits = getRevCommitSet(stRev, endRev, config);
            //Set<Revision> revisions = getRevisionSet(commits);
            Set<Revision> revisions = diff.doDiff(git, repository, commits);

            history.setRevisions(revisions);
            history.setEndRevision(endRev.getName());
            history.setStartRevision(stRev.getName());
        } catch (IsInterruptedException ex) {
            java.util.logging.Logger.getLogger(GitWrapperImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
      

        return history;
    }

    /**
     * Method downloads git repository to given directory
     *
     * @param repoURI URI of git repository
     */
    private void fetchRepository(Interrupter interrupted){
        try {
            java.io.File path = new java.io.File(repoDir);
            logger.info(String.format("Deleting %s", path.toString()));
            deleteDirectory(path);
            if (interrupted.getIsInterrupted()) interrupted.throwIntrerrupted();
            logger.info(String.format("Fetching git repostitory from %s", repoURI));
            buildRepository();
            
            logger.info(String.format("Fetched git repostitory to %s", repoDir));
            
        } catch (IOException ex) {
            logger.fatal(ex);
        } catch (final GitAPIException ex) {
            logger.fatal(ex);
            ErrorWindow.getInstance().handleError(ex, new Errors() {

                @Override
                public void tryAgain() {                  
                    fetchRepository(interrupted);
                }

                @Override
                public void endProccess() {
                    logger.warn("Operation cancelled.");
                    throw new SupportedException(ex);
                }
            });
        } catch (IsInterruptedException ex) {
            java.util.logging.Logger.getLogger(GitWrapperImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void buildRepository() throws IOException, GitAPIException {
        try{
            git = Git.cloneRepository()
                .setURI(repoURI)
                .setDirectory(new java.io.File(repoDir))
                .call();
//            git = Git.open(new java.io.File(repoDir + "/.git"));
            logger.info(String.format("Fetched git repostitory to %s", repoDir));
        }
        catch(Exception ex){
            
        }
        FileRepositoryBuilder builder = new FileRepositoryBuilder();
        repository = builder.setGitDir(new java.io.File(repoDir + "/.git"))
                .readEnvironment()
                .findGitDir()
                .build();
    }
    
    public Repository getRepository() {
        return repository;
    }
    
    public void setURI(String uri)
    {
        repoURI = uri;
    }
    
    public void setDir(String dir)
    {
        repoDir = dir;
    }
    
    
    /**
     * Method gets tag name from revision
     *
     * @param c Commit reference to a commit in the DAG
     * @return String with tag name
     */
    public String getRevTag(RevCommit c) {
        String tagName = "";
        String commitName = c.getId().getName();
        for (Entry<String, Ref> e : repository.getTags().entrySet()) {
            if (commitName.equals(e.getValue().getObjectId().getName())) {
                tagName = e.getKey();
            }
        }
        return tagName;
    }

    /**
     * Method gets branch name from revision
     *
     * @param c Commit reference to a commit in the DAG
     * @return String with branch name
     */
    public String getRevBranch(RevCommit c) {
        RevWalk revWalk = new RevWalk(repository);
        String branchName = null;
        List<Ref> call = null;
        try {
            // including remote branches
            call = new Git(repository).branchList().setListMode(ListMode.ALL).call();
        } catch (GitAPIException ex) {
            logger.fatal(ex);
        }
        for (Ref branch : call) {
            String tempBranchName = branch.getName();

            for (Map.Entry<String, Ref> e : repository.getAllRefs().entrySet()) {
                if (e.getKey().startsWith(Constants.R_HEADS)) {
                    try {
                        if (revWalk.isMergedInto(revWalk.parseCommit(e.getValue().getObjectId()), c)) {
                            String foundInBranch = e.getValue().getName();
                            if (tempBranchName.equals(foundInBranch)) {
                                branchName = tempBranchName;
                                break;
                            }
                        }
                    } catch (IncorrectObjectTypeException ex) {
                        logger.fatal(ex);
                    } catch (IOException ex) {
                        logger.fatal(ex);
                    }
                }
            }
        }
        return branchName;
    }

    /**
     * Build a revision instance based on data from RevCommit
     *
     * @param c RevCommit instance to fetch data from
     * @return Revision instance
     */
    private Revision getRevision(RevCommit c) {
        Revision r = new Revision();
        PersonIdent authorIdent = c.getAuthorIdent();
        String authorName = authorIdent.getName() + ", " + authorIdent.getEmailAddress();

        r.setAuthor(authorName);
        r.setComment(c.getShortMessage());
        r.setDate(authorIdent.getWhen().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime());
        r.setNumber(c.getName());
        r.setTags(getRevTag(c));
        r.setBranch(getRevBranch(c));
//        if (r.getTags() != "") {
//            JOptionPane.showMessageDialog(null, "Tag name:   " + r.getTags()); // only for presentation
//        }
//        JOptionPane.showMessageDialog(null, "Branch name:   " + r.getBranch()); // only for presentation
        return r;
    }

    /**
     * Creates set containing all commits between stRev and endRev
     *
     * @param stRev First (oldest) commit
     * @param endRev Last (newest) commit
     * @param config Properties instance
     * @return Set of all commits
     */
    private Set<Revision> getRevisionSet(Set<RevCommit> revSet) {
        Set<Revision> set = Sets.newHashSet();
        for (RevCommit r : revSet) {
            set.add(getRevision(r));
        }

        logger.info(String.format("Created set of %d Revisions", set.size()));
        return set;
    }

    private Set<RevCommit> getRevCommitSet(RevCommit stRev, RevCommit endRev, Properties config) {
        Set<RevCommit> set = Sets.newHashSet();
        RevWalk revWalk = new RevWalk(repository);
        RevFilter filter = CommitTimeRevFilter.between((Date) config.getRepostartrev(),
                (Date) config.getRepoendrev());
        revWalk.setRevFilter(filter);
        try {
            revWalk.markStart(revWalk.parseCommit(endRev));
            Iterator<RevCommit> i = revWalk.iterator();
            while (i.hasNext()) {
                RevCommit c = i.next();
                set.add(c);
            }
            // Add oldest revision
            set.add(stRev);
        } catch (MissingObjectException ex) {
            logger.fatal(ex);
        } catch (IOException ex) {
            logger.fatal(ex);
        }
        logger.info(String.format("Created set of %d RevCommits", set.size()));
        return set;
    }

    /**
     * Deletes given path recursivly. Use before cloning new repository.
     *
     * @param path Path to be removed
     * @return Whether deleting path ended successfully or not
     */
    static private boolean deleteDirectory(java.io.File path) {
        if (path.exists()) {
            java.io.File[] files = path.listFiles();
            for (java.io.File file : files) {
                if (file.isDirectory()) {
                    deleteDirectory(file);
                } else {
                    file.delete();
                }
            }
        }
        return (path.delete());
    }

    /**
     * Creates RevCommit object with first revision.
     *
     * @param startRevision Object that can be cast to Date object
     * @return RevCommit object with first revision.
     */
    private RevCommit getStartRevision(Object startRevision) {
        RevWalk revWalk = new RevWalk(repository);
        try {
            Date start = (Date) startRevision;
            RevFilter filter = CommitTimeRevFilter.after(start);
            revWalk.setRevFilter(filter);
            revWalk.markStart(revWalk.parseCommit(repository.resolve("HEAD")));
            Iterator<RevCommit> i = revWalk.iterator();
            RevCommit c = null;
            while (i.hasNext()) {
                c = i.next();
            }
            return c;
        } catch (IOException ex) {
            logger.fatal(ex);
        }
        return null;
    }

    /**
     * Creates RevCommit object with last revision.
     *
     * @param endRevision Object that can be cast to Date object
     * @return RevCommit object with last revision.
     */
    private RevCommit getEndRevision(Object endRevision) {
        RevWalk revWalk = new RevWalk(repository);
        try {
            Date end = (Date) endRevision;
            RevFilter filter = CommitTimeRevFilter.before(end);
            revWalk.setRevFilter(filter);
            revWalk.markStart(revWalk.parseCommit(repository.resolve("HEAD")));
            return revWalk.next();
        } catch (IOException ex) {
            logger.fatal(ex);
        }
        return null;
    }

}
