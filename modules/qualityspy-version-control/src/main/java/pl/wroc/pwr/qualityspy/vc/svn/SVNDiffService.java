/*
 * Added:
 * - counting added and removed lines between revisions
 * - javadoc code
 * Krzysiek C. 1818181
 */
package pl.wroc.pwr.qualityspy.vc.svn;

import org.apache.log4j.Logger;
import org.tmatesoft.svn.core.SVNDepth;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.wc.SVNClientManager;
import org.tmatesoft.svn.core.wc.SVNDiffClient;
import org.tmatesoft.svn.core.wc.SVNRevision;
import pl.wroc.pwr.qualityspy.model.*;
import pl.wroc.pwr.qualityspy.model.File;
import pl.wroc.pwr.qualityspy.model.Interrupter;

import java.io.*;
import java.util.Set;
import java.util.logging.Level;

/**
 * Class implements methods which support differences between revisions.
 */
public class SVNDiffService {

    /**
     * logger *
     */
    private static final Logger logger = Logger.getLogger(SVNDiffService.class);
    /**
     * Sets information about all revisions - ex. lines added/removed between
     * revisions.
     *
     * @param svnClientManager SVN client manager.
     * @see http
     * ://svnkit.com/kb/javadoc/org/tmatesoft/svn/core/wc/SVNClientManager .html
     * @param repositoryUrl URL address to repository.
     * @param classesToInvestigate
     * @param repoDiffRegex
     */
    public void doDiff(SVNClientManager svnClientManager, String repositoryUrl,
            /*
             * add
             */ Set<Revision> classesToInvestigate, String repoDiffRegex, String separator, Interrupter interrupted) {
        try {
            SVNDiffClient diffClient = svnClientManager.getDiffClient();
            long current = 0;
            for (Revision rev : classesToInvestigate) {
                current++;
                logger.info("Investigating revision " + rev.getNumber() + " ("
                        + current + "/" + classesToInvestigate.size() + ")");
                for (pl.wroc.pwr.qualityspy.model.File file : rev.getFiles()) {
                    if (interrupted.getIsInterrupted()) interrupted.throwIntrerrupted();
                    String fileName[] = file.getName().split(separator);
                    String shortFileName = fileName[fileName.length - 1];

                    if (shortFileName.matches(repoDiffRegex) || "".equals(repoDiffRegex)) {
                        if (!"".equals(repoDiffRegex) && !"".equals(shortFileName)) {
                            logger.info(shortFileName + " matches " + repoDiffRegex + " true");
                        }
                        logger.debug("Diff for " + file.getNewPath());
                        updateRevision(rev, file, repositoryUrl, diffClient);
                    } else {
                        logger.info(shortFileName + " matches " + repoDiffRegex + " false");
                        file.setNumberOfAddedLines(-1);
                        file.setNumberOfRemovedLines(-1);
                    }
                }
            }
        } catch (SVNException ex) {
            logger.fatal(ex);
        } catch (IsInterruptedException ex) {
            java.util.logging.Logger.getLogger(SVNDiffService.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Compares revisions and sets how much lines were added/removed.
     *
     * @param rev Revision object.
     * @param f
     * @param repositoryUrl
     * @param client
     * @throws SVNException
     */
    public void updateRevision(Revision rev, File f, String repositoryUrl,
            SVNDiffClient client) throws SVNException {
        // Line counter starts at -1 because of add and remove line which
        // contain revision number.
        int linesAdded = -1, linesRemoved = -1;
        ByteArrayOutputStream os = getDiffStream(client, repositoryUrl, rev, f);

        // parse difference to count how much lines were added / removed.
        ByteArrayInputStream bais = new ByteArrayInputStream(os.toByteArray());
        try (BufferedReader in = new BufferedReader(new InputStreamReader(bais))) {
            String s = in.readLine();

            if (s != null && s.getBytes().length == 0) {
                logger.warn("Zero-length file: " + f.getName() + " r"
                        + rev.getNumber());
            }

            while (s != null && s.getBytes().length > 0
                    && s.getBytes()[0] != '\0') {
                if (s.substring(0, 1).compareTo("+") == 0) {
                    linesAdded++;
                } else if (s.substring(0, 1).compareTo("-") == 0) {
                    linesRemoved++;
                }
                s = in.readLine();
            }
        } catch (IOException e) {
            logger.fatal(e);
        }

        if (linesAdded != -1) {
            f.setNumberOfAddedLines(linesAdded);
        }
        if (linesRemoved != -1) {
            f.setNumberOfRemovedLines(linesRemoved);
        }
    }

    private ByteArrayOutputStream getDiffStream(SVNDiffClient client,
            String repoUrl, Revision rev, File file) {
        long start = 0L;
        long end = 0L;
        try {
            start = Long.parseLong(rev.getNumber()) - 1L;
            end = Long.parseLong(rev.getNumber());
        } catch (NumberFormatException e) {
            logger.fatal(e);
        }

        ByteArrayOutputStream os = new ByteArrayOutputStream();
        if (file.getChangeType() == File.ChangeType.CREATION) {
            file.setNumberOfAddedLines(countNumberOfLines(file.getNewContent()));
            return os;
        }
        try {
            SVNURL diffURL = generateUrlForClass(repoUrl, file.getRepoPath());
            if (file.getOldPath() == null) {
                client.doDiff(diffURL, SVNRevision.UNDEFINED,
                        SVNRevision.create(start), SVNRevision.create(end),
                        SVNDepth.INFINITY, true, os);
            } else {
                SVNURL oldDiffURL = generateUrlForClass(repoUrl,
                        file.getOldPath());
                client.doDiff(oldDiffURL, SVNRevision.UNDEFINED, diffURL,
                        SVNRevision.create(end), SVNDepth.INFINITY, true, os);
            }
        } catch (SVNException e) {
            logger.warn(end
                    + "r "
                    + file.getRepoPath()
                    + " - file was removed or is not in the repo path; no diff will be executed.");
        }
        return os;
    }

    /**
     *
     * @param repositoryUrl URL address to repository.
     * @param path Path to class.
     * @return Generated URL for our class.
     * @throws SVNException
     */
    public SVNURL generateUrlForClass(String repositoryUrl, String path)
            throws SVNException {
        int i = 0;
        for (; i < path.length() - 1; i++) {
            if (!repositoryUrl.contains(path.substring(0, i + 1))) {
                break;
            }
        }
        if (i > 0 && (path.charAt(i) == '/' || path.charAt(i) == '\\')) {
            String overlap = path.substring(0, i);
            if (!"/".equals(overlap) && !"\\".equals(overlap)) {
                repositoryUrl = repositoryUrl.substring(0,
                        repositoryUrl.lastIndexOf(overlap));
            }
        }
        String url = repositoryUrl + path;

        SVNURL diffURL = SVNURL.parseURIEncoded(url);
        return diffURL;
    }

    private long countNumberOfLines(String content) {
        if (content == null || "".equals(content)) {
            return 0;
        }
        String[] lines = content.split("\n");
        if (lines == null) {
            return 0;
        }
        return lines.length;
    }
}
