package pl.wroc.pwr.qualityspy.vc.svn;

import java.io.IOException;
import java.util.Map;
import pl.wroc.pwr.qualityspy.model.File;
import pl.wroc.pwr.qualityspy.model.File.ChangeType;
import org.tmatesoft.svn.core.SVNLogEntry;
import org.tmatesoft.svn.core.SVNLogEntryPath;
import org.apache.log4j.Logger;
import org.tmatesoft.svn.core.SVNDepth;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.io.ISVNReporter;
import org.tmatesoft.svn.core.io.ISVNReporterBaton;
import org.tmatesoft.svn.core.io.SVNRepository;
import pl.wroc.pwr.qualityspy.common.Properties;
import pl.wroc.pwr.qualityspy.model.ProjectHistory;
import pl.wroc.pwr.qualityspy.vc.AbstractRepoLogListener;
import pl.wroc.pwr.qualityspy.vc.FileAnalyzer;

public class FileLogHelper extends AbstractRepoLogListener {

    private static final Logger logger = Logger.getLogger(FileLogHelper.class);

    private final SVNRepository repo;

    private ISVNReporterBaton reporter;

    private Map fileProps;

    private SVNWrapperImpl wrapper;

    public FileLogHelper(ProjectHistory result, Properties config, SVNRepository repo, SVNWrapperImpl wrapper) throws SVNException {
        super(result, config);
        this.repo = repo;
        this.wrapper = wrapper;
        if (repo != null) {
            prepReportEditor();
        }
    }

    private void fetchFileContent(SVNLogEntryPath entryPath,
            SVNLogEntry logEntry, File file, SVNWrapperImpl wrapper) {
        boolean firstRevision = file.getChangeType() == ChangeType.CREATION;
        boolean fetchContent = true; //firstRevision && file.getFileType() == FileType.CLASS;

        if (fetchContent) {
            String[] contents = wrapper.getContents(logEntry.getRevision(),
                    entryPath.getPath(), (Long) mConfig.getRepostartrev(),
                    mConfig.getRepourl(), firstRevision);
            file.setOldContent(contents[1]);
            file.setNewContent(contents[0]);
        }
    }

    /**
     * Sets description of file.
     *
     * @param entryPath Log about changed paths.
     *
     * @param logEntry Log about revision.
     * @param path The name and path of the file.
     *
     * @return Revision with setting information about author, date and the
     * number.
     */

    /**
     * Sets description of file.
     *
     * @param entryPath Log about changed paths.
     * @param logEntry Log about revision.
     * @param path The name and path of the file.
     * @return Revision with setting information about author, date and the
     * number.
     * @throws java.io.IOException
     */
    public File createFileDescription(SVNLogEntryPath entryPath,
            SVNLogEntry logEntry, String path) throws IOException {
        File file = new File();
        FileAnalyzer analyzer = new FileAnalyzer();
        file.setFileExtension(analyzer.checkFileExtension(path));
        file.setFileType(analyzer.checkFileType(file.getFileExtension()));
        file.setNewPath(entryPath.getPath());
        file.setName(getName(path));
        /*
         * If copyPath is not null, that means path of the file has changed. If
         * contents[0] equals "" means, that there is no previous content of the
         * file. If contents[1] equals "" means, that there is no current
         * content of the file. In other cases file has changed its content.
         */
        if (entryPath.getCopyPath() != null) {
            file.setOldPath(entryPath.getCopyPath());
        } 
        
        file.setChangeType(analyzer.getChangeTypeSvn(entryPath));
        
        String contentRegex = mConfig.getRepoContentRegex();
        String separator = String.valueOf(mConfig.getRepofileseparator().charValue());
        String fileName[] = file.getName().split(separator);
        String shortFileName = fileName[fileName.length - 1];
        if (contentRegex == null){
            fetchFileContent(entryPath, logEntry, file, wrapper);
        }
        else if (shortFileName.matches(contentRegex) || "".equals(contentRegex) ) {
            if (!"".equals(contentRegex)) {
                logger.info(shortFileName + " matches " + contentRegex + " true");
            }
            fetchFileContent(entryPath, logEntry, file, wrapper);
        } else {
            logger.info(shortFileName + " matches " + contentRegex + " false");
        }

        return file;
    }//2423 - r 12502

    private void prepReportEditor() throws SVNException {
        long rev = repo.getLatestRevision();
        reporter = new ISVNReporterBaton() {
            @Override
            public void report(ISVNReporter reporter1) throws SVNException {
                reporter1.setPath("", null, rev, SVNDepth.INFINITY, true/*we are empty, take us all like in checkout*/);
                reporter1.finishReport();
            }
        };

        PropFetchingEditor editor = new PropFetchingEditor();

        repo.status(rev, null, SVNDepth.INFINITY, reporter, editor);

        fileProps = editor.getFilesToProps();
    }
    
    private String getName(String path) {
        String[] split = path.split("/");
        return split[split.length - 1];
    }
    
    
}
