package pl.wroc.pwr.qualityspy.vc;

/**
*	Extending exception class. Constructing exceptions with the specified parameters. 
*/

public class RepositoryException extends Exception {

    public RepositoryException(String message) {
        super(message);
    }

    public RepositoryException() {
    }

    public RepositoryException(String message, Throwable cause) {
        super(message, cause);
    }
}
