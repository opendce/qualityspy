package pl.wroc.pwr.qualityspy.vc;

import pl.wroc.pwr.qualityspy.common.Properties;
import pl.wroc.pwr.qualityspy.model.Interrupter;
import pl.wroc.pwr.qualityspy.model.ProjectHistory;

/**
 *	Interface for repository classes. Invokes fetching history of project, setting or getting prefixes, postfixes and file separators
 */
public interface RepoWrapper {
    
	/**
	 * Unsupported yet.
	 */
    public ProjectHistory fetchProjectHistory( Properties config, Interrupter interrupted);
    
    /**
     *	Registers the prefix of the path.
     *
     *	@param regexPre	Prefix
     */
    void setSrcPathPrefix( String regexPre );
    
    /**
     * 	@return Prefix of the path.
     */
    String getSrcPathPrefix();
    
    /**
     *	Registers the postfix of the path.
     *
     *	@param regexPost	Postfix
     */
    void setSrcPathPostfix( String regexPost );
    
    /**
     * 	@return Postfix of the path.
     */
    String getSrcPathPostfix();
    
    /**
     *	Sets new file separator, '/' is default.
     *
     *	@param sep	Separator
     */
    void setFileSeparator( Character sep );
    
    /**
     * 	@return Current file separator.
     */
    char getFileSeparator();

}
