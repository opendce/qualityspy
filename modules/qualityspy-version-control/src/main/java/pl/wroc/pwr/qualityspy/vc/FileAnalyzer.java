/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wroc.pwr.qualityspy.vc;

import java.util.Iterator;
import java.util.Map;
import org.tmatesoft.svn.core.SVNLogEntryPath;
import org.tmatesoft.svn.core.SVNPropertyValue;
import pl.wroc.pwr.qualityspy.model.File;
import pl.wroc.pwr.qualityspy.model.enums.FileType;

/**
 *
 * @author Adam
 */
public class FileAnalyzer {

    public FileType checkFileType(String extension) {

        switch (extension.toLowerCase()) {
            case "java":
            case "h":
            case "c":
            case "cpp":
            case "txt":
            case "asc":
            case "md":
            case "pas":
            case "xml":
            case "xsd":
            case "html":
            case "js":
            case "css":
                return FileType.TEXT;
            case "exe":
            case "jar":
            case "rar":
            case "zip":
            case "gz":
            case "tgz":
            case "jpg":
            case "jpeg":
            case "gif":
            case "bmp":
            case "pcx":
            case "png":
            case "mpg":
            case "mpeg":
            case "mp4":
            case "avi":
            case "mp3":
            case "wav":
            case "au":
            case "mid":
                return FileType.BINARY;
            default:
                return FileType.OTHER;
        }
    }

    public String checkFileExtension(String path) {

        int tmp = path.lastIndexOf(".");
        if (tmp == -1) {
            return "File";
        } else {
            return path.substring(tmp + 1);

        }
    }
    
    /**
     * Returns ChangeType based on given diff
     *
     * @param trimDiff String with one file's diff without first line
     * @return ChangeType based on diff
     */
    public File.ChangeType getChangeTypeGit(String trimDiff, String oldPath, String newPath) {
         if (trimDiff.startsWith("deleted")) {
            return File.ChangeType.DELETION;
        } else if (trimDiff.startsWith("new file")) {
            return File.ChangeType.CREATION;
        } else if (!newPath.equals(oldPath)) {
            return File.ChangeType.CHANGED_PATH;
        } else {
            return File.ChangeType.CHANGED_CONTENT;
        } // ChangeType.CHANGED_PATH unsupported in git as of v1.7.9
    }
    
    public File.ChangeType getChangeTypeSvn(SVNLogEntryPath entryPath) {
        if (entryPath.getCopyPath() != null) {
           return File.ChangeType.CHANGED_PATH;
        } else if (entryPath.getType() == 'D') {
           return File.ChangeType.DELETION;
        } else if (entryPath.getType() == 'A') {
           return File.ChangeType.CREATION;
        } else {
          return File.ChangeType.CHANGED_CONTENT;
        }
    }
}
