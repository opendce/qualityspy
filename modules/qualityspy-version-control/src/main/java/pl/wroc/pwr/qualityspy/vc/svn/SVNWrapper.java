/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.wroc.pwr.qualityspy.vc.svn;

import pl.wroc.pwr.qualityspy.model.ProjectHistory;

/**
 * Interface of repository and transport classes. Invokes geeting bugs, changes
 * in prefixes, postfixes, file separators
 */
public interface SVNWrapper {

	ProjectHistory getBugs(String repositoryUrl, long startRevision);

	void setSrcPathPrefix(String regexPre);

	void setSrcPathPostfix(String regexPost);

	void setFileSeparator(char sep);

}
