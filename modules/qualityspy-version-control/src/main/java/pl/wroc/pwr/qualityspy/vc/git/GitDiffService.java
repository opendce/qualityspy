package pl.wroc.pwr.qualityspy.vc.git;

import com.google.common.collect.Sets;
import org.apache.log4j.Logger;
import org.eclipse.jgit.api.DiffCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.errors.CorruptObjectException;
import org.eclipse.jgit.errors.IncorrectObjectTypeException;
import org.eclipse.jgit.errors.MissingObjectException;
import org.eclipse.jgit.lib.*;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevTree;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.treewalk.AbstractTreeIterator;
import org.eclipse.jgit.treewalk.CanonicalTreeParser;
import org.eclipse.jgit.treewalk.TreeWalk;
import org.eclipse.jgit.treewalk.filter.PathFilter;
import pl.wroc.pwr.qualityspy.model.File;
import pl.wroc.pwr.qualityspy.model.File.ChangeType;
import pl.wroc.pwr.qualityspy.model.Revision;
import pl.wroc.pwr.qualityspy.model.enums.FileType;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.time.ZoneId;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.eclipse.jgit.api.ListBranchCommand;
import pl.wroc.pwr.qualityspy.vc.FileAnalyzer;

public class GitDiffService {

    private static final Logger logger = Logger.getLogger(GitDiffService.class);
    private Git git;
    private Repository repo;

    /**
     * Collects data from given commits and returns set of Revision. Revisions
     * have their data set as well.
     *
     * @param git Git instance connected to desired repository
     * @param repo Repository instance connected to desired repository
     * @param commits Commits which will be used to collect data
     * @return Revisions with all data filled-in
     */
    public Set<Revision> doDiff(Git git, Repository repo, Set<RevCommit> commits) {
        this.git = git;
        this.repo = repo;
        Set<Revision> set = Sets.newHashSet();
        Iterator<RevCommit> i = commits.iterator();
        RevCommit prev;

        if (i.hasNext()) {
            prev = i.next();
        } else {
            return set;
        }
        while (i.hasNext()) {
            RevCommit curr = i.next();

            TreeWalk treeWalk = new TreeWalk(repo);
            try {
                RevTree tree = curr.getTree();
                treeWalk.addTree(tree);
                treeWalk.setRecursive(true);
            } catch (MissingObjectException ex) {
                treeWalk = null;
            } catch (IncorrectObjectTypeException ex) {
                treeWalk = null;
            } catch (CorruptObjectException ex) {
                treeWalk = null;
            } catch (IOException ex) {
                treeWalk = null;
            }

            String diff = getDiff(prev.name(), curr.name());
            Revision r = getRevision(curr, diff, treeWalk);
            set.add(r);
            prev = curr;
        }
        return set;
    }

    /**
     * Get a diff between two given commits
     *
     * @param prev First commit's hash
     * @param curr Second commit's hash
     * @return Diff between given commits. Empty string on error.
     */
    private String getDiff(String prev, String curr) {
        try {
            OutputStream out = new ByteArrayOutputStream();
            DiffCommand diff = git.diff().setOutputStream(out)
                    .setOldTree(getTreeIterator(repo, prev))
                    .setNewTree(getTreeIterator(repo, curr));
            diff.call();
            parseDiff(out.toString(), null);
            return out.toString();
        } catch (IOException | GitAPIException ex) {
            logger.fatal(ex);
        }
        return "";
    }

    /**
     * Method gets tag name from revision
     *
     * @param c Commit reference to a commit in the DAG
     * @return String with tag name
     */
    public String getRevTag(RevCommit c) {
        String tagName = "";
        String commitName = c.getId().getName();
        for (Map.Entry<String, Ref> e : repo.getTags().entrySet()) {
            if (commitName.equals(e.getValue().getObjectId().getName())) {
                tagName = e.getKey();
            }
        }
        return tagName;
    }

    /**
     * Method gets branch name from revision
     *
     * @param c Commit reference to a commit in the DAG
     * @return String with branch name
     */
    public String getRevBranch(RevCommit c) {
        RevWalk revWalk = new RevWalk(repo);
        String branchName = null;
        List<Ref> call = null;
        try {
            // including remote branches
            call = new Git(repo).branchList().setListMode(ListBranchCommand.ListMode.ALL).call();
        } catch (GitAPIException ex) {
            logger.fatal(ex);
        }
        for (Ref branch : call) {
            String tempBranchName = branch.getName();

            for (Map.Entry<String, Ref> e : repo.getAllRefs().entrySet()) {
                if (e.getKey().startsWith(Constants.R_HEADS)) {
                    try {
                        if (revWalk.isMergedInto(revWalk.parseCommit(e.getValue().getObjectId()), c)) {
                            String foundInBranch = e.getValue().getName();
                            if (tempBranchName.equals(foundInBranch)) {
                                branchName = tempBranchName;
                                break;
                            }
                        }
                    } catch (IncorrectObjectTypeException ex) {
                        logger.fatal(ex);
                    } catch (IOException ex) {
                        logger.fatal(ex);
                    }
                }
            }
        }
        return branchName;
    }

    /**
     * Build a revision instance based on data from RevCommit
     *
     * @param c RevCommit instance to fetch data from
     * @param diff String with diff from c previous commit
     * @return Revision instance
     */
    private Revision getRevision(RevCommit c, String diff, TreeWalk tree) {
        Revision r = new Revision();
        PersonIdent authorIdent = c.getAuthorIdent();
        String authorName = authorIdent.getName() + ", " + authorIdent.getEmailAddress();

        r.setAuthor(authorName);
        r.setComment(c.getShortMessage());
        r.setDate(authorIdent.getWhen().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime());
        r.setNumber(c.getName());
        r.setFiles(parseDiff(diff, tree));
        r.setTags(getRevTag(c));
        r.setBranch(getRevBranch(c));

        return r;
    }

    /**
     * Construct set of Files with where changed. Files' data is filled-in based
     * on full diff
     *
     * @param diff String with diff from c previous commit
     * @return Set of filled-in Files
     */
    private Set<File> parseDiff(String diff, TreeWalk tree) {
        Set<File> set = Sets.newHashSet();
        Pattern oldPathRe = Pattern.compile("(?<=--- a?/).*?(?=\\n)");
        Pattern newPathRe = Pattern.compile("(?<=\\+\\+\\+ b?/).*?(?=\\n)");
        String[] split = diff.split("diff --git.*(\\r)?\\n");
        FileAnalyzer analyzer = new FileAnalyzer();
        for (String f : split) {
            if (f.length() == 0) {
                continue;
            }
            String oldPath = getMatch(oldPathRe, f);
            String newPath = getMatch(newPathRe, f);

            File file = new File();
            file.setOldPath(oldPath);
            file.setNewPath(newPath);
            file.setFileExtension(analyzer.checkFileExtension(newPath));
            file.setFileType(analyzer.checkFileType(file.getFileExtension()));

            file.setChangeType(analyzer.getChangeTypeGit(f, oldPath, newPath));
            setChangedLines(file, f);
            file.setName(getName(newPath));
            set.add(file);

            if (tree != null) {
                String newContent, oldContent;
                newContent = getContent(tree, newPath);
                oldContent = getContent(tree, oldPath);
                file.setNewContent(newContent);
                file.setOldContent(oldContent);
            }
        }
        return set;
    }

    /**
     * @param name Name of commit
     * @return Iterator which is set to commit with given name
     * @throws IOException
     */
    private AbstractTreeIterator getTreeIterator(Repository repo, String name) throws IOException {
        final ObjectId id = repo.resolve(name);
        if (id == null) {
            throw new IllegalArgumentException(name);
        }

        final CanonicalTreeParser p = new CanonicalTreeParser();
        final ObjectReader or = repo.newObjectReader();
        try {
            p.reset(or, new RevWalk(repo).parseTree(id));
            return p;
        } finally {
            or.release();
        }
    }

    

    /**
     * Try to find pattern in string f. Return empty string if not found
     *
     * @param pattern Desired regex
     * @param f String to search in
     * @return Found substring or empty string
     */
    private String getMatch(Pattern pattern, String f) {
        Matcher m = pattern.matcher(f);
        if (m.find()) {
            return m.group();
        }
        return "";
    }

    /**
     * Returns file (or directory) name in given path. Return substring after
     * last "/"
     *
     * @param path String with path
     * @return File or directory name
     */
    private String getName(String path) {
        String[] split = path.split("/");
        return split[split.length - 1];
    }

    /**
     * Changes file's data about added and removed lines based on f
     *
     * @param file File about to change
     * @param trimDiff String with one file's diff without first line
     */
    private void setChangedLines(File file, String trimDiff) {
        String[] changes = trimDiff.split("^(@@).*(@@)");
        int added = 0, removed = 0;
        for (String l : changes) {
            if (l.startsWith("+")) {
                added++;
            } else if (l.startsWith("-")) {
                removed++;
            }
        }
        file.setNumberOfAddedLines(added);
        file.setNumberOfRemovedLines(removed);
    }

    /**
     * Get content of file in given path
     *
     * @param tree
     * @param path
     * @return
     */
    private String getContent(TreeWalk tree, String path) {
        String content;
        tree.setFilter(PathFilter.create(path));
        ObjectId id = tree.getObjectId(0);
        try {
            ObjectLoader loader = repo.open(id);
            InputStream in = loader.openStream();
            java.util.Scanner s = new java.util.Scanner(in);
            content = s.hasNext() ? s.next() : "";
        } catch (MissingObjectException ex) {
            logger.fatal(ex);
            content = "";
        } catch (IOException ex) {
            logger.fatal(ex);
            content = "";
        }
        return content;
    }
}
