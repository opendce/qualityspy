/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wroc.pwr.qualityspy.vc.svn;

import java.io.OutputStream;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import org.tmatesoft.svn.core.SVNCommitInfo;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNProperty;
import org.tmatesoft.svn.core.SVNPropertyValue;
import org.tmatesoft.svn.core.io.ISVNEditor;
import org.tmatesoft.svn.core.io.diff.SVNDiffWindow;

/**
 *
 * @author Adam
 */
 class PropFetchingEditor implements ISVNEditor {
        private final LinkedList myDirectoriesStack = new LinkedList();
        private final Map myDirProps = new HashMap();
        private final Map myFileProps = new HashMap();
        
        @Override
        public void abortEdit() throws SVNException {
        }

        @Override
        public void absentDir(String path) throws SVNException {
        }

        @Override
        public void absentFile(String path) throws SVNException {
        }

        @Override
        public void addFile(String path, String copyFromPath, long copyFromRevision) throws SVNException {
        }

        @Override
        public SVNCommitInfo closeEdit() throws SVNException {
            return null;
        }

        @Override
        public void closeFile(String path, String textChecksum) throws SVNException {
        }

        @Override
        public void deleteEntry(String path, long revision) throws SVNException {
        }

        @Override
        public void openFile(String path, long revision) throws SVNException {
        }

        @Override
        public void targetRevision(long revision) throws SVNException {
        }

        @Override
        public void applyTextDelta(String path, String baseChecksum) throws SVNException {
        }

        @Override
        public OutputStream textDeltaChunk(String path, SVNDiffWindow diffWindow) throws SVNException {
            return null;
        }

        @Override
        public void textDeltaEnd(String path) throws SVNException {
        }

        @Override
        public void addDir(String path, String copyFromPath, long copyFromRevision) throws SVNException {
            String absouluteDirPath = "/" + path;
            myDirectoriesStack.push(absouluteDirPath);
        }

        @Override
        public void changeDirProperty(String name, SVNPropertyValue value) throws SVNException {
            //filter out svn:entry and svn:wc properties since we are interested in regular properties only
            if (!SVNProperty.isRegularProperty(name)) {
                return;
            }

            String currentDirPath = (String) myDirectoriesStack.peek();
            Map props = (Map) myDirProps.get(currentDirPath);
            if (props == null) {
                props = new HashMap();
                myDirProps.put(currentDirPath, props);
            }
            props.put(name, value);
        }

        @Override
        public void changeFileProperty(String path, String propertyName, SVNPropertyValue propertyValue) throws SVNException {
            //filter out svn:entry and svn:wc properties since we are interested in regular properties only
            if (!SVNProperty.isRegularProperty(propertyName)) {
                return;
            }

            String absolutePath = "/" + path;
            Map props = (Map) myFileProps.get(absolutePath);
            if (props == null) {
                props = new HashMap();
                myFileProps.put(absolutePath, props);
            }
            props.put(propertyName, propertyValue);
        }

        @Override
        public void closeDir() throws SVNException {
            myDirectoriesStack.pop();
        }

        @Override
        public void openDir(String path, long revision) throws SVNException {
            String absoluteDirPath = "/" + path;
            myDirectoriesStack.push(absoluteDirPath);
        }

        @Override
        public void openRoot(long revision) throws SVNException {
            String absoluteDirPath = "/";  
            myDirectoriesStack.push(absoluteDirPath);
        }

        public Map getDirsToProps() {
            return myDirProps;
        }
        
        public Map getFilesToProps() {
            return myFileProps;
        }
    }