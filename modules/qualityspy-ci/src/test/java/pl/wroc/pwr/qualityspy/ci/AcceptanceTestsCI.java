package pl.wroc.pwr.qualityspy.ci;

import org.easymock.EasyMock;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import pl.wroc.pwr.qualityspy.ci.connector.*;
import pl.wroc.pwr.qualityspy.common.Properties;
import pl.wroc.pwr.qualityspy.model.Build;
import pl.wroc.pwr.qualityspy.model.Interrupter;
import pl.wroc.pwr.qualityspy.model.Job;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author Filip Bieleszuk
 */
public class AcceptanceTestsCI {

    public Properties mProperties;
    private MockAcceptanceServer mockAcceptanceServer;

    @Before
    public void setUp() throws IOException {
        mockAcceptanceServer = new MockAcceptanceServer();
        mockAcceptanceServer.setUp();
        mProperties = EasyMock.createMock(Properties.class);
    }

    @After
    public void tearDown() throws IOException {
        mockAcceptanceServer.tearDown();
    }

    @Test
    public void testJenkinsCompat() throws IOException, URISyntaxException {
        URL uri = Thread.currentThread().getContextClassLoader().getResource("");
        Path resourcesDirectory = Paths.get(uri.toURI());
        Files.walk(resourcesDirectory, 1).forEach(filePath -> {
            if (filePath.getFileName().toString().startsWith("jenkins-")) {
                try {
                    testVersion(filePath); //Można użyć scenariuszy JUnit
                } catch (Exception e) {
                    e.printStackTrace();
                    fail(e.getMessage());
                }
            }
        });
    }

    @Test
    public void testHudsonCompat() throws IOException, URISyntaxException {
        URL uri = Thread.currentThread().getContextClassLoader().getResource("");
        Path resourcesDirectory = Paths.get(uri.toURI());
        Files.walk(resourcesDirectory, 1).forEach(filePath -> {
            if (filePath.getFileName().toString().startsWith("hudson-")) {
                try {
                    testVersion(filePath);
                } catch (Exception e) {
                    e.printStackTrace();
                    fail(e.getMessage());
                }
            }
        });
    }

    private void testVersion(Path filePath) throws Exception {
        final String folderName = filePath.getFileName().toString();
        final String uri = String.format("http://localhost:%d", MockAcceptanceServer.SOCKET);
        List<Job> jobs = testGetAllJobs(new JenkinsConnector(),
                Arrays.asList(
                        folderName + "/jobs.json",
                        folderName + "/ok_job.json",
                        folderName + "/failed_build.json",
                        folderName + "/ok_build.json",
                        folderName + "/unstable_build.json"));
        Job job = jobs.get(0);
        Job mockJob = new Job("testJob", String.format("%s/job/testJob/", uri), "blue");
        assertEquals(job.getColor(), mockJob.getColor());
        assertEquals(job.getName(), mockJob.getName());
        assertEquals(job.getUrl(), mockJob.getUrl()); //TODO zmienność diff'ów
        assertEquals(job.getBuilds().get(0), new Build(1, LocalDateTime.ofEpochSecond(1500000000001L, 0, ZoneOffset.UTC), "FAILURE", 3, 0, 3, 3));
        assertEquals(job.getBuilds().get(1), new Build(2, LocalDateTime.ofEpochSecond(1500000000002L, 0, ZoneOffset.UTC), "SUCCESS", 3, 0, 0, -1));
        assertEquals(job.getBuilds().get(2), new Build(3, LocalDateTime.ofEpochSecond(1500000000003L, 0, ZoneOffset.UTC), "UNSTABLE", 3, 0, 1, 0));
    }

    @Test
    public void testInvalidJenkinsJobList() {
        testInvalidJobList(new JenkinsConnector(), "jenkins/jenkins_ok_job.txt"); //intended wrong data
    }

    @Test
    public void testInvalidHudsonJobList() {
        testInvalidJobList(new HudsonConnector(), "hudson/hudson_ok_job.txt"); //intended wrong data
    }

    private void testInvalidJobList(AbstractConnector connector, String mockData) {
        try {
            mockAcceptanceServer.prepareResponse(Arrays.asList(mockData));
            String uri = String.format("http://localhost:%d", MockAcceptanceServer.SOCKET);
            List<Job> jobs = connector.getJobs(uri, "", "", new Interrupter());
            fail("Should throw exception");
        } catch (Exception ignored) {

        }
    }

    @Test
    public void testInvalidHudsonLogin() {
        try {
            testGetJob("QualitySpy", new HudsonConnector(), Arrays.asList("hudson/hudson_bad_job.txt"));
            fail("Should throw exception");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testInvalidJenkinsLogin() {
        try {
            testGetJob("vivid_kilo_trove_trunk", new JenkinsConnector(), Arrays.asList("jenkins/jenkins_bad_job.txt"));
            fail("Should throw exception");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Job testGetJob(String job, AbstractConnector connector, List<String> mockData) throws Exception {
        mockAcceptanceServer.prepareResponse(mockData); //intended wrong data
        String uri = String.format("http://localhost:%d", MockAcceptanceServer.SOCKET);
        Job jobResult = connector.getJob(job, uri, "", "", new Interrupter());
        return jobResult;

    }

    @Test
    public void testHudsonGetAllJobs() throws Exception {
        List<Job> jobs = testGetAllJobs(new HudsonConnector(),
                Arrays.asList("hudson/hudson_jobs.txt", "hudson/hudson_ok_job.txt", "hudson/hudson_failed_build.txt",
                        "hudson/hudson_ok_build.txt", "hudson/hudson_unstable_build.txt",
                        "hudson/hudson_ok_job.txt", "hudson/hudson_failed_build.txt", "hudson/hudson_ok_build.txt",
                        "hudson/hudson_unstable_build.txt"));
        Job job = HudsonConnectorTest.prepareJob();
        assertTrue(jobs.contains(job));
    }

    @Test
    public void testJenkinsGetAllJobs() throws Exception {
        assertTrue(testGetAllJobs(new JenkinsConnector(),
                Arrays.asList("jenkins/jenkins_jobs.txt", "jenkins/jenkins_ok_job.txt",
                        "jenkins/jenkins_ok_build.txt", "jenkins/jenkins_failed_build.txt",
                        "jenkins/jenkins_ok_job.txt", "jenkins/jenkins_ok_build.txt",
                        "jenkins/jenkins_failed_build.txt",
                        "jenkins/jenkins_ok_job.txt",
                        "jenkins/jenkins_ok_build.txt", "jenkins/jenkins_failed_build.txt",
                        "jenkins/jenkins_ok_job.txt", "jenkins/jenkins_ok_build.txt",
                        "jenkins/jenkins_failed_build.txt")).contains(JenkinsConnectorTest.prepareJob()));
    }

    private List<Job> testGetAllJobs(AbstractConnector connector, List<String> mockData) throws Exception {
        mockAcceptanceServer.prepareResponse(mockData); //intended wrong data
        String uri = String.format("http://localhost:%d", MockAcceptanceServer.SOCKET);
        return connector.getJobs(uri, "", "", new Interrupter());
    }


    @Test
    public void testGetJenkinsJob() throws Exception {
        testGetJob("vivid_kilo_trove_trunk", new JenkinsConnector(),
                Arrays.asList("jenkins/jenkins_ok_job.txt", "jenkins/jenkins_ok_build.txt", "jenkins/jenkins_failed_build.txt"));
    }

    @Test
    public void testGetHudsonJob() throws Exception {
        testGetJob("QualitySpy", new HudsonConnector(),
                Arrays.asList("hudson/hudson_ok_job.txt", "hudson/hudson_failed_build.txt", "hudson/hudson_ok_build.txt", "hudson/hudson_unstable_build.txt"));
    }

    @Test
    public void testFailJenkinsGetJob() throws Exception {
        try {
            testGetJob("vivid_kilo_trove_trunk", new JenkinsConnector(),
                    Arrays.asList("jenkins/jenkins_bad_job.txt", "jenkins/jenkins_ok_build.txt", "jenkins/jenkins_failed_build.txt"));
            fail("Should throw exception");
        } catch (Exception ignored) {

        }
    }

    @Test
    public void testFailHudsonGetJob() throws Exception {
        try {
            testGetJob("QualitySpy", new HudsonConnector(),
                    Arrays.asList("hudson/hudson_bad_job.txt", "hudson/hudson_failed_build.txt", "hudson/hudson_ok_build.txt", "hudson/hudson_unstable_build.txt"));
            fail("Should throw exception");
        } catch (Exception ignored) {

        }
    }



}
