package pl.wroc.pwr.qualityspy.ci;

import org.simpleframework.http.Request;
import org.simpleframework.http.Response;
import org.simpleframework.http.core.Container;
import org.simpleframework.http.core.ContainerServer;
import org.simpleframework.transport.Server;
import org.simpleframework.transport.connect.Connection;
import org.simpleframework.transport.connect.SocketConnection;
import pl.wroc.pwr.qualityspy.ci.tools.Utils;

import java.io.IOException;
import java.io.PrintStream;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.LinkedList;
import java.util.List;

/**
 * SImulates outer server for {@link AcceptanceTestsCI}
 *
 * @date 12.03.2016
 */
public class MockAcceptanceServer implements Container {

    protected Connection testConnection;
    public final static int SOCKET = 8282;
    private LinkedList<String> list = new LinkedList<>();

    public void setUp() throws IOException {
        // set up a local server
        Server testServer = new ContainerServer(this);
        testConnection = new SocketConnection(testServer);
        SocketAddress testSocketAddress = new InetSocketAddress(SOCKET);
        testConnection.connect(testSocketAddress);
    }

    public void tearDown() throws IOException {
        testConnection.close();
    }

    public void prepareResponse(List<String> mockResponsePath) {
        this.list = new LinkedList<>(mockResponsePath);
    }

    @Override
    public void handle(Request request, Response response) {
        try {
            long time = System.currentTimeMillis();

            response.setValue("Content-Type", "application/json");
            response.setValue("Server", "TestServer/1.0 (Simple 4.0)");
            response.setDate("Date", time);
            response.setDate("Last-Modified", time);

            PrintStream body = response.getPrintStream();
            body.print(Utils.readAll(list.removeFirst()));
            body.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
