package pl.wroc.pwr.qualityspy.ci.factory;

import org.easymock.EasyMock;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.util.Assert;
import pl.wroc.pwr.qualityspy.ci.MockAcceptanceServer;
import pl.wroc.pwr.qualityspy.ci.connector.ICIConnector;
import pl.wroc.pwr.qualityspy.common.CIProperties.CIServerType;
import pl.wroc.pwr.qualityspy.common.Properties;
import pl.wroc.pwr.qualityspy.model.Build;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;

import static org.junit.Assert.assertEquals;

public class CIFactoryTest {

    protected Properties mProperties = null;
    protected MockAcceptanceServer mockAcceptanceServer;

    @Before
    public void setUp() throws Exception {
        // set up a local servers
        mockAcceptanceServer = new MockAcceptanceServer();
        mockAcceptanceServer.setUp();

        // Properties mock
        mProperties = EasyMock.createNiceMock(Properties.class);
    }

    @After
    public void tearDown() throws IOException {
        mockAcceptanceServer.tearDown();
    }

    protected void testCreateConnector(CIServerType serverType, Class<? extends ICIConnector> connector) throws Exception {
        EasyMock.expect(mProperties.getCIServerType()).andReturn(serverType);
        EasyMock.replay(mProperties);

        ICIConnector conn = new CIFactory().Create(mProperties);
        Assert.isInstanceOf(connector, conn);
    }

    @Test(expected = UnknownConnectorTypeException.class)
    public void testCreateUnknownConnector() throws Exception {
        EasyMock.expect(mProperties.getCIServerType()).andReturn(CIServerType.UNKNOWN);
        EasyMock.replay(mProperties);
        new CIFactory().Create(mProperties);
    }

    @Test
    public void testGetColor() {
        assertEquals("grey", CIFactory.getColor(new Build(150, LocalDateTime.ofEpochSecond(1356032317000L, 0, ZoneOffset.UTC), "ASDFASDF", 162, 0, 4, 1)));
        assertEquals("yellow", CIFactory.getColor(new Build(150, LocalDateTime.ofEpochSecond(1356032317000L, 0, ZoneOffset.UTC), "UNSTABLE", 162, 0, 4, 1)));
        assertEquals("red", CIFactory.getColor(new Build(150, LocalDateTime.ofEpochSecond(1356032317000L, 0, ZoneOffset.UTC), "FAILURE", 162, 0, 4, 1)));
        assertEquals("blue", CIFactory.getColor(new Build(150, LocalDateTime.ofEpochSecond(1356032317000L, 0, ZoneOffset.UTC), "SUCCESS", 162, 0, 4, 1)));
    }
}
