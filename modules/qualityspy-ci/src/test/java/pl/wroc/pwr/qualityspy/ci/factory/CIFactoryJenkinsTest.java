package pl.wroc.pwr.qualityspy.ci.factory;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import pl.wroc.pwr.qualityspy.ci.connector.JenkinsConnector;
import pl.wroc.pwr.qualityspy.common.CIProperties.CIServerType;

import java.io.IOException;

public class CIFactoryJenkinsTest extends CIFactoryTest {


    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    @After
    public void tearDown() throws IOException {
        super.tearDown();
    }

    @Test
    public void testCreateJenkinsConnector() throws Exception {
        super.testCreateConnector(CIServerType.Jenkins, JenkinsConnector.class);
    }
}
