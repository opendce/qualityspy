package pl.wroc.pwr.qualityspy.ci.connector;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import pl.wroc.pwr.qualityspy.ci.MockAcceptanceServer;
import pl.wroc.pwr.qualityspy.model.Build;
import pl.wroc.pwr.qualityspy.model.Interrupter;
import pl.wroc.pwr.qualityspy.model.Job;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @author Jacek Burzyński
 */
public class HudsonConnectorTest extends AbstractConnectorTest {

    @Before
    public void setUp() throws IOException {
        super.setUp();
    }

    @After
    public void tearDown() throws IOException {
        super.tearDown();
    }

    /**
     * Test of getJob method, of class HudsonConnector.
     */
    @Test
    public void testGetJob() throws Exception {
        String uri = String.format("http://localhost:%d", MockAcceptanceServer.SOCKET);
        Job jobExpected = prepareJob();
        mockAcceptanceServer.prepareResponse(Arrays.asList("hudson/hudson_ok_job.txt", "hudson/hudson_failed_build.txt", "hudson/hudson_ok_build.txt", "hudson/hudson_unstable_build.txt"));
        Job jobActual = new JenkinsConnector().getJob("QualitySpy", uri, "", "", new Interrupter());
        assertEquals(jobExpected.toString(), jobActual.toString());
    }

    public static Job prepareJob() {
        String uri = String.format("http://localhost:%d", MockAcceptanceServer.SOCKET);
        Job jobExpected = new Job("QualitySpy", String.format("%s/job/QualitySpy/", uri), "red");
        jobExpected.setBuilds(prepareBuilds());
        return jobExpected;
    }

    public static List<Build> prepareBuilds() {
        List<Build> builds = new ArrayList<>();
        builds.add(new Build(175,
                LocalDateTime.ofEpochSecond(1457780161762L, 0, ZoneOffset.UTC),
                "FAILURE",
                125,
                0,
                0,
                0));

        builds.add(new Build(174,
                LocalDateTime.ofEpochSecond(1435649705000L, 0, ZoneOffset.UTC),
                "SUCCESS",
                125,
                1,
                0,
                -1));
        builds.add(new Build(148,
                LocalDateTime.ofEpochSecond(1432751214000L, 0, ZoneOffset.UTC),
                "UNSTABLE",
                124,
                0,
                1,
                0));
        return builds;
    }
    /*
     * Test of getBuilds method, of class HudsonConnector.
     */
    @Test
    public void testGetBuilds() throws Exception {
        mockAcceptanceServer.prepareResponse(Arrays.asList("hudson/hudson_ok_job.txt", "hudson/hudson_failed_build.txt", "hudson/hudson_ok_build.txt", "hudson/hudson_unstable_build.txt"));
        List<Build> buildsActual = new JenkinsConnector().getBuilds(String.format("http://localhost:%d/job/vivid_kilo_trove_trunk/", MockAcceptanceServer.SOCKET), null, null, new Interrupter());
        assertEquals(prepareBuilds().toString(), buildsActual.toString());
    }
}