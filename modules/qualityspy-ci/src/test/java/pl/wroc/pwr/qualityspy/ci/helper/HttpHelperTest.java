package pl.wroc.pwr.qualityspy.ci.helper;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.simpleframework.http.Request;
import org.simpleframework.http.Response;
import org.simpleframework.http.core.Container;
import org.simpleframework.http.core.ContainerServer;
import org.simpleframework.transport.Server;
import org.simpleframework.transport.connect.Connection;
import org.simpleframework.transport.connect.SocketConnection;

import java.io.IOException;
import java.io.PrintStream;
import java.net.InetSocketAddress;
import java.net.SocketAddress;

import static org.junit.Assert.*;

/**
 * Test HttpHelper
 *
 * @author Filip Bieleszuk, Jacek Burzyński
 */
public class HttpHelperTest {

    private static final String TEST_RESPONSE = "Test response";
    private Integer socket = 8282;
    private Connection testConnection;
    private TestContainer testContainer;

    @Before
    /**
     * Sets up local server
     */
    public void setUp() throws IOException {
        testContainer = new TestContainer();
        Server testServer = new ContainerServer(testContainer);
        testConnection = new SocketConnection(testServer);
        SocketAddress testSocketAddress = new InetSocketAddress(socket);

        testConnection.connect(testSocketAddress);
    }

    @After
    public void tearDown() throws IOException {
        testConnection.close();
    }

    /**
     * Test get method
     */
    @Test
    public void testGetMethod() throws Exception {
        final String uri = "http://localhost:" + socket.toString();
        final String username = "asdf";
        final String password = "asdf";

        String result = new HttpHelper(uri).addAuthentication(username, password).get();
        assertEquals(result, TEST_RESPONSE);
        assertTrue(testContainer.isAuthorization());


        result = new HttpHelper(uri).get();
        assertEquals(result, TEST_RESPONSE);
        assertFalse(testContainer.isAuthorization());

    }

    private class TestContainer implements Container {

        private boolean authorization = false;

        public boolean isAuthorization() {
            return authorization;
        }

        public void handle(Request request, Response response) {
            try {
                authorization = request.contains("Authorization");

                PrintStream body = response.getPrintStream();
                body.print(TEST_RESPONSE);
                body.close();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
