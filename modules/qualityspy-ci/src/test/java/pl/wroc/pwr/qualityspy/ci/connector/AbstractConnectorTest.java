package pl.wroc.pwr.qualityspy.ci.connector;

import pl.wroc.pwr.qualityspy.ci.MockAcceptanceServer;

import java.io.IOException;

/**
 * @author Jacek Burzyński
 */
public class AbstractConnectorTest {

    protected MockAcceptanceServer mockAcceptanceServer;

    public void setUp() throws IOException {
        mockAcceptanceServer = new MockAcceptanceServer();
        mockAcceptanceServer.setUp();
    }

    public void tearDown() throws IOException {
        mockAcceptanceServer.tearDown();
    }
}