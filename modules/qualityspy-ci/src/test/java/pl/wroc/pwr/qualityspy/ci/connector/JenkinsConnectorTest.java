package pl.wroc.pwr.qualityspy.ci.connector;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import pl.wroc.pwr.qualityspy.ci.MockAcceptanceServer;
import pl.wroc.pwr.qualityspy.model.Build;
import pl.wroc.pwr.qualityspy.model.Interrupter;
import pl.wroc.pwr.qualityspy.model.Job;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @author Jacek Burzyński
 */
public class JenkinsConnectorTest extends AbstractConnectorTest {

    @Before
    public void setUp() throws IOException {
        super.setUp();
    }

    @After
    public void tearDown() throws IOException {
        super.tearDown();
    }

    /**
     * Test of getJob method, of class JenkinsConnector.
     */
    @Test
    public void testGetJob() throws Exception {
        String uri = String.format("http://localhost:%d", MockAcceptanceServer.SOCKET);
        Job jobExpected = prepareJob();
        mockAcceptanceServer.prepareResponse(Arrays.asList("jenkins/jenkins_ok_job.txt", "jenkins/jenkins_ok_build.txt", "jenkins/jenkins_failed_build.txt"));
        Job jobActual = new JenkinsConnector().getJob("vivid_kilo_trove_trunk", uri, "", "", new Interrupter());
        assertEquals(jobExpected.toString(), jobActual.toString());
    }

    public static Job prepareJob() {
        String uri = String.format("http://localhost:%d", MockAcceptanceServer.SOCKET);
        Job jobExpected = new Job("vivid_kilo_trove_trunk", String.format("%s/job/vivid_kilo_trove_trunk/", uri), "blue");
        jobExpected.setBuilds(prepareBuilds());
        return jobExpected;
    }

    public static List<Build> prepareBuilds() {
        List<Build> builds = new LinkedList<>();
        builds.add(new Build(120,
                LocalDateTime.ofEpochSecond(1429522988000L, 0, ZoneOffset.UTC),
                "SUCCESS",
                0,
                0,
                0,
                0));

        builds.add(new Build(118,
                LocalDateTime.ofEpochSecond(1429414271000L, 0, ZoneOffset.UTC),
                "FAILURE",
                0,
                0,
                0,
                0));
        return builds;
    }

    /*
     * Test of getBuilds method, of class JenkinsConnector.
     */
    @Test
    public void testGetBuilds() throws Exception {
        mockAcceptanceServer.prepareResponse(Arrays.asList("jenkins/jenkins_ok_job.txt", "jenkins/jenkins_ok_build.txt", "jenkins/jenkins_failed_build.txt"));
        List<Build> buildsActual = new JenkinsConnector().getBuilds(String.format("http://localhost:%d/job/vivid_kilo_trove_trunk/", MockAcceptanceServer.SOCKET), null, null, new Interrupter());
        assertEquals(prepareBuilds().toString(), buildsActual.toString());
    }
}