package pl.wroc.pwr.qualityspy.ci.helper;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.Scanner;

/**
 *  Obtains content of endpoint url in string
 */
public class HttpHelper {

    private final Logger LOGGER = Logger.getLogger(HttpHelper.class.getName());
    private final HttpClient mHttpClient;
    private final String mUrl;
    private Header mHeader;

    /**
     * @param url url to connect with
     */
    public HttpHelper(String url) {
        mHttpClient = new DefaultHttpClient();
        this.mUrl = url;
    }

    /**
     * Adds password credentials to request
     * @param login Username for logging (omitted if empty or set to null)
     * @param pass Password for logging (omitted if empty or set to null)
     */
    public HttpHelper addAuthentication(String login, String pass) {
        mHeader = BasicScheme.authenticate(new UsernamePasswordCredentials(login, pass), "UTF-8", false);
        return this;
    }

    /**
     * Get HTTP GET response content as String
     *

     * @return String response content
     */
    public String get() throws IOException {
        HttpGet httpGet = new HttpGet(mUrl);
        if (mHeader != null) {
            httpGet.addHeader(mHeader);
        }
        LOGGER.debug("Executing HTTP GET: " + mUrl);
        HttpResponse httpResponse = mHttpClient.execute(httpGet);
        HttpEntity httpEntity = httpResponse.getEntity();

        Scanner scanner = new Scanner(httpEntity.getContent()).useDelimiter("\\A");
        return scanner.hasNext() ? scanner.next() : "";
    }
}
