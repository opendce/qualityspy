package pl.wroc.pwr.qualityspy.ci.factory;

import pl.wroc.pwr.qualityspy.ci.connector.HudsonConnector;
import pl.wroc.pwr.qualityspy.ci.connector.ICIConnector;
import pl.wroc.pwr.qualityspy.ci.connector.JenkinsConnector;
import pl.wroc.pwr.qualityspy.common.CIProperties.CIServerType;
import pl.wroc.pwr.qualityspy.common.Properties;
import pl.wroc.pwr.qualityspy.model.Build;

/**
 *  Fabryka Connectorów
 */
public class CIFactory {

    public ICIConnector Create(Properties config) throws UnknownConnectorTypeException {
        return createConnector(config.getCIServerType());
    }

    private ICIConnector createConnector(CIServerType serverType) throws UnknownConnectorTypeException {
        switch (serverType) {
            case Hudson:
                // Create new HudsonConnector and fetch data
                return new HudsonConnector();
            case Jenkins:
                // Create new JenkinsConnector and fetch data
                return new JenkinsConnector();
            default:
                throw new UnknownConnectorTypeException();
        }
    }

    public static String getColor(Build lastBuild) {
        if (lastBuild == null) {
            return "grey";
        }
        switch (lastBuild.getResult()) {
            case "UNSTABLE":
                return "yellow";
            case "FAILURE":
                return "red";
            case "SUCCESS":
                return "blue";
            default:
                return "grey";
        }
    }
}
