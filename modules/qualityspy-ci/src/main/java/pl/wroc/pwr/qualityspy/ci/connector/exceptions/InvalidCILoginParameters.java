/*
 * Class corresponds to error with invalid username or password for current
 * CI server.
 */
package pl.wroc.pwr.qualityspy.ci.connector.exceptions;

import java.io.IOException;

/**
 *
 */
public class InvalidCILoginParameters extends IOException {

    public InvalidCILoginParameters() {
        super();
    }

    public InvalidCILoginParameters(String uri, String login, String pass) {
        super(String.format("Invalid login (%s) or pass (%s) for requested URL: %s",
                login, pass, uri));
    }

    @Override
    public String toString() {
        return getMessage();
    }
}
