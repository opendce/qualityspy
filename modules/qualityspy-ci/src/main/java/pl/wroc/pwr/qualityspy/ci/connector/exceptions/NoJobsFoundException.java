package pl.wroc.pwr.qualityspy.ci.connector.exceptions;

/**
 * No jobs found on a given server.
 */
public class NoJobsFoundException extends Exception {

    public NoJobsFoundException(String s) {
        super(s);
    }
}
