package pl.wroc.pwr.qualityspy.ci.connector.exceptions;

/**
 * Given job not found on server.
 */
public class NoJobFoundException extends Exception {

    public NoJobFoundException(String s) {
        super(s);
    }
}
