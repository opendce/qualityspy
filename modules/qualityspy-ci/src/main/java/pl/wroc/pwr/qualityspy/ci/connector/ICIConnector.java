package pl.wroc.pwr.qualityspy.ci.connector;

import pl.wroc.pwr.qualityspy.common.Properties;
import pl.wroc.pwr.qualityspy.model.Build;
import pl.wroc.pwr.qualityspy.model.Interrupter;
import pl.wroc.pwr.qualityspy.model.Job;

import java.util.List;

public interface ICIConnector {

    /**
     * Get job with all builds
     *
     * @return Job Job entity
     */
    Job getJob(Properties config, Interrupter interrupted) throws Exception;
    
    /**
     * Get job with all builds
     *
     * @param jobName Name of a job
     * @return Job Job entity
     */
    Job getJob(String jobName, String serverUri, String login, String pass, Interrupter interrupted) throws Exception;
    
    /**
     * Get all jobs for a given server URI
     *
     * @param uri Server uri
     * @param login Username for logging (omitted if empty or set to null)
     * @param pass Password for logging (omitted if empty or set to null)
     * @return List<Job> List of all jobs
     */
    List<Job> getJobs(String uri, String login, String pass, Interrupter interrupted) throws Exception;

    /**
     * Get all builds for a given job URI
     *
     * @param jobUri Job's URI
     * @param login Username for logging (omitted if empty or set to null)
     * @param pass Password for logging (omitted if empty or set to null)
     * @return List<Build> List of all builds
     */
    List<Build> getBuilds(String jobUri, String login, String pass, Interrupter interrupted) throws Exception;
}
