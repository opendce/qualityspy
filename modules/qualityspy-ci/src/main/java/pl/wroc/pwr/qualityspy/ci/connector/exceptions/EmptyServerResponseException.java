package pl.wroc.pwr.qualityspy.ci.connector.exceptions;

/**
 * Server sent an empty response.
 */
public class EmptyServerResponseException extends Exception {

    public EmptyServerResponseException(String s) {
        super(s);
    }
}
