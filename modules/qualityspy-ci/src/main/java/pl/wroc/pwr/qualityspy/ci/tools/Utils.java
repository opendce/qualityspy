package pl.wroc.pwr.qualityspy.ci.tools;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Zawiera pomocnicze metody.
 */

@Slf4j
public class Utils {

    public static boolean isEmpty(CharSequence text) {
        return text == null || text.length() == 0;
    }

    public static String readAll(String filePath) throws IOException {
        try {
            //noinspection ConstantConditions
            URI uri = Thread.currentThread().getContextClassLoader().getResource(filePath).toURI();
            byte[] encoded = Files.readAllBytes(Paths.get(uri));
            return new String(encoded, StandardCharsets.UTF_8);
        } catch (Exception e) {
            log.warn(e.getMessage(), e);
            return "";
        }
    }
}
