package pl.wroc.pwr.qualityspy.ci.factory;

public class UnknownConnectorTypeException extends Exception {

    public UnknownConnectorTypeException() {
        super("Unknown connector type!");
    }

    @Override
    public String toString() {
        return getMessage();
    }
}
