package pl.wroc.pwr.qualityspy.ci.connector.exceptions;

/**
 * No builds found for a given job.
 */
public class NoBuildsFoundException extends Exception {
    
    public NoBuildsFoundException(String s) {
        super(s);
    }
}
