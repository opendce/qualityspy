package pl.wroc.pwr.qualityspy.ci.connector;

import net.sf.json.JSONArray;
import net.sf.json.JSONException;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import org.apache.log4j.Logger;
import pl.wroc.pwr.qualityspy.ci.connector.exceptions.*;
import pl.wroc.pwr.qualityspy.ci.factory.CIFactory;
import pl.wroc.pwr.qualityspy.ci.helper.HttpHelper;
import pl.wroc.pwr.qualityspy.ci.tools.Utils;
import pl.wroc.pwr.qualityspy.common.Properties;
import pl.wroc.pwr.qualityspy.model.Build;
import pl.wroc.pwr.qualityspy.model.Interrupter;
import pl.wroc.pwr.qualityspy.model.Job;

import java.time.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Abstract Jenkins / Hudson connector class.
 */
public abstract class AbstractConnector implements ICIConnector {

    protected final Logger logger = Logger.getLogger(AbstractConnector.class);

    @Override
    public List<Job> getJobs(String uri, String login, String pass, Interrupter interrupted) throws Exception {
        final String apiUri = String.format("%s/api/json", trimLastSlash(uri));
        HttpHelper helper = new HttpHelper(apiUri);
        if (!Utils.isEmpty(login) && !Utils.isEmpty(pass)) {
            helper.addAuthentication(login, pass);
        }
        final String response = helper.get();
        if (!Utils.isEmpty(response)) {
            if (response.contains("Error 401") || response.contains("HTTP Status 401")) {
                // Error with username or pass for given URL
                throw new InvalidCILoginParameters(apiUri, login, pass);
            } else {
                JSONObject json = (JSONObject) JSONSerializer.toJSON(response);
                if (!json.isEmpty()) {
                    JSONArray jobs = json.getJSONArray("jobs");
                    LinkedList<Job> jobList = new LinkedList<>();
                    for (int i = 0; i < jobs.size() && !interrupted.getIsInterrupted(); i++) {
                        JSONObject currObj = jobs.getJSONObject(i);
                        Job job = new Job(currObj.getString("name"), currObj.getString("url"), currObj.getString("color"));
                        job.setBuilds(getBuilds(job.getUrl(), login, pass, interrupted));
                        jobList.add(job);
                    }
                    return jobList;
                } else {
                    throw new NoJobsFoundException("No jobs found on server.");
                }
            }
        } else {
            throw new EmptyServerResponseException("Server sent an empty response.");
        }
    }


    @Override
    public List<Build> getBuilds(String jobUri, String login, String pass, Interrupter interrupted) throws Exception {
        String jobApiUri = String.format("%s/api/json", trimLastSlash(jobUri));
        HttpHelper helper = new HttpHelper(jobApiUri);
        if (!Utils.isEmpty(login) && !Utils.isEmpty(pass)) {
            helper.addAuthentication(login, pass);
        }
        String response = helper.get();

        if (Utils.isEmpty(response)) {
            throw new EmptyServerResponseException("Server sent an empty response.");
        }
        if (response.contains("Error 401") || response.contains("HTTP Status 401")) {
            // Error with username or pass for given URL
            throw new InvalidCILoginParameters(jobApiUri, login, pass);
        } else if (response.contains("Error 404") || response.contains("HTTP Status 404")) {
            return null;
        }
        JSONObject jobJson = (JSONObject) JSONSerializer.toJSON(response);
        if (jobJson.isEmpty()) {
            throw new NoJobsFoundException("No jobs found on server.");
        }

        JSONArray buildsJSONArray = jobJson.getJSONArray("builds");
        return getBuildListFromJSONArray(buildsJSONArray, login, pass, interrupted);
    }

    /**
     * Get list of all builds from a job's "builds" JSONArray
     *
     * @param buildsJSONArray Job's "builds" JSONArray
     * @param login           Username for logging (omitted if empty or set to null)
     * @param pass            Password for logging (omitted if empty or set to null)
     * @param interrupted
     * @return List<Build> List of all builds
     */
    protected List<Build> getBuildListFromJSONArray(JSONArray buildsJSONArray, String login, String pass, Interrupter interrupted)
            throws Exception {
        List<Build> buildList = new ArrayList<>();
        String currBuildResponse = null;
        JSONObject currBuild = null;
        for (int i = 0; i < buildsJSONArray.size() && !interrupted.getIsInterrupted(); i++) {
            try {
                currBuild = buildsJSONArray.getJSONObject(i);
                HttpHelper helper = new HttpHelper(currBuild.getString("url") + "api/json");
                if (!Utils.isEmpty(login) && !Utils.isEmpty(pass)) {
                    helper.addAuthentication(login, pass).get();
                }
                currBuildResponse = helper.get();

                JSONObject buildJson = (JSONObject) JSONSerializer.toJSON(currBuildResponse);
                if (!buildJson.isEmpty()) {
                    JSONArray actions = buildJson.getJSONArray("actions");
                    Integer totalCount = null, failCount = null;
                    for (int j = 0; j < actions.size(); j++) {
                        if(interrupted.getIsInterrupted()) interrupted.throwIntrerrupted();
                        JSONObject currObj = actions.getJSONObject(j);
                        if (currObj.containsKey("totalCount") && currObj.containsKey("failCount")) {
                            totalCount = currObj.getInt("totalCount");
                            failCount = currObj.getInt("failCount");
                        }
                    }

                    buildList.add(new Build(buildJson.containsKey("number") ? buildJson.getInt("number") : null,
                            //buildJson.containsKey("timestamp") ? LocalDateTime(buildJson.getLong("timestamp"), 0, ZoneOffset.UTC) : null,
                            buildJson.containsKey("timestamp") ? Instant.ofEpochMilli(buildJson.getLong("timestamp")).atZone(ZoneId.systemDefault()).toLocalDateTime() : null,
                            buildJson.containsKey("result") ? buildJson.getString("result") : null,
                            totalCount == null ? 0 : totalCount,
                            0,
                            failCount == null ? 0 : failCount,
                            0));
                } else {
                    throw new NoBuildsFoundException("No builds found for a given job.");
                }
            } catch (JSONException ex) {
                String url = "";
                if (currBuild != null) {
                    url = currBuild.getString("url");
                }
                logger.error(currBuildResponse = String.format("%s for %s => %s",
                        ex.getMessage(), url, currBuildResponse), ex);
            }
        }

        updateBuildListDiffs(buildList);

        return buildList;
    }

    /*
     * Update total diff and fail diff for a given builds list
     * 
     * @param buildList List of builds to update
     */
    protected void updateBuildListDiffs(List<Build> buildList) {
        for (int buildIndex = 0; buildIndex < buildList.size() - 1; buildIndex++) {
            Build previous = buildList.get(buildIndex);
            Build next = buildList.get(buildIndex + 1);
            if (previous.getTotalCount() != null && next.getTotalCount() != null) {
                previous.setTotalDiff(previous.getTotalCount() - next.getTotalCount());
            }
            if (previous.getFailCount() != null && next.getFailCount() != null) {
                previous.setFailDiff(previous.getFailCount() - next.getFailCount());
            }
        }
    }

    @Override
    public Job getJob(Properties config, Interrupter interrupted) throws Exception {
        Job job = getJob(config.getCIJobName(), config.getCIServerUrl(),
                config.getCIServerLogin(), config.getCIServerPassword(), interrupted);

        if (job == null) {
            throw new NoJobFoundException(String.format("Job '%s' not found on server",
                    config.getCIJobName()));
        }
        return job;
    }

    @Override
    public Job getJob(String jobName, String serverUri, String login, String pass, Interrupter interrupted) throws Exception {
        Job job = new Job();
        job.setName(jobName);
        job.setUrl(String.format("%s/job/%s/", trimLastSlash(serverUri), jobName));
        job.setBuilds(getBuilds(job.getUrl(), login, pass, interrupted));
        job.setColor(CIFactory.getColor(job.getBuilds().isEmpty() ? null : job.getBuilds().get(0)));
        return job;
    }

    protected String trimLastSlash(String string) {
        if (string.endsWith("/")) {
            string = string.substring(0, string.length() - 1);
        }
        return string;
    }
}
