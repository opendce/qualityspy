package pl.wroc.pwr.qualityspy.bugtracker.jira;

/**
 * Przekroczono maksymalny czas oczekiwania na odpowiedź serwera JIRA
 *
 * @author BugTracker Team
 * @version 1.0
 * @since 04.03.2012
 */
public class JiraTimeoutException extends Exception {

    public final static String TimedOutMessage = "Timeout expired. Check your internet connection.";

    public JiraTimeoutException(String s) {
        super(s);
    }
}