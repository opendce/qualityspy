package pl.wroc.pwr.qualityspy.bugtracker.jira;

import lombok.extern.slf4j.Slf4j;
import pl.wroc.pwr.qualityspy.model.IssueDescription;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
public class ParentKeysSetter {
    public void setParentKeysInIssues(final List<IssueDescription> issues) {
        final Map<String, IssueDescription> issuesByKey = issues.stream()
                .collect(Collectors.toMap(IssueDescription::getIssueKey, Function.identity()));

        parentIssues(issues)
                .forEach(issueDescription -> setParentKeysInSubtasks(issueDescription, issuesByKey));
    }

    private void setParentKeysInSubtasks(final IssueDescription issueDescription,
                                         final Map<String, IssueDescription> issueByKey) {
        final String issueDescriptionKey = issueDescription.getIssueKey();
        for (final String subtaskKey : issueDescription.getSubtasks()) {
            final IssueDescription subtask =  issueByKey.get(subtaskKey);
            if (subtask != null) {
                subtask.setParentKey(issueDescriptionKey);
            } else {
                log.warn(String.format("%s declares subtask %s which is not available in the collected set.",
                        issueDescriptionKey, subtaskKey));
            }
        }
    }

    private Stream<IssueDescription> parentIssues(final List<IssueDescription> issues) {
        return issues.stream()
                .filter(IssueDescription::isIsParent);
    }
}
