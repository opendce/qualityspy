package pl.wroc.pwr.qualityspy.bugtracker.jira;

/**
 * Nie znaleziono serwera JIRA pod wskazanym URL
 *
 * @author BugTracker Team
 * @version 1.0
 * @since 04.03.2012
 */
public class JiraServiceNotFoundException extends Exception {

    public final static String ServiceNotFoundMessage =
            "Błąd logowania do systemu Jira: błędny URL. Wpisz adres ponownie.";

    public JiraServiceNotFoundException(String s) {
        super(s);
    }
}
