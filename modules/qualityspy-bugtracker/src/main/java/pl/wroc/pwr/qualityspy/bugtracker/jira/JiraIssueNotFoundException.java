/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wroc.pwr.qualityspy.bugtracker.jira;

/**
 * Zagadnienie nie zostało odnalezione w systemie JIRA
 *
 * @author BugTracker Team
 * @version 1.0
 * @since 04.03.2012
 */
public class JiraIssueNotFoundException extends Exception {

    public JiraIssueNotFoundException(String url, String key) {
        super("Zagadnienie " + key + " nie zostało odnalezione w systemie JIRA - " + url);
    }
}
