/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wroc.pwr.qualityspy.bugtracker.github;

import org.apache.log4j.Logger;
import org.eclipse.egit.github.core.Issue;
import org.eclipse.egit.github.core.Milestone;
import org.eclipse.egit.github.core.Repository;
import org.eclipse.egit.github.core.client.GitHubClient;
import org.eclipse.egit.github.core.service.IssueService;
import org.eclipse.egit.github.core.service.MilestoneService;
import org.eclipse.egit.github.core.service.RepositoryService;
import pl.wroc.pwr.qualityspy.bugtracker.IBugTrackerConnector;
import pl.wroc.pwr.qualityspy.common.BugtrackerProperties;
import pl.wroc.pwr.qualityspy.errorHandling.ErrorWindow;
import pl.wroc.pwr.qualityspy.errorHandling.Errors;
import pl.wroc.pwr.qualityspy.model.Interrupter;
import pl.wroc.pwr.qualityspy.model.IsInterruptedException;
import pl.wroc.pwr.qualityspy.model.IssueDescription;
import pl.wroc.pwr.qualityspy.model.MilestoneDescription;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

/**
 *
 * @author Wielu
 */
public class GithubConnector implements IBugTrackerConnector {

    private static final Logger LOG = Logger.getLogger(GithubConnector.class);
    private final BugtrackerProperties props;
    private IssueService service;
    private MilestoneService milestoneService;
    private Repository repository;
    private final IssueParser parser;
    private Interrupter interrupted;

    public GithubConnector(BugtrackerProperties properties) {
        this(properties, new Interrupter(), new RepositoryService(new GitHubClient()), new IssueService(), new MilestoneService());
    }

    public GithubConnector(BugtrackerProperties properties, Interrupter interrupted) {
        this(properties, interrupted, new RepositoryService(new GitHubClient()), new IssueService(), new MilestoneService());
    }

    protected GithubConnector(BugtrackerProperties properties, Interrupter interrupted, RepositoryService repositoryService, IssueService service, MilestoneService milestoneService) {
        props = properties;
        this.parser = new IssueParser();
        this.interrupted = interrupted;
        this.service = service;
        this.milestoneService = milestoneService;
        final AtomicReference<Boolean> tryAgain = new AtomicReference<>();
        tryAgain.set(Boolean.TRUE);

        while(tryAgain.get()) {
            try {
                repository = repositoryService.getRepository(props.getGithubRepoOwner(), props.getGithubRepo());
                tryAgain.set(Boolean.FALSE);
            } catch (IOException e) {
                LOG.fatal(e);
                ErrorWindow.getInstance().handleError(e, new Errors() {

                    @Override
                    public void tryAgain() {
                        tryAgain.set(Boolean.TRUE);
                    }

                    @Override
                    public void endProccess() {
                        tryAgain.set(Boolean.FALSE);
                    }
                });

            }
        }


    }

    @Override
    public List<IssueDescription> getIssues() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public IssueDescription getIssueDescriptionHistory(IssueDescription issueDescription) throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<MilestoneDescription> getMilestones() throws Exception {
        List<MilestoneDescription> result = new ArrayList<>();
        List<Milestone> milestones;
        milestones = milestoneService.getMilestones(repository, null);
        
        for (Milestone milestone : milestones) {
            if(interrupted.getIsInterrupted()) interrupted.throwIntrerrupted();
            result.add(parser.convertGithubMilestoneToMilestoneDescription(milestone));
        }
        
        return result;
    }

    @Override
    public List<IssueDescription> getIssuesWithFullDescription() throws IOException, IsInterruptedException {
        Map<String, String> issueFilter = props.getGithubFilter();

        List<IssueDescription> result = new ArrayList<>();
        List<Issue> issues = service.getIssues(repository, issueFilter);

        for (Issue issue : issues) {
            if(interrupted.getIsInterrupted()) interrupted.throwIntrerrupted();
            result.add(parser.convertGithubIssueToIssueDescription(issue));
        }

        return result;
    }
}
