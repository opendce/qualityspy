/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wroc.pwr.qualityspy.bugtracker.github;

import org.eclipse.egit.github.core.Issue;
import org.eclipse.egit.github.core.Label;
import org.eclipse.egit.github.core.Milestone;
import pl.wroc.pwr.qualityspy.model.IssueDescription;
import pl.wroc.pwr.qualityspy.model.MilestoneDescription;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Klasa parsuje obiekty klasy SearchIssue pobierane z Githuba na obiekty klas
 * IssueDescription i IssueComment.
 *
 * @author Marcin, Maciek, Arek, Brian
 */
public class IssueParser {

    /**
     * Funkcja konwertująca SearchIssue w IssueDescription.
     *
     * @param issue - obiekt zwracany z Githuba
     * @return Skonwertowany obiekt IssueDescription
     */
    public IssueDescription convertGithubIssueToIssueDescription(Issue issue) {
        IssueDescription issueDescription = new IssueDescription();

        // Assignee
        if (issue.getAssignee() != null) {
            issueDescription.setAssignTo(issue.getAssignee().getLogin());
        }

        // Creation date
        if (issue.getCreatedAt() != null) {
            Date dateCreation = issue.getCreatedAt();
            Calendar calendar = new GregorianCalendar();
            calendar.setTime(dateCreation);
            issueDescription.setCreated(calendar);
        }

        // Update date
        if (issue.getUpdatedAt() != null) {
            Date dateUpdate = issue.getUpdatedAt();
            Calendar calendar = new GregorianCalendar();
            calendar.setTime(dateUpdate);
            issueDescription.setUpdated(calendar);
        }

        // Resolved Date
        if (issue.getClosedAt() != null) {
            Date dateClosed = issue.getClosedAt();
            Calendar calendar = new GregorianCalendar();
            calendar.setTime(dateClosed);
            issueDescription.setResolved(calendar);
        }

        // Description
        if (issue.getBody() != null) {
            issueDescription.setDescription(issue.getBody());
        }

        // ID
        issueDescription.setIssueId(issue.getId());

        // Key
        issueDescription.setIssueKey(String.valueOf(issue.getNumber()));

        // Labels
        if (!issue.getLabels().isEmpty()) {
            String temp = "";
            List<Label> labels = issue.getLabels();
            for (Label label : labels) {
                temp += label.getName() + " ";
            }
            issueDescription.setLabels(temp);
        }

        // Status
        if (issue.getState() != null) {
            issueDescription.setStatus(issue.getState());
        }

        // Summary
        if (issue.getTitle() != null) {
            issueDescription.setSummary(issue.getTitle());
        }

        // Reporter
        if (issue.getUser() != null) {
            issueDescription.setReporter(issue.getUser().getLogin());
        }

        return issueDescription;
    }
    
    

    public MilestoneDescription convertGithubMilestoneToMilestoneDescription(Milestone milestone) {
        MilestoneDescription milestoneDescription = new MilestoneDescription();
        
        if (milestone.getCreator() != null) {
            milestoneDescription.setCreator(milestone.getCreator().getLogin());
        }
        
        if (milestone.getTitle() != null) {
            milestoneDescription.setTitle(milestone.getTitle());
        }
        
        if (milestone.getDescription() != null) {
            milestoneDescription.setDescription(milestone.getDescription());
        }
        
        if (milestone.getState() != null) {
            milestoneDescription.setState(milestone.getState());
        }
        
        if (milestone.getCreatedAt() != null) {
            Date dateCreated = milestone.getCreatedAt();
            Calendar calendar = new GregorianCalendar();
            calendar.setTime(dateCreated);
           // dateCreated.
            milestoneDescription.setCreated(calendar);
        }
        
        if (milestone.getDueOn() != null) {
            Date dateDeadline = milestone.getDueOn();
            Calendar calendar = new GregorianCalendar();
            calendar.setTime(dateDeadline);
            milestoneDescription.setDeadline(calendar);
        }
        return milestoneDescription;
    }
}
