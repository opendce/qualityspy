package pl.wroc.pwr.qualityspy.bugtracker.helper;

import pl.wroc.pwr.qualityspy.bugtracker.CreatedDateException;
import pl.wroc.pwr.qualityspy.model.IssueChange;
import pl.wroc.pwr.qualityspy.model.IssueComment;
import pl.wroc.pwr.qualityspy.model.IssueDescription;
import pl.wroc.pwr.qualityspy.model.IssueLog;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

/**
 *
 */
public class IssueHistoryHelper {

    public IssueDescription getIssueHistoricalState(IssueDescription issDesc, LocalDateTime date) throws CreatedDateException {
        Calendar issueCreated = issDesc.getCreated();
        if (issueCreated.after(date)) {
            throw new CreatedDateException("Wybierz późniejszą datę, zagadnienie " + issDesc.getIssueKey() + " zostało utworzone "
                    + issDesc.getCreated());
        }

        ArrayList<IssueChange> changesAfterDate = getIssueHistoricalChanges(issDesc.getIssueHistory(), date);

        IssueDescription historicalIssue = new IssueDescription(issDesc);

        int changesSize = changesAfterDate.size();
        for (int i = 0; i < changesSize; i++) {
            String what = changesAfterDate.get(i).getWhatChanged();
            String oldV = changesAfterDate.get(i).getOldValue();

            if (what.equals("Attachment")) {
                historicalIssue.setAttachments(oldV);
            } else if (what.equals("Link")) {
                //
            } else if (what.equals("Labels")) {
                historicalIssue.setLabels(oldV);
            } else if (what.equals("Status")) {
                historicalIssue.setStatus(oldV);
            } else if (what.equals("Resolution")) {
                historicalIssue.setResolution(oldV);
            } else if (what.equals("Assignee")) {
                historicalIssue.setAssignTo(oldV);
            } else if (what.equals("Priority")) {
                historicalIssue.setPriority(oldV);
            } else if (what.equals("Issue Type")) {
                historicalIssue.setType(oldV);
            } else if (what.equals("Remote Link")) {
                //
            } else if (what.equals("Fix Version/s")) {
                historicalIssue.setFixVersion(oldV);
            } else if (what.equals("Value")) {
                historicalIssue.setValue(oldV);
            } else if (what.equals("Complexity")) {
                historicalIssue.setComplexity(oldV);
            } else if (what.equals("Description")) {
                historicalIssue.setDescription(oldV);
            } else if (what.equals("Summary")) {
                historicalIssue.setSummary(oldV);
            } else if (what.equals("Project")) {
                historicalIssue.setProject(oldV);
            } else if (what.equals("Key")) {
                historicalIssue.setIssueKey(oldV);
            } else if (what.equals("Component/s")) {
                historicalIssue.setComponent(oldV);
            } else if (what.equals("Affects Version/s")) {
                historicalIssue.setAffectsVersion(oldV);
            } else if (what.equals("Original Estimate")) {
//                historicalIssue.setOriginalEstimate(oldV);
//            } else if (what.equals("Remaining Estimate")) {
//                historicalIssue.setRemainingEstimate(oldV);
//            } else if (what.equals("Time Spent")) {
//                historicalIssue.setTimeSpend(oldV);
            }
        }
        historicalIssue = removeCommentsAfterDate(historicalIssue, date);

        historicalIssue = removeWorklogAfterDate(historicalIssue, date);

        historicalIssue = removeHistoryAfterDate(historicalIssue, date);

        historicalIssue = setNewUpdatedDate(historicalIssue);

        return historicalIssue;
    }

    private ArrayList<IssueChange> getIssueHistoricalChanges(Set<IssueChange> history, LocalDateTime date) {
        ArrayList<IssueChange> tmp = new ArrayList<IssueChange>();
        for (IssueChange change : history) {
            tmp.add(change);
        }

        ArrayList<IssueChange> changes = new ArrayList<IssueChange>();

        for (int i = (tmp.size() - 1); i >= 0; i--) {
            if (tmp.get(i).getDateOfChange().isAfter(date)) {
                changes.add(tmp.get(i));
            }
        }
        return changes;
    }

    private IssueDescription removeCommentsAfterDate(IssueDescription historicalIssue, LocalDateTime date) {
        Set<IssueComment> tmp = historicalIssue.getIssueComments();
        Set<IssueComment> newSet = new LinkedHashSet<IssueComment>();
        for (IssueComment comment : tmp) {
            if (!comment.getDate().isAfter(date)) {
                newSet.add(comment);
            }
        }
        historicalIssue.setIssueComments(newSet);
        return historicalIssue;
    }

    private IssueDescription removeWorklogAfterDate(IssueDescription historicalIssue, LocalDateTime date) {
        Set<IssueLog> tmp = historicalIssue.getIssueLogs();
        Set<IssueLog> newSet = new LinkedHashSet<IssueLog>();
        for (IssueLog log : tmp) {
            if (!log.getDate().isAfter(date)) {
                newSet.add(log);
            }
        }
        historicalIssue.setIssueLogs(newSet);
        return historicalIssue;
    }

    private IssueDescription removeHistoryAfterDate(IssueDescription historicalIssue, LocalDateTime date) {
        Set<IssueChange> tmp = historicalIssue.getIssueHistory();
        Set<IssueChange> newSet = new LinkedHashSet<IssueChange>();
        for (IssueChange change : tmp) {
            if (!change.getDateOfChange().isAfter(date)) {
                newSet.add(change);
            }
        }
        historicalIssue.setIssueHistory(newSet);
        return historicalIssue;
    }

    private IssueDescription setNewUpdatedDate(IssueDescription historicalIssue) {
        LocalDateTime data =
                LocalDateTime.ofInstant(historicalIssue.getCreated().toInstant(), ZoneId.systemDefault());
        for (IssueChange change : historicalIssue.getIssueHistory()) {
            if (change.getDateOfChange().isAfter(data)) {
                data = change.getDateOfChange();
            }
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(Date.from(data.atZone(ZoneId.systemDefault()).toInstant()));
        historicalIssue.setUpdated(calendar);
        return historicalIssue;
    }
}
