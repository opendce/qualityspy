package pl.wroc.pwr.qualityspy.bugtracker.jira;

/**
 * Uwierzytelnienie w systemie JIRA nie powiodło się
 *
 * @author BugTracker Team
 * @version 1.0
 * @since 04.03.2012
 */
public class JiraAuthorizationException extends Exception {

    public final static String NotLoggedInMessage = "Nie zalogowano się do systemu Jira.";
    public final static String IncorrectLoginOrPasswordMessage =
            "Błąd logowania do systemu Jira: nieprawidłowy login lub hasło. Podaj poprawne dane.";
    public final static String LogoutRequiredMessage =
            "Błąd logowania do systemu Jira: należy zakończyć aktualną sesję, aby się ponownie zalogować.";

    public JiraAuthorizationException(String s) {
        super(s);
    }
}
