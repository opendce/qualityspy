package pl.wroc.pwr.qualityspy.bugtracker.jira;

/**
 * Błąd krytyczny komunikacji lub przetwarzania danych z serwera JIRA. Wersja
 * zdalnego serwera JIRA może być niekompatybilna.
 *
 * @author BugTracker Team
 * @version 1.0
 * @since 04.03.2012
 */
public class JiraFatalException extends Exception {

    private final static String fatalErrorMessage =
            "Błąd krytyczny przetwarzania danych z systemu Jira.";

    public JiraFatalException() {
        super(fatalErrorMessage);
    }
    
    public JiraFatalException(String msg) {
        super(msg);
    }
}
