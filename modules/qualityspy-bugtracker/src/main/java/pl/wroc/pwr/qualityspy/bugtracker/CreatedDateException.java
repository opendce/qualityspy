/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wroc.pwr.qualityspy.bugtracker;

/**
 * @author Daniel
 */
public class CreatedDateException extends Exception {

    final String message;

    public CreatedDateException(String string) {
        super(string);
        this.message = string;
    }

}
