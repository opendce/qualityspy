package pl.wroc.pwr.qualityspy.bugtracker.jira;

import com.atlassian.jira.rest.client.api.IssueRestClient.Expandos;
import com.atlassian.jira.rest.client.api.JiraRestClient;
import com.atlassian.jira.rest.client.api.domain.*;
import com.atlassian.jira.rest.client.auth.AnonymousAuthenticationHandler;
import com.atlassian.jira.rest.client.internal.async.AsynchronousJiraRestClientFactory;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import pl.wroc.pwr.qualityspy.common.BugtrackerProperties;
import pl.wroc.pwr.qualityspy.errorHandling.ErrorWindow;
import pl.wroc.pwr.qualityspy.errorHandling.ExtendedErrors;
import pl.wroc.pwr.qualityspy.errorHandling.FetcherWithErrorHandling;
import pl.wroc.pwr.qualityspy.model.Interrupter;
import pl.wroc.pwr.qualityspy.model.IssueChange;
import pl.wroc.pwr.qualityspy.model.IssueDescription;
import pl.wroc.pwr.qualityspy.model.MilestoneDescription;

import java.io.IOException;
import java.net.URI;
import java.time.ZoneId;
import java.util.*;
import java.util.function.Function;

/**
 * watcge
 */
public class JiraConnector implements IJiraConnector {

    private static final Logger LOG = Logger.getLogger(JiraConnector.class);
    protected Interrupter interrupted;
    private BugtrackerProperties props;
    private AsynchronousJiraRestClientFactory factory;
    private JiraRestClient jiraClient;

    private final ParentKeysSetter parentKeysSetter;

    /**
     *
     */
    public JiraConnector(BugtrackerProperties properties, ParentKeysSetter parentKeysSetter,
                         AsynchronousJiraRestClientFactory asynchronousJiraRestClientFactory) throws IOException {
        this.parentKeysSetter = parentKeysSetter;
        props = properties;
        this.interrupted = new Interrupter();
        this.factory = asynchronousJiraRestClientFactory;
    }

    public JiraConnector(BugtrackerProperties properties, Interrupter interrupted, ParentKeysSetter parentKeysSetter,
                         AsynchronousJiraRestClientFactory asynchronousJiraRestClientFactory)
            throws IOException {
        this.parentKeysSetter = parentKeysSetter;
        props = properties;
        this.interrupted = interrupted;
        this.factory = asynchronousJiraRestClientFactory;
    }


    @Override
    public List<MilestoneDescription> getMilestones() throws Exception {
        List<MilestoneDescription> milestones;
        try {
            milestones = Collections.emptyList();
        } catch (Exception ex) {
            LOG.fatal("Unable to fetch the milestones", ex);
            throw ex;
        }
        return milestones;
    }

    /**
     * Pobiera z systemu JIRA historię o zagadnienium przekazanym jako parametr.
     * <p>
     * Przed wywołaniem metody powinno zostac zainicjalizowane polaczenie z JIRA
     * oraz pobrane informacje o zagadnieniu.
     *
     * @param issueDescription zagadnienie, dla którego zostanie pobrana
     *                         historia.
     * @return obiekt IssueDescription zawierający komplet informacji o
     * zagadnieniu.
     * @throws JiraTimeoutException         przekroczono czas oczekiwania na odpowiedź
     *                                      serwera. Domyślna wartość to 60 s. Można ją zmienić za pomocą metody {
     * @throws JiraAuthorizationException   uwierzytelnienie nie powiodło się,
     *                                      ponieważ nie ustanowiono sesji z serwem JIRA. Aby połączyć się z serwerem
     *                                      użyj metody {
     * @throws JiraFatalException           błąd krytyczny. Połączenie z serwerem JIRA nie
     *                                      może zostać ustanowione.
     * @throws JiraServiceNotFoundException nie odnaleziono serwera JIRA pod
     *                                      podanym URL. Jeśli serwer znajduje się pod podanym adresem, to nie jest
     *                                      uruchomiony.
     * @throws JiraIssueNotFoundException   nie znaleziono zagadnienia, dla
     *                                      którego miał zostać pobrany komplet informacji.
     * @see #connectToJira()}.
     */
    @SuppressWarnings("unchecked")
    @Override
    public IssueDescription getIssueDescriptionHistory(IssueDescription issueDescription) throws Exception {
        try {
            LinkedHashSet<IssueChange> issueChanges = new LinkedHashSet<>();
            Expandos[] expandArr = new Expandos[]{Expandos.CHANGELOG};
            List<Expandos> expand = Arrays.asList(expandArr);
            Issue issuePom = jiraClient.getIssueClient().getIssue(issueDescription.getIssueKey(), expand).claim();
            collectSubtasks(issueDescription, issuePom);
            Iterable<ChangelogGroup> changeLogGroupList = issuePom.getChangelog();
            for (ChangelogGroup changelogGroup : changeLogGroupList) {
                if (interrupted.getIsInterrupted()) break;
                BasicUser basicUser = changelogGroup.getAuthor();
                DateTime dateTime = changelogGroup.getCreated();
                if (basicUser != null && dateTime != null) {
                    for (ChangelogItem changelogItem : changelogGroup.getItems()) {
                        IssueChange issueChange = new IssueChange();
                        issueChange.setWhoChanged(basicUser.getDisplayName().toString());
                        issueChange.setDateOfChange(dateTime.toDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime());
                        issueChange.setWhatChanged(changelogItem.getField());
                        String pom = changelogItem.getFromString();
                        if (pom != null) {
                            issueChange.setOldValue(pom);
                        }
                        pom = changelogItem.getToString();
                        if (pom != null) {
                            issueChange.setNewValue(pom);
                        }
                        issueChanges.add(issueChange);
                    }
                }
            }
            issueDescription.setIssueHistory(issueChanges);
        } catch (Exception ex) {
            LOG.fatal("Unable to fetch the history", ex);
            throw ex;
        }
        return issueDescription;
    }

    /**
     * Pobiera id, klucz i podsumowanie wszystkich zagadnień z systemu JIRA
     * spełniających kryteria filtrowania.
     * <p>
     * Przed wywołaniem metody powinno zostac zainicjalizowane polaczenie z JIRA
     *
     * @return zwraca Listę ArrayList obiektów IssueDescription. Każdy obiekt
     * zawiera id zagadnienia, klucz i podsumowanie.
     * @throws JiraTimeoutException         przekroczono czas oczekiwania na odpowiedź
     *                                      serwera. Domyślna wartość to 60 s. Można ją zmienić za pomocą metody {
     * @throws JiraAuthorizationException   uwierzytelnienie nie powiodło się,
     *                                      ponieważ nie ustanowiono sesji z serwem JIRA. Aby połączyć się z serwerem
     *                                      użyj metody {
     * @throws JiraNoIssueFoundException    nie znaleziono zagadnienia o podanym id
     * @throws JiraFatalException           błąd krytyczny. Połączenie z serwerem JIRA nie
     *                                      może zostać ustanowione.
     * @throws JiraServiceNotFoundException nie odnaleziono serwera JIRA pod
     *                                      podanym URL. Jeśli serwer znajduje się pod podanym adresem, to nie jest
     *                                      uruchomiony.
     * @throws IOException                  niepowodzenie odczytania pliku z ustawieniami
     * @see #connectToJira()}.
     */
    @Override
    public List<IssueDescription> getIssues() throws Exception {
        final String jiraFilter = props.getJiraJQLString();
        final int pageSize = 50;
        Optional<SearchResult> searchResult = Optional.of(executeSearch(new SearchParams(jiraFilter, pageSize, 0)));
        final int total = searchResult.get().getTotal();
        final List<IssueDescription> issues = new ArrayList<>(total);
        LOG.info("Identified issues: " + total);

        final FetcherWithErrorHandling<BasicIssue, IssueDescription> issueDescriptionHandler = new FetcherWithErrorHandling<>();
        final Function<BasicIssue, IssueDescription> issueDescriptionFetcher = this::collectIssue;

        final FetcherWithErrorHandling<SearchParams, SearchResult> searchResultHandler = new FetcherWithErrorHandling<>();
        final Function<SearchParams, SearchResult> searchResultFetcher = this::executeSearch;

        int i = 0;
        while (i < total) {
            for (BasicIssue basicIssue : searchResult.get().getIssues()) {
                i++;
                final Optional<IssueDescription> result = issueDescriptionHandler.fetchWithErrorHandling(issueDescriptionFetcher, basicIssue);
                if (result.isPresent()) {
                    issues.add(result.get());
                    LOG.info("Collected " + i + "(th) of " + total + " issues.");
                }
            }
            if (i < total) {
                do {
                    searchResult = searchResultHandler.fetchWithErrorHandling(searchResultFetcher, new SearchParams(jiraFilter, pageSize, i));
                    if (!searchResult.isPresent()) {
                        LOG.info("Skipped " + i + "(th) of " + total + " issues.");
                        i++;
                    }
                } while (i < total && !searchResult.isPresent());
            }
        }
        return issues;
    }

    private void collectSubtasks(IssueDescription issueDescription, Issue issuePom) {
        if (issuePom.getSubtasks() != null) {
            for (Subtask subtask : issuePom.getSubtasks()) {
                issueDescription.addSubtask(subtask.getIssueKey());
            }
            if (!issueDescription.getSubtasks().isEmpty()) {
                issueDescription.setIsParent(true);
            }
        }
    }

    private IssueDescription collectIssue(BasicIssue basicIssue) {
        if (interrupted.getIsInterrupted()) {
            interrupted.throwIntrerrupted();
        }
        Issue issue = jiraClient.getIssueClient().getIssue(basicIssue.getKey().toString()).claim();
        return new IssueDescriptionBuilder(issue).buildIssueDescription();
    }

    private SearchResult executeSearch(SearchParams params) {
        return jiraClient
                .getSearchClient()
                .searchJql(params.jql, params.maxResults, params.startAt, null)
                .claim();
    }

    /**
     * Pobiera komplet informacji z JIRA. Inicjalizuje, a nastepnie zamyka
     * polazenie z JIRA.
     */
    @Override
    public List<IssueDescription> getIssuesWithFullDescription() throws Exception {
        connectToJira();
        List<IssueDescription> issues;
        issues = getIssues();

        final int size = issues.size();
        // iterator zriobiony za pomocą finalnej ArrayListy ponieważ w klasie wewnętrznej możemy operować tylko
        // na zmiennych finalnych
        final ArrayList<Integer> it = new ArrayList<>();
        // ustawiamy pierwszy element na 0 (nasz iterator)
        it.add(0);

        while (it.get(0) < size) {
            if (interrupted.getIsInterrupted())
                interrupted.throwIntrerrupted();
            IssueDescription issueDescription = issues.get(it.get(0));
            try {
                LOG.info("Fetching details of " + it.get(0) + " of " + size + " issues...");
                getIssueDescriptionHistory(issueDescription);
                int tempIndex = it.get(0);
                it.set(0, ++tempIndex);
            } catch (final Exception ex) {
                ErrorWindow.getInstance().handleError(ex, new ExtendedErrors() {

                    // skip działa poprzez powiększenie iteratora
                    @Override
                    public void skip() {
                        System.out.print("skip");
                        int tempIndex = it.get(0);
                        it.set(0, ++tempIndex);
                    }

                    // skip all działa poprzez ustawienie iteratora na wielkosc przeglądanej listy
                    @Override
                    public void skipAll() {
                        System.out.print("skipall");
                        it.set(0, size);
                    }

                    //pomijamy powiększenie licznika przez co zostanie wykonane powtórzenie dla danego elementu
                    @Override
                    public void tryAgain() {
                        System.out.print("tryagain");
                        //nothing to do here
                    }

                    // ustawiamy licznik na Size + 1 które będzie oznaczać przerwanie procesu
                    @Override
                    public void endProccess() {
                        System.out.print("end");
                        it.set(0, size + 1);
                    }
                });
            }

        }
        LOG.info("The data was collected.");

        // sprawdzamy czy proces został przerwany. Jeśli tak zwracamy pustą listę
        if (it.get(0) == size + 1) {
            return new ArrayList<>();
        }
        parentKeysSetter.setParentKeysInIssues(issues);
        return issues;
    }

    private boolean noLoginAndPassword() {
        boolean isPass = props.getJiraPassword() != null && props.getJiraPassword().length() > 0;
        boolean isLogin = props.getJiraLogin() != null && props.getJiraLogin().length() > 0;
        return !isPass && !isLogin;

    }

    /**
     * Ustanawia połączenie z serwerem JIRA, wykorzystując wprowadzone
     * poświadczenia.
     *
     * @throws JiraTimeoutException         przekroczono czas oczekiwania na odpowiedź
     *                                      serwera. Domyślna wartość to 60 s. Można ją zmienić za pomocą metody {
     * @throws JiraAuthorizationException   uwierzytelnienie nie powiodło się,
     *                                      ponieważ: <ul> <li>wprowadzono niepoprawny login lub hasło,</li> <li>trwa
     *                                      bieżąca sesja.</li></ul>
     * @throws JiraServiceNotFoundException nie odnaleziono serwera JIRA pod
     *                                      podanym URL. Jeśli serwer znajduje się pod podanym adresem, to nie jest
     *                                      uruchomiony.
     * @throws JiraFatalException           błąd krytyczny. Połączenie z serwerem JIRA nie
     *                                      może zostać ustanowione.
     */
    void connectToJira() throws Exception {
        try {
            URI jiraServerUri = new URI(props.getJiraUrl());
            if (noLoginAndPassword()) {
                jiraClient = factory.create(jiraServerUri, new AnonymousAuthenticationHandler());
            } else {
                jiraClient = factory.createWithBasicHttpAuthentication(jiraServerUri, props.getJiraLogin(), props.getJiraPassword());
            }
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
            throw ex;
        }
    }

}
