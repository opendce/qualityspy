/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wroc.pwr.qualityspy.bugtracker.helper;

import org.apache.log4j.Logger;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
/**
 */
public class DateHelper {

    private static final Logger LOG = Logger.getLogger(DateHelper.class);
    private final String jbossFormatHourAndMinuts = "(\\d+(\\.\\d+)?h\\s\\d+(\\.\\d+)?m)";
    private final String jbossFormatHours = "(\\d+(\\.\\d+)?h)";
    private final String jbossFormatMinutes = "(\\d+(\\.\\d+)?m)";

    /**
     * Dekoduje datę podaną w polach 'created' i 'updated' opisu zagadnienia.
     *
     * @param timestamp stempel czasowy w formacie: Sun, 26 Feb 2012 08:43:59
     * -0600
     * @return obiekt Calendar przechowujący datę i czas. Metoda toString()
     * obiektu zwraca łańcuch w postaci DD-MM-YYYY HH:MM:SS.
     */
    public Calendar decodeTimeStampInDescription(String timestamp) {
        String[] updatedFields = timestamp.split(" ");

        String[] months = {
            "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
        };
        for (int i = 0; i < 12; i++) {
            if (updatedFields[2].compareToIgnoreCase(months[i]) == 0) {
                updatedFields[2] = Integer.toString(i);
            }
        }

        String[] timeFields = updatedFields[4].split(":");
        Calendar stamp = new GregorianCalendar();
        stamp.set(Integer.parseInt(updatedFields[3]), Integer.parseInt(updatedFields[2]), Integer.parseInt(updatedFields[1]),
                Integer.parseInt(timeFields[0]), Integer.parseInt(timeFields[1]), Integer.parseInt(timeFields[2]));

        return stamp;
    }

    /**
     * Dekoduje datę podaną w polach 'created', 'updated' i 'resolved' opisu
     * zagadnienia.
     *
     * @param timestamp stempel czasowy w formacie: 2013-06-25T23:46:22.282
     * -0700
     * @return obiekt Calendar przechowujący datę i czas. Metoda toString()
     * obiektu zwraca łańcuch w postaci DD-MM-YYYY HH:MM:SS.
     */
    public Calendar decodeTimeStampRest(String timestamp) {
        String[] dateFields = timestamp.split("T");
        int i = 0;
        if (dateFields.length == 2) {
            String[] timeFields = dateFields[1].split(":");
            dateFields = dateFields[0].split("-");
            if (dateFields.length == 3 && timeFields.length == 3) {
                String[] pom = timeFields[2].split("\\.");
                if (pom.length == 2) {
                    timeFields[2] = pom[0];
                    Calendar stamp = new GregorianCalendar();
                    stamp.set(Integer.parseInt(dateFields[0]), Integer.parseInt(dateFields[1])-1, Integer.parseInt(dateFields[2]),
                            Integer.parseInt(timeFields[0]), Integer.parseInt(timeFields[1]), Integer.parseInt(timeFields[2]));
                    return stamp;
                }
            }
        }
        return null;
    }

    /**
     * Dekoduje datę podaną w opisie zmiany zagadnienia (dla pobierania historii
     * zagadnienia).
     *
     * @param timestamp stempel czasowy w formacie: 12/Sep/11 11:03 PM
     */
    public Date decodeTimeStampInChange(String timestamp) {
        LOG.info("Timestamp: " + timestamp);
        Date date = null;
        try {
            date = new SimpleDateFormat("dd/MMM/yy hh:mm a", Locale.ENGLISH).parse(timestamp);
        } catch (ParseException ex) {
            LOG.fatal(null, ex);
        }
        return date;
    }

    /**
     * E.g. 27/Aug/12 22:33
     */
    protected Calendar decodeTimestamInSimpleFormat(String timestamp) {
        DateFormat formatter = new SimpleDateFormat("dd/MMM/yy HH:mm", Locale.US);
        Date date;
        try {
            date = formatter.parse(timestamp);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            return calendar;
        } catch (ParseException ex) {
            //LOG.info(ex.getMessage(),ex);
            return null;
        }
    }

    public long convertJiraTimePeriodToSeconds(String period) {
        period = period.toLowerCase();

        if (period.matches(jbossFormatHourAndMinuts + "|" + jbossFormatHours + "|" + jbossFormatMinutes)) {
            return handleJBossTimePeriodFormat(period);
        }

        String[] timeUnits = period.split(",");
        if (timeUnits == null || timeUnits.length == 0) {
            return 0;
        }
        long result = 0L;
        for (String unit : timeUnits) {
            long numberOfUnits = extractNumber(unit.trim());
            if (unit.contains("week")) {
                result += numberOfUnits * 3600 * 8 * 5;
            } else if (unit.contains("day")) {
                result += numberOfUnits * 3600 * 8;
            } else if (unit.contains("hour")) {
                result += numberOfUnits * 3600;
            } else if (unit.contains("minute")) {
                result += numberOfUnits * 60;
            }
        }
        return result;
    }

    private long extractNumber(String period) {
        int i = 0;
        for (; i < period.length(); i++) {
            if (!Character.isDigit(period.charAt(i))) {
                break;
            }
        }

        if (i <= 0) { //no number means one unit 
            return 1L;
        }

        String number = period.substring(0, i);
        return Long.parseLong(number);
    }

    private long handleJBossTimePeriodFormat(String period) {
        if (period.matches(jbossFormatMinutes)) {
            return readTime(period.split("m")[0], 60);
        } else if (period.matches(jbossFormatHours)) {
            return readTime(period.split("h")[0], 3600);
        } else {
            String[] time = period.split("h ");
            long hours = readTime(time[0], 3600);
            long minutes = readTime(time[1].split("m")[0], 60);
            return hours + minutes;
        }
    }

    private long readTime(String number, int numOfSeconds) {
        double time = Double.parseDouble(number);
        return (long) (time * numOfSeconds);
    }
}
