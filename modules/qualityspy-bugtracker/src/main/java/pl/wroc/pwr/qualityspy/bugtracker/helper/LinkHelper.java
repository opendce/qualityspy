/**
 *
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
package pl.wroc.pwr.qualityspy.bugtracker.helper;

import org.apache.log4j.Logger;
import pl.wroc.pwr.qualityspy.common.BugtrackerProperties;

import java.net.MalformedURLException;
import java.net.URL;

/**
 *
 */
public class LinkHelper {

    private static final Logger LOG = Logger.getLogger(LinkHelper.class);
    /**
     * Removes the part from path that is overlapped in root.
     */
    public static String removeOverlapingPart(String root, String path) {
        if (path.startsWith(root)) {
            return path.substring(root.length());
        }
        int pathIndex = 1;
        if (root.contains("://")) {
            pathIndex += 2;
        }
        String parts[] = root.split("/");
        if (parts != null && parts.length > pathIndex) {
            String prefix = "";
            for (int i = pathIndex; i < parts.length; i++) {
                prefix += "/" + parts[i];
            }
            if (!path.startsWith("/")) {
                path = "/" + path;
            }
            if (path.startsWith(prefix)) {
                return path.replaceFirst(prefix, "");
            }
        }
        return path;
    }

    public static String getParamValue(String url, String param) {
        String query = getQueryFromUrl(url);
        return getParamValueFromQuery(query, param);

    }

    private static String getQueryFromUrl(String url) {
        String query;
        try {
            query = new URL(url).getQuery();
        } catch (MalformedURLException ex) {
            LOG.error(null, ex);
            if (url.contains("?")) {
                url = url.split("\\?")[1];
            }
            query = url;
        }
        return query;
    }

    private static String getParamValueFromQuery(String query, String param) {
        String[] params = query.split("&");
        for (String p : params) {
            String[] splitted = p.split("=");
            if (splitted.length == 2) {
                String name = splitted[0];
                String value = splitted[1];
                if (param.equalsIgnoreCase(name)) {
                    return value;
                }
            }
        }
        return "";
    }

    public static int getParamValueAsInt(String url, String param) {
        String value = getParamValue(url, param);
        return Integer.parseInt(value);
    }

    /**
     * @return url with the param equal to value
     */
    public String setParamValue(String url, String param, String value) {
        if (!url.contains(param)) {
            LOG.error("The param " + param + "is not available in the given url: " + url);
            return null; //TODO: add param
        }
        if (!param.endsWith("=")) {
            param += "=";
        }
        int paramStartIndex = url.indexOf(param);
        int paramEndIndex = url.indexOf("&", paramStartIndex);
        String changedUrl = url.substring(0, paramStartIndex) + param + value + url.substring(paramEndIndex);
        return changedUrl;
    }

    /**
     * Ustawia kryterium filtrowania zagadnień.
     *
     * @param bugProps obiekt z parametrami filtrowania.
     */
    public String setIssuesFilterForJiraRest(BugtrackerProperties bugProps) {
        String typeFilter = bugProps.getJiraIssuesType();
        String priorityFilter = bugProps.getJiraIssuesPriority();
        String resolutionFilter = bugProps.getJiraIssuesResolution();
        String statusFilter = bugProps.getJiraIssuesStatus();
        String reporterFilter = bugProps.getJiraIssuesReporter();
        String assigneeFilter = bugProps.getJiraIssuesAssignee();
        String projectFilter = bugProps.getJiraIssuesProject();
        String createdBeforeFilter = bugProps.getJiraIssuesCreatedBefore();
        String createdAfterFilter = bugProps.getJiraIssuesCreatedAfter();
        String updatedBeforeFilter = bugProps.getJiraIssuesUpdatedBefore();
        String updatedAfterFilter = bugProps.getJiraIssuesUpdatedAfter();
        String resolvedBeforeFilter = bugProps.getJiraIssuesResolvedBefore();
        String resolvedAfterFilter = bugProps.getJiraIssuesResolvedAfter();
        String versionFilter = bugProps.getJiraIssuesVersion();
        String searchTextFilter = bugProps.getJiraIssuesSearchText();

        String typeInfo = "", priorityInfo = "", resolutionInfo = "", statusInfo = "",
                reporterInfo = "", assigneeInfo = "", projectInfo = "", createdBeforeInfo = "",
                createdAfterInfo = "", updatedBeforeInfo = "", updatedAfterInfo = "",
                resolvedBeforeInfo = "", resolvedAfterInfo = "", versionInfo = "",
                searchTextInfo = "";

        boolean isFiltered = false;
        String and = " AND ";
        String filter = "";
        if (typeFilter != null && !(typeFilter.compareTo("") == 0)) {
            filter += "issuetype=\"" + typeFilter + "\"" + and;
            typeInfo = "\n\t- typ: " + typeFilter;
            isFiltered = true;
        }

        if (priorityFilter != null && !(priorityFilter.compareTo("") == 0)) {
            filter += "priority=\"" + priorityFilter + "\"" + and;
            priorityInfo = "\n\t- priorytet: " + priorityFilter;
            isFiltered = true;
        }

        if (resolutionFilter != null && !(resolutionFilter.compareTo("") == 0)) {
            filter += "resolution=\"" + resolutionFilter + "\"" + and;
            resolutionInfo = "\n\t- analiza: " + resolutionFilter;
            isFiltered = true;
        }

        if (statusFilter != null && !(statusFilter.compareTo("") == 0)) {
            filter += "status=\"" + statusFilter + "\"" + and;
            statusInfo = "\n\t- status: " + statusFilter;
            isFiltered = true;
        }

        if (reporterFilter != null && !(reporterFilter.compareTo("") == 0)) {
            filter += "reporter=\"" + reporterFilter + "\"" + and;
            reporterInfo = "\n\t- reporter: " + reporterFilter;
            isFiltered = true;
        }

        if (assigneeFilter != null && !(assigneeFilter.compareTo("") == 0)) {
            filter += "assignee=\"" + assigneeFilter + "\"" + and;
            assigneeInfo = "\n\t- przypisane do: " + assigneeFilter;
            isFiltered = true;
        }

        if (projectFilter != null && !(projectFilter.compareTo("") == 0)) {
            filter += "project = " + projectFilter + "" + and;
            projectInfo = "\n\t- projekt: " + projectFilter;
            isFiltered = true;
        }

        if (createdBeforeFilter != null && !(createdBeforeFilter.compareTo("") == 0)) {
            filter += "created<\"" + createdBeforeFilter + "\"" + and;
            createdBeforeInfo = "\n\t- stworzone przed: " + createdBeforeFilter;
            isFiltered = true;
        }

        if (createdAfterFilter != null && !(createdAfterFilter.compareTo("") == 0)) {
            filter += "created>\"" + createdAfterFilter + "\"" + and;
            createdAfterInfo = "\n\t- stworzone po: " + createdAfterFilter;
            isFiltered = true;
        }

        if (updatedBeforeFilter != null && !(updatedBeforeFilter.compareTo("") == 0)) {
            filter += "updated<\"" + updatedBeforeFilter + "\"" + and;
            updatedBeforeInfo = "\n\t- zaktualizowane przed: " + updatedBeforeFilter;
            isFiltered = true;
        }

        if (updatedAfterFilter != null && !(updatedAfterFilter.compareTo("") == 0)) {
            filter += "updated>\"" + updatedAfterFilter + "\"" + and;
            updatedAfterInfo = "\n\t- zaktualizowane po: " + updatedAfterFilter;
            isFiltered = true;
        }

        if (resolvedBeforeFilter != null && !(resolvedBeforeFilter.compareTo("") == 0)) {
            filter += "resolved<\"" + resolvedBeforeFilter + "\"" + and;
            resolvedBeforeInfo = "\n\t- rozwiazane przed: " + updatedBeforeFilter;
            isFiltered = true;
        }

        if (resolvedAfterFilter != null && !(resolvedAfterFilter.compareTo("") == 0)) {
            filter += "resolved>\"" + resolvedAfterFilter + "\"" + and;
            resolvedAfterInfo = "\n\t- rozwiazane po: " + resolvedAfterFilter;
            isFiltered = true;
        }

        if (versionFilter != null && !(versionFilter.compareTo("") == 0)) {
            if (projectFilter == null || projectFilter.compareTo("") == 0) {
                LOG.error("Pobieranie danych najprawdopodobniej sie nie powiedzie: filtrowanie "
                        + "zagadnien wg wersji wymaga okreslenia nazwy projektu.");
            }

            String escapedVersionFilter = "";
            for (int i = 0; i < versionFilter.length(); i++) {
                escapedVersionFilter += "\\u" + Integer.toHexString(versionFilter.charAt(i) | 0x10000).substring(1);
            }

            filter += "fixVersion=\"" + escapedVersionFilter + "\"" + and;
            versionInfo = "\n\t- w wersji: " + versionFilter;
            isFiltered = true;
        }

        if (searchTextFilter != null && !(searchTextFilter.compareTo("") == 0)) {
            filter += "(summary+~+\"" + searchTextFilter + "\"+OR+description+~+\""
                    + searchTextFilter + "\")" + and;
            searchTextInfo = "\n\t- zawiera tekst: " + searchTextFilter;
            isFiltered = true;
        }

        if (filter.length() > and.length()) {
            filter = filter.substring(0, filter.length() - and.length());
        }

        if (isFiltered) {
            LOG.info("Określono filtr wyszukiwania zagadnien:"
                    + typeInfo + priorityInfo + resolutionInfo + statusInfo
                    + reporterInfo + assigneeInfo + projectInfo + createdBeforeInfo
                    + createdAfterInfo + updatedBeforeInfo + updatedAfterInfo + versionInfo 
                    + searchTextInfo + resolvedAfterInfo + resolvedBeforeInfo);
        }
        return filter;
    }
}
