package pl.wroc.pwr.qualityspy.bugtracker.jira;

import com.atlassian.jira.rest.client.api.domain.*;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import pl.wroc.pwr.qualityspy.bugtracker.helper.DateHelper;
import pl.wroc.pwr.qualityspy.model.IssueAttachment;
import pl.wroc.pwr.qualityspy.model.IssueComment;
import pl.wroc.pwr.qualityspy.model.IssueDescription;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@Slf4j
public class IssueDescriptionBuilder {
    private final Issue issue;

    public IssueDescriptionBuilder(Issue issue) {
        this.issue = issue;
    }

    /**
     * Konwertuje obiekt Issue do obiektu IssueDescription
     *
     * @return obiekt IssueDescription.
     * @throws JiraTimeoutException przekroczono czas oczekiwania na odpowiedź
     *                              serwera. Domyślna wartość to 60 s. Można ją zmienić za pomocą metody {@see #setTimeout(int)}.
     * @throws JiraFatalException   błąd krytyczny. Nie można odczytać danych
     *                              zagadnienia
     *                              TODO: method too long.
     */
    public IssueDescription buildIssueDescription() {
        final IssueDescription issueDescription = new IssueDescription();
        String currentElement = "";
        try {
            currentElement = "AffectsVersion";
            final String affectedVersionsString =
                    iterableToString(issue.getAffectedVersions(), version -> version.getName(), ", ");
            issueDescription.setAffectsVersion(affectedVersionsString);

            currentElement = "AssignTo";
            final String assignToString = Optional.ofNullable(issue.getAssignee())
                    .map(basicUser -> basicUser.getDisplayName()).orElse("");
            issueDescription.setAssignTo(assignToString);


            currentElement = "Attachments";
            final Set<IssueAttachment> issueAttachments = getIssueAttachments(issue);
            issueDescription.setIssueAttachments(issueAttachments);


            currentElement = "Complexity";
            final String complexityFieldString = Optional.ofNullable(issue.getFieldByName("Complexity"))
                    .map(complexityField -> complexityField.getValue().toString())
                    .orElse("");
            issueDescription.setComplexity(complexityFieldString);


            currentElement = "Component";
            final String componentsString =
                    iterableToString(issue.getComponents(), basicComponent -> basicComponent.getName(), ", ");
            issueDescription.setComponent(componentsString);


            currentElement = "CreationDate";
            final DateTime creationDate = issue.getCreationDate();
            if (creationDate != null) {
                issueDescription.setCreated(creationDate.toGregorianCalendar());
            }


            currentElement = "Description";
            final String descriptionString = Optional.ofNullable(issue.getDescription()).orElse("");
            issueDescription.setDescription(descriptionString);


            currentElement = "Environment";
            final String environmentString = Optional.ofNullable(issue.getFieldByName("Environment"))
                    .flatMap(field -> Optional.ofNullable(field.getValue()).map(value -> value.toString()))
                    .orElse("");
            issueDescription.setEnvironment(environmentString);


            currentElement = "FixVersion";
            final String fixVersionsString =
                    iterableToString(issue.getFixVersions(), version -> version.getName(), ", ");
            issueDescription.setFixVersion(fixVersionsString);


            currentElement = "IssueKey";
            issueDescription.setIssueKey(issue.getKey());


            currentElement = "IssueID";
            final String[] uriParts = issue.getSelf().toString().split("/");
            final String issueIDString = uriParts[uriParts.length - 1];
            issueDescription.setIssueId(Long.parseLong(issueIDString));


            currentElement = "Labels";
            final String labelsString = issue.getLabels().toString().substring(1);
            issueDescription.setLabels(labelsString);


            currentElement = "Reporter";
            final String reporterString = Optional.ofNullable(issue.getReporter())
                    .flatMap(reporter -> Optional.ofNullable(reporter.getDisplayName()))
                    .orElse("");
            issueDescription.setReporter(reporterString);


            currentElement = "Watchers";
            final String watchersNumberString = Optional.ofNullable(issue.getWatchers())
                    .map(watchers -> watchers.getNumWatchers())
                    .map(number -> number > 0 ? Integer.toString(number) : "No watchers for this issue")
                    .orElse("");
            issueDescription.setWatchers(watchersNumberString);


            currentElement = "Priority";
            final BasicPriority basicPriority = issue.getPriority();
            if (basicPriority != null) {
                issueDescription.setPriority(basicPriority.getName());
            }


            currentElement = "Project";
            final BasicProject basicProject = issue.getProject();
            if (basicProject != null) {
                issueDescription.setProject(basicProject.getName());
            }


            currentElement = "Resolution";
            final String resolutionString = Optional.ofNullable(issue.getResolution())
                    .map(basicResolution -> basicResolution.getName())
                    .orElse("Unresolved");
            issueDescription.setResolution(resolutionString);


            currentElement = "Status";
            final String statusString = Optional.ofNullable(issue.getStatus())
                    .map(status -> status.getName())
                    .orElse("");
            issueDescription.setStatus(statusString);


            currentElement = "Summary";
            final String summary = issue.getSummary();
            if (summary != null) {
                issueDescription.setSummary(summary);
            }


            currentElement = "TimeSpend";
            final TimeTracking timeTracking = issue.getTimeTracking();
            if (timeTracking != null && timeTracking.getTimeSpentMinutes() != null) {
                issueDescription.setTimeSpend(timeTracking.getTimeSpentMinutes());
            }


            currentElement = "Type";
            final IssueType basicIssue = issue.getIssueType();
            if (basicIssue != null) {
                issueDescription.setType(basicIssue.getName());
            }


            currentElement = "Updated";
            final DateTime updateDate = issue.getUpdateDate();
            if (updateDate != null) {
                issueDescription.setUpdated(updateDate.toGregorianCalendar());
            }


            currentElement = "Comments";
            final Set<IssueComment> issueComments = getIssueComments(issue);
            issueDescription.setIssueComments(issueComments);


            currentElement = "Resolved";
            final Calendar resolvedCalendar = getCalendar(issue);
            issueDescription.setResolved(resolvedCalendar);
        } catch (Exception e) {
            log.error(String.format("Unable to convert issue to issueDescription: %s", currentElement), e);
            throw e;
        }
        return issueDescription;
    }

    private Calendar getCalendar(Issue issue) {
        return Optional.ofNullable(issue.getFieldByName("Resolved"))
                .flatMap(resolved -> Optional.ofNullable(resolved.getValue()))
                .flatMap(value -> Optional.ofNullable(new DateHelper().decodeTimeStampRest(value.toString())))
                .orElse(null);
    }

    private Set<IssueComment> getIssueComments(Issue issue) {
        return Optional.ofNullable(issue.getComments())
                .map(comments -> {
                    final Stream<Comment> knownAuthorComments =
                            iterableToStream(comments).filter(comment -> comment.getAuthor() != null);
                    final Stream<IssueComment> issueCommentsStream =
                            knownAuthorComments.map(comment -> {
                                final String author = comment.getAuthor().getDisplayName();
                                final Date commentCreationDate = comment.getCreationDate().toDate();
                                return new IssueComment(author,
                                        LocalDateTime.ofInstant(commentCreationDate.toInstant(), ZoneId.systemDefault()), comment.getBody());
                            });
                    return new LinkedHashSet<>(issueCommentsStream.collect(Collectors.toList()));

                })
                .orElse(new LinkedHashSet<>(Collections.emptyList()));
    }



    private Set<IssueAttachment> getIssueAttachments(Issue issue) {

        return Optional.ofNullable(issue.getAttachments())
                .map(attachments -> {
                    final Stream<Attachment> byMimeTypes = iterableToStream(attachments);
                    final Stream<IssueAttachment> issueAttachmentsStream =
                            byMimeTypes.map(attachment -> {
                                final String mimeType = attachment.getMimeType();
                                final String fileName = attachment.getFilename();
                                final int size = attachment.getSize();
                                final LocalDateTime creationDate = LocalDateTime.of(attachment.getCreationDate().getYear(),attachment.getCreationDate().getMonthOfYear(),attachment.getCreationDate().getDayOfMonth(),attachment.getCreationDate().getHourOfDay(),attachment.getCreationDate().getMinuteOfHour(),attachment.getCreationDate().getSecondOfMinute());
                                return new IssueAttachment(mimeType, fileName, size, creationDate);
                            });
                    return new LinkedHashSet<>(issueAttachmentsStream.collect(Collectors.toSet()));

                })
                .orElse(new LinkedHashSet<>(Collections.emptyList()));
    }

    private <T> Stream<T> iterableToStream(Iterable<T> iterable) {
        return StreamSupport.stream(iterable.spliterator(), false);
    }

    private <T> String iterableToString(Iterable<T> iterable, Function<T, String> stringMapper, String delimiter) {
        return Optional.ofNullable(iterable).map(itr -> {
            final Iterable<String> titles =
                    iterableToStream(itr)
                            .map(object -> stringMapper.apply(object))
                            .collect(Collectors.toList());
            return String.join(delimiter, titles);
        }).orElse("");
    }
}
