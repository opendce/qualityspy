/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.wroc.pwr.qualityspy.bugtracker;

import pl.wroc.pwr.qualityspy.model.IssueDescription;
import pl.wroc.pwr.qualityspy.model.MilestoneDescription;

import java.util.List;

/**
 *
 */
public interface IBugTrackerConnector
{
    IssueDescription getIssueDescriptionHistory(IssueDescription issueDescription) throws Exception;
    List<IssueDescription> getIssues() throws Exception;
    List<IssueDescription> getIssuesWithFullDescription() throws Exception;
    List<MilestoneDescription> getMilestones() throws Exception;

}
