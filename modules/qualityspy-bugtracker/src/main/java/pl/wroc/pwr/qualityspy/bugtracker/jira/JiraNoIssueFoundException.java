package pl.wroc.pwr.qualityspy.bugtracker.jira;

/**
 * Nie znaleziono żadnych zagadnień spełniających kryterium filtrowania na
 * serwerze JIRA
 *
 * @author BugTracker Team
 * @version 1.0
 * @since 04.03.2012
 */
public class JiraNoIssueFoundException extends Exception {

    final static String NoIssueFoundMessage =
            "Brak zagadnień w systemie JIRA - ";

    public JiraNoIssueFoundException(String xmlPage) {
        super(NoIssueFoundMessage + xmlPage);
    }
}
