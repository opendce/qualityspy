/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.wroc.pwr.qualityspy.bugtracker.jira;

/**
 *
 * @author AREK
 */
public class JiraWrongIssueNumberException extends Exception{
    public static final String WrongIssueNumberInfo = "Number of retrieved issues does not fit the total issues number on Jira.";
    
    public JiraWrongIssueNumberException(String str) {
        super(str);
    }
    
}
