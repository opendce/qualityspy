package pl.wroc.pwr.qualityspy.bugtracker.jira;

/**
 */
class SearchParams {
    final String jql;
    final int maxResults;
    final int startAt;

    SearchParams(String jql, int maxResults, int startAt) {
        this.jql = jql;
        this.maxResults = maxResults;
        this.startAt = startAt;
    }
}
