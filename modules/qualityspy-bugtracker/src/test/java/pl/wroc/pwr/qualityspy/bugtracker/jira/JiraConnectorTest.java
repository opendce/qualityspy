package pl.wroc.pwr.qualityspy.bugtracker.jira;

import com.atlassian.jira.rest.client.api.RestClientException;
import com.atlassian.jira.rest.client.internal.async.AsynchronousJiraRestClientFactory;
import com.github.jknack.handlebars.internal.Files;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.google.common.collect.Lists;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import pl.wroc.pwr.qualityspy.common.BugtrackerProperties;
import pl.wroc.pwr.qualityspy.common.ConfigurationManager;
import pl.wroc.pwr.qualityspy.model.Interrupter;
import pl.wroc.pwr.qualityspy.model.IsInterruptedException;
import pl.wroc.pwr.qualityspy.model.IssueAttachment;
import pl.wroc.pwr.qualityspy.model.IssueDescription;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.junit.Assert.*;

/**
 * @author Piotrek && Daniel Woźniak
 */
public class JiraConnectorTest {

    public static Pattern regex, regex2, regex3;
    public static Matcher matcher;

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(8089);

    // TODO : Mocked AsynchronousJiraRestClientFactory should be injected to JiraConnector object in each test
    private AsynchronousJiraRestClientFactory asynchronousJiraRestClientFactory;

    public JiraConnectorTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        regex = Pattern.compile("^.This\\scommit\\s*(.*)", Pattern.DOTALL);
        regex2 = Pattern.compile(".*once HHH-8635 is fixed", Pattern.DOTALL);
        regex3 = Pattern.compile("", Pattern.DOTALL);


    }

    /**
     * Test akceptacyjny dla Jira 6.0 (https://hibernate.atlassian.net/) jest w
     * JiraConnectorAcceptanceTest.testGetIssues()
     */
    @Test
    public void testGetIssues() throws Exception {
        //given
        setupWireMock("/rest/api/latest/search?jql=reporter%3D%22Scott+Hernandez%22+and+issuetype%3DTask+and+status%3Dclosed+and+assignee%3D%22hari.khalsa@10gen.com%22+and+fixVersion%3D%222.6.0-rc3%22&expand=schema,names&maxResults=50&startAt=0", "testGetIssues_1.json");
        setupWireMock("/rest/api/latest/issue/SERVER-13336?expand=schema,names,transitions", "testGetIssues_2.json");

        BugtrackerProperties testProperties = new ConfigurationManager(getClass().
                getResource("/").getPath()).loadConfiguration("", "jiraResource3");
        JiraConnector connector = new JiraConnector(testProperties, new ParentKeysSetter(),
                new AsynchronousJiraRestClientFactory());

        List<IssueDescription> expectedResults = Lists.newArrayList();

        IssueDescription issue = new IssueDescription();

        issue.setSummary("Count with no results, never yields");
        issue.setDescription("This commit causes a regression such that no yielding will happen if the policy isn't set due to never entering the loop, which enables the auto-yielding policy. This can also be seen in more complicated scenarios where the timer has yet to trigger the policy and nothing is returned thus delaying the policy being set until a result is found (after the timer triggers), and then yielding will happen after that. "
                + "The easiest way to see this is to run a count with a query which matches nothing, causing a collection scan. "
                + "This also affects maxTimeMS timeouts, and trying to kill the op, since without the yielding checks the count cannot be terminated. "
                + "https://github.com/mongodb/mongo/blob/2850cc7253344c639bc9a01bc6dff3ffd2cca422/src/mongo/db/ops/count.cpp#L98");
        issue.setReporter("Scott Hernandez");
        issue.setAssignTo("hari.khalsa@10gen.com");
        issue.setIssueKey("SERVER-13336");
        issue.setType("Task");
        issue.setPriority("Major - P3");
        issue.setStatus("Closed");
        issue.setResolution("Fixed");
        Calendar created = new GregorianCalendar();
        created.set(2014, 3, 25, 12, 9, 0);
        issue.setCreated(created);
        Calendar updated = new GregorianCalendar();
        updated.set(2014, 3, 25, 12, 55, 0);
        issue.setUpdated(updated);
        issue.setAffectsVersion("2.6.0-rc0");
        issue.setFixVersion("2.6.0-rc3");
        issue.setEnvironment("");
        issue.setAttachments(null);
        issue.setLabels("");
        issue.setProject("Core Server");
        Calendar resolved = new GregorianCalendar();
        resolved.set(2014, 2, 25, 15, 11, 49);
        issue.setResolved(resolved);

        expectedResults.add(issue);

        //when
        connector.connectToJira();

        List<IssueDescription> result = connector.getIssues();

        //then
        matcher = regex.matcher(result.get(0).getDescription());
        Assert.assertEquals(expectedResults.get(0).getAffectsVersion(), result.get(0).getAffectsVersion());
        Assert.assertEquals(expectedResults.get(0).getSummary(), result.get(0).getSummary());
        assertTrue(matcher.find());
        Assert.assertEquals(expectedResults.get(0).getIssueKey(), result.get(0).getIssueKey());
        Assert.assertEquals(expectedResults.get(0).getReporter(), result.get(0).getReporter());
        Assert.assertEquals(expectedResults.get(0).getAssignTo(), result.get(0).getAssignTo());
        Assert.assertEquals(expectedResults.get(0).getType(), result.get(0).getType());
        Assert.assertEquals(expectedResults.get(0).getPriority(), result.get(0).getPriority());
        Assert.assertEquals(expectedResults.get(0).getStatus(), result.get(0).getStatus());
        Assert.assertEquals(expectedResults.get(0).getResolution(), result.get(0).getResolution());
        Assert.assertEquals(1, expectedResults.get(0).getCreated().compareTo(result.get(0).getCreated()));
        assertNotNull(result.get(0).getUpdated());
        assertTrue(updated.getTime().before(result.get(0).getUpdated().getTime()));
        Assert.assertEquals(expectedResults.get(0).getFixVersion(), result.get(0).getFixVersion());
        Assert.assertEquals(expectedResults.get(0).getEnvironment(), result.get(0).getEnvironment());
        Assert.assertEquals(expectedResults.get(0).getAttachment(), result.get(0).getAttachment());
        Assert.assertEquals(expectedResults.get(0).getProject(), result.get(0).getProject());
        Calendar expResoultCalendar = expectedResults.get(0).getResolved();
        Calendar resoultCalendar = result.get(0).getResolved();
        assertEquals(expResoultCalendar == null ? ""
                        : expResoultCalendar.getTime(),
                resoultCalendar == null ? "" : resoultCalendar.getTime()
        );
    }

    /**
     * Test if function throws RestClientException when bad password is written.
     *
     * @throws Exception
     */
    @Test(expected = RestClientException.class)
    public void shouldTestGetIssueWithBadPassword() throws Exception {
        //given
        //In real world should be with header: Authorization: Basic dGVzdHVzZXIxMjM6YmxlZG5l
        setupWireMock("/rest/api/latest/search?jql=reporter%3D%22Scott+Hernandez%22+and+issuetype%3DTask+and+status%3Dclosed+and+assignee%3D%22Hari+Khalsa%22&expand=schema,names&maxResults=50&startAt=0", "shouldTestGetIssueWithBadPassword.json");

        BugtrackerProperties badPassword = new ConfigurationManager(getClass().
                getResource("/").getPath()).loadConfiguration("", "jiraResource5");
        JiraConnector connector = new JiraConnector(badPassword, new ParentKeysSetter(),
                new AsynchronousJiraRestClientFactory());

        //when
        connector.connectToJira();
        connector.getIssues();
    }

    //Trzeba ustawić zapytanie sql znajdujące 2 zapytania i pozmieniać wartości zmiennych, bo na razie tak zostawiłem, najlepiej zmodyfikować to
    //poprzednie, tzn zostawić to co już mamy i odpowiednio zmodyfikować zapytanie JQL, by zwróciło dodatkowo jeszcze jeden wynik
    @Test
    public void shouldTestGetTwoIssues() throws Exception {
        setupWireMock("/rest/api/latest/search?jql=issuetype%3D%22Task%22+and+reporter%3D%22Gunnar+Morling%22+and+(summary+~+%22Re-enable%22+OR+summary+~+%22Update+OGM%22)&expand=schema,names&maxResults=50&startAt=0", "shouldTestGetTwoIssues_1.json");
        setupWireMock("/rest/api/latest/issue/WEBSITE-32?expand=schema,names,transitions", "shouldTestGetTwoIssues_2.json");
        setupWireMock("/rest/api/latest/issue/OGM-369?expand=schema,names,transitions", "shouldTestGetTwoIssues_3.json");
        setupWireMock("/rest/api/latest/issue/WEBSITE-32?expand=schema,names,transitions,changelog", "shouldTestGetTwoIssues_4.json");
        setupWireMock("/rest/api/latest/issue/OGM-369?expand=schema,names,transitions,changelog", "shouldTestGetTwoIssues_5.json");

        BugtrackerProperties twoIssues = new ConfigurationManager(getClass().
                getResource("/").getPath()).loadConfiguration("", "jiraResource6");
        JiraConnector connector = new JiraConnector(twoIssues, new ParentKeysSetter(),
                new AsynchronousJiraRestClientFactory());

        List<IssueDescription> expectedResults = Lists.newArrayList();

        IssueDescription issue = new IssueDescription();
        IssueDescription issue2 = new IssueDescription();

        //issue #1 Summary ~ Re-enable
        issue2.setSummary("Re-enable test disabled due to ORM version update");
        issue2.setDescription("once HHH-8635 is fixed");
        issue2.setReporter("Gunnar Morling");
        issue2.setAssignTo("");
        issue2.setIssueKey("OGM-369");
        issue2.setType("Zadanie");
        issue2.setPriority("Poważny");
        issue2.setStatus("Open");
        issue2.setResolution("Unresolved");
        Calendar created2 = new GregorianCalendar();
        created2.set(2013, 10, 25, 3, 1, 0);
        issue2.setCreated(created2);
        Calendar updated2 = new GregorianCalendar();
        updated2.set(2014, 2, 19, 6, 36, 0);
        issue2.setUpdated(updated2);
        issue2.setAffectsVersion("");
        issue2.setFixVersion("4.1.0.Beta6");
        issue2.setEnvironment("");
        issue2.setAttachments(null);
        issue2.setLabels("");
        issue2.setProject("Hibernate OGM");

        expectedResults.add(issue2);

        //issue #2 Summary ~ Update OGM
        issue.setSummary("Update OGM pages");
        issue.setDescription("");
        issue.setReporter("Gunnar Morling");
        issue.setAssignTo("");
        issue.setIssueKey("WEBSITE-32");
        issue.setType("Zadanie");
        issue.setPriority("Poważny");
        issue.setStatus("Rozwiązane");
        issue.setResolution("Naprawiony");
        Calendar created = new GregorianCalendar();
        created.set(2013, 10, 28, 10, 19, 0);
        issue.setCreated(created);
        Calendar updated = new GregorianCalendar();
        updated.set(2013, 10, 31, 11, 51, 0);
        issue.setUpdated(updated);
        issue.setAffectsVersion("");
        issue.setFixVersion("");
        issue.setEnvironment("");
        issue.setAttachments(null);
        issue.setLabels("");
        issue.setProject("Hibernate Websites");

        expectedResults.add(issue);

        connector.connectToJira();

        final List<IssueDescription> result = connector.getIssuesWithFullDescription();
        //TODO: get the issues by issue key, not by index
        final IssueDescription firstIssue = result.get(0);
        final IssueDescription secondIssue = result.get(1);
        Assert.assertEquals(expectedResults.get(0).getAffectsVersion(), secondIssue.getAffectsVersion());
        Assert.assertEquals(expectedResults.get(0).getSummary(), secondIssue.getSummary());
        assertTrue(secondIssue.getDescription().contains("once HHH-8636 is fixed"));
        Assert.assertEquals(expectedResults.get(0).getReporter(), secondIssue.getReporter());
        Assert.assertEquals("Gunnar Morling", secondIssue.getAssignTo());
        Assert.assertEquals(expectedResults.get(0).getType(), secondIssue.getType());
        Assert.assertEquals(expectedResults.get(0).getPriority(), secondIssue.getPriority());
        Assert.assertEquals("Rozwiązane", secondIssue.getStatus());
        Assert.assertEquals("Naprawiony", secondIssue.getResolution());
        Assert.assertEquals(1, expectedResults.get(0).getCreated().compareTo(secondIssue.getCreated()));
        Assert.assertEquals(-1, expectedResults.get(0).getUpdated().compareTo(secondIssue.getUpdated()));
        Assert.assertEquals(expectedResults.get(0).getFixVersion(), secondIssue.getFixVersion());
        Assert.assertEquals(expectedResults.get(0).getEnvironment(), secondIssue.getEnvironment());
        Assert.assertEquals(expectedResults.get(0).getAttachment(), secondIssue.getAttachment());
        Assert.assertEquals(expectedResults.get(0).getProject(), secondIssue.getProject());

        //#issue2
        matcher = regex3.matcher(firstIssue.getDescription());
        Assert.assertEquals(expectedResults.get(1).getAffectsVersion(), firstIssue.getAffectsVersion());
        Assert.assertEquals(expectedResults.get(1).getSummary(), firstIssue.getSummary());
        assertTrue(matcher.find());
        Assert.assertEquals(expectedResults.get(1).getReporter(), firstIssue.getReporter());
        Assert.assertEquals(expectedResults.get(1).getAssignTo(), firstIssue.getAssignTo());
        Assert.assertEquals(expectedResults.get(1).getType(), firstIssue.getType());
        Assert.assertEquals(expectedResults.get(1).getPriority(), firstIssue.getPriority());
        Assert.assertEquals(expectedResults.get(1).getStatus(), firstIssue.getStatus());
        Assert.assertEquals(expectedResults.get(1).getResolution(), firstIssue.getResolution());
        Assert.assertEquals(1, expectedResults.get(1).getCreated().compareTo(firstIssue.getCreated()));
        Assert.assertEquals(1, expectedResults.get(1).getUpdated().compareTo(firstIssue.getUpdated()));
        Assert.assertEquals(expectedResults.get(1).getFixVersion(), firstIssue.getFixVersion());
        Assert.assertEquals(expectedResults.get(1).getEnvironment(), firstIssue.getEnvironment());
        Assert.assertEquals(expectedResults.get(1).getAttachment(), firstIssue.getAttachment());
        Assert.assertEquals(expectedResults.get(1).getProject(), firstIssue.getProject());
    }

    @Test(expected = IsInterruptedException.class)
    public void testInterruptedException() throws Exception {
        //given
        setupWireMock("/rest/api/latest/search?jql=reporter%3D%22Scott+Hernandez%22+and+issuetype%3DTask+and+status%3Dclosed+and+assignee%3D%22hari.khalsa@10gen.com%22+and+fixVersion%3D%222.6.0-rc3%22&expand=schema,names&maxResults=50&startAt=0", "testGetIssues_1.json");

        BugtrackerProperties testProperties = new ConfigurationManager(getClass().
                getResource("/").getPath()).loadConfiguration("", "jiraResource3");
        JiraConnector connector = new JiraConnector(testProperties, new Interrupter(true), new ParentKeysSetter(),
                new AsynchronousJiraRestClientFactory());

        //when
        connector.connectToJira();
        connector.getIssues();

    }

    @Test
    public void shouldSetDownloadedIssuesWithParentKey() throws Exception {
        // given
        setupWireMock("/rest/api/latest/search?jql=key%3D%22WEBSITE-78%22+OR+key%3D%22WEBSITE-87%22+OR+key%3D%22WEBSITE-22%22&expand=schema,names&maxResults=50&startAt=0", "shouldSetDownloadedIssuesWithParentKey_1.json");
        setupWireMock("/rest/api/latest/issue/WEBSITE-87?expand=schema,names,transitions", "shouldSetDownloadedIssuesWithParentKey_87.json");
        setupWireMock("/rest/api/latest/issue/WEBSITE-87?expand=schema,names,transitions,changelog", "shouldSetDownloadedIssuesWithParentKey_87_2.json");
        setupWireMock("/rest/api/latest/issue/WEBSITE-78?expand=schema,names,transitions", "shouldSetDownloadedIssuesWithParentKey_78.json");
        setupWireMock("/rest/api/latest/issue/WEBSITE-78?expand=schema,names,transitions,changelog", "shouldSetDownloadedIssuesWithParentKey_78_2.json");
        setupWireMock("/rest/api/latest/issue/WEBSITE-22?expand=schema,names,transitions", "shouldSetDownloadedIssuesWithParentKey_22.json");
        setupWireMock("/rest/api/latest/issue/WEBSITE-22?expand=schema,names,transitions,changelog", "shouldSetDownloadedIssuesWithParentKey_22_2.json");

        final String resourcesPath = getClass().getResource("/").getPath();
        final ConfigurationManager configurationManager = new ConfigurationManager(resourcesPath);
        System.out.println(resourcesPath);
        final BugtrackerProperties bugtrackerProperties =
                configurationManager.loadConfiguration("", "jiraResource9");
        final JiraConnector jiraConnector = new JiraConnector(bugtrackerProperties, new ParentKeysSetter(), new AsynchronousJiraRestClientFactory());

        // when
        final List<IssueDescription> issues = jiraConnector.getIssuesWithFullDescription();

        // then
        IssueDescription parent = null, firstSubtask = null, secondSubtask = null;
        for (IssueDescription issue : issues) {
            if (issue.getIssueKey().equals("WEBSITE-78")) {
                parent = issue;
            } else if (issue.getIssueKey().equals("WEBSITE-22")) {
                firstSubtask = issue;
            } else if (issue.getIssueKey().equals("WEBSITE-87")) {
                secondSubtask = issue;
            }
        }

        assertNull(parent.getParentKey());
        assertTrue(parent.isIsParent());

        assertFalse(firstSubtask.isIsParent());
        assertEquals(parent.getIssueKey(), firstSubtask.getParentKey());

        assertFalse(secondSubtask.isIsParent());
        assertEquals(parent.getIssueKey(), secondSubtask.getParentKey());
    }

    private void setupWireMock(String url, String fileWithResponseBody) throws IOException {
        stubFor(get(urlEqualTo(url))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "text/html")
                        .withBody(loadResource(fileWithResponseBody))));
    }

    @Test
    public void getIssuesShouldFetchMoreThan100Issues() throws Exception {
        // given
        final String resourcesPath = getClass().getResource("/").getPath();
        final ConfigurationManager configurationManager = new ConfigurationManager(resourcesPath);
        final BugtrackerProperties bugtrackerProperties =
                configurationManager.loadConfiguration("", "jiraResource10");
        final int pageSize = 50; // hard-coded in JiraConnector
        final int pageCount = 4;
        AsynchronousJiraRestClientFactoryFixture fixture = new AsynchronousJiraRestClientFactoryFixture();
        asynchronousJiraRestClientFactory =
                fixture.mockedAsynchronousJiraRestClientFactory(bugtrackerProperties.getJiraJQLString(),
                        pageSize, pageCount);
        final JiraConnector jiraConnector = new JiraConnector(bugtrackerProperties, new ParentKeysSetter(),
                asynchronousJiraRestClientFactory);
        jiraConnector.connectToJira();

        // when
        final List<IssueDescription> issues = jiraConnector.getIssues();

        // then
        assertEquals(pageSize * pageCount, issues.size());
        for (int i = 0; i < issues.size(); i++) {
            final IssueDescription issueDescription = issues.get(i);
            assertNotNull(issueDescription);
            assertTrue(issueDescription.getIssueKey().equals(String.format("KEY-%d", i)));
        }
    }

    @Test
    public void getIssuesAttachmentsClass() throws Exception {
        // given
        final String resourcesPath = getClass().getResource("/").getPath();
        final ConfigurationManager configurationManager = new ConfigurationManager(resourcesPath);
        final BugtrackerProperties bugtrackerProperties =
                configurationManager.loadConfiguration("", "jiraResource10");
        final int pageSize = 50; // hard-coded in JiraConnector
        final int pageCount = 4;
        AsynchronousJiraRestClientFactoryFixture fixture = new AsynchronousJiraRestClientFactoryFixture();
        asynchronousJiraRestClientFactory =
                fixture.mockedAsynchronousJiraRestClientFactory(bugtrackerProperties.getJiraJQLString(),
                        pageSize, pageCount);
        final JiraConnector jiraConnector = new JiraConnector(bugtrackerProperties, new ParentKeysSetter(),
                asynchronousJiraRestClientFactory);
        jiraConnector.connectToJira();

        // when
        final List<IssueDescription> issues = jiraConnector.getIssues();
        final LocalDateTime creationDate = LocalDateTime.parse("2018-05-22T13:57:47");
        final String fileName = "Testfile.pdf";
        final int size = 1024;
        final String mimeType = "pdf";

        // then
        for (int i = 0; i < issues.size(); i++) {
            final IssueDescription issueDescription = issues.get(i);
            final Set<IssueAttachment> issueAttachments = issueDescription.getIssueAttachments();

            IssueAttachment issueAttachment = new IssueAttachment("pdf", "Testfile.pdf", 1024, creationDate);

            assertNotNull(issueAttachments);
            assertEquals(issueAttachment.getCreationDate(), creationDate);
            assertEquals(issueAttachment.getMimeType(), mimeType);
            assertEquals(issueAttachment.getSize(), size);
            assertEquals(issueAttachment.getFileName(), fileName);
        }
    }

    private String loadResource(String fileName) throws IOException {
        InputStream is = getClass().getResourceAsStream(getClass().getSimpleName() + "/" + fileName);
        return Files.read(is);
    }
}

