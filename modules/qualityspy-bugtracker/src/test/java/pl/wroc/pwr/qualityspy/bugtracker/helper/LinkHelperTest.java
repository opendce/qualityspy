/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wroc.pwr.qualityspy.bugtracker.helper;

import org.junit.Test;
import pl.wroc.pwr.qualityspy.common.BugtrackerProperties;
import pl.wroc.pwr.qualityspy.common.ConfigurationManager;

import static org.junit.Assert.assertEquals;

/**
 * @author DK && Daniel Woźniak
 */
public class LinkHelperTest {
    private static BugtrackerProperties props;


    /**
     * Test of setIssuesFilterForJiraRest method, of class LinkHelper.
     */
    @Test
    public void testSetIssuesFilterForJiraRest() {
        System.out.println("setIssuesFilterForJiraRest");
        BugtrackerProperties bugProps = new ConfigurationManager(getClass().
                getResource("/").getPath()).loadConfiguration("", "helperResource1");
        LinkHelper instance = new LinkHelper();
        String expResult = "project = HHH AND resolved<\"2004-10-01\"";
        String result = instance.setIssuesFilterForJiraRest(bugProps);
        assertEquals(expResult, result);

        bugProps = new ConfigurationManager(getClass().
                getResource("/").getPath()).loadConfiguration("", "helperResource2");
        expResult = "project = HHH AND resolved<\"2004-10-01\" AND resolved>\"2013-05-01\"";
        result = instance.setIssuesFilterForJiraRest(bugProps);
        assertEquals(expResult, result);

        bugProps = new ConfigurationManager(getClass().
                getResource("/").getPath()).loadConfiguration("", "helperResource3");
        expResult = "project = HHH";
        result = instance.setIssuesFilterForJiraRest(bugProps);
        assertEquals(expResult, result);

    }
}