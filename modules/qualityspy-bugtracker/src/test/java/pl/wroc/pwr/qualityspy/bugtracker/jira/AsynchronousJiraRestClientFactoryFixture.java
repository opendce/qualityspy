package pl.wroc.pwr.qualityspy.bugtracker.jira;

import com.atlassian.jira.rest.client.api.AuthenticationHandler;
import com.atlassian.jira.rest.client.api.IssueRestClient;
import com.atlassian.jira.rest.client.api.JiraRestClient;
import com.atlassian.jira.rest.client.api.SearchRestClient;
import com.atlassian.jira.rest.client.api.domain.Attachment;
import com.atlassian.jira.rest.client.api.domain.Issue;
import com.atlassian.jira.rest.client.api.domain.SearchResult;
import com.atlassian.jira.rest.client.internal.async.AsynchronousJiraRestClientFactory;
import com.atlassian.util.concurrent.Promise;
import org.easymock.EasyMock;

import java.net.URI;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.joda.time.DateTime;

public class AsynchronousJiraRestClientFactoryFixture {

    final String fileName = "Testfile.pdf";
    final DateTime jodaCreationDate = new DateTime("2018-05-22T13:57:47.629");
    final int size = 1024;
    final String mimeType = "pdf";

    public AsynchronousJiraRestClientFactory mockedAsynchronousJiraRestClientFactory(final String jql, final int pageSize, final int pageCount) {
        final JiraRestClient mockedJiraClient = EasyMock.createMock(JiraRestClient.class);
        EasyMock.expect(mockedJiraClient.getSearchClient())
                .andReturn(mockedSearchRestClient(jql, pageSize, pageCount))
                .anyTimes();
        final IssueRestClient mockedIssueRestClient = mockedIssueRestClient(pageSize * pageCount);
        EasyMock.expect(mockedJiraClient.getIssueClient())
                .andReturn(mockedIssueRestClient)
                .anyTimes();

        final AsynchronousJiraRestClientFactory mockedFactory =
                EasyMock.createMock(AsynchronousJiraRestClientFactory.class);
        EasyMock.expect(mockedFactory.create(EasyMock.anyObject(), EasyMock.isA(AuthenticationHandler.class)))
                .andReturn(mockedJiraClient)
                .anyTimes();

        EasyMock.replay(mockedFactory, mockedJiraClient);
        return mockedFactory;
    }

    private SearchRestClient mockedSearchRestClient(final String jql, final int pageSize, final int pageCount) {
        final SearchRestClient searchRestClient = EasyMock.createMock(SearchRestClient.class);
        for (int page = 0; page < pageCount; page++) {
            final int startAt = page * pageSize;
            final SearchParams searchParams = new SearchParams(jql, pageSize, startAt);
            final Promise<SearchResult> searchResult = mockedSearchResultFor(searchParams, pageSize * pageCount);
            EasyMock.expect(searchRestClient.searchJql(EasyMock.eq(jql), EasyMock.eq(pageSize), EasyMock.eq(startAt), EasyMock.eq(null)))
                    .andReturn(searchResult)
                    .anyTimes();
        }
        EasyMock.replay(searchRestClient);
        return searchRestClient;
    }

    private Promise<SearchResult> mockedSearchResultFor(final SearchParams searchParams, final int totalIssues) {
        final List<Issue> issues = new LinkedList<>();
        for (int i = 0; i < searchParams.maxResults; i++) {
            issues.add(new Issue(null, null, String.format("KEY-%d", i + searchParams.startAt), null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null)); //Easy mock z tymi 3 parametrami
        }

        final SearchResult searchResultMock = EasyMock.createMock(SearchResult.class);
        EasyMock.expect(searchResultMock.getIssues())
                .andReturn(issues)
                .anyTimes();
        EasyMock.expect(searchResultMock.getTotal())
                .andReturn(totalIssues)
                .anyTimes();

        final Promise<SearchResult> promiseMock = EasyMock.createMock(Promise.class);
        EasyMock.expect(promiseMock.claim())
                .andReturn(searchResultMock);

        EasyMock.replay(promiseMock, searchResultMock);
        return promiseMock;
    }

    private Promise<Issue> mockedIssue(int no) {
        final Issue issue = EasyMock.createNiceMock(Issue.class);
        final Attachment issueAttachment = EasyMock.createNiceMock(Attachment.class);

        EasyMock.expect(issue.getKey())
                .andReturn(String.format("KEY-%d", no))
                .anyTimes();

        EasyMock.expect(issue.getAttachments())
                .andReturn(Collections.singletonList(issueAttachment))
                .anyTimes();

        EasyMock.expect(issueAttachment.getMimeType())
                .andReturn(mimeType)
                .anyTimes();

        EasyMock.expect(issueAttachment.getFilename())
                .andReturn(fileName)
                .anyTimes();

        EasyMock.expect(issueAttachment.getSize())
                .andReturn(size)
                .anyTimes();

        EasyMock.expect(issueAttachment.getCreationDate())
                .andReturn(jodaCreationDate)
                .anyTimes();

        URI uri = null;
        try {
            uri = new URI(Integer.toString(no));
        } catch (Exception ignore) {
        }
        EasyMock.expect(issue.getSelf())
                .andReturn(uri)
                .anyTimes();
        EasyMock.expect(issue.getLabels())
                .andReturn(Collections.emptySet())
                .anyTimes();

        final Promise<Issue> promise = EasyMock.createMock(Promise.class);
        EasyMock.expect(promise.claim())
                .andReturn(issue)
                .anyTimes();

        EasyMock.replay(promise, issue, issueAttachment);
        return promise;
    }

    private IssueRestClient mockedIssueRestClient(final int issueCount) {
        final IssueRestClient issueRestClient = EasyMock.createMock(IssueRestClient.class);
        for (int i = 0; i < issueCount; i++) {
            final String key = String.format("KEY-%d", i);
            EasyMock.expect(issueRestClient.getIssue(EasyMock.eq(key)))
                    .andReturn(mockedIssue(i))
                    .anyTimes();
        }
        EasyMock.replay(issueRestClient);
        return issueRestClient;
    }
}
