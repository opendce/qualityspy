package pl.wroc.pwr.qualityspy.bugtracker.jira;

import org.junit.Test;
import pl.wroc.pwr.qualityspy.model.IssueDescription;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class ParentKeysSetterTest {
    @Test
    public void shouldSetChildrenIssuesParenKeyWithParentKey() {
        //given
        final IssueDescription child1 = createChild("Child1");
        final IssueDescription child2 = createChild("Child2");
        final IssueDescription child3 = createChild("Child3");
        final List<IssueDescription> children = Arrays.asList(child1, child2, child3);
        final IssueDescription parent = createParent("Parent", children);
        final IssueDescription independent = createChild("Child4");
        final ArrayList<IssueDescription> issues = new ArrayList<>();
        issues.addAll(children);
        issues.add(parent);
        issues.add(independent);

        //when
        new ParentKeysSetter().setParentKeysInIssues(issues);

        //then
        assertNull(parent.getParentKey());
        assertNull(independent.getParentKey());
        assertEquals(parent.getIssueKey(), child1.getParentKey());
        assertEquals(parent.getIssueKey(), child2.getParentKey());
        assertEquals(parent.getIssueKey(), child2.getParentKey());
    }

    private IssueDescription createParent(final String key, List<IssueDescription> children) {
        final IssueDescription parent = new IssueDescription();
        parent.setIssueKey(key);
        parent.setIsParent(true);
        children.stream()
                .map(IssueDescription::getIssueKey)
                .forEach(childKey -> parent.addSubtask(childKey));
        return parent;
    }

    private IssueDescription createChild(final String key) {
        final IssueDescription child = new IssueDescription();
        child.setIssueKey(key);
        child.setIsParent(false);
        return child;
    }
}