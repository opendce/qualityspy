package pl.wroc.pwr.qualityspy.bugtracker.github;

import com.google.common.collect.Lists;
import org.easymock.EasyMockSupport;
import org.eclipse.egit.github.core.*;
import org.eclipse.egit.github.core.service.IssueService;
import org.eclipse.egit.github.core.service.MilestoneService;
import org.eclipse.egit.github.core.service.RepositoryService;
import org.junit.Test;
import pl.wroc.pwr.qualityspy.bugtracker.IBugTrackerConnector;
import pl.wroc.pwr.qualityspy.common.ConfigurationManager;
import pl.wroc.pwr.qualityspy.common.Properties;
import pl.wroc.pwr.qualityspy.model.Interrupter;
import pl.wroc.pwr.qualityspy.model.IsInterruptedException;
import pl.wroc.pwr.qualityspy.model.IssueDescription;
import pl.wroc.pwr.qualityspy.model.MilestoneDescription;

import java.time.temporal.ChronoUnit;
import java.util.*;

import static org.easymock.EasyMock.anyObject;
import static org.easymock.EasyMock.expect;
import static org.junit.Assert.assertEquals;

/**
 * @author Marcin && Daniel Woźniak
 */
public class GithubConnectorIT {

    Properties githubPropsOneIssue = new ConfigurationManager(getClass().getResource("/").getPath()).loadConfiguration("", "githubResource1");
    Issue i1 = createIssue(1);
    Issue i2 = createIssue(2);
    Issue i3 = createIssue(3);
    Issue i4 = createIssue(4);
    Issue i5 = createIssue(5);
    Issue i6 = createIssue(34866501, new Date(2014, 5, 3, 16, 9, 11), new Date(2014, 5, 4, 22, 24, 24), 6, null, null, null, "Tobiasz, co ty znowu zrobiłeś.\n", "https://github.com/QualitySpy/QS/issues/6", "open", "błąd w persystencji", "https://api.github.com/repos/QualitySpy/QS/issues/6", "marcin992");
    Issue i7 = createIssue(34999565, new Date(2014, 5, 4, 22, 27, 0), new Date(2014, 5, 4, 22, 27, 0), 7, null, null, null, "Grupo GUI, zmieńcie ikonki, bo są brzydkie.\n", "https://github.com/QualitySpy/QS/issues/7", "open", "Brzydkie ikonki w GUI", "https://api.github.com/repos/QualitySpy/QS/issues/7", "marcin992");
    Issue i8 = createIssue(35734317, new Date(2014, 5, 14, 20, 52, 42), new Date(2014, 5, 15, 20, 52, 58), 8, "e6e6e6", "invalid", "https://api.github.com/repos/QualitySpy/QS/labels/invalid", "One to use them all @QualitySpy\n", "https://github.com/QualitySpy/QS/issues/8", "open", "Example Issue 1", "https://api.github.com/repos/QualitySpy/QS/issues/8", "QualitySpy");
    Issue i9 = createIssue(35738639, new Date(2014, 5, 15, 1, 10, 34), new Date(2014, 5, 15, 1, 37, 51), 9, "e6e6e6", "invalid", "https://api.github.com/repos/QualitySpy/QS/labels/invalid", "On to gather them in darkness @example\n", "https://github.com/QualitySpy/QS/issues/9", "open", "Example Issue 2", "https://api.github.com/repos/QualitySpy/QS/issues/9", "QualitySpy");
    private EasyMockSupport support = new EasyMockSupport();
    RepositoryService repoService = support.createMock(RepositoryService.class);
    IssueService issueService = support.createMock(IssueService.class);
    MilestoneService milestoneService = support.createMock(MilestoneService.class);
    Repository repo = support.createMock(Repository.class);

    @Test
    public void testShouldReturnAllOpenIssues() throws Exception {
        //given
        Properties githubPropsAllIssues = new ConfigurationManager(getClass().getResource("/").getPath()).loadConfiguration("", "githubResource2");
        expect(repoService.getRepository("QualitySpy", "QS")).andReturn(repo);
        expect(issueService.getIssues(repo, githubPropsAllIssues.getGithubFilter())).andReturn(Arrays.asList(i1, i2, i3, i4, i5, i6, i7, i8, i9));
        support.replayAll();
        IBugTrackerConnector issuesConnector = new GithubConnector(githubPropsAllIssues, new Interrupter(false), repoService, issueService, milestoneService);

        //when
        List<IssueDescription> result = issuesConnector.getIssuesWithFullDescription();

        //then
        assertEquals(9, result.size());
        assertEquals("open", result.get(0).getStatus());
    }

    @Test
    public void testShouldTwoIssues() throws Exception {
        //given
        Properties githubPropsTwoIssues = new ConfigurationManager(getClass().getResource("/").getPath()).loadConfiguration("", "githubResource3");
        expect(repoService.getRepository("QualitySpy", "QS")).andReturn(repo);
        expect(issueService.getIssues(repo, githubPropsTwoIssues.getGithubFilter())).andReturn(Arrays.asList(i7, i6));
        support.replayAll();
        IBugTrackerConnector issuesConnector = new GithubConnector(githubPropsTwoIssues, new Interrupter(false), repoService, issueService, milestoneService);

        //when
        List<IssueDescription> result = issuesConnector.getIssuesWithFullDescription();

        //then
        assertEquals(2, result.size());
        assertEquals("Brzydkie ikonki w GUI", result.get(0).getSummary());
        assertEquals("marcin992", result.get(0).getReporter());
        assertEquals("open", result.get(0).getStatus());

        assertEquals("błąd w persystencji", result.get(1).getSummary());
        assertEquals("marcin992", result.get(1).getReporter());
        assertEquals("open", result.get(1).getStatus());

    }

    @Test
    public void testShouldReturnOneIssueAllFiltersUsed() throws Exception {
        //given
        Properties githubPropsOneIssueAllFiltersUsed = new ConfigurationManager(getClass().getResource("/").getPath()).loadConfiguration("", "githubResource6");
        expect(repoService.getRepository("QualitySpy", "QS")).andReturn(repo);
        expect(issueService.getIssues(repo, githubPropsOneIssueAllFiltersUsed.getGithubFilter())).andReturn(Arrays.asList(i9));
        support.replayAll();
        IBugTrackerConnector issuesConnector = new GithubConnector(githubPropsOneIssueAllFiltersUsed, new Interrupter(false), repoService, issueService, milestoneService);

        //when
        List<IssueDescription> result = issuesConnector.getIssuesWithFullDescription();

        //then
        assertEquals(1, result.size());
        assertEquals("Example Issue 2", result.get(0).getSummary());
        assertEquals("invalid ", result.get(0).getLabels());
        assertEquals("QualitySpy", result.get(0).getReporter());
        assertEquals("QualitySpy", result.get(0).getAssignTo());
        assertEquals("open", result.get(0).getStatus());
    }

    @Test
    public void testShouldReturnTwoIssueAllFiltersUsed() throws Exception {
        //given
        Properties githubPropsTwoIssueAllFiltersUsed = new ConfigurationManager(getClass().getResource("/").getPath()).loadConfiguration("", "githubResource7");
        expect(repoService.getRepository("QualitySpy", "QS")).andReturn(repo);
        expect(issueService.getIssues(repo, githubPropsTwoIssueAllFiltersUsed.getGithubFilter())).andReturn(Arrays.asList(i9, i8));
        support.replayAll();
        IBugTrackerConnector issuesConnector = new GithubConnector(githubPropsTwoIssueAllFiltersUsed, new Interrupter(false), repoService, issueService, milestoneService);

        //when
        List<IssueDescription> result = issuesConnector.getIssuesWithFullDescription();

        //then
        assertEquals(2, result.size());
        assertEquals("Example Issue 2", result.get(0).getSummary());
        assertEquals("invalid ", result.get(0).getLabels());
        assertEquals("QualitySpy", result.get(0).getReporter());
        assertEquals("QualitySpy", result.get(0).getAssignTo());
        assertEquals("open", result.get(0).getStatus());

        assertEquals("Example Issue 1", result.get(1).getSummary());
        assertEquals("invalid ", result.get(1).getLabels());
        assertEquals("QualitySpy", result.get(1).getReporter());
        assertEquals("QualitySpy", result.get(1).getAssignTo());
        assertEquals("open", result.get(1).getStatus());
    }

    private Issue createIssue(int number) {
        Issue issue = new Issue();
        issue.setNumber(number);
        issue.setState("open");
        issue.setLabels(new ArrayList<>());
        return issue;
    }

    private Issue createIssue(int id, Date createdAt, Date updatedAt, int number, String labelColour, String labelName, String labelUrl, String body, String htmlUrl, String state, String title, String url, String login) {
        Issue issue = new Issue();
        issue.setId(id);
        issue.setCreatedAt(createdAt);
        issue.setUpdatedAt(updatedAt);
        issue.setNumber(number);
        if (labelColour != null) {
            Label label = new Label();
            label.setColor(labelColour);
            label.setName(labelName);
            label.setUrl(labelUrl);
            issue.setLabels(Arrays.asList(label));
        } else {
            issue.setLabels(new ArrayList<>());
        }
        issue.setBody(body);
        issue.setHtmlUrl(htmlUrl);
        issue.setState(state);
        issue.setTitle(title);
        issue.setUrl(url);
        User assignee = new User();
        assignee.setLogin(login);
        issue.setAssignee(assignee);
        issue.setUser(assignee);
        return issue;
    }

    @Test
    public void testShouldReturnTwoMilestones() throws Exception {
        //given
        Properties githubPropsTwoMilestones = new ConfigurationManager(getClass().getResource("/").getPath()).loadConfiguration("", "githubResource8");
        expect(repoService.getRepository("duojs", "test")).andReturn(repo);
        Milestone m1 = createMilestone("yields", 2, "open", "0.4.0", "https://api.github.com/repos/duojs/test/milestones/2", 0, 1, new Date(2014, 9, 1, 13, 32, 10));
        Milestone m2 = createMilestone("yields", 1, "open", "0.1.0", "https://api.github.com/repos/duojs/test/milestones/1", 2, 0, new Date(2014, 5, 25, 20, 0, 21));
        expect(milestoneService.getMilestones(repo, null)).andReturn(Arrays.asList(m1, m2));
        support.replayAll();

        IBugTrackerConnector issuesConnector = new GithubConnector(githubPropsTwoMilestones, new Interrupter(false), repoService, issueService, milestoneService);

        //when
        List<MilestoneDescription> actual = issuesConnector.getMilestones();

        //then
        assertMilestontesEquals(m1, actual.get(0));
        assertMilestontesEquals(m1, actual.get(0));
    }

    private void assertMilestontesEquals(Milestone expected, MilestoneDescription actual) {
        assertEquals(expected.getCreator().getLogin(), actual.getCreator());
        assertEquals(expected.getState(), actual.getState());
        assertEquals(expected.getTitle(), actual.getTitle());
        assertEquals(expected.getDescription(), actual.getDescription());
        assertEquals(expected.getCreatedAt().toInstant().truncatedTo(ChronoUnit.SECONDS), actual.getCreated().toInstant().truncatedTo(ChronoUnit.SECONDS));
        assertEquals(expected.getDueOn().toInstant().truncatedTo(ChronoUnit.SECONDS), actual.getDeadline().toInstant().truncatedTo(ChronoUnit.SECONDS));
    }

    @Test
    public void testShouldReturnNoMilestones() throws Exception {
        //given
        Properties githubPropsNoMilestones = new ConfigurationManager(getClass().getResource("/").getPath()).loadConfiguration("", "githubResource9");
        expect(repoService.getRepository("QualitySpy", "QS")).andReturn(repo);
        expect(milestoneService.getMilestones(repo, null)).andReturn(Lists.newArrayList());
        support.replayAll();

        IBugTrackerConnector githubConnector = new GithubConnector(githubPropsNoMilestones, new Interrupter(true), repoService, issueService, milestoneService);

        //when
        List<MilestoneDescription> result = githubConnector.getMilestones();

        //then
        assertEquals(0, result.size());
    }

    @Test(expected = IsInterruptedException.class)
    public void testShouldReturnInterruptedException() throws Exception {
        //given
        expect(issueService.getIssues(anyObject(), anyObject())).andReturn(Collections.singletonList(new Issue()));
        expect(repoService.getRepository(anyObject(), anyObject())).andReturn(repo);
        support.replayAll();

        IBugTrackerConnector githubConnector = new GithubConnector(githubPropsOneIssue, new Interrupter(true), repoService, issueService, milestoneService);

        //when
        githubConnector.getIssuesWithFullDescription();
    }

    private Milestone createMilestone(String creatorLogin, int number, String state, String title, String url, int closedIssues, int openIssues, Date createdAt) {
        Milestone m1 = new Milestone();
        User creator = new User();
        creator.setLogin(creatorLogin);
        m1.setCreator(creator);
        m1.setDescription("random");
        m1.setNumber(number);
        m1.setState(state);
        m1.setTitle(title);
        m1.setUrl(url);
        m1.setClosedIssues(closedIssues);
        m1.setCreatedAt(createdAt);
        m1.setOpenIssues(openIssues);
        m1.setDueOn(new Date());
        return m1;
    }
}
