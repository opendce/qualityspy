/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wroc.pwr.qualityspy.bugtracker.helper;

import org.apache.log4j.Logger;
import org.junit.*;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static org.junit.Assert.assertEquals;

/**
 *
 */
public class DateHelperTest extends DateHelper{

    private static final Logger logger = Logger.getLogger(DateHelperTest.class.getName());
    
    @BeforeClass
    public static void setUpClass() throws Exception
    {
    }

    @AfterClass
    public static void tearDownClass() throws Exception
    {
    }

    @Before
    public void setUp() throws Exception
    {
        
    }

    @After
    public void tearDown() throws Exception
    {
    }

    /**
     * Test of decodeTimeStampInDescription method, of class DateHelper.
     */
    @Test
    public void testDecodeTimeStampInDescription() {
       //TODO
    }

    /**
     * Test of decodeTimeStampInChange method, of class DateHelper.
     */
    @Test
    public void testDecodeTimeStampInChange() {
        DateHelper dh = new DateHelper();
        String toParse = "12/Sep/11 11:03 PM";
        Date date = new GregorianCalendar(2011,8,12,23,03).getTime();
        assertEquals(date,dh.decodeTimeStampInChange(toParse));
        
        toParse = "12/Sep/99 11:03 AM";
        date = new GregorianCalendar(1999,8,12,11,03).getTime();
        assertEquals(date,dh.decodeTimeStampInChange(toParse));
    }

    /**
     * Test of decodeTimestamInSimpleFormat method, of class DateHelper.
     */
    @Test
    public void testDecodeTimestamInSimpleFormat() {
        String timestamp = "27/Aug/12 22:33";
        
        DateHelper instance = new DateHelper();
        Calendar result = instance.decodeTimestamInSimpleFormat(timestamp);
        assertEquals(2012, result.get(Calendar.YEAR));
        assertEquals(8 - 1, result.get(Calendar.MONTH));
        assertEquals(27, result.get(Calendar.DATE));
        assertEquals(22, result.get(Calendar.HOUR_OF_DAY));
        assertEquals(33, result.get(Calendar.MINUTE));
    }
    
    @Test
    public void testConvertJiraTimePeriodToSeconds() {
        DateHelper helper = new DateHelper();
        assertEquals(8*3600 + 4*3600, helper.convertJiraTimePeriodToSeconds("1 day, 4 hours"));
        assertEquals(2*8*3600, helper.convertJiraTimePeriodToSeconds("2 days"));
        assertEquals(8*3600 + 4*3600 + 7*60, helper.convertJiraTimePeriodToSeconds("1 day, 4 hours, 7 minutes"));
        assertEquals(0, helper.convertJiraTimePeriodToSeconds("0 minutes")); 
        assertEquals(8*5*8*3600 + 2*8*3600, helper.convertJiraTimePeriodToSeconds("8 weeks, 2 days"));
        assertEquals(181920, helper.convertJiraTimePeriodToSeconds("1 week, 1 day, 2 hours, 32 minutes"));
        
        assertEquals(19*3600 + 46*60, helper.convertJiraTimePeriodToSeconds("19h 46m"));
        assertEquals(30*60, helper.convertJiraTimePeriodToSeconds("0.5h"));
        assertEquals(15*60, helper.convertJiraTimePeriodToSeconds("15m"));
        
    }

    /**
     * Test of decodeTimeStampRest method, of class DateHelper.
     */
    @Test
    public void testDecodeTimeStampRest()
    {
        System.out.println("decodeTimeStampRest");
        String timestamp = "2004-09-29T04:07:39.977-0700";
        DateHelper instance = new DateHelper();
        Calendar expResult = new GregorianCalendar();
        expResult.set(2004, 8, 29, 4, 7, 39);
        Calendar result = instance.decodeTimeStampRest(timestamp);
        assertEquals(expResult.getTime().toString(), result.getTime().toString());
        
    }
    
}
