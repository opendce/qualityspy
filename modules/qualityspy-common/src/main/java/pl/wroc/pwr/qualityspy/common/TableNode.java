/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wroc.pwr.qualityspy.common;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author VAIO
 */
public class TableNode {
     
    private String label;
    private List<Object> dataRow;
 
    public TableNode(String s, Object x) {
        label = s;
        dataRow = new ArrayList<>();
        dataRow.add(x);
    }
 
    @Override
    public String toString() {
        return label;
    }

    public List getDataRow() {
        return dataRow;
    }
}
