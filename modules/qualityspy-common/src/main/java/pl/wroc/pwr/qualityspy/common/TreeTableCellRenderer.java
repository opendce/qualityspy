/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wroc.pwr.qualityspy.common;

import java.awt.Component;
import javax.swing.ImageIcon;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeCellRenderer;

/**
 *
 * @author VAIO
 */
public class TreeTableCellRenderer extends DefaultTreeCellRenderer {

    private static final long serialVersionUID = 1;
    
    public TreeTableCellRenderer() {
        setOpenIcon(new ImageIcon("icons/minus.gif"));
        setClosedIcon(new ImageIcon("icons/plus.gif"));
        
    }
    
    public Component getTreeCellRendererComponent(JTree tree, Object value,
            boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        super.getTreeCellRendererComponent(tree, value, leaf, expanded, leaf, row, hasFocus);
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
        
        if(node != null && node.getUserObject() != null && (node.getUserObject() instanceof TableRowData))
        {
        	TableRowData item = (TableRowData)(node.getUserObject());
        	setText(item.dane[0]);
        	if(item.isRoot())
        	{
        		setOpenIcon(new ImageIcon("icons/minus.gif"));
            	setClosedIcon(new ImageIcon("icons/plus.gif"));
        	}
        	else
        	{
        		setIcon(null);
        	}
        }
        else
        {
        	setIcon(null);
        }
        return this;
    }
    
}
