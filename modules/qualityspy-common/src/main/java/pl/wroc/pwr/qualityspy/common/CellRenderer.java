/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wroc.pwr.qualityspy.common;

import java.awt.BorderLayout;
import java.awt.Component;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeCellRenderer;

/**
 *
 * @author VAIO
 */
class CellRenderer extends JPanel implements TreeCellRenderer  {

     JTable table;
    
    public CellRenderer(DefaultTableModel tmodel) { 
        //super(new BorderLayout()); 
        table = new JTable(tmodel); 
//        JScrollPane scrollPane = new JScrollPane(table); 
//        this.add(scrollPane); 
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) {
           final String v = (String) ((DefaultMutableTreeNode) value).getUserObject();
            this.table.setModel(new DefaultTableModel() {

                @Override
                public int getRowCount() {
                    return 2;
                }

                @Override
                public int getColumnCount() {
                    return 2;
                }

                @Override
                public Object getValueAt(int row, int column) {
                    return v + ":" + row + ":" + column;
                }
            });

            return this;
        }
}







