/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.wroc.pwr.qualityspy.common;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import static pl.wroc.pwr.qualityspy.common.CkjmValueRenderer.isEmpty;

/**
 *
 * @author Adam
 */
public class RepositoryValueRenderer extends DefaultTableCellRenderer
{
    Properties  configProperties = null;
    public  RepositoryValueRenderer(Properties  configProperties){
        this.configProperties = configProperties;
    }
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
    {
        Component cellComponent = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
      
        cellComponent.setBackground(Color.white);
        if(configProperties != null){
            String valueString = ((String)value);
            if(column-1<0)
                return cellComponent;
            if(((String)table.getValueAt(row, column-1)).equals(configProperties.getRepoSvnPassKey()+"*"))
            {   
                if(isEmpty(valueString))
                {
                    cellComponent.setBackground(Color.pink);    
                }  
            }
            else if(((String)table.getValueAt(row, column-1)).equals(configProperties.getRepoSvnUserKey()+"*"))
            {
                if(isEmpty(valueString))
                {
                    cellComponent.setBackground(Color.pink);    
                }  
            }
            else if(((String)table.getValueAt(row, column-1)).equals(configProperties.getRepofileseparatorKey()+"*"))
            {
                ((JComponent)cellComponent).setToolTipText(null);
                if(isEmpty(valueString))
                {
                    cellComponent.setBackground(Color.pink);    
                } 
                
                if(!(valueString.equals("/")) && !(valueString.equals("\\")) )
                {
                    cellComponent.setBackground(Color.pink);
                    ((JComponent)cellComponent).setToolTipText("Repo FIle Separator can be only / or \\ ! SeparatorKey not valid");
                }
                        
                        
            }
            else if(((String)table.getValueAt(row, column-1)).equals(configProperties.getReposrcpathpostfixregexKey()+"*"))
            {
                if(isEmpty(valueString))
                {
                    cellComponent.setBackground(Color.pink);    
                }
                else
                {
                    ((JComponent)cellComponent).setToolTipText(null);
                    try {
                        Pattern p = Pattern.compile(valueString);
                    } catch (PatternSyntaxException pse) {
                        cellComponent.setBackground(Color.pink);
                        ((JComponent)cellComponent).setToolTipText(pse.getDescription());
                    }   
                }
            }
            else if(((String)table.getValueAt(row, column-1)).equals(configProperties.getReposrcpathprefixregexKey()+"*"))
            {
                if(isEmpty(valueString))
                {
                    cellComponent.setBackground(Color.pink);    
                }
                else
                {
                    ((JComponent)cellComponent).setToolTipText(null);
                    try {
                        Pattern p = Pattern.compile(valueString);
                    } catch (PatternSyntaxException pse) {
                        cellComponent.setBackground(Color.pink);
                        ((JComponent)cellComponent).setToolTipText(pse.getDescription());
                    }
                }
            }
            else if(((String)table.getValueAt(row, column-1)).equals(configProperties.getRepotypeKey()+"*"))
            {
                ((JComponent)cellComponent).setToolTipText(null);
                if(isEmpty(valueString))
                {
                    cellComponent.setBackground(Color.pink);    
                }
                
                if(!valueString.equals("git") && !valueString.equals("svn"))
                {
                    cellComponent.setBackground(Color.pink);
                    ((JComponent)cellComponent).setToolTipText("Repo Type can be only git or svn ! RepoType not valid");
                }
            }
            else if(((String)table.getValueAt(row, column-1)).equals(configProperties.getRepourlKey()+"*"))
            {
                Pattern p = Pattern.compile("^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]");
                if (!p.matcher(valueString).matches())
                {
                     cellComponent.setBackground(Color.pink);
                } 
            }
            else if(((String)table.getValueAt(row, column-1)).equals(configProperties.getRepopathKey()))
            {
                Pattern p = Pattern.compile("^[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]");
                if (valueString.length()>0 && ! p.matcher(valueString).matches())
                {
                     cellComponent.setBackground(Color.pink);
                }
                
                
            }
            else if(((String)table.getValueAt(row, column-1)).equals(configProperties.getRepoContentRegexKey()))
            {
                ((JComponent)cellComponent).setToolTipText(null);
                if (valueString.length()>0)
                {
                    try { 
                        Pattern.compile(valueString);

                    } catch(PatternSyntaxException pse) {
                        cellComponent.setBackground(Color.pink);
                        ((JComponent)cellComponent).setToolTipText(pse.getDescription());
                    }
                }
            }
            else if(((String)table.getValueAt(row, column-1)).equals(configProperties.getRepoDiffRegexKey()))
            {
                ((JComponent)cellComponent).setToolTipText(null);
                if(valueString.length()>0) 
                {
                   try {
                       Pattern.compile(valueString);
                   } catch(PatternSyntaxException pse) {
                       cellComponent.setBackground(Color.pink);
                       ((JComponent)cellComponent).setToolTipText(pse.getDescription());
                   }
                }
            }
            else if(((String)table.getValueAt(row, column-1)).equals(configProperties.getRepoworkingcopyKey()))
            {
                   
            }
            else if(((String)table.getValueAt(row, column-1)).equals(configProperties.getRepostartrevKey()+"*"))
            {
                Pattern p = Pattern.compile("(\\d+)");
                if (!p.matcher(valueString).matches())
                {
                     cellComponent.setBackground(Color.pink);
                }
                
                 if(isEmpty(valueString))
                {
                    cellComponent.setBackground(Color.pink);    
                } 
            }
            else if(((String)table.getValueAt(row, column-1)).equals(configProperties.getRepostartrevKey()+"* "))
            {
                Pattern p1 = Pattern.compile("(\\d\\d\\d\\d\\/\\d\\d\\/\\d\\d\\ \\d\\d:\\d\\d:\\d\\d)");
                if (!p1.matcher(valueString).matches())
                {
                     cellComponent.setBackground(Color.pink);
                }
                if(isEmpty(valueString))
                {
                    cellComponent.setBackground(Color.pink);
                }

            }
            else if(((String)table.getValueAt(row, column-1)).equals(configProperties.getRepoendrevKey()+"*"))
            {
                Pattern p = Pattern.compile("(\\d+)");
                if (!p.matcher(valueString).matches())
                {
                     cellComponent.setBackground(Color.pink);
                }                   
                
                 if(isEmpty(valueString))
                {
                    cellComponent.setBackground(Color.pink);    
                } 
            }
            else if(((String)table.getValueAt(row, column-1)).equals(configProperties.getRepoendrevKey()+"(data)*"))
            {
                Pattern p = Pattern.compile("(\\d\\d\\d\\d\\/\\d\\d\\/\\d\\d\\ \\d\\d:\\d\\d:\\d\\d)");
                if (!p.matcher(valueString).matches())
                {
                     cellComponent.setBackground(Color.pink);
                }
                if(isEmpty(valueString))
                {
                    cellComponent.setBackground(Color.pink);
                }
            }
           
        }
        return cellComponent;
    }     
}
