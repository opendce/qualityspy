package pl.wroc.pwr.qualityspy.common.db;

import lombok.Getter;
import lombok.Setter;

import java.io.IOException;
import java.util.Properties;

@Getter
@Setter
public class HibernateConfigPropertiesWrapper {

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    private String user;
    private String password;
    private String url;
    private final HibernateConfigPropertiesIO io;

    private static final String USER_KEY = "username";
    private static final String PASSWORD_KEY = "password";
    private static final String URL_KEY = "urll";
    private static final String DRIVER_KEY = "driver";
    private static final String DATABASE_KEY = "database";

    public HibernateConfigPropertiesWrapper() throws IOException {
        this(new HibernateConfigPropertiesIO());
    }

    public HibernateConfigPropertiesWrapper(HibernateConfigPropertiesIO io) throws IOException {
        this.io = io;
        final Properties properties = io.loadProperties();
        user = properties.getProperty(USER_KEY);
        password = properties.getProperty(PASSWORD_KEY);
        url = properties.getProperty(URL_KEY);
    }

    public void saveChanges() throws IOException {
        final Properties properties = new Properties();
        properties.setProperty(USER_KEY, user);
        properties.setProperty(PASSWORD_KEY, password);
        properties.setProperty(URL_KEY, url);
        if(url != null) {
            if (url.startsWith("jdbc:mysql")) {
                properties.setProperty(DATABASE_KEY, "MYSQL");
                properties.setProperty(DRIVER_KEY, "com.mysql.jdbc.Driver");
            } else if(url.startsWith("jdbc:hsqldb")) {
                properties.setProperty(DATABASE_KEY, "HSQL");
                properties.setProperty(DRIVER_KEY, "org.hsqldb.jdbcDriver");
            }
        }
        io.saveProperties(properties);
    }
}
