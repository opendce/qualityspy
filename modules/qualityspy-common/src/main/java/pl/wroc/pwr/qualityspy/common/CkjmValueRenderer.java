package pl.wroc.pwr.qualityspy.common;

import java.awt.Color;
import java.awt.Component;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Pit
 */
public class CkjmValueRenderer  extends DefaultTableCellRenderer
{
    Properties  configProperties = null;
    public  CkjmValueRenderer(Properties  configProperties){
        this.configProperties = configProperties;
    }
    static boolean isEmpty(String s)
    {
          if(s == null )
                {                   
                   return true;   
                }else
                {
                     s = s.replaceAll(" +", "");
                    if(s.length() == 0)
                    {
                              return true; 
                    }
                } 
          return false;
    }
 public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
    {
        Component cellComponent = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    
        cellComponent.setBackground(Color.white);
        if(configProperties != null)
        {
            String valueString = ((String)value);
            if(column-1<0)
                return cellComponent;

            if(((String)table.getValueAt(row, column-1)).equals(configProperties.getCkjmSourceTypeKey()+"*"))
            {
                if (isEmpty(valueString))
                {
                     cellComponent.setBackground(Color.pink);
                }
            }
            else if(((String)table.getValueAt(row, column-1)).equals(configProperties.getCkjmJarKey()))
            {
                
            }
            else if(((String)table.getValueAt(row, column-1)).equals(configProperties.getCkjmClassKey()))
            {
            }
            else if(((String)table.getValueAt(row, column-1)).equals(configProperties.getCkjmLibrariesSeparatorKey()))
            {
                ((JComponent)cellComponent).setToolTipText(null);
                if (valueString.length() > 1)
                {
                    cellComponent.setBackground(Color.pink);
                    ((JComponent)cellComponent).setToolTipText("SeparatorKey can be only one character! SeparatorKey not valid");
                    
                    
                }
            }
            else if(((String)table.getValueAt(row, column-1)).equals(configProperties.getCkjmLibrariesKey()))
            {
                Pattern p = Pattern.compile("^[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]");
                if (! p.matcher(valueString).matches() && !isEmpty(valueString))
                {
                     cellComponent.setBackground(Color.pink);
                }
            }
           
        }
        return cellComponent;
    }     
}
