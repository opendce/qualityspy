/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.wroc.pwr.qualityspy.common;

import javax.swing.table.TableCellEditor;
import java.util.Hashtable;

/**
 *
 * @author Pit
 */
public class RowEditorModel
{
    private Hashtable<Integer, TableCellEditor> data;

     public RowEditorModel()
     {
         data = new Hashtable<>();
     }
     public void addEditorForRow(int row, TableCellEditor e )
    {
        data.put(row, e);
     }
     public void removeEditorForRow(int row)
     {
         data.remove(row);
     }
     public TableCellEditor getEditor(int row)
     {
         return data.get(row);
     }
}
