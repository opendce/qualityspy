/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.wroc.pwr.qualityspy.common;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author Pit
 */
public class CiParametrRenderer extends DefaultTableCellRenderer {
       
    Properties  configProperties = null;

    public  CiParametrRenderer(Properties  configProperties) {
        this.configProperties = configProperties;
    }

    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

        String valueString = ((String)value);

        Component cellComponent = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        cellComponent.setBackground(Color.white);

        Font f =  cellComponent.getFont();
        cellComponent.setFont(new Font(f.getName(),Font.BOLD ,f.getSize()));

        if(configProperties != null) {
            if(valueString.equals(configProperties.getCIServerTypeKey()+"*")) {
               ((JComponent)cellComponent).setToolTipText("Name of the continuous integration server used.");  
            } else if(valueString.equals(configProperties.getCIServerUrlKey()+"*")) {
               ((JComponent)cellComponent).setToolTipText("URL address of CI server e.g. https://jenkins.qa.ubuntu.com/");
            } else if(valueString.equals(configProperties.getCIServerLoginKey())) {
               ((JComponent)cellComponent).setToolTipText(valueString);  
            } else if(valueString.equals(configProperties.getCIServerPasswordKey())) {
                ((JComponent)cellComponent).setToolTipText("CI server's user password.");
            } else if(valueString.equals(configProperties.getCIServerJobNameKey()+"*")) {
               ((JComponent)cellComponent).setToolTipText("Specify the job name for CI server.");  
            }
        }

        return cellComponent;
    }   
}
