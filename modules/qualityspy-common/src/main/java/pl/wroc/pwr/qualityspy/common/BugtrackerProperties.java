package pl.wroc.pwr.qualityspy.common;

import java.util.Map;

/**
 * Properties for bug (issue) tracker connector, e.g. Jira
 */
public interface BugtrackerProperties extends GenericProperties{
 
    public String getJiraUrl();
    public String getJiraLogin();
    public String getJiraPassword();
    public String getJiraTimeout();
    public String getJiraJQLString();
    public String getJiraIssuesType();
    public String getJiraIssuesPriority();
    public String getJiraIssuesResolution();
    public String getJiraIssuesStatus();
    public String getJiraIssuesReporter();
    public String getJiraIssuesAssignee();
    public String getJiraIssuesProject();
    public String getJiraIssuesCreatedBefore();
    public String getJiraIssuesCreatedAfter();
    public String getJiraIssuesUpdatedBefore();
    public String getJiraIssuesUpdatedAfter();
    public String getJiraIssuesResolvedBefore();
    public String getJiraIssuesResolvedAfter();
    public String getJiraIssuesVersion();
    public String getJiraIssuesSearchText();
    
    public String getGithubRepoOwner();
    public String getGithubRepo();
    public String getGithubIssueUser();
    public String getGithubIssueUserPassword();
    public String getGithubIssueState();
    public String getGithubIssueTitle();
    public String getGithubIssueMentioned();
    public String getGithubIssueHtmlUrl();
    public String getGithubIssueCreatedAt();
    public String getGithubIssueUpdatedAt();
    
    public String getGithubIssueLabels();
    public String getGithubIssueAssignee();
    
    public Map<String, String> getGithubFilter();
    public String getGithubIssueNumber();                                                      
    public String getBugtracker();
}