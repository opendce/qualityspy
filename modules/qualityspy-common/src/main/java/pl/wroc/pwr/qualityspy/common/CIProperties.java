package pl.wroc.pwr.qualityspy.common;

/**
 * Properties for continuous integration connector
 */
public interface CIProperties extends GenericProperties {

    /**
     * Type of continuous integration server (Jenkins or Hudson)
     */
    CIServerType getCIServerType();

    /**
     * Type of continuous integration server (Jenkins or Hudson)
     */
    String getCIServerTypeName(CIServerType type);

    /**
     * URL of continuous integration server
     */
    String getCIServerUrl();

    /**
     * Login to CI server
     */
    String getCIServerLogin();

    /*
     * Password to CI server
     */
    String getCIServerPassword();

    /*
     * Name of job to review
     */
    String getCIJobName();

    enum CIServerType {
        Jenkins,
        Hudson,
        UNKNOWN
    }
}
