/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wroc.pwr.qualityspy.common;

/**
 *
 * @author Zdybson
 */
public class SupportedException extends RuntimeException{
    public SupportedException (Throwable ex) {
        super(ex);
    }
}
