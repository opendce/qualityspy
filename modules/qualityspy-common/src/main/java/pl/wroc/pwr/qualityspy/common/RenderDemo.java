/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wroc.pwr.qualityspy.common;

import java.awt.GridLayout;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author VAIO
 */
public class RenderDemo extends JPanel{
    
    JTable table;
    
    RenderDemo(DefaultTableModel tmodel){
        super(new GridLayout(1,0));
        table = new JTable(tmodel);
    }
    
}
