package pl.wroc.pwr.qualityspy.common.db;

import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Properties;

public class HibernateConfigPropertiesIO {

    private Resource propertyFile;

    public HibernateConfigPropertiesIO() {
        propertyFile = new FileSystemResource(new File("config" + File.separator + "HibernateConfig.properties"));
    }

    public Properties loadProperties() throws IOException {
        Properties prop = new Properties();
        try (final InputStream inputStream = propertyFile.getInputStream()) {
            prop.load(inputStream);
            return prop;
        }
    }

    public void saveProperties(Properties properties) throws IOException {
        final File file = propertyFile.getFile();
        if (!file.exists()) {
            Files.createDirectories(file.getParentFile().toPath());
            file.createNewFile();
        }
        try (FileOutputStream fos = new FileOutputStream(file)) {
            properties.store(fos, "");
        }
    }
}
