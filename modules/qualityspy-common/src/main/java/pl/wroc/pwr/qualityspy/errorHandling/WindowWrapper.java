/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wroc.pwr.qualityspy.errorHandling;

import javax.swing.JOptionPane;

/**
 *
 * @author Daniel Matuła
 */
public class WindowWrapper {

    public int showWindow(Exception ex, Object[] options) {
        String errorMessage = ex.getMessage();
        if(errorMessage.length() > 256) {
            errorMessage = errorMessage.substring(0, 256);
            errorMessage = errorMessage + "...";
        }
        
        int n = JOptionPane.showOptionDialog(null, errorMessage, ex.getClass().getName(),
                JOptionPane.DEFAULT_OPTION,
                JOptionPane.WARNING_MESSAGE,
                null,
                options,
                options[0]);

        return n;
    }
}