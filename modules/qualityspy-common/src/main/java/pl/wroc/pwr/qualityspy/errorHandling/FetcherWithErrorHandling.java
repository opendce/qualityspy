package pl.wroc.pwr.qualityspy.errorHandling;

import org.apache.log4j.Logger;
import pl.wroc.pwr.qualityspy.model.IsInterruptedException;

import java.util.Optional;
import java.util.function.Function;

/**
 * TODO: it need to be decoupled from the Swing code (i.e. WindowWrapper should be injectable) for the sake of unit test.
 * <p>
 * TODO: it does't support {@link pl.wroc.pwr.qualityspy.model.IsInterruptedException}
 */
public class FetcherWithErrorHandling<A, R> {

    private final static Logger LOG = Logger.getLogger(FetcherWithErrorHandling.class);
    private final static Object[] options = new Object[]{Button.Retry, Button.Skip, Button.SkipAll, Button.Cancel};
    private final static WindowWrapper pane = new WindowWrapper();
    private boolean skipAll = false;

    public Optional<R> fetchWithErrorHandling(Function<A, R> fetcher, final A argument) {
        R result = null;
        try {
            result = fetcher.apply(argument);
        } catch (IsInterruptedException interruptedByUser) {
            throw interruptedByUser;
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
            if (skipAll) {
                return resultForSkip();
            } else {
                int userSelection = getUserSelection(ex);
                switch ((Button) options[userSelection]) {
                    case Retry:
                        return fetchWithErrorHandling(fetcher, argument);
                    case SkipAll:
                        skipAll = true;
                        break;
                    case Skip:
                        return resultForSkip();
                    case Cancel:
                        throw new RuntimeException("User decided to abort", ex);
                }
            }
        }
        return Optional.of(result);
    }

    private int getUserSelection(Exception ex) {
        int userSelection;
        if ("true".equalsIgnoreCase(System.getProperty("always.retry"))) {
            userSelection = 0;
            try {
                LOG.error("RETRYING");
                Thread.sleep((long) (Math.random() * 6000L));
            } catch (InterruptedException e) {
                //do nothing
            }
        } else {
            userSelection = pane.showWindow(ex, options);
        }
        return userSelection;
    }

    private Optional<R> resultForSkip() {
        return Optional.empty();
    }

}
