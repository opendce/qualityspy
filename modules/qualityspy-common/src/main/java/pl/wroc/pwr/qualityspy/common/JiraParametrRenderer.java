/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.wroc.pwr.qualityspy.common;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import org.apache.log4j.Logger;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import static pl.wroc.pwr.qualityspy.common.CkjmValueRenderer.isEmpty;

/**
 *
 * @author Pit
 */
public class JiraParametrRenderer extends DefaultTableCellRenderer
{
    private static final Logger LOG = Logger.getLogger(JiraParametrRenderer.class);
    
    Properties  configProperties = null;
    public  JiraParametrRenderer(Properties  configProperties){
        this.configProperties = configProperties;
    }
   public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
    {
        Component cellComponent = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        cellComponent.setBackground(Color.white);
        Font f =  cellComponent.getFont(); 
        cellComponent.setFont(new Font(f.getName(),Font.BOLD ,f.getSize()));
        
        if(configProperties != null){
            String valueString = ((String)value);
              if((valueString).equals(configProperties.getBugtrackerKey()+"*"))
            {   
                 ((JComponent)cellComponent).setToolTipText("Enter the name of issue tracking tool, ex. jira");
            }
            else if((valueString).equals(configProperties.getJiraUrlKey()+"*"))
            { 
               ((JComponent)cellComponent).setToolTipText("Enter the URL address for JIRA system , ex. https://jira.atlassian.com");
            }
            else if((valueString).equals(configProperties.getJiraLoginKey()))
            {
               ((JComponent)cellComponent).setToolTipText("Your issue tracking tool's login name.");
            }
            else if((valueString).equals(configProperties.getJiraPasswordKey()))
            {
               ((JComponent)cellComponent).setToolTipText("Your issue tracking tool's password.");
              
            }
            else if((valueString).equals(configProperties.getJiraTimeoutKey()))
            {  
                ((JComponent)cellComponent).setToolTipText("Timeout in miliseconds [ms]. Default timeout is 60 000 [ms].");
            }
            else if((valueString).equals(configProperties.getJiraJQLStringKey()))
            {
               ((JComponent)cellComponent).setToolTipText("Enter JQL query as filter for searching issues in JIRA, ex. Query: project=crucible and fixVersion=3.0.2 -> returns issues from project crucible, fixed in version 3.0.2");
            }
            else if((valueString).equals(configProperties.getGithubIssueNumber()))
            {
               ((JComponent)cellComponent).setToolTipText("Enter number of GitHub repository for issue tracking.");
            }
            else if((valueString).equals(configProperties.getGithubIssueCreatorKey()))
            {
               ((JComponent)cellComponent).setToolTipText("Enter the name of GitHub's issue creator.");
            }
            else if((valueString).equals(configProperties.getGithubIssueHtmlUrlKey()))//Reporter
            {
               ((JComponent)cellComponent).setToolTipText("Enter Reporter of issue.");
            }
            else if((valueString).equals(configProperties.getGithubIssueStateKey()))
            {
               ((JComponent)cellComponent).setToolTipText("Enter the state of reported issue, possible: open, closed or all. Default: open");
            }
            else if((valueString).equals(configProperties.getGithubIssueNumberKey()))
            {
               ((JComponent)cellComponent).setToolTipText("Enter the GitHub's issue number precedeed by '#' character.");
            }
            else if((valueString).equals(configProperties.getGithubIssueAssigneeKey()))
            {
               ((JComponent)cellComponent).setToolTipText("Enter the name of the person assigned for given GitHub's issue. 'none' for no assignee or '*' when assigned to any user");
            }
            else if((valueString).equals(configProperties.getGithubIssueLabelsKey()))
            {
               ((JComponent)cellComponent).setToolTipText("Enter the name of GitHub's issue label, ex. 'bug'.");
            }
            else if((valueString).equals(configProperties.getGithubIssueSinceKey()))
            {
               ((JComponent)cellComponent).setToolTipText("Show issues updated at or after entered time. Time is a timestamp in ISO 8601 format: YYYY-MM-DDTHH:MM:SSZ.");
            }
            else if((valueString).equals(configProperties.getGithubIssueCreatedAtKey()))//CreatedAt
            {
               ((JComponent)cellComponent).setToolTipText("Enter issues created at.");
            }
            else if((valueString).equals(configProperties.getGithubIssueTitleKey()))//Title
            {
               ((JComponent)cellComponent).setToolTipText("Enter Title of issue:");
            }
            else if((valueString).equals(configProperties.getGithubIssueUpdatedAtKey()))//UpdatedAt
            {
               ((JComponent)cellComponent).setToolTipText("Enter issues updated at.");
            }
            else if((valueString).equals(configProperties.getGithubIssueMentionedKey()))
            {
               ((JComponent)cellComponent).setToolTipText("Name of the user mentioned in the issue.");
            }
            
            
            else
            {
                ((JComponent)cellComponent).setToolTipText(valueString);
            }
        }
        return cellComponent;
    }   
}
