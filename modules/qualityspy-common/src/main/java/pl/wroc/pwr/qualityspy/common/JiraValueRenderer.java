/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.wroc.pwr.qualityspy.common;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import static pl.wroc.pwr.qualityspy.common.CkjmValueRenderer.isEmpty;

/**
 *
 * @author Pit
 */
public class JiraValueRenderer  extends DefaultTableCellRenderer
{
    Properties  configProperties = null;
    public  JiraValueRenderer(Properties  configProperties){
        this.configProperties = configProperties;
    }
 public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
    {
        Component cellComponent = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
      
        cellComponent.setBackground(Color.white);
        if(configProperties != null){
            String valueString = ((String)value);
            if(column-1<0)
                return cellComponent;
             if(((String)table.getValueAt(row, column-1)).equals(configProperties.getBugtrackerKey()+"*"))
            {   
                ((JComponent)cellComponent).setToolTipText(null);
                if (isEmpty(valueString)) {
                     cellComponent.setBackground(Color.pink);
                }
                
                 if(!valueString.equals("github") && !valueString.equals("jira"))
                {
                    cellComponent.setBackground(Color.pink);
                    ((JComponent)cellComponent).setToolTipText("Bugtracker can be only github or jira ! Bugtracker not valid");
                }
            }
            else if(((String)table.getValueAt(row, column-1)).equals(configProperties.getJiraUrlKey()+"*"))
            {   
               Pattern p = Pattern.compile("^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]");
                if (!p.matcher(valueString).matches()) {
                     cellComponent.setBackground(Color.pink);
                }
            }
            else if(((String)table.getValueAt(row, column-1)).equals(configProperties.getJiraLoginKey()))
            {
 
            }
            else if(((String)table.getValueAt(row, column-1)).equals(configProperties.getJiraPasswordKey()))
            {

            }
            else if(((String)table.getValueAt(row, column-1)).equals(configProperties.getJiraTimeoutKey()))
            {
                ((JComponent)cellComponent).setToolTipText(null);
                Pattern p = Pattern.compile("(\\d*)");
                if (!p.matcher(valueString).matches())
                {
                     cellComponent.setBackground(Color.pink);
                    ((JComponent)cellComponent).setToolTipText("Jira Timeout can contain only digits! Jira Timeout is not valid.");
                }
            }
            else if(((String)table.getValueAt(row, column-1)).equals(configProperties.getJiraJQLStringKey()))
            {

            }
            else if(((String)table.getValueAt(row, column-1)).equals(configProperties.getGithubIssueNumberKey()))
            {
                Pattern p = Pattern.compile("(\\d+)");
                if (!p.matcher(valueString).matches()&& !isEmpty(valueString))
                {
                     cellComponent.setBackground(Color.pink);
                }
            }
             
         
        }
        return cellComponent;
    }     
}
