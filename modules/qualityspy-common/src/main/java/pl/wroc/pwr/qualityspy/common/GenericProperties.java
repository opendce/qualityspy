
package pl.wroc.pwr.qualityspy.common;

/**
 * Properties that might be used in all connectors
 */
public interface GenericProperties {
    /**Name of the project.*/
   public String getProject();
   /**Name of the version of the project.*/
   public String getVersion();
}
