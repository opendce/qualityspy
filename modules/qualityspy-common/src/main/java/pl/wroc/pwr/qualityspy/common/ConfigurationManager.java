package pl.wroc.pwr.qualityspy.common;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 */
public class ConfigurationManager {

    public static final String separator = System.getProperty("file.separator");
    protected String pathToCfgDirectory = "config" + separator;
    private String selectedProject;
    private String selectedVersion;
    private Properties properties;
    private static final Logger logger = Logger.getLogger(ConfigurationManager.class);

    public ConfigurationManager() {
    }
    public void setPathToCfgDirectory(String path){
            pathToCfgDirectory = path +separator;
    }
    public ConfigurationManager(String pathToCfgDirectory) {
        this.pathToCfgDirectory = pathToCfgDirectory;
    }

    public void setSelectedProjAndVer(String project, String version) {
        try {
            selectedProject = project;
            selectedVersion = version;
            properties = new Properties(pathToCfgDirectory + selectedProject + separator + selectedVersion + separator);
            properties.setProject(project);
            properties.setVersion(version);
        } catch (IOException ex) {
            logger.fatal(ex.getLocalizedMessage());
        }

    }

    public String getSelectedProject() {
        return selectedProject;
    }

    public String getSelectedVersion() {
        return selectedVersion;
    }

    public Properties getConfiguration() {
        return properties;
    }

    public Properties loadConfiguration(String project, String version) {
        setSelectedProjAndVer(project, version);
        return properties;
    }

    /**
     * Saves current properties
     */
    public void saveProperties() throws IOException {
        properties.saveProperties();
    }

    public void savePropertiesAs(String version) throws IOException {
        new File(pathToCfgDirectory + getSelectedProject() + separator + version).mkdir();
        properties.saveProperties(pathToCfgDirectory + getSelectedProject() + separator + version + separator);

    }
     public void addVersion(String project, String version) throws IOException {
         clearConfiguration();
     
        new File(pathToCfgDirectory + project + separator + version).mkdir();
 
        savePropertiesAs(project, version);
    }
    public void savePropertiesAs(String project, String version) throws IOException {
        new File(pathToCfgDirectory + project).mkdir();
        new File(pathToCfgDirectory + project + separator + version).mkdir();
        properties.saveProperties(pathToCfgDirectory + project + separator + version + separator);

    }

    /**
     * Removes directory if exists
     * @param path to directory
     * @return true if directory was removed, false otherwise
     */
    private boolean deleteDirectory(File path) {
        if (path == null) {
            return false;
        }
        if (path.exists()) {
            for (File f : path.listFiles()) {
                if (f.isDirectory()) {
                    deleteDirectory(f);
                    f.delete();
                } else {
                    f.delete();
                }
            }
            path.delete();
            return true;
        }
        return false;
    }
        
    /**
     * Removes the selected configuration. When that is the last configuration
     * (version) in project it removes the whole project.
     * @param project
     * @param version
     */
    public void removeConfiguration(String project, String version) {
        
        
        File dirToRemove = new File(pathToCfgDirectory + project + separator + version);
        deleteDirectory(dirToRemove);
        /*File projectDirectory = new File(pathToCfgDirectory + project);
        if (projectDirectory.list().length == 0) {
            deleteDirectory(projectDirectory);
        }*/
    }

    /**
     * Removes project
     * @param projectName
     * @return true if project was removed, false otherwise
     */
    public boolean removeProject(final String projectName) {
        final File projectDirectory = new File(pathToCfgDirectory + projectName);
        return deleteDirectory(projectDirectory);
    }

    public List<String> getListOfProjects() {
        File f = new File(".");
        try {
            logger.info("#####" + f.getCanonicalPath());
        } catch (IOException ex) {
            logger.fatal(ex.getLocalizedMessage());
        }
        return getDirectoryList(pathToCfgDirectory);
    }

    /**
     * List of versions for given project
     */
    public List<String> getListOfVersions(String project) {
        return getDirectoryList(pathToCfgDirectory + project);
    }

    /**
     * Loads default (empty) configuration.
     */
    public void clearConfiguration() {
        loadConfiguration(".default", ".default");
    }

    public void noConfiguration() {
        properties.emptyProperties();
    }

    /**
     * Skips directories that starts with dots, e.g. .svn
     */
   public List<String> getVersionList(String path) {
        File dir = new File(path);

        FileFilter fileFilter = new FileFilter() {

            @Override
            public boolean accept(File file) {

                return file.isFile();
            }
        };

        File[] directories = dir.listFiles(fileFilter);

        List<String> wynik = new ArrayList<String>();
        for (int i = 0; i < directories.length; i++) {
            wynik.add(directories[i].getName());
        }

        return wynik;
    }
    public void importProject(String dir,String name) throws Exception{ 
            if(dir != null){
                String versionPath = dir+separator+name;
                List<String> versions = getDirectoryList(versionPath);
                if(versions.isEmpty())
                {
                    Exception e = new Exception();
                     throw e;
                }
                File f = new java.io.File("");
                String path = f.getAbsolutePath()+separator+"config"+separator+name ;
                 
                boolean success = (new File(path)).mkdirs();
                if (!success) {
                    Exception e = new Exception();
                     throw e;
                }
                else {

                    for(int j = 0; j < versions.size() ;j++)
                    {
                       String path2 = path+separator+versions.get(j);
                       
                        success = (new File(path2)).mkdirs();
                        if (!success) {
                           
                        }
                        else {
                            
                        }
                        String propertiesPath = versionPath+separator+versions.get(j);
                        List<String> properties = getVersionList(propertiesPath);
                        for(int i = 0; i < properties.size() ;i++)
                        {
                            File afile = new File(propertiesPath+separator+properties.get(i));
                            File bfile = new File(path2+separator+properties.get(i));
                            FileInputStream  inStream = new FileInputStream(afile);
                            FileOutputStream outStream = new FileOutputStream(bfile);

                            byte[] buffer = new byte[1024];

                            int length;
                            //copy the file content in bytes
                            while ((length = inStream.read(buffer)) > 0){

                            outStream.write(buffer, 0, length);

                            }

                            inStream.close();
                            outStream.close();
                        }
                    }
                    System.out.println("Plik zostal skopiowany!");
                }
              
            }
    }
    public List<String> getDirectoryList(String path) {
        File dir = new File(path);

        FileFilter fileFilter = new FileFilter() {

            @Override
            public boolean accept(File file) {

                return file.isDirectory() && !file.getName().startsWith(".");
            }
        };

        File[] directories = dir.listFiles(fileFilter);

        List<String> wynik = new ArrayList<String>();
        if(directories != null){
            for (int i = 0; i < directories.length; i++) {
                wynik.add(directories[i].getName());
            }
        }
        return wynik;
    }
}
