package pl.wroc.pwr.qualityspy.common;

import org.apache.log4j.Logger;
import org.springframework.util.StringUtils;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static pl.wroc.pwr.qualityspy.common.CkjmValueRenderer.isEmpty;

/**
 * Daje dostep do ustawien znajdujacych sie w pliku qs.properties
 */
public class Properties implements BugtrackerProperties, CkjmProperties,
		RepoProperties, CIProperties {
	public static final String WRONG_NUMBER_OF_CHARACTERS = "Wrong number of characters, only the first one was removed.";
	private static String configDir = ".";
	private static final String cfgFileName = "qs.properties";
	private java.util.Properties mProps;
	private String project;
	private String version;

    private static final Logger logger = Logger.getLogger(Properties.class);

	/**
	 * Only configuration manager shuld create new instances of Properties
	 */
	Properties(String directory) throws IOException {
		configDir = directory;
		String fullPath = configDir + System.getProperty("file.separator")
				+ cfgFileName;
		init(fullPath);
	}

	public static String getCfgFileName() {
		return cfgFileName;
	}

	public void emptyProperties() {
		mProps = new java.util.Properties();
	}

	public String getCfgDir() {
		return configDir;
	}

	public void setCfgDir(String path) {
		configDir = path;
	}

	public String getRepoendrevKey() {
		return "repoendrev";
	}

	@Override
	public Object getRepoendrev() {
		String res = getProperty("repoendrev");
		if (isEmpty(res)) { // TODO: Refactoring - the
														// same code in
														// Repostartrev
			return "";
		}
		if (getRepotype().compareToIgnoreCase("svn") == 0) {
			return Long.parseLong(res);
		} else if (getRepotype().compareToIgnoreCase("git") == 0) {
			try {
				SimpleDateFormat df = new SimpleDateFormat(
						"yyyy/MM/dd HH:mm:ss");
				Date dat = df.parse(res);
				return dat;
			} catch (ParseException ex) {
				try {
					SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd");
					Date dat = df.parse(res);
					return dat;
				} catch (ParseException ex1) {
                    logger.warn(ex1.getLocalizedMessage());
//					logger.log(Level.WARNING, ex1.getLocalizedMessage());
					return "";
				}
			}
		} else {
			throw new RuntimeException("Unsupported repo type!");
		}
	}

	public void setRepoendrev(String value) throws NumberFormatException,
			NullPointerException, ParseException {
		if (value.equalsIgnoreCase("") || value.compareTo("") == 0) {
			setProperty("repoendrev", "");
		}

		if (getRepotype().compareToIgnoreCase("svn") == 0) {
			try {
				Long.parseLong(value);
			} catch (NumberFormatException ex) {
                logger.fatal(ex.getLocalizedMessage());
//				logger.log(Level.SEVERE, ex.getLocalizedMessage());
			}
			setProperty("repoendrev", value);
		} else if (getRepotype().compareToIgnoreCase("git") == 0) {
			try {
				SimpleDateFormat df = new SimpleDateFormat(
						"yyyy/MM/dd HH:mm:ss");
				df.parse(value);
			} catch (ParseException ex) {
				try {
					SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd");
					df.parse(value);
				} catch (ParseException parseError) {
                    logger.fatal(parseError.getLocalizedMessage());
//					logger.log(Level.SEVERE, parseError.getLocalizedMessage());
				}
			}
			setProperty("repoendrev", value);
		}

	}

	public String getRepoSvnPassKey() {
		return "reposvnpass";
	}

	@Override
	public String getRepoSvnPass() {
		return getProperty(getRepoSvnPassKey());
	}

	public void setRepoSvnPass(String value) {
		setProperty("reposvnpass", value);
	}

	public String getRepoSvnUserKey() {
		return "reposvnuser";
	}

	@Override
	public String getRepoSvnUser() {
		return getProperty(getRepoSvnUserKey());
	}

	public void setRepoSvnUser(String value) {
		setProperty("reposvnuser", value);
	}

	public String getRepoworkingcopyKey() {
		return "repoworkingcopy";
	}

	@Override
	public String getRepoworkingcopy() {
		return getProperty(getRepoworkingcopyKey());
	}

	public void setRepoworkingcopy(String value) {
		setProperty("repoworkingcopy", value);
	}

	public String getRepotypeKey() {
		return "repotype";
	}
        
	@Override
	public String getRepotype() {
		return getProperty(getRepotypeKey());
	}

	public void setRepotype(String value) {
		setProperty("repotype", value);
	}

	public String getRepourlKey() {
		return "repourl";
	}

	@Override
	public String getRepourl() {
		return getProperty(getRepourlKey());
	}

	public void setRepourl(String value) {
		setProperty("repourl", value);
	}

	public String getRepostartrevKey() {
		return "repostartrev";
	}

	@Override
	public Object getRepostartrev() {
		String res = getProperty("repostartrev");
		if (isEmpty(res)) {
			return "";
		}
		if (getRepotype().compareToIgnoreCase("svn") == 0) {
			return Long.parseLong(res);
		} else if (getRepotype().compareToIgnoreCase("git") == 0) {
			try {
				SimpleDateFormat df = new SimpleDateFormat(
						"yyyy/MM/dd HH:mm:ss");
				Date dat = df.parse(res);
				return dat;
			} catch (ParseException ex) {
				try {
					SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd");
					Date dat = df.parse(res);
					return dat;
				} catch (ParseException ex1) {
                    logger.warn(ex1.getLocalizedMessage());
//					logger.log(Level.WARNING, ex1.getLocalizedMessage());
					return "";
				}
			}
		} else {
			throw new RuntimeException("Unsupported repo type!");
		}
	}

	public void setRepostartrev(String value) throws NumberFormatException,
			NullPointerException, ParseException {
		if (value.equalsIgnoreCase("")) {
			setProperty("repostartrev", "");
		}

		if (getRepotype().compareToIgnoreCase("svn") == 0) {
			try {
				Long.parseLong(value);
			} catch (NumberFormatException ex) {
                logger.fatal(ex.getLocalizedMessage());
//                logger.log(Level.SEVERE, ex.getLocalizedMessage());
			}
			setProperty("repostartrev", value);
		} else if (getRepotype().compareToIgnoreCase("git") == 0) {
			try {
				SimpleDateFormat df = new SimpleDateFormat(
						"yyyy/MM/dd HH:mm:ss");
				Date dat = df.parse(value);
			} catch (ParseException ex) {
				try {
					SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd");
					df.parse(value);
				} catch (ParseException parseError) {
                    logger.fatal(parseError.getLocalizedMessage());
//					logger.log(Level.SEVERE, parseError.getLocalizedMessage());
				}
			}
			setProperty("repostartrev", value);
		} else {
			throw new RuntimeException("Unsupported repo type!");
		}

	}

	public String getReposrcpathprefixregexKey() {
		return "reposrcpathprefixregex";
	}

	@Override
	public String getReposrcpathprefixregex() {
		return getProperty(getReposrcpathprefixregexKey());
	}

	public void setReposrcpathprefixregex(String value) {
		setProperty("reposrcpathprefixregex", value);
	}

	public String getReposrcpathpostfixregexKey() {
		return "reposrcpathpostfixregex";
	}

	@Override
	public String getReposrcpathpostfixregex() {
		return getProperty(getReposrcpathpostfixregexKey());
	}

	public void setReposrcpathpostfixregex(String value) {
		setProperty("reposrcpathpostfixregex", value);
	}

	public String getRepofileseparatorKey() {
		return "repofileseparator";
	}

	@Override
	public Character getRepofileseparator() {
		String res = getProperty(getRepofileseparatorKey());
		if (res == null || res.compareTo("") == 0) {
			return null;
		}
		return res.charAt(0);
	}

	public void setRepofileseparator(String value) throws Exception {
		Exception ex2;
		if (value.length() != 1) {
			value = value.substring(0, 0);
			setProperty("repofileseparator", value);
			throw ex2 = new Exception(WRONG_NUMBER_OF_CHARACTERS);
		}
		setProperty("repofileseparator", value);
		logger.warn(WRONG_NUMBER_OF_CHARACTERS);
	}

	public String getJiraUrlKey() {
		return "trackerUrl";
	}

	@Override
	public String getJiraUrl() {
		return getProperty(getJiraUrlKey());
	}

	public void setJiraUrl(String value) {
		setProperty(getJiraUrlKey(), value);
	}

	public String getJiraLoginKey() {
		return "Login";
	}

	@Override
	public String getJiraLogin() {
		return getProperty(getJiraLoginKey());
	}

	public void setJiraLogin(String value) {
		setProperty(getJiraLoginKey(), value);
	}

	public String getJiraPasswordKey() {
		return "Password";
	}

	@Override
	public String getJiraPassword() {
		return getProperty(getJiraPasswordKey());
	}

	public void setJiraPassword(String value) {
		setProperty(getJiraPasswordKey(), value);
	}

	public String getJiraTimeoutKey() {
		return "Timeout";
	}
        
	@Override
	public String getJiraTimeout() {
		return getProperty(getJiraTimeoutKey());
	}

	public void setJiraTimeout(String value) {
		setProperty(getJiraTimeoutKey(), value);
	}
        

	public String getJiraIssuesTypeKey() {
		return "jiraIssuesType";
	}

	@Override
	public String getJiraIssuesType() {
		return getProperty(getJiraIssuesTypeKey());
	}

	public void setJiraIssuesType(String value) {
		setProperty(getJiraIssuesTypeKey(), value);
	}

	public String getJiraIssuesPriorityKey() {
		return "jiraIssuesPriority";
	}

	@Override
	public String getJiraIssuesPriority() {
		return getProperty(getJiraIssuesPriorityKey());
	}

	public void setJiraIssuesPriority(String value) {
		setProperty(getJiraIssuesPriorityKey(), value);
	}

	public String getJiraIssuesResolutionKey() {
		return "jiraIssuesResolution";
	}

	@Override
	public String getJiraIssuesResolution() {
		return getProperty(getJiraIssuesResolutionKey());
	}

	public void setJiraIssuesResolution(String value) {
		setProperty(getJiraIssuesResolutionKey(), value);
	}

	public String getJiraIssuesStatusKey() {
		return "jiraIssuesStatus";
	}

	@Override
	public String getJiraIssuesStatus() {
		return getProperty(getJiraIssuesStatusKey());
	}

	public void setJiraIssuesStatus(String value) {
		setProperty(getJiraIssuesStatusKey(), value);
	}

	public String getJiraIssuesReporterKey() {
		return "jiraIssuesReporter";
	}

	@Override
	public String getJiraIssuesReporter() {
		return getProperty(getJiraIssuesReporterKey());
	}

	public void setJiraIssuesReporter(String value) {
		setProperty(getJiraIssuesReporterKey(), value);
	}
        public String getJQLString(){
            return "JQLString";
        }
	public String getJiraIssuesAssigneeKey() {
		return "jiraIssuesAssignee";
	}

	@Override
	public String getJiraIssuesAssignee() {
		return getProperty(getJiraIssuesAssigneeKey());
	}

	public void setJiraIssuesAssignee(String value) {
		setProperty(getJiraIssuesAssigneeKey(), value);
	}

	public String getJiraIssuesProjectKey() {
		return "jiraIssuesProject";
	}

	@Override
	public String getJiraIssuesProject() {
		return getProperty(getJiraIssuesProjectKey());
	}

	public void setJiraIssuesProject(String value) {
		setProperty(getJiraIssuesProjectKey(), value);
	}

	public String getJiraIssuesCreatedBeforeKey() {
		return "jiraIssuesCreatedBefore";
	}

	@Override
	public String getJiraIssuesCreatedBefore() {
		return getProperty(getJiraIssuesCreatedBeforeKey());
	}

	public void setJiraIssuesCreatedBefore(String value) {
		setProperty(getJiraIssuesCreatedBeforeKey(), value);
	}

	public String getJiraIssuesCreatedAfterKey() {
		return "jiraIssuesCreatedAfter";
	}

	@Override
	public String getJiraIssuesCreatedAfter() {
		return getProperty(getJiraIssuesCreatedAfterKey());
	}

	public void setJiraIssuesCreatedAfter(String value) {
		setProperty(getJiraIssuesCreatedAfterKey(), value);
	}

	public String getJiraIssuesUpdatedBeforeKey() {
		return "jiraIssuesUpdatedBefore";
	}

	@Override
	public String getJiraIssuesUpdatedBefore() {
		return getProperty(getJiraIssuesUpdatedBeforeKey());
	}

	public void setJiraIssuesUpdatedBefore(String value) {
		setProperty(getJiraIssuesUpdatedBeforeKey(), value);
	}

	public String getJiraIssuesUpdatedAfterKey() {
		return "jiraIssuesUpdatedAfter";
	}

	@Override
	public String getJiraIssuesUpdatedAfter() {
		return getProperty(getJiraIssuesUpdatedAfterKey());
	}

	public void setJiraIssuesUpdatedAfter(String value) {
		setProperty(getJiraIssuesUpdatedAfterKey(), value);
	}

	public String getJiraIssuesResolvedBeforeKey() {
		return "jiraIssuesResolvedBefore";
	}

	@Override
	public String getJiraIssuesResolvedBefore() {
		return getProperty(getJiraIssuesResolvedBeforeKey());
	}

	public void setJiraIssuesResolvedBefore(String value) {
		setProperty(getJiraIssuesResolvedBeforeKey(), value);
	}

	public String getJiraIssuesResolvedAfterKey() {
		return "jiraIssuesResolvedAfter";
	}

	@Override
	public String getJiraIssuesResolvedAfter() {
		return getProperty(getJiraIssuesResolvedAfterKey());
	}

	public void setJiraIssuesResolvedAfter(String value) {
		setProperty(getJiraIssuesResolvedAfterKey(), value);
	}

	public String getJiraIssuesVersionKey() {
		return "jiraIssuesVersion";
	}

	@Override
	public String getJiraIssuesVersion() {
		return getProperty(getJiraIssuesVersionKey());
	}

	public void setJiraIssuesVersion(String value) {
		setProperty(getJiraIssuesVersionKey(), value);
	}

	public String getJiraIssuesSearchTextKey() {
		return "jiraIssuesSearchText";
	}

	@Override
	public String getJiraIssuesSearchText() {
		return getProperty(getJiraIssuesSearchTextKey());
	}

	public void setJiraIssuesSearchText(String value) {
		setProperty(getJiraIssuesSearchTextKey(), value);
	}

	public String getCkjmJarKey() {
		return "ckjmJar";
	}
        
        public String getaddTestKey() {
		return "AddTest";
	}
	
	public String addTest() {
		return getProperty(getaddTestKey());
	}

	public void setaddTest(String value) {
		setProperty(getaddTestKey(), value);
	}

	public String getaddTestSeparator() {
		return "addTestSeparator";
	}

	@Override
	public String getCkjmJar() {
		return getProperty(getCkjmJarKey());
	}

	public void setCkjmJar(String value) {
		setProperty(getCkjmJarKey(), value);
	}

	public String getCkjmClassKey() {
		return "ckjmClass";
	}

	@Override
	public String getCkjmClass() {
		return getProperty(getCkjmClassKey());
	}

	public void setCkjmClass(String value) {
		setProperty(getCkjmClassKey(), value);
	}

	public String getCkjmLibrariesSeparatorKey() {
		return "ckjmLibrariesSeparator";
	}

	@Override
	public String getCkjmLibrariesSeparator() {
		return getProperty(getCkjmLibrariesSeparatorKey());
	}

	public void setCkjmLibrariesSeparator(String value) {
		setProperty(getCkjmLibrariesSeparatorKey(), value);
	}

	public String getCkjmLibrariesKey() {
		return "ckjmLibraries";
	}

	@Override
	public String getCkjmLibraries() {
		return getProperty(getCkjmLibrariesKey());
	}

	public void setCkjmLibraries(String value) {
		setProperty(getCkjmLibrariesKey(), value);
	}

	public String getCkjmSourceTypeKey() {
		return "ckjmSourceType";
	}
        
        @Override
	public String getCkjmMavenProjectDirectory() {
		return getProperty(getCkjmMavenProjectDirectoryKey());
	}

	public void setCkjmMavenProjectDirectory(String value) {
		setProperty(getCkjmMavenProjectDirectoryKey(), value);
	}

	public String getCkjmMavenProjectDirectoryKey() {
		return "ckjmMavenProjectDirectory";
	}
        
        @Override
	public String getCkjmJavaHome() {
		return getProperty(getCkjmJavaHomeKey());
	}

	public void setCkjmJavaHome(String value) {
		setProperty(getCkjmJavaHomeKey(), value);
	}

	public String getCkjmJavaHomeKey() {
		return "ckjmJavaHome";
	}
        
        public String getaddTestHome() {
		return getProperty(getaddTestHomeKey());
	}

	public void setaddTestHome(String value) {
		setProperty(getaddTestHomeKey(), value);
	}
        
        
	public String getaddTestHomeKey() {
		return "addTestHome";
	}
        
        @Override
	public String getCkjmMavenHome() {
		return getProperty(getCkjmMavenHomeKey());
	}

	public void setCkjmMavenHome(String value) {
		setProperty(getCkjmMavenHomeKey(), value);
	}

	public String getCkjmMavenHomeKey() {
		return "ckjmMavenHome";
	}

	@Override
	public SourceTypeEnum getCkjmSourceType() {
		String type = getProperty(getCkjmSourceTypeKey());
		if (type != null) {
			if (type.equalsIgnoreCase("JAR")) {
				return SourceTypeEnum.JAR;
			} else if (type.equalsIgnoreCase("CLASS")) {
				return SourceTypeEnum.CLASS;
			}
		}
		return SourceTypeEnum.UNKNOWN;
	}

	public void setCkjmSourceType(String type) {
		setProperty(getCkjmSourceTypeKey(), type);
	}

	private void init(String fileName) throws IOException {

		File f;
		mProps = new java.util.Properties();

		f = new File(fileName);
		try(FileInputStream inn = new FileInputStream(f)){
			mProps.load(inn);
		}
	}

	private String getProperty(String key) {
		String res = mProps.getProperty(key);
		if (res == null) {
			return null;
		} else if (res.compareTo("") == 0) {
			return "";
		}
		return res;
	}

	public void setProperty(String key, String value) {
		Object Obj;
		Obj = mProps.setProperty(key, value);
	}

	private boolean handleBoolean(String res) {
            return res != null && res.compareToIgnoreCase("true") == 0;
	}

	public void saveProperties(String directory) throws IOException,
			FileNotFoundException {
		FileOutputStream fss;

		String pt = directory + System.getProperty("file.separator")
				+ cfgFileName;

		fss = new FileOutputStream(pt);
		mProps.store(fss, "QualitySpy properties");
		fss.close();

	}

	public void saveProperties() throws IOException, FileNotFoundException {
		saveProperties(configDir);
	}

	@Override
	public String getProject() {
		return project;
	}

	public void setProject(String proj) {
		project = proj;
	}

	@Override
	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getCIServerUrlKey() {
		return "CI server URL".intern();
	}

	@Override
	public String getCIServerUrl() {
		String url = getProperty(getCIServerUrlKey());
		if (url != null) {
			return url;
		}
		return "https://jenkins.qa.ubuntu.com/".intern();
	}

	public void setCIServerUrl(String value) {
		setProperty(getCIServerUrlKey(), value);
	}

	public String getCIServerLoginKey() {
		return "CI server login".intern();
	}

	@Override
	public String getCIServerLogin() {
		String url = getProperty(getCIServerLoginKey());
		if (url != null) {
			return url;
		}
		return "";
	}

	public void setCIServerLogin(String value) {
		setProperty(getCIServerLoginKey(), value);
	}

	public String getCIServerPasswordKey() {
		return "CI server password".intern();
	}

	@Override
	public String getCIServerPassword() {
		String url = getProperty(getCIServerPasswordKey());
		if (url != null) {
			return url;
		}
		return "";
	}

	public void setCIServerPassword(String value) {
		setProperty(getCIServerPasswordKey(), value);
	}

	public String getCIServerJobNameKey() {
		return "CI job name".intern();
	}

	@Override
	public String getCIJobName() {
		String url = getProperty(getCIServerJobNameKey());
		if (url != null) {
			return url;
		}
		return "UNKNOWN".intern();
	}

	public void setCIServerJobName(String value) {
		setProperty(getCIServerJobNameKey(), value);
	}

	public String getCIServerTypeKey() {
		return "CI server type";
	}

	@Override
	public CIServerType getCIServerType() {
		String type = getProperty(getCIServerTypeKey());
		if (type != null)
			if (type.equals("Jenkins"))
				return CIServerType.Jenkins;
			else if (type.equals("Hudson"))
				return CIServerType.Hudson;
			else
				return CIServerType.UNKNOWN;
		else
			return CIServerType.Jenkins;
	}

	@Override
	public String getCIServerTypeName(CIServerType type) {
		switch (type) {
		case Jenkins:
			return "Jenkins";
		case Hudson:
			return "Hudson";
		default:
			return "Unknown";
		}
	}

	public void setCIServerType(String type) {
		setProperty(getCIServerTypeKey(), type);
	}

	public String getRepopathKey() {
		return "repopath";
	}

	@Override
	public String getRepopath() {
		String result = getProperty(getRepopathKey());
		if (result == null) {
			return "";
		}
		return result;
	}

    public String getRepoContentRegexKey() {
		return "repocontentregex";
    }
    public String getRepoDiffRegexKey() {
		return "repodiffregex";
    }
    public String getJiraJQLStringKey() {
		return "jiraJQLString";
    }
        
    @Override
    public String getRepoContentRegex() {
		return getProperty(getRepoContentRegexKey());
    }
    
    public void setRepoContentRegex(String value) {
		setProperty("", value);
    }

    @Override
    public String getRepoDiffRegex() {
		return getProperty(getRepoDiffRegexKey());
    }

	@Override
	public void validateRepoParameters() {

		if(StringUtils.isEmpty(getRepofileseparator()))
			throw new IllegalArgumentException("Invalid repo filesystem separator");
		else if(StringUtils.isEmpty(getReposrcpathpostfixregex()))
			throw new IllegalArgumentException("Invalid repo path postfix regex");
		else if(StringUtils.isEmpty(getReposrcpathprefixregex()))
			throw new IllegalArgumentException("Invalid repo path prefix regex");
		else if(StringUtils.isEmpty(getRepotype()))
			throw new IllegalArgumentException("Invalid repo type");
		else if(StringUtils.isEmpty(getRepourl()))
			throw new IllegalArgumentException("Invalid repo URL");
		else if(StringUtils.isEmpty(getRepostartrev()))
			throw new IllegalArgumentException("Invalid repo startrev parameter - field is empty");
		else if(StringUtils.isEmpty(getRepoendrev()))
			throw new IllegalArgumentException("Invalid repo endrev parameter - field is empty");
	}


	public void setRepoDiffRegex(String value) {
		setProperty("", value);
    }
    
    @Override
    public String getJiraJQLString() {
        return getProperty(getJiraJQLStringKey());
    }
    public void setJiraJQLString(String value) {
		setProperty("", value);
    }
    
    @Override
    public Map<String, String> getGithubFilter() {
        Map<String, String> filter = new HashMap<>();
        if (getGithubIssueState() != null && !"".equals(getGithubIssueState()))
            filter.put(getGithubIssueStateKey(), getGithubIssueState());
        if (getGithubIssueSince() != null && !"".equals(getGithubIssueSince()))
            filter.put(getGithubIssueSinceKey(), getGithubIssueSince());
        if (getGithubIssueAssignee() != null && !"".equals(getGithubIssueAssignee()))
            filter.put(getGithubIssueAssigneeKey(), getGithubIssueAssignee());
        if (getGithubIssueLabels() != null  && !"".equals(getGithubIssueLabels()))
            filter.put(getGithubIssueLabelsKey(), getGithubIssueLabels());
        if (getGithubIssueNumber() != null && !"".equals(getGithubIssueNumber()))
            filter.put(getGithubIssueNumberKey(), getGithubIssueNumber());
        if (getGithubIssueCreator() != null && !"".equals(getGithubIssueCreator()))
            filter.put(getGithubIssueCreatorKey(), getGithubIssueCreator());
        /////
        
        if (getGithubRepo() != null && !"".equals(getGithubRepo()))
            filter.put(getGithubRepoKey(), getGithubRepo());
        if (getGithubRepoOwner() != null && !"".equals(getGithubRepoOwner()))
            filter.put(getGithubRepoOwnerKey(), getGithubRepoOwner());
        if (getGithubIssueUser() != null && !"".equals(getGithubIssueUser()))
            filter.put(getGithubIssueUserKey(), getGithubIssueUser());
        if (getGithubIssueUserPassword() != null && !"".equals(getGithubIssueUserPassword()))
            filter.put(getGithubIssueUserPasswordKey(), getGithubIssueUserPassword());
        if (getGithubIssueTitle() != null && !"".equals(getGithubIssueTitle()))
            filter.put(getGithubIssueTitleKey(), getGithubIssueTitle());
        if (getGithubIssueHtmlUrl() != null && !"".equals(getGithubIssueHtmlUrl()))
            filter.put(getGithubIssueHtmlUrlKey(), getGithubIssueHtmlUrl());
        if (getGithubIssueCreatedAt() != null && !"".equals(getGithubIssueCreatedAt()))
            filter.put(getGithubIssueCreatedAtKey(), getGithubIssueCreatedAt());
        if (getGithubIssueUpdatedAt() != null && !"".equals(getGithubIssueUpdatedAt()))
            filter.put(getGithubIssueUpdatedAtKey(), getGithubIssueUpdatedAt());
        if (getGithubIssueMentioned() != null && !"".equals(getGithubIssueMentioned()))
            filter.put(getGithubIssueMentionedKey(), getGithubIssueMentioned());
        
        return filter;
        
    }
    
    //Github Fields getters/setters
    public String getGithubIssueStateKey() {
            return "state";
    }

    @Override
    public String getGithubIssueState() {
            return getProperty(getGithubIssueStateKey());
    }

    public void setGithubIssueState(String value) {
            setProperty(getGithubIssueStateKey(), value);
    }
    public String getGithubRepoOwnerKey() {
        return "githubOwner";
    }
    
        @Override
    public String getGithubRepoOwner() {
        return getProperty(getGithubRepoOwnerKey());
    }
    
    public void setGithubRepoOwner(String value) {
        setProperty(getGithubRepoOwnerKey(), value);
    }
    
    public String getGithubRepoKey() {
        return "githubIssueRepo";
    }
    
        @Override
    public String getGithubRepo() {
        return getProperty(getGithubRepoKey());
    }
    
    public void setGithubRepo(String value) {
        setProperty(getGithubRepoKey(), value);
    }

    public String getGithubIssueSinceKey() {
            return "since";                   
    }

    public String getGithubIssueSince() {
            return getProperty(getGithubIssueSinceKey());
    }

    public void setGithubIssueSince(String value) {
            setProperty(getGithubIssueSinceKey(), value);
    }

    public String getGithubIssueCreatorKey() {         	
            return "creator";                   
    }
                                                      
    public String getGithubIssueCreator() {
    	return getProperty(getGithubIssueCreatorKey());
    }
                                                      
    public void setGithubIssueCreator(String value) {
    	setProperty(getGithubIssueCreatorKey(), value);
    }
    
    
    public String getGithubIssueAssigneeKey() {         	
	   	return "assignee";                   
    }
                      @Override                                
    public String getGithubIssueAssignee() {
    	return getProperty(getGithubIssueAssigneeKey());
    }
                                                      
    public void setGithubIssueAssignee(String value) {
    	setProperty(getGithubIssueAssigneeKey(), value);
    }
    
    
    public String getGithubIssueMentionedKey() {         	
	   	return "mentioned";                   
    }
         @Override                              
    public String getGithubIssueMentioned() {
    	return getProperty(getGithubIssueMentionedKey());
    }
                                                      
    public void setGithubIssueMentioned(String value) {
    	setProperty(getGithubIssueMentionedKey(), value);
    }
    
    
    public String getGithubIssueLabelsKey() {         	
	   	return "labels";                   
    }
       @Override                
    public String getGithubIssueLabels() {
    	return getProperty(getGithubIssueLabelsKey());
    }
                                                      
    public void setGithubIssueLabels(String value) {
    	setProperty(getGithubIssueLabelsKey(), value);
    }
    
    
    public String getGithubIssueNumberKey() {         	
	   	return "number";                   
    }
                                                      
        @Override
    public String getGithubIssueNumber() {
    	return getProperty(getGithubIssueNumberKey());
    }
                                                      
    public void setGithubIssueNumber(String value) {
    	setProperty(getGithubIssueNumberKey(), value);
    }
    
    
    
    
           
   /////////////////////
    
    @Override
    public String getGithubIssueUser() {
        return getProperty(getGithubIssueUserKey());
    }
    
    public String getGithubIssueUserKey() {
        return "Login";
    }
       
    public void setGithubIssueUser(String value) {
        setProperty(getGithubIssueUserKey(),value);
    }
    
    @Override
    public String getGithubIssueUserPassword() {
        return getProperty(getGithubIssueUserPasswordKey());
    }
    public String getGithubIssueUserPasswordKey() {
        return "Password";
    }
    public void setGithubIssueUserPassword(String value) {
        setProperty(getGithubIssueUserPasswordKey(),value);
    }
    @Override
    public String getGithubIssueTitle() {
        return getProperty(getGithubIssueTitleKey());
    }
    public String getGithubIssueTitleKey() {
        return "Title";
    }
    public void setGithubIssueTitle(String value) {
        setProperty(getGithubIssueTitleKey(),value);
    }
         @Override
    public String getGithubIssueHtmlUrl() {
        return getProperty(getGithubIssueHtmlUrlKey());
    }
    public String getGithubIssueHtmlUrlKey() {
        return "Reporter";
    }
    public void setGithubIssueHtmlUrl(String value) {
        setProperty(getGithubIssueHtmlUrlKey(), value);
    }
    
    @Override
    public String getGithubIssueCreatedAt() {
        return getProperty(getGithubIssueCreatedAtKey());
    }
    public String getGithubIssueCreatedAtKey() {
        return "CreatedAt";
    }
    public void setGithubIssueCreatedAt(String value) {
        setProperty(getGithubIssueHtmlUrlKey(), value);
    }
    
    @Override
    public String getGithubIssueUpdatedAt() {
        return getProperty(getGithubIssueUpdatedAtKey());
    }
    public String getGithubIssueUpdatedAtKey() {
        return "UpdatedAt";
    }
    public void setGithubIssueUpdatedAt(String value) {
        setProperty(getGithubIssueHtmlUrlKey(), value);
    }
    
    ////////////////////
    
    public String getBugtrackerKey() {         	
	   	return "bugtracker";                   
    }
                                                      
        @Override
    public String getBugtracker() {
    	return getProperty(getBugtrackerKey());
    }
                                                      
    public void setBugtracker(String value) {
    	setProperty(getBugtrackerKey(), value);
    }
    
    


}