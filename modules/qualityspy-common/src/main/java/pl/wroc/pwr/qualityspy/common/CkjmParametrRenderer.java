/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.wroc.pwr.qualityspy.common;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author Pit
 */
public class CkjmParametrRenderer extends DefaultTableCellRenderer
{
       
    Properties  configProperties = null;
    public  CkjmParametrRenderer(Properties  configProperties)
    {
        this.configProperties = configProperties;
    }
   public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
    {
        Component cellComponent = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        cellComponent.setBackground(Color.white);
        Font f =  cellComponent.getFont(); 
        cellComponent.setFont(new Font(f.getName(),Font.BOLD ,f.getSize()));

        if(configProperties != null){
            String valueString = ((String)value);
            if((valueString).equals(configProperties.getCkjmSourceTypeKey()+"*"))
            {  
               ((JComponent)cellComponent).setToolTipText("Insert a type of artifact to process. Possible: JAR (Java Class File) or CLASS (list of Java classes)");
            }
            else if((valueString).equals(configProperties.getCkjmJarKey()))
            {
               ((JComponent)cellComponent).setToolTipText("Name with path to the JAR file to analyze. Uused only when ckjmSourceType is set with JAR file.");
            }
            else if((valueString).equals(configProperties.getCkjmClassKey()))
            {
               ((JComponent)cellComponent).setToolTipText("Path to the directory containing classes to analyze (without package directories). Used only when ckjmSourceType set to CLASS.");   
            }
            else if((valueString).equals(configProperties.getCkjmLibrariesSeparatorKey()))
            {  
                ((JComponent)cellComponent).setToolTipText("A characted used to separate libraries on the property ckjmLibraries. It not set, default separator is used. (: - Linux, ; - Windows)");
            }
            else  if((valueString).equals(configProperties.getCkjmLibrariesKey()))
            {
               ((JComponent)cellComponent).setToolTipText("List of libraries (jar files) necessary to calculate metrics. A jar archieve is necessary when at least one of the measured classes refers to a class that is in the jar archive.");
            }
        }
        return cellComponent;
    }   
}

