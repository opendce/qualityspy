package pl.wroc.pwr.qualityspy.common;

/**
 * Properties for ckjm connector
 */
public interface CkjmProperties extends GenericProperties {

    /**
     * Name with path (can be relative) of the jar file to analyse.
     */
    public String getCkjmJar();

    /**
     * Path to the directory that contains classes to analyse (without package
     * directores).
     */
    public String getCkjmClass();

    /**
     * The character that is used to separate libraries on the property
     * CkjmLibraries. If is not set, the default path separator
     * (File.pathSeparator, ':' on Linux, ';' on Windows) is used.
     */
    public String getCkjmLibrariesSeparator();

    /**
     * List of libraries (jar files) that are neccessary to calculate metrics. A
     * jar archive is neccessary to calculate the metrics when at leas one of
     * the measured classes refers to a class that is in the jar archive.
     */
    public String getCkjmLibraries();

    /**
     * Type of the input artifact. Ckjm will be used to calculate metrics for
     * this artifact. Possible values: JAR - for a jar archive; CLASS - for a
     * list of classes.
     */
    public SourceTypeEnum getCkjmSourceType();
    
    /**
     * Path to maven project directory. Should contains pom.xml file
     */
    public String getCkjmMavenProjectDirectory();
    
    /**
     * Maven home
     */
    public String getCkjmMavenHome();
    
    /**
     * Java home
     */
    public String getCkjmJavaHome();
}
