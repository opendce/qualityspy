/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wroc.pwr.qualityspy.common;

/**
 *
 * @author VAIO
 */
public class TableRowData {
    
    public String [] dane;
    boolean leaf = true;

    public TableRowData(String [] str, boolean isleaf) {
        dane = new String[str.length];
        dane = str;
        leaf = isleaf;
    }

    boolean isRoot() {
        return leaf;
    }
    
}
