/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pl.wroc.pwr.qualityspy.common;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import org.apache.log4j.Logger;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author Adam
 */
public class RepositoryParametrRenderer extends DefaultTableCellRenderer
{
    private static final Logger LOG = Logger.getLogger(RepositoryParametrRenderer.class);
    
    Properties  configProperties = null;
    public  RepositoryParametrRenderer(Properties  configProperties){
        this.configProperties = configProperties;
    }
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
    {
        Component cellComponent = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        cellComponent.setBackground(Color.white);
        Font f =  cellComponent.getFont(); 
        cellComponent.setFont(new Font(f.getName(),Font.BOLD ,f.getSize()));
        
        if(configProperties != null){
            String valueString = ((String)value);
            if((valueString).equals(configProperties.getRepoSvnPassKey()))
            { 
               ((JComponent)cellComponent).setToolTipText("Enter the repository's password.");
               
               
            } else if((valueString).equals(configProperties.getRepoSvnUserKey()))
            {
               ((JComponent)cellComponent).setToolTipText("Enter the repository's username (login).");
            }
            else if((valueString).equals(configProperties.getRepofileseparatorKey()+"*"))
            {
               ((JComponent)cellComponent).setToolTipText("Enter a character to be a path separator. The default is '/'.");
            }
            else if((valueString).equals(configProperties.getReposrcpathpostfixregexKey()+"*"))
            {   
                ((JComponent)cellComponent).setToolTipText("Source code postfix defined by an regular expression, ex. *.java" );
            }
            else if((valueString).equals(configProperties.getReposrcpathprefixregexKey()+"*"))
            {
               ((JComponent)cellComponent).setToolTipText("Source code prefix defined by an regular expression, ex. .*/src/main/java" );
            }
            else if((valueString).equals(configProperties.getRepotypeKey()+"*"))
            {
               ((JComponent)cellComponent).setToolTipText("Insert the type of repository. Currently git and svn are supported.");
            }
            else  if((valueString).equals(configProperties.getRepourlKey()+"*"))
            {
               ((JComponent)cellComponent).setToolTipText("URL address of svn or git repository, ex. http://svn.apache.org/repos/asf/");
            }
            else if((valueString).equals(configProperties.getRepopathKey()))
            {
               ((JComponent)cellComponent).setToolTipText("Insert exact path to the project's repository (continuation of URL entered in repourl field)." );
            }
            else if((valueString).equals(configProperties.getRepoContentRegexKey()))
            {
               ((JComponent)cellComponent).setToolTipText("Obtain information about files in repository based on filenames, which are described by the inserted regular expression." );
            }
            else if((valueString).equals(configProperties.getRepoDiffRegexKey()))
            {
               ((JComponent)cellComponent).setToolTipText("Enter regular expression describing on which files should diff operation be executed (comparing files between the revisions and setting how many lines were added / deleted).)" );
            }
            else if((valueString).equals(configProperties.getRepoworkingcopyKey()))
            {
               ((JComponent)cellComponent).setToolTipText("Enter the path for your local repository's copy (working directory).");
            }
            else if((valueString).equals(configProperties.getRepostartrevKey()+"*"))
            {
               ((JComponent)cellComponent).setToolTipText("Enter the project's revision number, from which you want to start analyze." );
            }
            else if((valueString).equals(configProperties.getRepostartrevKey()+"* "))
            {
               ((JComponent)cellComponent).setToolTipText(configProperties.getRepostartrevKey()+ " must contain data format(yyyy/MM/dd HH:mm:ss)" );
            }
            else if((valueString).equals(configProperties.getRepoendrevKey() + "*"))
            {
               ((JComponent)cellComponent).setToolTipText("Specify the revision where analyze should end. It is the number for svn repository." );
            }
            else if((valueString).equals(configProperties.getRepoendrevKey() + "(data)*"))
            {
               ((JComponent)cellComponent).setToolTipText(configProperties.getRepoendrevKey() + " must contain data format(yyyy/MM/dd HH:mm:ss)" );
            }
        }
        return cellComponent;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
