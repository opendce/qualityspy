
package pl.wroc.pwr.qualityspy.common;

/**
 * Properties for repository connector, e.g. SVVN
 */
public interface RepoProperties extends GenericProperties{
    public Object getRepoendrev();
    public String getRepoSvnPass();
    public String getRepoSvnUser();
    public String getRepoworkingcopy();
    public String getRepotype();
    public String getRepourl();
    public String getRepopath();
    public Object getRepostartrev();
    public String getReposrcpathprefixregex();
    public String getReposrcpathpostfixregex();
    public Character getRepofileseparator();
    public String getRepoContentRegex();
    public String getRepoDiffRegex();
    public void validateRepoParameters();
}
