package pl.wroc.pwr.qualityspy.errorHandling;

/**
 * @deprecated
 * TODO: reconsider the design, {@link FetcherWithErrorHandling} may be used instead. Please note that {@link FetcherWithErrorHandling} doesn't support error handling without 'Skip'.
 */
public class ErrorWindow {

    private static ErrorWindow error = new ErrorWindow();
    private Object[] options;
    protected WindowWrapper pane = new WindowWrapper();

    protected ErrorWindow() {
    }

    public static ErrorWindow getInstance() {
        return error;
    }

    public void handleError(Exception ex, Errors errors) {
        options = new Object[]{Button.Retry, Button.Cancel};
        selectActivities((Button) options[pane.showWindow(ex, options)], errors);
    }

    public void extendedHandleError(Exception ex, ExtendedErrors errors) {
        options = new Object[]{Button.Retry, Button.Skip, Button.SkipAll, Button.Cancel};
        selectActivities((Button) options[pane.showWindow(ex, options)], errors);
    }

    private void selectActivities(Button button, Errors errors) {
        switch (button) {
            case Retry:
                errors.tryAgain();
                break;

            case Cancel:
                errors.endProccess();
                break;
        }
    }

    private void selectActivities(Button button, ExtendedErrors errors) {
        switch (button) {
            case Retry:
                errors.tryAgain();
                break;
            case Skip:
                errors.skip();
                break;
            case SkipAll:
                errors.skipAll();
                break;
            case Cancel:
                errors.endProccess();
                break;
        }
    }
}