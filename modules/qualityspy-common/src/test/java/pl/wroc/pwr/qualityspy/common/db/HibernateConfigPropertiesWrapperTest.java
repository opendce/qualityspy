package pl.wroc.pwr.qualityspy.common.db;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.util.Properties;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class HibernateConfigPropertiesWrapperTest {

    @Mock(answer = Answers.RETURNS_MOCKS)
    private HibernateConfigPropertiesIO io;

    private HibernateConfigPropertiesWrapper wrapper;

    @Before
    public void setup() throws IOException {
        wrapper = new HibernateConfigPropertiesWrapper(io);
    }

    @Test
    public void saveChanges_mysql() throws Exception {
        //given
        final String pass = "pass";
        final String user = "user";
        final String url = "jdbc:mysql://localhost:3306/qs_dev";
        wrapper.setPassword(pass);
        wrapper.setUser(user);
        wrapper.setUrl(url);

        //when
        wrapper.saveChanges();

        //then
        ArgumentCaptor<Properties> captor = ArgumentCaptor.forClass(Properties.class);
        verify(io).saveProperties(captor.capture());
        final Properties actual = captor.getValue();
        assertEquals(pass, actual.getProperty("password"));
        assertEquals(user, actual.getProperty("username"));
        assertEquals(url, actual.getProperty("urll"));
        assertEquals("com.mysql.jdbc.Driver", actual.getProperty("driver"));
        assertEquals("MYSQL", actual.getProperty("database"));
    }

    @Test
    public void saveChanges_hsqldb() throws Exception {
        //given
        final String pass = "";
        final String user = "sa";
        final String url = "jdbc:hsqldb:file:db;shutdown=true";
        wrapper.setPassword(pass);
        wrapper.setUser(user);
        wrapper.setUrl(url);

        //when
        wrapper.saveChanges();

        //then
        ArgumentCaptor<Properties> captor = ArgumentCaptor.forClass(Properties.class);
        verify(io).saveProperties(captor.capture());
        final Properties actual = captor.getValue();
        assertEquals(pass, actual.getProperty("password"));
        assertEquals(user, actual.getProperty("username"));
        assertEquals(url, actual.getProperty("urll"));
        assertEquals("org.hsqldb.jdbcDriver", actual.getProperty("driver"));
        assertEquals("HSQL", actual.getProperty("database"));
    }

}