package pl.wroc.pwr.qualityspy.errorHandling;

import org.apache.log4j.Logger;
import org.easymock.EasyMock;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * @author Daniel Matuła
 */
public class ErrorWindowTest {

    private static final Logger logger = Logger.getLogger(ErrorWindowTest.class.getName());
    private Errors errorsMock = EasyMock.createMock(Errors.class);
    private  ExtendedErrors extendedErrorsMock = EasyMock.createMock(ExtendedErrors.class);
    private WindowWrapper paneMock = EasyMock.createMock(WindowWrapper.class);
    /*
     * Testy dla 2 przycisków
     */

    @Test
    public void testRetryButton() {
        logger.info("testRetryButton");
        errorsMock.tryAgain();

        EasyMock.expect(paneMock.showWindow(EasyMock.anyObject(Exception.class), EasyMock.anyObject(new Object[0].getClass()))).andReturn(0);
        ErrorWindow.getInstance().pane = paneMock;

        EasyMock.replay(errorsMock, paneMock);
        ErrorWindow.getInstance().handleError(null, errorsMock);
        EasyMock.verify(errorsMock, paneMock);
    }
    
    @Test
    public void testCancelButton() {
        logger.info("testCancelButton");
        errorsMock.endProccess();

        EasyMock.expect(paneMock.showWindow(EasyMock.anyObject(Exception.class), EasyMock.anyObject(new Object[0].getClass()))).andReturn(1);
        ErrorWindow.getInstance().pane = paneMock;

        EasyMock.replay(errorsMock, paneMock);
        ErrorWindow.getInstance().handleError(null, errorsMock);
        EasyMock.verify(errorsMock, paneMock);
    }
    
    /*
     * Testy dla 3 przycisków
     */

    @Test
    public void testExtendedRetryButton() {
        logger.info("testExtendedRetryButton");
        extendedErrorsMock.tryAgain();

        EasyMock.expect(paneMock.showWindow(EasyMock.anyObject(Exception.class), EasyMock.anyObject(new Object[0].getClass()))).andReturn(0);
        ErrorWindow.getInstance().pane = paneMock;

        EasyMock.replay(extendedErrorsMock, paneMock);
        ErrorWindow.getInstance().extendedHandleError(null, extendedErrorsMock);
        EasyMock.verify(extendedErrorsMock, paneMock);
    }
    
    @Test
    public void testExtendedSkiplButton() {
        logger.info("testExtendedSkiplButton");
        extendedErrorsMock.skip();

        EasyMock.expect(paneMock.showWindow(EasyMock.anyObject(Exception.class), EasyMock.anyObject(new Object[0].getClass()))).andReturn(1);
        ErrorWindow.getInstance().pane = paneMock;

        EasyMock.replay(extendedErrorsMock, paneMock);
        ErrorWindow.getInstance().extendedHandleError(null, extendedErrorsMock);
        EasyMock.verify(extendedErrorsMock, paneMock);
    }
    
    //@Ignore
    @Test
    public void testExtendedCancelButton() {
        logger.info("testExtendedCancelButton");
        extendedErrorsMock.endProccess();

        EasyMock.expect(paneMock.showWindow(EasyMock.anyObject(Exception.class), EasyMock.anyObject(new Object[0].getClass()))).andReturn(3);
        ErrorWindow.getInstance().pane = paneMock;

        EasyMock.replay(extendedErrorsMock, paneMock);
        ErrorWindow.getInstance().extendedHandleError(null, extendedErrorsMock);
        EasyMock.verify(extendedErrorsMock, paneMock);
    }
}
