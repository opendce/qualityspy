package pl.wroc.pwr.qualityspy.common;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by Damian Nowak on 02.04.2017.
 */
public class ConfigurationManagerTest {

    @Test
    public void testGetDirectoryListShouldReturnEmptyListWhenParameterPathIsEmpty() {
        ConfigurationManager instance = new ConfigurationManager("src/");
        List<String> expResult = new ArrayList<>();
        List<String> result = instance.getDirectoryList("");
        assertEquals(expResult, result);
    }

    @Test
    public void shouldDeleteProjectProject() throws IOException {
        // given
        final String projectName = "testProject";
        final String testFileName = "testfile";
        final ConfigurationManager configurationManager = new ConfigurationManager("src/");
        final File directory = new File(String.format("src/%s", projectName));
        if (!directory.exists() || !directory.isDirectory()) {
            directory.mkdir();
        }
        final File testFile = new File(String.format("src/%s/%s", projectName, testFileName));
        if (!testFile.exists()) {
            testFile.createNewFile();
        }

        // when
        final boolean wasRemoved = configurationManager.removeProject("testProject");

        // then
        assertTrue(wasRemoved);
        assertFalse(directory.exists());
        assertFalse(testFile.exists());
    }

    @Test
    public void shouldNotDeleteNotExistingProject() {
        // given
        final ConfigurationManager configurationManager = new ConfigurationManager("src/");

        // when
        final boolean wasRemoved = configurationManager.removeProject("notExistingProjectName");

        // then
        assertFalse(wasRemoved);
    }
}
