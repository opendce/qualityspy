/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.wroc.pwr.qualityspy.common;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.text.ParseException;

import static org.junit.Assert.assertEquals;

/**
 * @author Equal
 */
public class PropertiesTest {

    /**
     * logger *
     */
    private static final Logger logger = Logger.getLogger(PropertiesTest.class.getName());
    private Properties instance;

    @Before
    public void setUp() {
        instance = new ConfigurationManager("src/").loadConfiguration("test", "resources");
    }


    /**
     * Test of GetRepostartrevKey method, of class Properties.
     */
    @Test
    public void testGetRepoStartRevKey() throws IOException {
        logger.info("GetRepoStartRevKey");

        String expResult = "repostartrev";
        String result = instance.getRepostartrevKey();
        assertEquals(expResult, result);

    }

    /**
     * Test of setRepostartrev method, of class Properties.
     */
    @Test
    public void testSetRepoStartRevSVN() throws IOException, NumberFormatException, NullPointerException, ParseException {
        logger.info("setRepoStartRev");
        String value = "9999";

        instance.setRepotype("svn");
        long expResult = 9999;
        instance.setRepostartrev(value);
        Object result = instance.getRepostartrev();
        assertEquals(expResult, result);
    }


    /**
     * Test of SetRepoendrevKey method, of class Properties.
     */
    @Test
    public void testGetRepoEndRevKey() throws IOException {
        logger.info("GetRepoEndRevKey");

        String expResult = "repoendrev";
        String result = instance.getRepoendrevKey();
        assertEquals(expResult, result);

    }

    /**
     * Test of SetRepoendrev method, of class Properties.
     */
    @Test
    public void testSetRepoEndRevSVN() throws IOException, NumberFormatException, NullPointerException, ParseException {
        logger.info("setRepoEndRev");
        String value = "9999";

        instance.setRepotype("svn");
        long expResult = 9999;
        instance.setRepoendrev(value);
        Object result = instance.getRepoendrev();
        assertEquals(expResult, result);
    }

    /**
     * Test of getRepoSvnPassKey method, of class Properties.
     */
    @Test
    public void testGetRepoSvnPassKey() throws IOException {
        logger.info("getRepoSvnPassKey");

        String expResult = "reposvnpass";
        String result = instance.getRepoSvnPassKey();
        assertEquals(expResult, result);

    }

    /**
     * Test of setRepoSvnpass method, of class Properties.
     */
    @Test
    public void testSetRepoSvnpass() throws IOException {
        logger.info("setRepoSvnpass");
        String value = "testRepoSvnpass";

        String expResult = "testRepoSvnpass";
        instance.setRepoSvnPass(value);
        String result = instance.getRepoSvnPass();
        assertEquals(expResult, result);
    }

    /**
     * Test of getRepoSvnUserKey method, of class Properties.
     */
    @Test
    public void testGetRepoSvnUserKey() throws IOException {
        logger.info("getRepoSvnUserKey");

        String expResult = "reposvnuser";
        String result = instance.getRepoSvnUserKey();
        assertEquals(expResult, result);

    }

    /**
     * Test of setRepoSvnUser method, of class Properties.
     */
    @Test
    public void testSetRepoSvnUser() throws IOException {
        logger.info("setRepoSvnUser");
        String value = "testRepoSvnUser";

        String expResult = "testRepoSvnUser";
        instance.setRepoSvnUser(value);
        String result = instance.getRepoSvnUser();
        assertEquals(expResult, result);

    }

    @Test
    public void testgetCkjmClassKey() throws IOException {
        logger.info("getCkjmClassKey");

        String expResult = "ckjmClass";
        String result = instance.getCkjmClassKey();
        assertEquals(expResult, result);
    }

    @Test
    public void testsetCkjmClass() throws IOException {
        logger.info("setCkjmClass");
        String value = "testCkjmClass";

        String expResult = "testCkjmClass";
        instance.setCkjmClass(value);
        String result = instance.getCkjmClass();
        assertEquals(expResult, result);
    }

    @Test
    public void testgetCkjmJarKey() throws IOException {
        logger.info("getCkjmJarKey");

        String expResult = "ckjmJar";
        String result = instance.getCkjmJarKey();
        assertEquals(expResult, result);
    }

    @Test
    public void testsetCkjmJar() throws IOException {
        logger.info("setCkjmJar");
        String value = "testCkjmJar";

        String expResult = "testCkjmJar";
        instance.setCkjmJar(value);
        String result = instance.getCkjmJar();
        assertEquals(expResult, result);
    }

    @Test
    public void testgetCkjmLibrariesSeparatorKey() throws IOException {
        logger.info("getCkjmLibrariesSeparatorKey");

        String expResult = "ckjmLibrariesSeparator";
        String result = instance.getCkjmLibrariesSeparatorKey();
        assertEquals(expResult, result);
    }

    @Test
    public void testsetCkjmLibrariesSeparator() throws IOException {
        logger.info("setCkjmLibrariesSeparator");
        String value = "testCkjmLibrariesSeparator";

        String expResult = "testCkjmLibrariesSeparator";
        instance.setCkjmLibrariesSeparator(value);
        String result = instance.getCkjmLibrariesSeparator();
        assertEquals(expResult, result);
    }

    @Test
    public void testgetCkjmLibrariesKey() throws IOException {
        logger.info("getCkjmLibrariesKey");

        String expResult = "ckjmLibraries";
        String result = instance.getCkjmLibrariesKey();
        assertEquals(expResult, result);
    }

    @Test
    public void testsetCkjmLibraries() throws IOException {
        logger.info("setCkjmLibraries");
        String value = "testCkjmLibraries";

        String expResult = "testCkjmLibraries";
        instance.setCkjmLibraries(value);
        String result = instance.getCkjmLibraries();
        assertEquals(expResult, result);
    }

    /**
     * Test of getRepoworkingcopyKey method, of class Properties.
     */
    @Test
    public void testGetRepoworkingcopyKey() throws IOException {
        logger.info("getRepoworkingcopyKey");

        String expResult = "repoworkingcopy";
        String result = instance.getRepoworkingcopyKey();
        assertEquals(expResult, result);

    }

    /**
     * Test of setRepoworkingcopy method, of class Properties.
     */
    @Test
    public void testSetRepoworkingcopy() throws IOException {
        logger.info("setRepoworkingcopy");
        String value = "testRepoworkingcopy";

        String expResult = "testRepoworkingcopy";
        instance.setRepoworkingcopy(value);
        String result = instance.getRepoworkingcopy();
        assertEquals(expResult, result);
    }

    /**
     * Test of getRepotypeKey method, of class Properties.
     */
    @Test
    public void testGetRepotypeKey() throws IOException {
        logger.info("getRepotypeKey");

        String expResult = "repotype";
        String result = instance.getRepotypeKey();
        assertEquals(expResult, result);

    }

    /**
     * Test of setRepotype method, of class Properties.
     */
    @Test
    public void testSetRepotype() throws IOException {
        logger.info("setRepotype");
        String value = "testRepotype";

        String expResult = "testRepotype";
        instance.setRepotype(value);
        String result = instance.getRepotype();
        assertEquals(expResult, result);

    }

    /**
     * Test of getRepourlKey method, of class Properties.
     */
    @Test
    public void testGetRepourlKey() throws IOException {
        logger.info("getRepourlKey");

        String expResult = "repourl";
        String result = instance.getRepourlKey();
        assertEquals(expResult, result);

    }

    /**
     * Test of setRepourl method, of class Properties.
     */
    @Test
    public void testSetRepourl() throws IOException {
        logger.info("setRepourl");
        String value = "testRepourl";

        String expResult = "testRepourl";
        instance.setRepourl(value);
        String result = instance.getRepourl();
        assertEquals(expResult, result);

    }

    /**
     * Test of getReposrcpathprefixregexKey method, of class Properties.
     */
    @Test
    public void testGetReposrcpathprefixregexKey() throws IOException {
        logger.info("getReposrcpathprefixregexKey");

        String expResult = "reposrcpathprefixregex";
        String result = instance.getReposrcpathprefixregexKey();
        assertEquals(expResult, result);

    }

    /**
     * Test of setReposrcpathprefixregex method, of class Properties.
     */
    @Test
    public void testSetReposrcpathprefixregex() throws IOException {
        logger.info("setReposrcpathprefixregex");
        String value = "testReposrcpathprefixregex";

        String expResult = "testReposrcpathprefixregex";
        instance.setReposrcpathprefixregex(value);
        String result = instance.getReposrcpathprefixregex();
        assertEquals(expResult, result);

    }

    /**
     * Test of getReposrcpathpostfixregexKey method, of class Properties.
     */
    @Test
    public void testGetReposrcpathpostfixregexKey() throws IOException {
        logger.info("getReposrcpathpostfixregexKey");

        String expResult = "reposrcpathpostfixregex";
        String result = instance.getReposrcpathpostfixregexKey();
        assertEquals(expResult, result);

    }

    /**
     * Test of setReposrcpathpostfixregex method, of class Properties.
     */
    @Test
    public void testSetReposrcpathpostfixregex() throws IOException {
        logger.info("setReposrcpathpostfixregex");
        String value = "testReposrcpathpostfixregex";

        String expResult = "testReposrcpathpostfixregex";
        instance.setReposrcpathpostfixregex(value);
        String result = instance.getReposrcpathpostfixregex();
        assertEquals(expResult, result);

    }

    /**
     * Test of getRepofileseparatorKey method, of class Properties.
     */
    @Test
    public void testGetRepofileseparatorKey() throws IOException {
        logger.info("getRepofileseparatorKey");

        String expResult = "repofileseparator";
        String result = instance.getRepofileseparatorKey();
        assertEquals(expResult, result);

    }

    /**
     * Test of setRepofileseparator method, of class Properties.
     */
    @Test
    public void testSetRepofileseparator() throws IOException, Exception {
        logger.info("setRepofileseparator");
        String value = "t";

        String expResult = "t";
        instance.setRepofileseparator(value);
        String result = instance.getRepofileseparator().toString();
        assertEquals(expResult, result);

    }

    /**
     * Test of setRepofileseparator method, of class Properties.
     */
    @Test(expected = Exception.class)
    public void testSetRepofileseparatorWrongString() throws IOException, Exception {
        logger.info("setRepofileseparator");
        String value = "/swiecejnizjedenznak";

        String expResult = "/";
        instance.setRepofileseparator(value);
        String result = instance.getRepofileseparator().toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of getJiraUrlKey method, of class Properties.
     */
    @Test
    public void testGetJiraUrlKey() throws IOException {
        logger.info("getJiraUrlKey");

        String expResult = "trackerUrl";
        String result = instance.getJiraUrlKey();
        assertEquals(expResult, result);
    }

    /**
     * Test of setJiraUrl method, of class Properties.
     */
    @Test
    public void testSetJiraUrl() throws IOException {
        logger.info("setJiraUrl");
        String value = "testjiraUrl";

        String expResult = "testjiraUrl";
        instance.setJiraUrl(value);
        String result = instance.getJiraUrl();
        assertEquals(expResult, result);
    }

    /**
     * Test of getJiraLoginKey method, of class Properties.
     */
    @Test
    public void testGetJiraLoginKey() throws IOException {
        logger.info("getJiraLoginKey");

        String expResult = "Login";
        String result = instance.getJiraLoginKey();
        assertEquals(expResult, result);
    }

    /**
     * Test of setJiraLogin method, of class Properties.
     */
    @Test
    public void testSetJiraLogin() throws IOException {
        logger.info("setJiraLogin");
        String value = "testJiraLogin";

        String expResult = "testJiraLogin";
        instance.setJiraLogin(value);
        String result = instance.getJiraLogin();
        assertEquals(expResult, result);
    }

    /**
     * Test of getJiraPasswordKey method, of class Properties.
     */
    @Test
    public void testGetJiraPasswordKey() throws IOException {
        logger.info("getJiraPasswordKey");

        String expResult = "Password";
        String result = instance.getJiraPasswordKey();
        assertEquals(expResult, result);
    }

    /**
     * Test of setJiraPassword method, of class Properties.
     */
    @Test
    public void testSetJiraPassword() throws IOException {
        logger.info("setJiraPassword");
        String value = "testJiraPassword";

        String expResult = "testJiraPassword";
        instance.setJiraPassword(value);
        String result = instance.getJiraPassword();
        assertEquals(expResult, result);
    }

    /**
     * Test of getJiraTimeoutKey method, of class Properties.
     */
    @Test
    public void testGetJiraTimeoutKey() throws IOException {
        logger.info("getJiraTimeoutKey");

        String expResult = "Timeout";
        String result = instance.getJiraTimeoutKey();
        assertEquals(expResult, result);
    }

    /**
     * Test of setJiraTimeout method, of class Properties.
     */
    @Test
    public void testSetJiraTimeout() throws IOException {
        logger.info("setJiraTimeout");
        String value = "testJiraTimeout";

        String expResult = "testJiraTimeout";
        instance.setJiraTimeout(value);
        String result = instance.getJiraTimeout();
        assertEquals(expResult, result);
    }

    /**
     * Test of getJiraIssuesTypeKey method, of class Properties.
     */
    @Test
    public void testGetJiraIssuesTypeKey() throws IOException {
        logger.info("getJiraIssuesTypeKey");

        String expResult = "jiraIssuesType";
        String result = instance.getJiraIssuesTypeKey();
        assertEquals(expResult, result);
    }

    /**
     * Test of setJiraIssuesType method, of class Properties.
     */
    @Test
    public void testSetJiraIssuesType() throws IOException {
        logger.info("setJiraIssuesType");
        String value = "testJiraIssuesType";

        String expResult = "testJiraIssuesType";
        instance.setJiraIssuesType(value);
        String result = instance.getJiraIssuesType();
        assertEquals(expResult, result);
    }

    /**
     * Test of getJiraIssuesPriorityKey method, of class Properties.
     */
    @Test
    public void testGetJiraIssuesPriorityKey() throws IOException {
        logger.info("getJiraIssuesPriorityKey");

        String expResult = "jiraIssuesPriority";
        String result = instance.getJiraIssuesPriorityKey();
        assertEquals(expResult, result);
    }

    /**
     * Test of setJiraIssuesPriority method, of class Properties.
     */
    @Test
    public void testSetJiraIssuesPriority() throws IOException {
        logger.info("setJiraIssuesPriority");
        String value = "testJiraIssuesPriority";

        String expResult = "testJiraIssuesPriority";
        instance.setJiraIssuesPriority(value);
        String result = instance.getJiraIssuesPriority();
        assertEquals(expResult, result);
    }

    /**
     * Test of getJiraIssuesResolutionKey method, of class Properties.
     */
    @Test
    public void testGetJiraIssuesResolutionKey() throws IOException {
        logger.info("getJiraIssuesResolutionKey");

        String expResult = "jiraIssuesResolution";
        String result = instance.getJiraIssuesResolutionKey();
        assertEquals(expResult, result);
    }

    /**
     * Test of setJiraIssuesResolution method, of class Properties.
     */
    @Test
    public void testSetJiraIssuesResolution() throws IOException {
        logger.info("setJiraIssuesResolution");
        String value = "testJiraIssuesResolution";

        String expResult = "testJiraIssuesResolution";
        instance.setJiraIssuesResolution(value);
        String result = instance.getJiraIssuesResolution();
        assertEquals(expResult, result);
    }

    /**
     * Test of getJiraIssuesStatusKey method, of class Properties.
     */
    @Test
    public void testGetJiraIssuesStatusKey() throws IOException {
        logger.info("getJiraIssuesStatusKey");

        String expResult = "jiraIssuesStatus";
        String result = instance.getJiraIssuesStatusKey();
        assertEquals(expResult, result);
    }

    /**
     * Test of setJiraIssuesStatus method, of class Properties.
     */
    @Test
    public void testSetJiraIssuesStatus() throws IOException {
        logger.info("setJiraIssuesStatus");
        String value = "testJiraIssuesStatus";

        String expResult = "testJiraIssuesStatus";
        instance.setJiraIssuesStatus(value);
        String result = instance.getJiraIssuesStatus();
        assertEquals(expResult, result);
    }

    /**
     * Test of getJiraIssuesReporterKey method, of class Properties.
     */
    @Test
    public void testGetJiraIssuesReporterKey() throws IOException {
        logger.info("getJiraIssuesReporterKey");

        String expResult = "jiraIssuesReporter";
        String result = instance.getJiraIssuesReporterKey();
        assertEquals(expResult, result);
    }

    /**
     * Test of setJiraIssuesReporter method, of class Properties.
     */
    @Test
    public void testSetJiraIssuesReporter() throws IOException {
        logger.info("setJiraIssuesReporter");
        String value = "testJiraIssuesReporter";

        String expResult = "testJiraIssuesReporter";
        instance.setJiraIssuesReporter(value);
        String result = instance.getJiraIssuesReporter();
        assertEquals(expResult, result);
    }

    /**
     * Test of getJiraIssuesAssigneeKey method, of class Properties.
     */
    @Test
    public void testGetJiraIssuesAssigneeKey() throws IOException {
        logger.info("getJiraIssuesAssigneeKey");

        String expResult = "jiraIssuesAssignee";
        String result = instance.getJiraIssuesAssigneeKey();
        assertEquals(expResult, result);
    }

    /**
     * Test of setJiraIssuesAssignee method, of class Properties.
     */
    @Test
    public void testSetJiraIssuesAssignee() throws IOException {
        logger.info("setJiraIssuesAssignee");
        String value = "testJiraIssuesAssignee";

        String expResult = "testJiraIssuesAssignee";
        instance.setJiraIssuesAssignee(value);
        String result = instance.getJiraIssuesAssignee();
        assertEquals(expResult, result);
    }

    /**
     * Test of getJiraIssuesProjectKey method, of class Properties.
     */
    @Test
    public void testGetJiraIssuesProjectKey() throws IOException {
        logger.info("getJiraIssuesProjectKey");

        String expResult = "jiraIssuesProject";
        String result = instance.getJiraIssuesProjectKey();
        assertEquals(expResult, result);
    }

    /**
     * Test of setJiraIssuesProject method, of class Properties.
     */
    @Test
    public void testSetJiraIssuesProject() throws IOException {
        logger.info("setJiraIssuesProject");
        String value = "testJiraIssuesProject";

        String expResult = "testJiraIssuesProject";
        instance.setJiraIssuesProject(value);
        String result = instance.getJiraIssuesProject();
        assertEquals(expResult, result);
    }

    /**
     * Test of getJiraIssuesCreatedBeforeKey method, of class Properties.
     */
    @Test
    public void testGetJiraIssuesCreatedBeforeKey() throws IOException {
        logger.info("getJiraIssuesCreatedBeforeKey");

        String expResult = "jiraIssuesCreatedBefore";
        String result = instance.getJiraIssuesCreatedBeforeKey();
        assertEquals(expResult, result);
    }

    /**
     * Test of setJiraIssuesCreatedBefore method, of class Properties.
     */
    @Test
    public void testSetJiraIssuesCreatedBefore() throws IOException {
        logger.info("setJiraIssuesCreatedBefore");
        String value = "testJiraIssuesCreatedBefore";

        String expResult = "testJiraIssuesCreatedBefore";
        instance.setJiraIssuesCreatedBefore(value);
        String result = instance.getJiraIssuesCreatedBefore();
        assertEquals(expResult, result);
    }

    /**
     * Test of getJiraIssuesCreatedAfterKey method, of class Properties.
     */
    @Test
    public void testGetJiraIssuesCreatedAfterKey() throws IOException {
        logger.info("getJiraIssuesCreatedAfterKey");

        String expResult = "jiraIssuesCreatedAfter";
        String result = instance.getJiraIssuesCreatedAfterKey();
        assertEquals(expResult, result);
    }

    /**
     * Test of setJiraIssuesCreatedAfter method, of class Properties.
     */
    @Test
    public void testSetJiraIssuesCreatedAfter() throws IOException {
        logger.info("setJiraIssuesCreatedAfter");
        String value = "testJiraIssuesCreatedAfter";

        String expResult = "testJiraIssuesCreatedAfter";
        instance.setJiraIssuesCreatedAfter(value);
        String result = instance.getJiraIssuesCreatedAfter();
        assertEquals(expResult, result);
    }

    /**
     * Test of getJiraIssuesUpdatedBeforeKey method, of class Properties.
     */
    @Test
    public void testGetJiraIssuesUpdatedBeforeKey() throws IOException {
        logger.info("getJiraIssuesUpdatedBeforeKey");

        String expResult = "jiraIssuesUpdatedBefore";
        String result = instance.getJiraIssuesUpdatedBeforeKey();
        assertEquals(expResult, result);
    }

    /**
     * Test of setJiraIssuesUpdatedBefore method, of class Properties.
     */
    @Test
    public void testSetJiraIssuesUpdatedBefore() throws IOException {
        logger.info("setJiraIssuesUpdatedBefore");
        String value = "testJiraIssuesUpdatedBefore";

        String expResult = "testJiraIssuesUpdatedBefore";
        instance.setJiraIssuesUpdatedBefore(value);
        String result = instance.getJiraIssuesUpdatedBefore();
        assertEquals(expResult, result);
    }

    /**
     * Test of getJiraIssuesUpdatedAfterKey method, of class Properties.
     */
    @Test
    public void testGetJiraIssuesUpdatedAfterKey() throws IOException {
        logger.info("getJiraIssuesUpdatedAfterKey");

        String expResult = "jiraIssuesUpdatedAfter";
        String result = instance.getJiraIssuesUpdatedAfterKey();
        assertEquals(expResult, result);
    }

    /**
     * Test of setJiraIssuesUpdatedAfter method, of class Properties.
     */
    @Test
    public void testSetJiraIssuesUpdatedAfter() throws IOException {
        logger.info("setJiraIssuesUpdatedAfter");
        String value = "testJiraIssuesUpdatedAfter";

        String expResult = "testJiraIssuesUpdatedAfter";
        instance.setJiraIssuesUpdatedAfter(value);
        String result = instance.getJiraIssuesUpdatedAfter();
        assertEquals(expResult, result);
    }

    /**
     * Test of getJiraIssuesSearchTextKey method, of class Properties.
     */
    @Test
    public void testGetJiraIssuesSearchTextKey() throws IOException {
        logger.info("getJiraIssuesSearchTextKey");

        String expResult = "jiraIssuesSearchText";
        String result = instance.getJiraIssuesSearchTextKey();
        assertEquals(expResult, result);
    }

    /**
     * Test of setJiraIssuesSearchText method, of class Properties.
     */
    @Test
    public void testSetJiraIssuesSearchText() throws IOException {
        logger.info("setJiraIssuesSearchText");
        String value = "testJiraIssuesSearchText";

        String expResult = "testJiraIssuesSearchText";
        instance.setJiraIssuesSearchText(value);
        String result = instance.getJiraIssuesSearchText();
        assertEquals(expResult, result);
    }

    /*
     * Test of getCIServerTypeKey method, of class Properties.
     */
    @Test
    public void testGetCIServerTypeKey() throws IOException {
        logger.info("getCIServerTypeKey");

        String expResult = "CI server type";
        String result = instance.getCIServerTypeKey();
        assertEquals(expResult, result);
    }

    /**
     * Test of setCIServerType method, of class Properties.
     */
    @Test
    public void testSetCIServerTypeName() throws IOException {
        logger.info("getCIServerTypeName");
        String value = "Hudson";

        String expResult = "Hudson";
        instance.setCIServerType(value);
        String result = instance.getCIServerTypeName(instance.getCIServerType());
        assertEquals(expResult, result);
    }

    /*
     * Test of getCIServerUrlKey method, of class Properties.
     */
    @Test
    public void testGetCIServerUrlKey() throws IOException {
        logger.info("getCIServerUrlKey");

        String expResult = "CI server URL";
        String result = instance.getCIServerUrlKey();
        assertEquals(expResult, result);
    }

    /**
     * Test of setCIServerType method, of class Properties.
     */
    @Test
    public void testSetCIServerUrl() throws IOException {
        logger.info("setCIServerUrl");
        String value = "http://google.com";

        String expResult = "http://google.com";
        instance.setCIServerUrl(value);
        String result = instance.getCIServerUrl();
        assertEquals(expResult, result);
    }

    /*
     * Test of getCIServerLoginKey method, of class Properties.
     */
    @Test
    public void testGetCIServerLoginKey() throws IOException {
        logger.info("getCIServerLoginKey");

        String expResult = "CI server login";
        String result = instance.getCIServerLoginKey();
        assertEquals(expResult, result);
    }

    /**
     * Test of setCIServerLogin method, of class Properties.
     */
    @Test
    public void testSetCIServerLogin() throws IOException {
        logger.info("setJiraIssuesSearchText");
        String value = "John";

        String expResult = "John";
        instance.setCIServerLogin(value);
        String result = instance.getCIServerLogin();
        assertEquals(expResult, result);
    }

    /*
     * Test of getCIServerPasswordKey method, of class Properties.
     */
    @Test
    public void testGetCIServerPasswordKey() throws IOException {
        logger.info("getCIServerPasswordKey");

        String expResult = "CI server password";
        String result = instance.getCIServerPasswordKey();
        assertEquals(expResult, result);
    }

    /**
     * Test of setCIServerPassword method, of class Properties.
     */
    @Test
    public void testSetCIServerPassword() throws IOException {
        logger.info("setJiraIssuesSearchText");
        String value = "Smith";

        String expResult = "Smith";
        instance.setCIServerPassword(value);
        String result = instance.getCIServerPassword();
        assertEquals(expResult, result);
    }

    /*
     * Test of getCIServerJobNameKey method, of class Properties.
     */
    @Test
    public void testGetCIServerJobNameKey() throws IOException {
        logger.info("getCIServerJobNameKey");

        String expResult = "CI job name";
        String result = instance.getCIServerJobNameKey();
        assertEquals(expResult, result);
    }

    /**
     * Test of setCIJobName method, of class Properties.
     */
    @Test
    public void testSetCIJobName() throws IOException {
        logger.info("setCIJobName");
        String value = "JOB1";

        String expResult = "JOB1";
        instance.setCIServerJobName(value);
        String result = instance.getCIJobName();
        assertEquals(expResult, result);
    }


    /*
    Test of validateRepoParameters method, of class Properties.
     */
    @Test
    public void testValidateRepofileseparator() {
        try {
            String value = "";
            instance.setRepofileseparator(value);
            instance.validateRepoParameters();
            Assert.fail("Exception expected");
        } catch (Exception e) {
            String expected = "Wrong number of characters, only the first one was removed.";
            assertEquals(expected, e.getMessage());
        }
    }

    @Test
    public void testValidateReposrcpathpostfixregex() {
        try {
            String value = "";
            instance.setReposrcpathpostfixregex(value);
            instance.validateRepoParameters();
            Assert.fail("Exception expected");
        } catch (Exception e) {
            String expected = "Invalid repo path postfix regex";
            assertEquals(expected, e.getMessage());
        }
    }


    @Test
    public void testValidateReposrcpathprefixregex() {
        try {
            String value = "";
            instance.setReposrcpathprefixregex(value);
            instance.validateRepoParameters();
            Assert.fail("Exception expected");
        } catch (Exception e) {
            String expected = "Invalid repo path prefix regex";
            assertEquals(expected, e.getMessage());
        }
    }

    @Test
    public void testValidateRepotype() {
        try {
            String value = "";
            instance.setRepotype(value);
            instance.validateRepoParameters();
            Assert.fail("Exception expected");
        } catch (Exception e) {
            String expected = "Invalid repo type";
            assertEquals(expected, e.getMessage());
        }
    }

    @Test
    public void testValidateRepourl() {
        try {
            String value = "";
            instance.setRepourl(value);
            instance.validateRepoParameters();
            Assert.fail("Exception expected");
        } catch (Exception e) {
            String expected = "Invalid repo URL";
            assertEquals(expected, e.getMessage());
        }
    }


    @Test
    public void testValidateRepostartrev() {
        try {
            String value = "  ";
            instance.setRepostartrev(value);
            instance.validateRepoParameters();
            Assert.fail("Exception expected");
        } catch (Exception e) {
            String expected = "Invalid repo startrev parameter - field is empty";
            assertEquals(expected, e.getMessage());
        }
    }


    @Test
    public void testValidateRepoendrev() {
        try {
            String value = "  ";
            instance.setRepoendrev(value);
            instance.validateRepoParameters();
            Assert.fail("Exception expected");
        } catch (Exception e) {
            String expected = "Invalid repo endrev parameter - field is empty";
            assertEquals(expected, e.getMessage());
        }
    }


}
