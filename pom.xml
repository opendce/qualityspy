<project xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns="http://maven.apache.org/POM/4.0.0"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>pl.wroc.pwr</groupId>
    <artifactId>qualityspy</artifactId>
    <version>0.7-SNAPSHOT</version>
    <packaging>pom</packaging>

    <name>QualitySpy</name>
    <url>https://opendce.atlassian.net/wikin/display/QS/QualitySpy</url>

    <licenses>
        <license>
            <name>Apache License, Version 2.0</name>
            <url>https://www.apache.org/licenses/LICENSE-2.0</url>
        </license>
    </licenses>

    <developers>
        <developer>
            <name>Marian Jureczko</name>
        </developer>
        <developer>
            <name>Olaf Krawczyk</name>
        </developer>
        <developer>
            <name>Piotr Majcher</name>
        </developer>
        <developer>
            <name>Rafał Gubała</name>
        </developer>
        <developer>
            <name>Adam Kępa</name>
        </developer>
        <developer>
            <name>Maciej Czekaj</name>
        </developer>
        <developer>
            <name>Paulina Kozikowska</name>
        </developer>
        <developer>
            <name>Aleksandra Kutrowska</name>
        </developer>
        <developer>
            <name>Damian Nowak</name>
        </developer>
        <developer>
            <name>Pawel Lowczowski</name>
        </developer>
        <developer>
            <name>Maciej Grzeszczak</name>
        </developer>
        <developer>
            <name>Grzegorz Kontek</name>
        </developer>
        <developer>
            <name>Krzysztof Dorczyński</name>
        </developer>
        <developer>
            <name>Marcin Buciora</name>
        </developer>
        <developer>
            <name>Grzegorz Oliwa</name>
        </developer>
        <developer>
            <name>Jakub Batogowski</name>
        </developer>
        <developer>
            <name>Kacper Bober</name>
        </developer>
        <developer>
            <name>Kamil Barys</name>
        </developer>
        <developer>
            <name>Ewelina Lorenc</name>
        </developer>
        <developer>
            <name>Lukasz Broll</name>
        </developer>
    </developers>

    <scm>
        <url>scm:git:https://bitbucket.org/opendce/qualityspy.git</url>
        <connection>scm:svn:https://bitbucket.org/opendce/qualityspy.git</connection>
        <developerConnection>scm:svn:https://bitbucket.org/opendce/qualityspy.git</developerConnection>
    </scm>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <sonar.host.url>http://snow.iiar.pwr.wroc.pl:8081</sonar.host.url>
        <sonar.login>55f15214073914eabf3e92b23bd93a16ebbd420f</sonar.login>
        <selenium.version>2.25.0</selenium.version>
        <spring.version>4.2.4.RELEASE</spring.version>
        <jersey.version>1.11</jersey.version>
        <hibernate.version>5.1.0.Final</hibernate.version>
        <slf4j.version>1.6.1</slf4j.version>
        <jacoco-maven-plugin.version>0.7.7.201606060606</jacoco-maven-plugin.version>
        <spring-data.version>1.10.1.RELEASE</spring-data.version>
        <spring-data-jpa.version>1.10.1.RELEASE</spring-data-jpa.version>
        <servlet.version>2.5</servlet.version>
    </properties>

    <modules>
        <module>modules</module>
        <module>apps</module>
    </modules>

    <build>
        <resources>
            <resource>
                <directory>src/main/resources</directory>
                <filtering>true</filtering>
            </resource>
        </resources>
        <plugins>
            <plugin>
                <groupId>org.jacoco</groupId>
                <artifactId>jacoco-maven-plugin</artifactId>
                <version>${jacoco-maven-plugin.version}</version>
            </plugin>
            <plugin>
                <groupId>org.pitest</groupId>
                <artifactId>pitest-maven</artifactId>
                <configuration>
                    <targetClasses>
                        <param>pl.wroc.pwr.qualityspy.*</param>
                    </targetClasses>
                    <targetTests>
                        <param>pl.wroc.pwr.qualityspy.*</param>
                    </targetTests>
                    <outputFormats>
                        <outputFormat>XML</outputFormat>
                    </outputFormats>
                    <excludedClasses>
                        <param>**.Acceptance*</param>
                        <param>**.*IT</param>
                        <param>**.*IntegrationTest</param>
                    </excludedClasses>
                </configuration>
            </plugin>
        </plugins>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.sonarsource.scanner.maven</groupId>
                    <artifactId>sonar-maven-plugin</artifactId>
                    <version>3.3.0.603</version>
                </plugin>
                <plugin>
                    <groupId>org.pitest</groupId>
                    <artifactId>pitest-maven</artifactId>
                    <version>1.2.2</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-compiler-plugin</artifactId>
                    <version>3.3</version>
                    <configuration>
                        <source>1.8</source>
                        <target>1.8</target>
                        <encoding>UTF-8</encoding>
                    </configuration>
                </plugin>

                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-surefire-plugin</artifactId>
                    <version>2.19.1</version>
                    <configuration>
        				<argLine>-Xmx1024m -Xms1024m -Djdk.net.URLClassPath.disableClassPathURLCheck=true</argLine>
                        <includes>
                            <include>**/*Test.java</include>
                            <include>**/Test*.java</include>
                            <include>**/*TestCase.java</include>
                            <include>**/*IT*.java</include>
                            <include>**/*Integration*Test*.java</include>
                        </includes>
                    </configuration>
                </plugin>


                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-release-plugin</artifactId>
                    <version>2.5.2</version>
                    <configuration>
                        <branchName>${project.version}</branchName>
                        <branchBase>https://svn.java.net/svn/qualityspy~QualitySpy/branches
                        </branchBase>
                        <tagNameFormat>v@{project.version}</tagNameFormat>
                        <autoVersionSubmodules>true</autoVersionSubmodules>
                        <suppressCommitBeforeTag>true</suppressCommitBeforeTag>
                        <suppressCommitBeforeBranch>true</suppressCommitBeforeBranch>
                        <remoteTagging>false</remoteTagging>
                        <updateBranchVersions>true</updateBranchVersions>
                        <updateWorkingCopyVersions>true</updateWorkingCopyVersions>
                    </configuration>
                </plugin>

            </plugins>
        </pluginManagement>
    </build>

    <dependencyManagement>
        <dependencies>

            <dependency>
                <groupId>log4j</groupId>
                <artifactId>log4j</artifactId>
                <version>1.2.12</version>
            </dependency>
            <dependency>
                <groupId>com.sun.jersey</groupId>
                <artifactId>jersey-core</artifactId>
                <version>${jersey.version}</version>
            </dependency>
            <dependency>
                <groupId>com.sun.jersey.contribs</groupId>
                <artifactId>jersey-spring</artifactId>
                <version>${jersey.version}</version>
                <exclusions>
                    <exclusion>
                        <groupId>org.springframework</groupId>
                        <artifactId>spring-core</artifactId>
                    </exclusion>
                    <exclusion>
                        <groupId>org.springframework</groupId>
                        <artifactId>spring-beans</artifactId>
                    </exclusion>
                    <exclusion>
                        <groupId>org.springframework</groupId>
                        <artifactId>spring-context</artifactId>
                    </exclusion>
                    <exclusion>
                        <groupId>org.springframework</groupId>
                        <artifactId>spring-aop</artifactId>
                    </exclusion>
                    <exclusion>
                        <groupId>org.springframework</groupId>
                        <artifactId>spring-web</artifactId>
                    </exclusion>
                </exclusions>
            </dependency>


            <dependency>
                <groupId>net.sf.opencsv</groupId>
                <artifactId>opencsv</artifactId>
                <version>2.0</version>
            </dependency>

            <dependency>
                <groupId>org.eclipse.jgit</groupId>
                <artifactId>org.eclipse.jgit</artifactId>
                <version>2.3.1.201302201838-r</version>
            </dependency>


            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-test</artifactId>
                <version>${spring.version}</version>
                <scope>test</scope>
            </dependency>

            <dependency>
                <groupId>org.swinglabs</groupId>
                <artifactId>swing-layout</artifactId>
                <version>1.0.3</version>
            </dependency>


            <dependency>
                <groupId>org.seleniumhq.selenium</groupId>
                <artifactId>selenium-api</artifactId>
                <version>${selenium.version}</version>
            </dependency>
            <dependency>
                <groupId>org.seleniumhq.selenium</groupId>
                <artifactId>selenium-firefox-driver</artifactId>
                <version>${selenium.version}</version>
            </dependency>
            <dependency>
                <groupId>org.seleniumhq.selenium</groupId>
                <artifactId>selenium-java</artifactId>
                <version>${selenium.version}</version>
                <type>jar</type>
                <exclusions>
                    <exclusion>
                        <artifactId>selenium-iphone-driver</artifactId>
                        <groupId>org.seleniumhq.selenium</groupId>
                    </exclusion>
                    <exclusion>
                        <artifactId>selenium-android-driver</artifactId>
                        <groupId>org.seleniumhq.selenium</groupId>
                    </exclusion>
                    <exclusion>
                        <artifactId>selenium-chrome-driver</artifactId>
                        <groupId>org.seleniumhq.selenium</groupId>
                    </exclusion>
                    <exclusion>
                        <artifactId>selenium-ie-driver</artifactId>
                        <groupId>org.seleniumhq.selenium</groupId>
                    </exclusion>
                </exclusions>
            </dependency>
            <dependency>
                <groupId>org.seleniumhq.selenium</groupId>
                <artifactId>selenium-support</artifactId>
                <version>${selenium.version}</version>
            </dependency>
            <dependency>
                <groupId>org.hibernate</groupId>
                <artifactId>hibernate-entitymanager</artifactId>
                <version>${hibernate.version}</version>
            </dependency>
            <dependency>
                <groupId>org.hsqldb</groupId>
                <artifactId>hsqldb</artifactId>
                <version>2.3.3</version>
            </dependency>

            <dependency>
                <groupId>javassist</groupId>
                <artifactId>javassist</artifactId>
                <version>3.3</version>
            </dependency>
            <dependency>
                <groupId>commons-collections</groupId>
                <artifactId>commons-collections</artifactId>
                <version>3.1</version>
            </dependency>
            <dependency>
                <groupId>gr.spinellis.ckjm</groupId>
                <artifactId>ckjm_ext</artifactId>
                <version>2.1</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>org.dbunit</groupId>
                <artifactId>dbunit</artifactId>
                <version>2.5.2</version>
                <scope>test</scope>
            </dependency>


            <dependency>
                <groupId>org.slf4j</groupId>
                <artifactId>slf4j-api</artifactId>
                <version>${slf4j.version}</version>
            </dependency>
            <dependency>
                <groupId>org.slf4j</groupId>
                <artifactId>slf4j-jdk14</artifactId>
                <version>${slf4j.version}</version>
            </dependency>
            <dependency>
                <groupId>org.hamcrest</groupId>
                <artifactId>hamcrest-all</artifactId>
                <version>1.1</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>org.tmatesoft.svnkit</groupId>
                <artifactId>svnkit</artifactId>
                <version>1.3.5</version>
            </dependency>
            <dependency>
                <groupId>org.easymock</groupId>
                <artifactId>easymock</artifactId>
                <version>3.0</version>
                <scope>test</scope>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>com.sun.jersey</groupId>
                <artifactId>jersey-server</artifactId>
                <version>${jersey.version}</version>
            </dependency>

            <dependency>
                <groupId>mysql</groupId>
                <artifactId>mysql-connector-java</artifactId>
                <version>5.1.17</version>
            </dependency>
            <dependency>
                <groupId>com.jayway.restassured</groupId>
                <artifactId>rest-assured</artifactId>
                <version>1.6</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>com.sun.jersey.jersey-test-framework</groupId>
                <artifactId>jersey-test-framework-grizzly2</artifactId>
                <version>${jersey.version}</version>
                <scope>test</scope>
            </dependency>

            <!-- internal dependencies -->
            <dependency>
                <groupId>pl.wroc.pwr</groupId>
                <artifactId>qualityspy-bugtracker</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>pl.wroc.pwr</groupId>
                <artifactId>qualityspy-model</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>pl.wroc.pwr</groupId>
                <artifactId>qualityspy-common</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>pl.wroc.pwr</groupId>
                <artifactId>qualityspy-metrics</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>pl.wroc.pwr</groupId>
                <artifactId>qualityspy-dao</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>pl.wroc.pwr</groupId>
                <artifactId>qualityspy-version-control</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>pl.wroc.pwr</groupId>
                <artifactId>qualityspy-gui</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>pl.wroc.pwr</groupId>
                <artifactId>qualityspy-ci</artifactId>
                <version>${project.version}</version>
            </dependency>
        </dependencies>
    </dependencyManagement>
    <dependencies>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.12</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>info.cukes</groupId>
            <artifactId>cucumber-java</artifactId>
            <version>1.2.5</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>info.cukes</groupId>
            <artifactId>cucumber-junit</artifactId>
            <version>1.2.5</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-aop</artifactId>
            <version>${spring.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-beans</artifactId>
            <version>${spring.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context</artifactId>
            <version>${spring.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context-support</artifactId>
            <version>${spring.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-expression</artifactId>
            <version>${spring.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-jdbc</artifactId>
            <version>${spring.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-orm</artifactId>
            <version>${spring.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-tx</artifactId>
            <version>${spring.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-web</artifactId>
            <version>${spring.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-webmvc</artifactId>
            <version>${spring.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework.data</groupId>
            <artifactId>spring-data-jpa</artifactId>
            <version>${spring-data-jpa.version}</version>
        </dependency>
        <dependency>
            <groupId>org.easymock</groupId>
            <artifactId>easymock</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.google.guava</groupId>
            <artifactId>guava</artifactId>
            <version>20.0</version>
        </dependency>
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>1.16.16</version>
        </dependency>
        <dependency>
            <groupId>io.github.benas</groupId>
            <artifactId>random-beans</artifactId>
            <version>3.7.0</version>
        </dependency>
    </dependencies>

    <repositories>
        <repository>
            <id>jgit-repository</id>
            <url>http://download.eclipse.org/jgit/maven</url>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
            <releases>
                <enabled>true</enabled>
            </releases>
        </repository>
        <repository>
            <id>atlassian</id>
            <url>https://maven.atlassian.com/content/repositories/atlassian-public/</url>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
            <releases>
                <enabled>true</enabled>
            </releases>
        </repository>
    </repositories>

</project>
